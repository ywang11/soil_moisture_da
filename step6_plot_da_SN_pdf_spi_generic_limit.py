"""
20190910
ywang254@utk.edu

Plot the times series of the signal, and threshold of 
   likely (S/N > 0.95, detectable at 66% confidence)
   very likely (S/N > 1.64, 90% confidence)
   virtually certain (S/N > 2.57, 99% confidence).

The thresholds follow Marvel et al. 2019 DOI: 10.1038/s41586-019-1149-8

The EOF is flipped already in the fingerprint calculation step to ensure most
  are negative values.

Fix the last year of the signal to 2016. Vary the beginning year.
"""
import matplotlib.pyplot as plt
import pandas as pd
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median # depth_cm
from misc.plot_utils import plot_ts_shade, cmap_gen
import numpy as np
import cartopy.crs as ccrs
##from cartopy.mpl.gridliner import Gridliner
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil
from scipy.stats import norm # gaussian_kde
from misc.cmip6_utils import mrsol_availability


res = '0.5'
lag = 3
lat_eof = [float(x) for x in lat_median]
season_list_list = {'bymonth': [str(i) for i in range(12)],
                    'byyear': ['annual_mean', 'annual_maximum',
                               'annual_minimum',
                               'growing_season_mean', 'growing_season_maximum',
                               'growing_season_minimum']}


#Setup / MODIFY
prod_list = ['mean_noncmip'] # , 'mean_products'] # 'mean_lsm'
prod_name_list = ['Mean NonCMIP'] # , 'Mean Products'] # 'Mean ORS'
prod_clist = ['k', '#636363']
folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
season_name = 'bymonth'
base = 'historical'
expr_list = ['historical', 'NoNAT', 'hist-nat', 'hist-GHG', 'hist-aer',
             'GHGAER']
expr_name = {'historical': 'ALL', 'NoNAT': 'ANT', 'hist-nat': 'NAT',
             'hist-GHG': 'GHG', 'hist-aer': 'AER', 'GHGAER': 'GHGAER'}
season_list = season_list_list[season_name]
season_list_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug',
                    'Sep','Oct','Nov','Dec']


for i, expr in enumerate(expr_list):
    if expr == 'historical':
        a, _ = mrsol_availability('0-10cm', 'vanilla', 'historical', res)
        b, _ = mrsol_availability('0-10cm', 'vanilla', 'ssp585', res)
        temp = sorted(list(set(a) & set(b)))
    else:
        temp, _ = mrsol_availability('0-10cm', 'vanilla', expr, res)
    if i == 0:
        cmip6_list = set(temp)
    else:
        cmip6_list = cmip6_list & set(temp)
cmip6_list = sorted(cmip6_list)


cmap = mpl.cm.get_cmap('nipy_spectral')
clist = [cmap((i+0.5)/(1+len(expr_list))) for i in range(1+len(expr_list))]
mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titleweight'] = 'normal'
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


#year_range_list = [range(1951, 1976), range(1975, 1986), range(1985, 2017)]
#year_list = [str(yr[0]) + '-' + str(yr[-1]) for yr in year_range_list]
#year_range_list = [range(1971, 2017), range(1976, 2017), range(1981, 2017),
#                   range(1986, 2017)]
#year_range_list = [range(1951, 2017), range(1975, 2017)]
year_range_list = [range(1971, 2017)]
year_list = [str(year[0])+'-'+str(year[-1]) for year in year_range_list]


#
def plot_fit_norm(mu, std, x, ax, color = 'k'):
    ##mu, std = norm.fit(vector)
    p = norm.pdf(x, mu, std)
    h1, = ax.plot(x, p, '-', lw = .5, color = color)

    ci = norm.interval(.95, loc=mu, scale = std)
    pi = norm.pdf(ci, mu, std)

    x1 = np.insert(x[x<ci[0]], np.sum(x<ci[0]), ci[0])
    p1 = np.insert(p[x<ci[0]], np.sum(x<ci[0]), pi[0])
    h2 = ax.fill_between(x1, p1, color = color, edgecolor = color,
                         alpha = 0.5, lw = .5)

    x2 = np.insert(x[x>ci[1]], 0, ci[1])
    p2 = np.insert(p[x>ci[1]], 0, pi[1])
    h3 = ax.fill_between(x2, p2, color = color, edgecolor = color,
                         alpha = 0.5, lw = .5)

    return h1, h2, h3


for limit, dist, yind in it.product([False, True], ['gmm', 'weibull'],
                                    range(len(year_list))):
##def plot(option):
##    limit, dist, yind = option

## DEBUG
##    if limit | (dist == 'weibull') | (yind < (len(year_list)-1)):
##        continue

    if limit:
        prefix = 'limit_'
    else:
        prefix = ''
    year_str = year_list[yind]
    year = year_range_list[yind]

    flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                    prefix + 'flip_bylat' + suffix + '.csv'),
                       index_col = [0,1,2,3])

    fig, axes = plt.subplots(ncols = 4,
                             nrows = len(season_list) // 4 * len(depth_cm_new),
                             figsize = (6.5, 8), sharex = True, sharey = True)
    fig.subplots_adjust(wspace = 0.05, hspace = 0.17)
    for dind, dcm in enumerate(depth_cm_new):
        for sind, season in enumerate(season_list):
            signal_list = {}

            # CMIP6 noise
            noise_std = np.std(pd.read_csv(os.path.join(mg.path_out(),
                                                 'cmip6_spi_noise', folder,
                                                 base + '_eofs', prefix + \
                                                 dist + '_trend_' + dcm + \
                                                 '_' + str(lag) + '_' + \
                                                 season + '_' + res + \
                                                 suffix + '.csv'),
                                           index_col = 0), axis = 0)
            signal_list['noise'] = noise_std.loc[str(len(year))]

##            if (dcm != '0-10cm') | (sind != 11):
##                continue
            
##            print(signal_list['noise'])
            
            for bind, expr in enumerate(expr_list):
                # CMIP6 signals
                signal_hist = pd.read_csv(os.path.join(mg.path_out(),
                                                       'cmip6_spi_signal',
                                                       folder, base + '_eofs',
                                                       prefix + dist + '_' + \
                                                       expr + '_signal_' + \
                                                       dcm + '_' + str(lag) +\
                                                       '_' + season + '_' + \
                                                       res + suffix + '.csv'),
                                          header = [0,1] \
                ).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
                signal_hist.index = pd.to_datetime(signal_hist.index).year
                signal_hist.index.name = 'year'
                signal_hist = signal_hist.loc[:, (slice(None), cmip6_list)]
                signal_hist.columns = \
                    signal_hist.columns.remove_unused_levels()

                signal_list[expr] = signal_hist.loc[year[0],
                                                    str(len(year))] * \
                    flip.loc[(dcm, int(season), dist, lag), base]

                
                # (weight by model)
                model_list = [x.split('_')[0] for x in \
                              signal_list[expr].index]
                signal_list[expr + '_weight'] = np.array( \
                    [ 1 / np.sum([m == x for x in model_list]) \
                      for m in model_list ] )
            # Products signals
            signal2 = pd.read_csv(os.path.join(mg.path_out(), 
                'products_spi_signal', folder, base + '_eofs', prefix + \
                dist + '_signal_' + dcm + '_' + str(lag) + '_' + \
                season + '_' + res + suffix + '.csv'), header = [0,1] \
            ).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
            signal2.index = pd.to_datetime(signal2.index).year
            signal2.index.name = 'year'

##            print(signal2.loc[year[0], (slice(None), 'mean_noncmip')])
            
            for prod in prod_list:
                signal_list[prod] = signal2.loc[year[0],
                                                (str(len(year)), prod)] * \
                    flip.loc[(dcm, int(season), dist, lag), base]

            ###################################################################
            # Signal/Noise ratio pdf
            ###################################################################
            ax = axes.flat[sind + len(season_list)*dind]

            h = [None] * (len(expr_list) + 2 + len(prod_list))

            for pind, prod in enumerate(prod_list):
                h[pind] = ax.axvline(signal_list[prod], lw = 1.,
                                     color = prod_clist[pind])
            for bind, expr in enumerate(expr_list):
                #kernel = gaussian_kde(signal_list[expr],
                #                      weights = signal_list[expr + '_weight'])
                #x = np.linspace(signal_list[expr].min(),
                #                signal_list[expr].max(), 100)
                #v = kernel(x)
                # h[bind+2], = ax.plot(x, v, color = clist[bind+2])
                mu = np.average(signal_list[expr],
                                weights = signal_list[expr + '_weight'])
                std = np.std(signal_list[expr])
                x = np.linspace( mu - 3 * std, mu + 3 * std, 51 )
                h1, h2, h3 = plot_fit_norm(mu, std, x, ax,
                                           color = clist[bind+1])
                h[bind+len(prod_list)+2] = h1
            h[len(prod_list)+1] = ax.axvspan(-2.57*signal_list['noise'],
                                             2.57*signal_list['noise'],
                                             color = clist[0], alpha = 0.1,
                                             lw = 0.5)
            h[len(prod_list)] = ax.axvspan(-1.96*signal_list['noise'],
                                           1.96*signal_list['noise'],
                                           color = clist[0], alpha = 0.2,
                                           lw = 0.5)
            ax.set_title(season_list_name[sind], pad = 3.)
            if ((dcm == '0-100cm') and (sind >= 8)):
                ax.tick_params('x', length = 2., pad = 2.)
            else:
                ax.tick_params('x', length = 0.)
            if np.mod(sind, 4) == 0:
                #ax.set_yticks([-3, 0, 3, 6])
                ax.tick_params('y', length = 2., pad = 2.)
                ax.set_ylabel(dcm)
            else:
                ax.tick_params('y', length = 2.)

            ax.text(0.01, 1.05, lab[sind + len(season_list)*dind],
                    transform = ax.transAxes, fontweight = 'bold')

    ax.legend(h + [h3], prod_name_list + ['Noise 95%', 'Noise 99%'] + \
              [expr_name[e] for e in expr_list] + ['Gaussian 95% CI'],
              ncol = 5, loc = 'lower center', bbox_to_anchor = (-1.5, -0.6))
    fig.savefig(os.path.join(mg.path_out(), 'figures',
                             prefix + 'fig3-pdf_limit_' + dist + \
                             '_' + str(lag) + '_' + res + '_' + year_str + \
                             suffix + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
    ##break


#p = mp.Pool(4)
#p.map_async(plot, list(it.product([False, True], ['gmm', 'weibull'],
#                                  range(len(year_list)))))
#p.close()
#p.join()
