"""
2019/07/05

ywang254@utk.edu

Map the 1950-2016 trend in soil moisture for the merged products, 
 and merged CMIP6 historical models (with stipple for 90% model agreement,
 not averaging between ensemble simulations, so that models with more ensemble
 members receive more weight.
"""
from utils_management.constants import depth_cm, depth
import utils_management as mg
from misc.plot_utils import plot_map_w_stipple
import numpy as np
import os
import itertools as it
from misc.plot_utils import cmap_gen
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from misc.cmip6_utils import mrsol_availability


res = '0.5' # Just this will be enough.
season_list = ['annual', 'DJF', 'MAM', 'JJA', 'SON',
               'annual_maximum', 'annual_minimum']
# 'growing_season_mean', 'growing_season_minimum', 'growing_season_maximum'
expr_list = ['historical', 'hist-aer', 'hist-nat', 'hist-GHG']
product_list = ['dolce_lsm', 'dolce_cmip5', 'dolce_cmip6',
                'em_lsm', 'em_cmip5', 'em_cmip6', 'dolce_products']
year_str = '1950-2016'


levels = np.linspace(-0.0004, 0.0004, 21)
cmap = cmap_gen('autumn', 'winter_r')


# MODIFY
season = season_list[0]
d = depth[0]
dcm = depth[0]


fig, axes = plt.subplots(nrows = 4, ncols = 3, figsize = (15, 15),
                         subplot_kw = {'projection': ccrs.PlateCarree()})
axes = axes.T
###############################################################################
# (1) Products, with p <= 0.05 significance.
###############################################################################
for i, prod in enumerate(product_list):
    if i < 3:
        ax = axes.flat[i]
    else:
        ax = axes.flat[i+1]

    data = xr.open_dataset(os.path.join(mg.path_out(),
                                        'calc_trend_map_products', prod + \
                                        '_' + d + '_' + season + '_' + \
                                        year_str + '.nc'))
    plot_map_w_stipple(ax, data.lat.values, data.lon.values,
                       data['trend'].values,
                       hatch_mask = (data['p_value'].values <= 0.05),
                       add_cyc = True,
                       contour_style = {'levels': levels, 'cmap': cmap},
                       cbar_style = {'boundaries': levels,
                                     'ticks': 0.5*(levels[1:]+levels[:-1])})
    data.close()
###############################################################################
# (2) CMIP6 ensemble mean.
###############################################################################
for i, expr in enumerate(expr_list, len(product_list)):
    ax = axes.flat[i]

    data = xr.open_dataset(os.path.join(mg.path_out(), 'calc_trend_map_cmip6',
                                        'ensemble_merge', d + '_' + season + \
                                        '_' + year_str + '.nc'))
    trend = data['trend'].values.copy()
    data.close()

    ###########################################################################
    # Collect the relevant CMIP6 individual models and calculate % agreement.
    ###########################################################################
    if expr == 'historical':
        list1, _ = mrsol_availability(dcm, land_mask, 'historical', res)
        list2, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
        cmip6_list = list(set(list1) & set(list2))
    else:
        cmip6_list, _ = mrsol_availability(dcm, land_mask, expr, res)

    trend_src = np.full([len(cmip6_list), trend.shape[0],
                         trend.shape[1]], np.nan)
    for j, m_v in enumerate(cmip6_list):
        data = xr.open_dataset(os.path.join(mg.path_out(),
                                            'calc_trend_map_cmip6', expr,
                                            m_v + '_' + d + '_' + season + \
                                            '_' + year_str + '.nc'))
        trend_src[j, :, :] = data['trend'].values.copy()
        data.close()

    consistent_sign = 
