"""
2019/10/07
ywang254@utk.edu

Calculate the 
 : by-latitude (in 10-degrees, -50 to 70)
 land-area weighted mean soil moisture of the following LSM/merged products
 : Concatenated DOLCE LSM product
 : DOLCE-CMIP5 & DOLCE-CMIP6
 : EM-LSM & EM-CMIP5 & EM-CMIP6
 : Median of the 6 different products
 : Mean of all the source raw products
"""
from utils_management.constants import depth_cm_new, target_lat, target_lon
import utils_management as mg
from misc.analyze_utils import by_continent_avg, by_biome_avg, by_AI_avg, \
    by_lat_avg, by_subcontinent_avg
from misc.cmip6_utils import mrsol_availability
from glob import glob
import os
import xarray as xr
import pandas as pd
import multiprocessing as mp
import numpy as np
import itertools as it


# Build the list of products at this resolution.
ec_list = ['em_' + x for x in ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']]
prod_list = ['mean_lsm', 'dolce_lsm'] + ec_list
noncmip_list = ['mean_lsm', 'dolce_lsm', 'em_lsm']

res = '0.5'

season_list = [str(m) for m in range(12)]
ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])

period = pd.date_range('1951-01-01', '2016-12-31', freq = 'YS')
year = range(1951, 2017)


for dist, dcm, season, lag in it.product(['gmm', 'weibull'], depth_cm_new,
                                         season_list, [1,3,6]):
    for a, b in zip(['mean_noncmip', 'mean_products'],
                    [noncmip_list, prod_list]):

        spi = np.full([len(year), len(target_lat[res]),
                       len(target_lon[res])], np.nan)
        for pind, prod in enumerate(b):
            h = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                             'SPI_Products',
                                             'get_' + ref_period_str, dist,
                                             prod + '_' + dcm + '_' + \
                                             str(lag) + '_' + res + '_' + \
                                             season + '.nc'))
            if pind == 0:
                spi[:, :, :] = h['spi'].values.copy()
            else:
                spi[:, :, :] += h['spi'].values.copy()
            h.close()

        spi = spi / len(b)

        spi = xr.DataArray(spi, coords = {'time': period,
                                          'lat': target_lat[res],
                                          'lon': target_lon[res]},
                           dims = ['time', 'lat', 'lon'])

        spi.to_dataset(name = 'spi').to_netcdf(os.path.join( \
            mg.path_intrim_out(), 'SPI_Products', 'get_' + ref_period_str,
            dist, a + '_' + dcm + '_' + str(lag) + '_' + res + '_' + \
                                                             season + '.nc'))
