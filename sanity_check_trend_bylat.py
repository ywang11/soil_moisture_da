"""
20191126

Compare the trend between the latitudinal mean soil moisture of CMIP6 
 models of different experiments, and the products.
"""
import matplotlib.pyplot as plt
import statsmodels.api as stats
import utils_management as mg
import os
import pandas as pd
import numpy as np


dcm = '10-30cm'
season = 'JJA'
res = '0.5'


lat_median = [str(x) for x in range(-45, 66, 10)]

trend = pd.DataFrame(data = np.nan,
                     index = lat_median,
                     columns = ['hist-nat', 'hist-GHG', 'hist-aer', 
                                'historical'])

sd = pd.DataFrame(data = np.nan, index = lat_median,
                  columns = ['hist-nat', 'hist-GHG', 'hist-aer', 
                             'historical'])

for expr in ['hist-nat', 'hist-GHG', 'hist-aer', 'historical']:
    data = pd.read_csv(os.path.join(mg.path_out(), 
                                    'cmip6_ensemble_merge_bylat', 
                                    expr + '_' + dcm + '_' + res + '_' + \
                                    season + '.csv'), index_col = 0)


    for c in range(data.shape[1]):
        r, p = pearsonr(range(data.shape[0]), 
                        data.iloc[:, c].values)
        trend.loc[lat_median[c], expr] = r

        sd.loc[lat_median[c], expr] = np.std(data.iloc[:, c].values)


trend.to_csv('trend.csv')
sd.to_csv('sd.csv')




#
trend = pd.DataFrame(data = np.nan,
                     index = lat_median,
                     columns = ['dolce_lsm', 'dolce_cmip5', 'dolce_cmip6', 
                                'em_lsm', 'em_cmip5', 'em_cmip6'])

sd = pd.DataFrame(data = np.nan, index = lat_median,
                  columns = ['dolce_lsm', 'dolce_cmip5', 'dolce_cmip6', 
                             'em_lsm', 'em_cmip5', 'em_cmip6'])

for expr in ['dolce_lsm', 'dolce_cmip5', 'dolce_cmip6', 
             'em_lsm', 'em_cmip5', 'em_cmip6']:
    data = pd.read_csv(os.path.join(mg.path_out(), 
                                    'aggregate_ts_products',
                                    expr + '_' + dcm + '_' + season + '_' + \
                                    res + '_g_lat_ts.csv'), index_col = 0)


    for c in range(data.shape[1]):
        mod = stats.OLS(data.iloc[:, c].values, 
                        stats.add_constant(range(data.shape[0])))
        results = mod.fit()
        trend.loc[lat_median[c], expr] = results.params[1]
        sd.loc[lat_median[c], expr] = np.std(data.iloc[:, c].values)


trend.to_csv('trend_products.csv')
sd.to_csv('sd_products.csv')
