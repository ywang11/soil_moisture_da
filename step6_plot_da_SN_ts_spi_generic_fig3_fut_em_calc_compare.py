import pandas as pd
import os
import matplotlib.pyplot as plt
import utils_management as mg


a = pd.read_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc', 'SSI', 'gmm_signal_collect_se.csv'),
                index_col = 0, header = [0,1])
a = a.sort_index(axis = 1)

b = pd.read_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc', 'SSI', 'gmm_gam_signal_collect_se.csv'),
                index_col = 0, header = [0,1]).sort_index(axis = 1)

fig, axes = plt.subplots(2, 1, sharex = True, sharey = True, figsize = (6.5, 6.5))
ax = axes[0]
ax.boxplot( (a - b).loc[:, (slice(None), '0-10cm')], whis = [5, 95])
ax.set_title('0-10cm')
ax.text(0., 1.05, 'a', transform = ax.transAxes, fontweight = 'bold')
ax.set_ylabel('diff. std. resid.')
ax = axes[1]
ax.boxplot( (a - b).loc[:, (slice(None), '0-100cm')], whis = [5, 95])
ax.set_title('0-100cm')
ax.text(0., 1.05, 'b', transform = ax.transAxes, fontweight = 'bold')
ax.set_xlabel('Month')
ax.set_ylabel('diff. std. resid.')
fig.savefig(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc', 'compare_GAM_and_rolling.png'), dpi = 600.)
plt.close(fig)
