"""
2020/10/19

P,ET,P-ET,soil moisture trends for the cmip6_list set of models.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import utils_management as mg
from misc.analyze_utils import calc_seasonal_trend0
from misc.cmip6_utils import one_layer_availability, mrsol_availability
from utils_management.constants import depth_cm, depth_cm_new
import itertools as it


def read_ssi(model, dcm):
    ssi = np.full([(2100-1951+1)*12, 360, 720], np.nan, dtype = float)
    for m in range(12):
        hr = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                                          'get_historical_1970-2014', 'gmm',
                                          'historical',
                                          model + '_' + dcm + '_3_0.5_' + \
                                          str(m) + '.nc'),
                             decode_times = False)
        ssi[m::12, :, :] = hr['spi'].values.copy()
        hr.close()
    ssi = xr.DataArray(ssi, dims = ['time','lat','lon'],
                       coords = {'time': pd.date_range('1951-01-01',
                                                       '2100-12-31',
                                                       freq = 'MS'),
                                 'lat': hr['lat'],
                                 'lon': hr['lon']})
    ssi = ssi.loc[(ssi['time'].to_index().year >= year[0]) & \
                  (ssi['time'].to_index().year <= year[-1]), :, :]
    return ssi


def get_flist2(model, varname):
    flist = []
    ylist = []
    for yy in year:
        if yy <= 2014:
            expr = 'historical'
        else:
            expr = 'ssp585'
        fname = os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                             'CMIP6', expr, model, varname + '_0.5_' + \
                             str(yy) + '.nc')
        if os.path.exists(fname):
            flist.append(fname)
            ylist.append(yy)
    return flist, ylist


def read_var2(model, varname):
    flist, ylist = get_flist2(model, varname)
    if len(flist) == 0:
        return None
    hr = xr.open_mfdataset(flist, decode_times = False)
    var = hr[varname].copy(deep = True)
    var['time'] = pd.date_range(str(ylist[0]) + '-01-01',
                                str(ylist[-1]) + '-12-31', freq = 'MS')
    hr.close()
    if varname == 'evspsbl':
        var = var.where((var >= 0.) | np.isnan(var), 0)
    return var


def laavg(var, bounding_box):
    lon2D, lat2D = np.meshgrid(var.lon.values, var.lat.values)
    values = np.nanmean(np.nanmean(var.where((lon2D >= bounding_box[0]) & \
                                             (lon2D <= bounding_box[1]) & \
                                             (lat2D >= bounding_box[2]) & \
                                             (lat2D <= bounding_box[3])),
                                   axis = 2), axis = 1)
    #return pd.Series(values,
    #                 index = var['time'].to_index().to_period(freq = 'Q-NOV'))
    return pd.Series(values, index = var['time'].to_index())


def get_bm(dcm, sign):
    if sign == 'dry':
        if dcm == '0-10cm':
            bounding_month = list(range(1,7)) # note month starts from 0
        elif dcm == '0-100cm':
            bounding_month = list(range(12))
    else:
        if dcm == '0-10cm':
            bounding_month = list(range(1, 5))
        else:
            bounding_month = [0,1,2,11]
    return bounding_month


def extract(var, bm_list):
    if sign == 'dry':
        if dcm == '0-10cm':
            # lon-1, lon-2, lat-1, lat-2
            bounding_box = [0, 55, 4, 28]
        elif dcm == '0-100cm':
            bounding_box = [5, 55, 15, 30]
    else:
        if dcm == '0-10cm':
            bounding_box = [-110, -90, 48, 55]
        else:
            bounding_box = [-180, 180, 55, 70]
    var1d = laavg(var, bounding_box)
    var1d = var1d.loc[[(mon-1) in bm_list for mon in var1d.index.month]]
    return var1d


year = range(1971, 2017)
year_str = str(year[0]) + '-' + str(year[-1])
land_mask = 'vanilla'
a, _ = mrsol_availability('0-10cm', land_mask, 'historical', '0.5')
b, _ = mrsol_availability('0-10cm', land_mask, 'ssp585', '0.5')
cmip6_list = sorted(set(a) & set(b))


#
for dcm, sign, varname in it.product(depth_cm_new, ['dry', 'wet'],
                                     ['pr', 'evspsbl', 'mrro', 'snw', 'ssi']):
    bm_list = get_bm(dcm, sign)
    var_collect = pd.DataFrame(np.nan, index = year,
        columns = pd.MultiIndex.from_product([cmip6_list, bm_list]))
    for mind, model in enumerate(cmip6_list):
        if varname != 'ssi':
            var = read_var2(model, varname)
        else:
            var = read_ssi(model, dcm)
        if type(var) == type(None):
            continue
        var1d = extract(var, bm_list)
        for mon in bm_list:
            var_collect.loc[:, (model,mon)] = \
                var1d.loc[var1d.index.month == (mon+1)].values
    var_collect.to_csv(os.path.join(mg.path_out(), 'cmip6_drivers_summary',
                                    'sahara_model_avg_' + varname + \
                                    '_collect_' + sign + '_' + dcm + '.csv'))
