"""
20200529

ywang254@utk.edu

Convert the CMIP6 historical soil moisture to 1-, 3-, and 6-month SSI, 
 following Hao & AghaKouchak 2013 Multivariate Standardized Drought Index:
                                  A parametric multi-index model.
# This script finds the optimal bandwidth for KDE for all the models that
#   exist in piControl, historical, hist-aer, hist-nat, hist-GHG.
This script fits the parameters for Weibull and GMM distributions for all the
 models that exist in piControl, historical, hist-aer, hist-nat, hist-GHG.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, year_cmip6, target_lat, \
    target_lon
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
from misc.spi_utils import *
from scipy.integrate import quad
import sys
import psutil


start = time.time()


land_mask = 'vanilla'
res = '0.5'
ref_expr = 'historical'
if ref_expr == 'piControl':
    ref_period = range(0, 201)
else:
    ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


i = REPLACE1
dcm = depth_cm_new[i]


# All the necessary models for HIST85, piControl, hist-aer, hist-nat, hist-GHG
model_list = get_model_list([ref_expr], dcm, land_mask, res)
# sort(model_list.keys())
mlist = ['BCC-CSM2-MR', 'BCC-ESM1', 'CESM2', 'CESM2-WACCM', 'CNRM-CM6-1',
         'CNRM-ESM2-1', 'CanESM5', 'EC-Earth3', 'EC-Earth3-Veg',
         'GISS-E2-1-G', 'GISS-E2-1-H', 'HadGEM3-GC31-LL', 'IPSL-CM6A-LR',
         'MIROC6', 'MPI-ESM1-2-HR', 'NorESM2-LM', 'SAM0-UNICON', 'UKESM1-0-LL']
# ---- slash down for piControl needs
mlist = [x for x in mlist if x in model_list.keys()]

#for m in model_list.keys():
m_ind = REPLACE2 # {0..17}, or fewer
m = mlist[m_ind]
if not m in model_list.keys():
    sys.exit()

s = REPLACE3 # 1, 3, 6

month = REPLACE4 # range(12)

print(i)
print(m)
print(s)
print(month)


#######################################################################
# Read and concatenate all the ensemble members during the reference
# period.
#######################################################################
start_r = time.time()
for r_ind, r in enumerate(model_list[m]):
    print('Reading ' + m + '_' + r)

    if ref_expr == 'piControl':
        N = 201
        fpath = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                         land_mask, 'CMIP6', ref_expr, 
                                         m + '_' + r, 'mrsol_' + \
                                         res + '_*_' + dcm + '.nc')),
                       key = lambda x: int(x.split('_')[-2]))
        fpath = fpath[-N:]
    else:
        fpath = [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                              land_mask, 'CMIP6', ref_expr,
                              m + '_' + r, 'mrsol_' + res + '_' + str(y) + \
                              '_' + dcm + '.nc') for y in ref_period]

    data = xr.open_mfdataset(fpath, decode_times = False)

    data_array = data.sm.values.copy()
    data_array = data_array.reshape(data_array.shape[0], -1)

    # Subset to within the land mask.
    retain = ~np.all(np.isnan(data_array), axis = 0)
    data_array = data_array[:, retain]

    if r_ind == 0:
        data_new = rolling_mean(data_array, s, month)
    else:
        data_new = np.concatenate([data_new, 
                                   rolling_mean(data_array, s, month)],
                                  axis = 0)
    data.close()
end_r = time.time()
print(('Spent %.6f minutes reading model ' + m + ' month ' + \
       str(month)) % ((end_r - start_r)/60))


#######################################################################
# Estimate the empirical pdf.
#######################################################################
data_new = data_new[np.isfinite(data_new[:, 0]), :]
# ---- deal with small negative & large positive values
data_new[(data_new > -1) & (data_new <= 0.)] = 1e-8
data_new[(data_new >= (1. - 1e-8))] = 1. - 1e-8


pid = os.getpid()
py = psutil.Process(pid)
memoryUse = py.memory_info()[0]/2.**30  # memory use in GB...I think
print('memory use:', memoryUse)
# memory use: 2.6400604248046875 (GB)


for dist in ['weibull', 'gmm']:
    start_in = time.time()
    # ---- limit the thread count to reduce memory usage
    result = fit_pdf(data_new, dist, 3)

    end_in = time.time()
    print(('Spent %.6f minutes fitting ' + dist + ' pdf') % \
          ((end_in - start_in)/60))

    if dist == 'weibull':
        a = np.full(len(target_lat[res])*len(target_lon[res]), np.nan)
        shape = np.full(len(target_lat[res])*len(target_lon[res]), np.nan)
        loc = np.full(len(target_lat[res])*len(target_lon[res]), np.nan)
        scale = np.full(len(target_lat[res])*len(target_lon[res]), np.nan)
        loglik = np.full(len(target_lat[res])*len(target_lon[res]), np.nan)

        a[retain] = result[0]
        shape[retain] = result[1]
        loc[retain] = result[2]
        scale[retain] = result[3]
        loglik[retain] = result[4]

        xr.Dataset({'a': (['lat','lon'], a.reshape(len(target_lat[res]),
                                                   len(target_lon[res]))),
                    'shape': (['lat','lon'],
                              shape.reshape(len(target_lat[res]),
                                            len(target_lon[res]))),
                    'loc': (['lat','lon'],
                            loc.reshape(len(target_lat[res]),
                                        len(target_lon[res]))),
                    'scale': (['lat','lon'],
                              scale.reshape(len(target_lat[res]),
                                            len(target_lon[res]))),
                    'loglik': (['lat','lon'],
                               loglik.reshape(len(target_lat[res]),
                                              len(target_lon[res])))},
                   coords = {'lat': target_lat[res],
                             'lon': target_lon[res]} \
        ).to_netcdf(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6', 
                                 'fit_' + ref_expr + '_' + ref_period_str,
                                 dist, m + '_' + dcm + '_' + str(s) + \
                                 '_' + res + '_' + str(month) + '.nc'))
    else:
        weights = np.full([2, len(target_lat[res])*len(target_lon[res])],
                          np.nan)
        means = np.full([2, len(target_lat[res])*len(target_lon[res])],
                        np.nan)
        sd = np.full([2, len(target_lat[res])*len(target_lon[res])],
                     np.nan)
        loglik = np.full(len(target_lat[res])*len(target_lon[res]), np.nan)

        weights[:, retain] = result[0]
        means[:, retain] = result[1]
        sd[:, retain] = result[2]
        loglik[retain] = result[3]

        xr.Dataset({'weights': (['component','lat','lon'],
                                weights.reshape(2, len(target_lat[res]),
                                                len(target_lon[res]))),
                    'means': (['component','lat','lon'],
                              means.reshape(2, len(target_lat[res]),
                                            len(target_lon[res]))),
                    'sd': (['component', 'lat','lon'],
                           sd.reshape(2, len(target_lat[res]),
                                      len(target_lon[res]))),
                    'loglik': (['lat','lon'],
                               loglik.reshape(len(target_lat[res]),
                                              len(target_lon[res])))},
                   coords = {'component': [1,2],
                             'lat': target_lat[res],
                             'lon': target_lon[res]} \
        ).to_netcdf(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                                 'fit_' + ref_expr + '_' + ref_period_str,
                                 dist, m + '_' + dcm + '_' + str(s) + '_' + \
                                 res + '_' + str(month) + '.nc'))

end = time.time()
print(('Spent %.6f minutes calculating spi ' + m + ' month ' + \
       str(month)) % ((end - start)/60))
