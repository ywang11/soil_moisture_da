"""
2020/05/21

Calculate future trends for the CMIP6 HIST85 simulations.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, target_lat, target_lon
import itertools as it
from misc.cmip6_utils import mrsol_availability
from misc.standard_diagnostics import standard_diagnostics
from misc.analyze_utils import calc_seasonal_trend0
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
import sys


res = '0.5'
lat = target_lat[res]
lon = target_lon[res]

land_mask = 'vanilla'
lag = 3
expr = 'historical'

#
ref_expr = 'historical'
if ref_expr == 'piControl':
    ref_period = range(0, 201)
else:
    ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


L = 2016 - 1973 + 1
year_list = [range(i, i+L) for i in [1951, 1971, 1991, 2011, 2031, 2051]]


i = REPLACE1
dcm = depth_cm_new[i]
mod = REPLACE2 # too many models. Need indexer.
year_trend = year_list[REPLACE3] # range(6)
year_str = str(year_trend[0]) + '-' + str(year_trend[-1])
dist = 'REPLACE4'


a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
cmip6_list = list(set(a) & set(b))
cmip6_list = sorted(cmip6_list)


stepsize = 20 # Number of models to calculate within each call
cmip6_list_chunks = [cmip6_list[cc:(cc+stepsize)] for cc in \
                     range(0, len(cmip6_list), stepsize)]
if mod >= len(cmip6_list_chunks):
    sys.exit('Not so many chunks')
cmip6_list = cmip6_list_chunks[mod]


for model in cmip6_list:
    ###########################################################################
    # Read the data and subset to the year trend
    ###########################################################################
    spi = np.full([len(year_trend), 12, len(lat), len(lon)], np.nan)
    for m in range(12):
        fi = os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                          'get_' + ref_expr + '_' + ref_period_str,
                          dist, expr, model + '_' + dcm + '_' + \
                          str(lag) + '_' + res + '_' + str(m) + '.nc')
        data = xr.open_dataset(fi)
        tmp = data['spi'].values[(data['time'].to_index().year >= \
                                  year_trend[0]) & \
                                 (data['time'].to_index().year <= \
                                  year_trend[-1]), :, :].copy()
        spi[:, m, :, :] = tmp
        data.close()
    spi = spi.reshape(len(year_trend) * 12, len(lat), len(lon))

    ###########################################################################
    # Calculate the trends.
    ###########################################################################
    period = pd.date_range(str(year_trend[0])+'-01-01',
                           str(year_trend[-1])+'-12-31', freq = 'MS')
    spi_data = xr.DataArray(spi, coords={'time':period,
                                         'lat':lat, 'lon':lon},
                            dims = ['time', 'lat', 'lon'])
    for s in ['All', 'Annual', 'DJF', 'MAM', 'JJA', 'SON']:
        g_map_trend, g_p_values, g_intercepts = \
            calc_seasonal_trend0(spi_data, s)
        xr.Dataset({'g_map_trend': (['lat','lon'], g_map_trend),
                    'g_p_values': (['lat','lon'], g_p_values),
                    'g_intercepts': (['lat','lon'], g_intercepts)},
                   coords = {'lon':lon, 'lat':lat} \
        ).to_netcdf(os.path.join(mg.path_out(), 'standard_diagnostics_spi',
                                 'historical_extra', dist,
                                 model + '_' + year_str + '_' + \
                                 str(lag) + '_' + res + '_' + \
                                 dcm + '_g_map_trend_' + s +'.nc'))

    pos = np.where([x == model for x in cmip6_list])[0][0]
    print('Finished ' + model + ' which is ' + str(pos) + '-th in the chunck.')
