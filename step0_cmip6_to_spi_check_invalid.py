"""
2020/01/16
Check for weird values in products SPI.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm, depth, target_lat, \
    target_lon, year_cmip6
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib import cm
import sys


res = '0.5'
land_mask = 'vanilla'

data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                                    'merge_' + land_mask + '.nc'))
mask = data['mask'].values.copy()
data.close()


i = 0 # [0,1,2,3]
dcm = depth_cm[i]
expr = 'historical'


print(dcm)
print(expr)


#
ref_expr = 'historical'
if ref_expr == 'piControl':
    ref_period = range(0, 201)
else:
    ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


# All the necessary models for HIST85, piControl, hist-aer, hist-nat, hist-GHG
# model_list = get_model_list(['historical'], dcm, land_mask, res)
# sort(model_list.keys())
mlist = ['BCC-CSM2-MR', 'BCC-ESM1', 'CESM2', 'CESM2-WACCM', 'CNRM-CM6-1',
         'CNRM-ESM2-1', 'CanESM5', 'EC-Earth3', 'EC-Earth3-Veg',
         'GISS-E2-1-G', 'GISS-E2-1-H', 'HadGEM3-GC31-LL', 'IPSL-CM6A-LR',
         'MIROC6', 'MPI-ESM1-2-HR', 'NorESM2-LM', 'SAM0-UNICON', 'UKESM1-0-LL']
m_ind = 0 # {0..17}
m = mlist[m_ind]

# The CMIP6 list of models for individual experiments.
if expr == 'historical':
    model_list = get_model_list(['historical', 'ssp585'], dcm,
                                land_mask, res)
else:
    model_list = get_model_list([expr], dcm, land_mask, res)

if not m in model_list.keys():
    sys.exit()

r = model_list[m][0]


# Use 50 years for historical, and 200 years for piControl, for later
# omission of 1st year.
if expr == 'historical':
    period = pd.date_range(str(year_cmip6['historical'][1]) + '-01-01',
                           str(year_cmip6['ssp585'][-1]) + '-12-31',
                           freq = 'YS')
elif expr == 'piControl':
    period = pd.date_range('2001-01-01', '2200-12-31', freq = 'YS')
elif (expr == 'hist-aer') | (expr == 'hist-nat') | (expr == 'hist-GHG'):
    period = pd.date_range('1951-01-01', '2020-12-31', freq = 'YS')


for s, month in it.product([3, 6, 12], range(12)):
    data = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_to_spi',
                                        'get_' + ref_expr + \
                                        '_' + ref_period_str,
                                        expr, m + '_' + r + '_' + dcm + \
                                        '_' + str(s) + \
                                        '_' + res + '_' + str(month) + '.nc'))

    spi = data['spi'].values.copy()
    data.close()

    # Check invalid values.
    invalid = np.isnan(spi) & mask

    ts_invalid = np.sum(np.sum(invalid, axis = 2), axis = 1)
    map_invalid = np.sum(invalid, axis = 0)

    fig = plt.figure(figsize = (12, 12))
    ax = fig.add_subplot(211)
    ax.plot(period.year.values + (period.month.values-0.5)/12, ts_invalid)
    ax.set_xticks(period.year[::60])
    ax.set_title('# invalids per time step')

    ax = fig.add_subplot(212, projection = ccrs.PlateCarree())
    ax.coastlines()
    ax.set_extent([-180, 180, -60, 90])
    h = ax.contourf(data.lon.values, data.lat.values, map_invalid,
                    corner_mask = False)
    plt.colorbar(h, ax = ax, shrink = 0.7)

    fig.savefig(os.path.join(mg.path_out(), 'cmip6_to_spi', 'check', expr,
                             m + '_' + r + '_' + dcm + '_' + str(s) + \
                             '_' + res + \
                             '_' + str(month) + '.png'), dpi = 600.)
    plt.close(fig)

