"""
20200529

ywang254@utk.edu

Convert the merged products historical soil moisture to 1-, 3-, and 6-
 month SSI, following Hao & AghaKouchak 2013 Multivariate Standardized
                      Drought Index: A parametric multi-index model.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, depth_new, target_lat, \
    target_lon
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
from misc.spi_utils import *
from scipy.integrate import quad
import sys

#
land_mask = 'vanilla'
res = '0.5'

#
ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])

#
product_list = ['mean_lsm', 'dolce_lsm', 
                'em_lsm', 'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all']
period = pd.date_range('1951-01-01', '2016-12-31', freq = 'YS')


#
i = REPLACE1 # 0, 1

d = depth_new[i]
dcm = depth_cm_new[i]

m = product_list[REPLACE2]

month = REPLACE3 # range(12)

print(i)
print(m)
print(month)


#
def apply_spi(params, dist, vector, order):
    return (get_zscore(params, dist, vector), order)


#######################################################################
# Read merged products data.
#######################################################################
data = xr.open_mfdataset(os.path.join(mg.path_intrim_out(),
                                      'interp_merged_products', m + \
                                      '_' + d + '_' + res + '.nc'),
                         decode_times = False)
data_array = data.sm.values.copy()
data.close()

data_array = data_array.reshape(data_array.shape[0], -1)

# Subset to within the land mask.
retain = np.where(~np.all(np.isnan(data_array), axis = 0))[0]
data_array = data_array[:, retain]


#######################################################################
# SSI calculation.
#######################################################################
start = time.time()

for dist, s in it.product(['weibull', 'gmm'], [1, 3, 6]):
    print(dist + ' ' + str(s))

    # Convert to moving average
    data_array_apply = rolling_mean(data_array, s, month)
    # Limit to the reference period
    data_new = data_array_apply[(ref_period[0] - 1950):-2, :]
    data_new = data_new[np.isfinite(data_new[:, 0]), :]
    # Deal with small negative & large positive values
    data_new[(data_new > -1) & (data_new <= 0.)] = 1e-8
    data_new[(data_new >= (1. - 1e-8))] = 1. - 1e-8

    #######################################################################
    # Estimate the empirical pdf using 1950-2014 as the reference period.
    #######################################################################
    start_in = time.time()
    # ---- keep down threads for memory issue
    results0 = fit_pdf(data_new, dist, 4)
    # Debug with fewer threads
    ##results0 = fit_pdf(data_new, dist, 3)
    end_in = time.time()
    # thread_count = 3: Spent 71.436603 minutes fitting pdf s = 1
    print(('Spent %.6f minutes fitting pdf s = ' + str(s)) % \
          ((end_in - start_in)/60))


    if dist == 'weibull':
        a = np.full(len(target_lat[res])*len(target_lon[res]), np.nan)
        shape = np.full(len(target_lat[res])*len(target_lon[res]), np.nan)
        loc = np.full(len(target_lat[res])*len(target_lon[res]), np.nan)
        scale = np.full(len(target_lat[res])*len(target_lon[res]), np.nan)
        loglik = np.full(len(target_lat[res])*len(target_lon[res]), np.nan)

        a[retain] = results0[0]
        shape[retain] = results0[1]
        loc[retain] = results0[2]
        scale[retain] = results0[3]
        loglik[retain] = results0[4]

        xr.Dataset({'a': (['lat','lon'], a.reshape(len(target_lat[res]),
                                                   len(target_lon[res]))),
                    'shape': (['lat','lon'],
                              shape.reshape(len(target_lat[res]),
                                            len(target_lon[res]))),
                    'loc': (['lat','lon'],
                            loc.reshape(len(target_lat[res]),
                                        len(target_lon[res]))),
                    'scale': (['lat','lon'],
                              scale.reshape(len(target_lat[res]),
                                            len(target_lon[res]))),
                    'loglik': (['lat','lon'],
                               loglik.reshape(len(target_lat[res]),
                                              len(target_lon[res])))},
                   coords = {'lat': target_lat[res],
                             'lon': target_lon[res]} \
        ).to_netcdf(os.path.join(mg.path_intrim_out(), 'SPI_Products', 
                                 'fit_' + ref_period_str,
                                 dist, m + '_' + dcm + '_' + str(s) + '_' + \
                                 res + '_' + str(month) + '.nc'))

        params = [None] * len(retain)
        for ind, rr in enumerate(retain):
            params[ind] = {'a': a[rr], 'shape': shape[rr],
                           'loc': loc[rr], 'scale': scale[rr],
                           'loglik': loglik[rr]}
    else:
        weights = np.full([2, len(target_lat[res])*len(target_lon[res])],
                          np.nan)
        means = np.full([2, len(target_lat[res])*len(target_lon[res])],
                        np.nan)
        sd = np.full([2, len(target_lat[res])*len(target_lon[res])],
                     np.nan)
        loglik = np.full(len(target_lat[res])*len(target_lon[res]), np.nan)

        weights[:, retain] = results0[0]
        means[:, retain] = results0[1]
        sd[:, retain] = results0[2]
        loglik[retain] = results0[3]

        xr.Dataset({'weights': (['component','lat','lon'],
                                weights.reshape(2, len(target_lat[res]),
                                                len(target_lon[res]))),
                    'means': (['component','lat','lon'],
                              means.reshape(2, len(target_lat[res]),
                                            len(target_lon[res]))),
                    'sd': (['component','lat','lon'],
                           sd.reshape(2, len(target_lat[res]),
                                      len(target_lon[res]))),
                    'loglik': (['lat','lon'],
                               loglik.reshape(len(target_lat[res]),
                                              len(target_lon[res])))},
                   coords = {'component': [1,2],
                             'lat': target_lat[res],
                             'lon': target_lon[res]} \
        ).to_netcdf(os.path.join(mg.path_intrim_out(), 'SPI_Products', 
                                 'fit_' + ref_period_str,
                                 dist, m + '_' + dcm + '_' + str(s) + '_' + \
                                 res + '_' + str(month) + '.nc'))

        params = [None] * len(retain)
        for ind, rr in enumerate(retain):
            params[ind] = {'weights': weights[:,rr], 'means': means[:,rr],
                           'sd': sd[:,rr], 'loglik': loglik[rr]}

    #######################################################################
    # Apply the empirical pdf.
    #######################################################################
    # Remove the first year.
    data_array_apply2 = data_array_apply[1:, :]

    spi = np.full([data_array_apply2.shape[0],
                   len(data.lat) * len(data.lon)], np.nan)

    data_array_apply2 = np.array_split(data_array_apply2,
                                       data_array_apply2.shape[1],
                                       axis = 1)

    start_in = time.time()
    # ---- keep down threads for memory issue
    p = mp.Pool(4)
    # Debug with fewer threads
    ##p = mp.Pool(3)
    results = [p.apply_async(apply_spi,
                             args = (params[order], dist,
                                     data_array_apply2[order], order)) \
               for order in range(len(data_array_apply2))]
    p.close()
    p.join()
    end_in = time.time()
    print(('Spent %.6f minutes applying pdf s = ' + str(s)) % \
          ((end_in - start_in)/60))

    results = [result.get() for result in results]

    for a in range(len(data_array_apply2)):
        spi[:, retain[results[a][1]]] = results[a][0].reshape(-1)

    spi = spi.reshape(-1, len(target_lat[res]), len(target_lon[res]))

    #
    spi = xr.DataArray(spi, coords = {'time': period,
                                      'lat': target_lat[res],
                                      'lon': target_lon[res]},
                       dims = ['time', 'lat', 'lon'])

    # Save to file.
    spi.to_dataset(name = 'spi').to_netcdf( os.path.join( \
        mg.path_intrim_out(), 'SPI_Products', 'get_' + \
        ref_period_str, dist, m + '_' + \
        dcm + '_' + str(s) + '_' + res + '_' + str(month) + '.nc') )

end = time.time()
print('Spent %.6f minutes ' % ((end - start)/60))
