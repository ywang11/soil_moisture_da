"""
20200529

ywang254@utk.edu

Convert the merged products historical soil moisture to 1-, 3-, and 6-
 month SSI, following Hao & AghaKouchak 2013 Multivariate Standardized
                      Drought Index: A parametric multi-index model.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, depth_new, target_lat, \
    target_lon
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
from misc.spi_utils import *
from scipy.integrate import quad
import sys


land_mask = 'vanilla'
res = '0.5'

product_list = ['mean_lsm', 'dolce_lsm', 
                'em_lsm', 'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all']
period = pd.date_range('1951-01-01', '2016-12-31', freq = 'YS')


#
ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


#count_inf = pd.DataFrame(data = 0.,
#                         index = pd.MultiIndex.from_product([depth_cm_new,
#                             ['weibull','gmm'], [1,3,6],
#                             list(range(12))]),
#                         columns = product_list)
for i, m, dist, s in it.product(range(2), product_list,
                                ['weibull', 'gmm'], [1, 3, 6]):
    d = depth_new[i]
    dcm = depth_cm_new[i]

    print(i)
    print(m)
    print(dist + ' ' + str(s))

    hr = xr.open_dataset( os.path.join(mg.path_intrim_out(),
                                       'interp_merged_products', m + \
                                       '_' + d + '_' + res + '.nc'),
                          decode_times = False )
    sm = hr['sm'].values.copy()
    hr.close()

    for month in range(12):
        hr = xr.open_dataset( os.path.join( \
            mg.path_intrim_out(), 'SPI_Products',
            'get_' + ref_period, dist, m + '_' + \
            dcm + '_' + str(s) + '_' + res + '_' + str(month) + '.nc') )
        spi = hr['spi'].values.copy()
        spi_inf = (hr['spi'].values == np.inf) | (hr['spi'].values == -np.inf)
        hr.close()

        count_inf.loc[(dcm, dist, s, month), m] = np.sum(spi_inf)

        if np.sum(spi_inf) > 0:
            sm_temp = rolling_mean(sm, s, month)[1:, :, :]

            f = open(os.path.join(mg.path_intrim_out(), 'SPI_Products',
                                  'debug', dist + '_' + m + '_' + dcm + '_' + \
                                  str(s) + '_' + str(month) + '.csv'), 'w')
            f.write('Year_Ind,Lat_Ind,Lon_Ind,Year,Lat,Lon,SPI,SM\n')
            a,b,c = np.where(spi_inf)
            for aa,bb,cc in zip(a,b,c):
                f.write(str(aa) + ',' + str(bb) + ',' + str(cc) + ',' + \
                        str(hr['time'].to_index().year[aa]) + ',' + \
                        ('%.2f' % hr['lat'].values[bb]) + ',' + \
                        ('%.2f' % hr['lon'].values[cc]) + ',' + \
                        str(spi[aa,bb,cc]) + ',' + \
                        ('%.3f' % sm_temp[aa,bb,cc]) + '\n')
            f.close()
