"""
2020/09/18

Global trend in the CMIP6 drivers of soil moisture.
1951-2014 <to accomodate GRUN runoff>
1980-2014 <to accomodate GlobSnow SWE>

20210708

1971-2016 to conform to SSI.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, target_lat, target_lon, drivers
import itertools as it
from misc.cmip6_utils import one_layer_availability, mrsol_availability
from misc.analyze_utils import calc_seasonal_trend0, calc_seasonal_mean
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
import sys


def get_model_list(expr, drivers, dcm, land_mask, res):
    if expr == 'historical':
        a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
        b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
        cmip6_list = sorted(list(set(a) & set(b)))
    else:
        cmip6_list, _ = mrsol_availability(dcm, land_mask, expr, res)
    for var in drivers:
        if expr == 'historical':
            a, _ = one_layer_availability(var, land_mask, 'historical', res)
            b, _ = one_layer_availability(var, land_mask, 'ssp585', res)
            cmip6_list = sorted(set(cmip6_list) & set(a) & set(b))
        else:
            a, _ = one_layer_availability(var, land_mask, expr, res)
            cmip6_list = sorted(set(cmip6_list) & set(a))
    return cmip6_list


def standard_read(vv, expr, model, yrng):
    res = '0.5'
    land_mask = 'vanilla'

    # ----- note that the year in the trend skips the first year, because
    #       of DJF.
    if expr == 'historical':
        flist = [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', 'historical', model,
                              vv + '_' + res + '_' + str(yy) + '.nc') \
                 for yy in range(max(1950, yrng[0]-1), min(2015, yrng[-1]+1))] + \
                [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', 'ssp585', model,
                              vv + '_' + res + '_' + str(yy) + '.nc') \
                 for yy in range(max(2015, yrng[0]-1), max(2015, yrng[-1]+1))]
    else:
        flist = [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', expr, model,
                              vv + '_' + res + '_' + str(yy) + '.nc') \
                 for yy in range(max(1950, yrng[0]-1), min(2020, yrng[-1]+1))]

    ylist = [int(f.split('_')[-1].split('.')[0]) for f in flist]

    hr = xr.open_mfdataset(flist, decode_times = False)
    hr['time'] = pd.date_range(str(ylist[0]) + '-01-01',
                               str(ylist[-1]) + '-12-31', freq = 'MS')
    var = hr[vv].copy(deep = True)
    ##if vv == 'evspsbl':
    ##    var = var.where((var >= 0.) | np.isnan(var), 0)
    hr.close()
    return var


def standard_read2(dcm, expr, model, yrng):
    res = '0.5'
    land_mask = 'vanilla'

    if expr == 'historical':
        flist = [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', 'historical', model,
                              'mrsol_' + res + '_' + str(yy) + '_' + dcm + '.nc') \
                 for yy in range(max(1950, yrng[0]-1), min(2015, yrng[-1]+1))] + \
                [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', 'ssp585', model,
                              'mrsol_' + res + '_' + str(yy) + '_' + dcm + '.nc') \
                 for yy in range(max(2015, yrng[0]-1), max(2015, yrng[-1]+1))]
    else:
        flist = [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', expr, model,
                              'mrsol_' + res + '_' + str(yy) + '_' + dcm + '.nc') \
                 for yy in range(max(1950, yrng[0]-1), min(2020, yrng[-1]+1))]

    ylist = [int(f.split('_')[-2]) for f in flist]

    hr = xr.open_mfdataset(flist, decode_times = False)
    hr['time'] = pd.date_range(str(ylist[0]) + '-01-01',
                               str(ylist[-1]) + '-12-31', freq = 'MS')
    var = hr['sm'].copy(deep = True)
    hr.close()
    return var


##for model in cmip6_list:
##expr = 'hist-aer'
##year = range(1971, 2017)
##year_str = '1971-2016'
##varname = 'tas'
##for model in ['IPSL-CM6A-LR_r9i1p1f1']:
def calc(model):
    if varname == 'pr-evspsbl':
        pr = standard_read('pr', expr, model, year)
        evspsbl = standard_read('evspsbl', expr, model, year)
        var = pr - evspsbl
    elif varname == 'pr-evspsbl-mrro':
        pr = standard_read('pr', expr, model, year)
        evspsbl = standard_read('evspsbl', expr, model, year)
        mrro = standard_read('mrro', expr, model, year)
        var = pr - evspsbl - mrro
    else:
        var = standard_read(varname, expr, model, year)

    # calculate trend
    res = '0.5'
    for s in ['Annual', 'DJF', 'MAM', 'JJA', 'SON']:
    ##for s in ['MAM']:
        g_map_trend, g_p_values, g_intercepts = calc_seasonal_trend0(var, s)
        xr.Dataset({'g_map_trend': (['lat','lon'], g_map_trend),
                    'g_p_values': (['lat','lon'], g_p_values),
                    'g_intercepts': (['lat','lon'], g_intercepts)},
                   coords = {'lon':target_lon[res],
                             'lat':target_lat[res]} \
        ).to_netcdf(os.path.join(mg.path_out(), 'cmip6_drivers_summary',
                                 varname, expr, model + '_' + res + \
                                 '_g_map_trend_' + s + '_' + year_str + '.nc'))

    ### calculate mean
    ##for s in ['Annual','DJF', 'MAM', 'JJA', 'SON']:
    ##    g_map_mean = calc_seasonal_mean(var, s)
    ##    xr.Dataset({'g_map_mean': (['lat','lon'], g_map_mean)},
    ##               coords = {'lon':target_lon[res],
    ##                         'lat':target_lat[res]} \
    ##    ).to_netcdf(os.path.join(mg.path_out(), 'cmip6_drivers_summary',
    ##                             varname, model + '_' + res + \
    ##                             '_g_map_mean_' + s + '_' + year_str + '.nc'))

    pos = np.where([x == model for x in cmip6_list])[0][0]
    print('Finished ' + model + ' which is ' + str(pos) + '-th in the chunk.')


if __name__ == '__main__':
    # dsnw = accumulated snw within the month
    # ['pr', 'evspsbl', 'snw', 'pr-evspsbl', 'pr-evspsbl-mrro']
    var_list = ['pr', 'tas', 'lai', 'snw']
    varname = var_list[REPLACE1]

    mod = REPLACE2 # too many models. Need indexer.

    # for LAI: only use 1982-2016
    year_list = [range(1971, 2017)] # range(1971, 2017), range(1971, 2015)
                                    # range(1981, 2015), range(1981, 2011), range(1980, 2017)]
    ##L = 45
    ##year_list = [range(i, i+L) for i in [1975, 1995, 2015, 2035, 2045, 2050, 2055]]
    year = year_list[REPLACE3]
    year_str = str(year[0]) + '-' + str(year[-1])

    expr = 'REPLACE4'
    
    stepsize = 6 # Number of models to calculate within each call
    cmip6_list = get_model_list(expr, drivers, '0-10cm', 'vanilla', '0.5')

    cmip6_list_chunks = [cmip6_list[cc:min(len(cmip6_list), (cc+stepsize))] \
                         for cc in range(0, len(cmip6_list), stepsize)]
    if mod >= len(cmip6_list_chunks):
        sys.exit('Not so many chunks')
    cmip6_list = cmip6_list_chunks[mod]

    ##p = mp.Pool(2)
    ##p.map_async(calc, cmip6_list)
    ##p.close()
    ##p.join()

    for model in cmip6_list:
        calc(model)

    ### Fill the gap
    ##from glob import glob
    ##for ff in cmip6_list:
    ##    flist = glob(os.path.join(mg.path_out(), 'cmip6_drivers_summary', varname, expr,
    ##                              ff + '_0.5_g_map_trend_*.nc'))
    ##    if len(flist) != 5:
    ##        #print(ff)
    ##        calc(ff)
