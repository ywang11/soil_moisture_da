"""
20200529

ywang254@utk.edu

Convert the CMIP6 historical soil moisture to 1-, 3-, and 6-month SSI, 
 following Hao & AghaKouchak 2013 Multivariate Standardized Drought Index:
                                  A parametric multi-index model.
# For the soil moisture pdf, use KDE implemented in sklearn and 5-fold
# cross-validation to find the optimal bandwidth.
Use the fitted parameters for Weibull and GMM distributions to find SPI for
 models that exist in piControl, historical, hist-aer, hist-nat, hist-GHG.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, year_cmip6, target_lat, \
    target_lon
import os
import numpy as np
import pandas as pd
from misc.cmip6_utils import mrsol_availability
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
from misc.spi_utils import *
from glob import glob
import sys


#
land_mask = 'vanilla'
res = '0.5'


#
ref_expr = 'historical'
if ref_expr == 'piControl':
    ref_period = range(0, 201)
else:
    ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])
folder = 'get_' + ref_expr + '_' + ref_period_str # 'get'


#
for expr, i in it.product(['GHGAER', 'NoNAT'], range(2)):
    dcm = depth_cm_new[i]
    model_list, _ = mrsol_availability(dcm, land_mask, expr, res)
    period = pd.date_range('1951-01-01', '2020-12-31', freq = 'YS')

    for s, dist, m_r in it.product([1,3,6], ['gmm','weibull'], model_list):

        ##if not 'MIROC6' in m_r:
        ##    continue

        for month in range(12):
            if expr == 'GHGAER':
                hr1 = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                                   'SPI_CMIP6', folder,
                                                   dist, 'hist-GHG',
                                                   m_r + '_' + dcm + '_' + \
                                                   str(s) + '_' + res + '_' + \
                                                   str(month) + '.nc'))
                hr2 = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                                   'SPI_CMIP6', folder,
                                                   dist, 'hist-aer',
                                                   m_r + '_' + dcm + '_' + \
                                                   str(s) + '_' + res + '_' + \
                                                   str(month) + '.nc'))

                spi = hr1['spi'] + hr2['spi']
                hr1.close()
                hr2.close()
            elif expr == 'NoNAT':
                hr1 = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                                   'SPI_CMIP6', folder,
                                                   dist, 'historical',
                                                   m_r + '_' + dcm + '_' + \
                                                   str(s) + '_' + res + '_' + \
                                                   str(month) + '.nc'))
                hr2 = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                                   'SPI_CMIP6', folder,
                                                   dist, 'hist-nat',
                                                   m_r + '_' + dcm + '_' + \
                                                   str(s) + '_' + res + '_' + \
                                                   str(month) + '.nc'))

                spi = hr1['spi'] - hr2['spi']
                hr1.close()
                hr2.close()

            # Save to file.
            spi.to_dataset(name = 'spi').to_netcdf( \
                os.path.join(mg.path_intrim_out(), 'SPI_CMIP6', folder, dist,
                             expr, m_r + '_' + dcm + '_' + str(s) + \
                             '_' + res + '_' + str(month) + '.nc'))
