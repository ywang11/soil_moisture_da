"""
20200514
ywang254@utk.edu

Obtain the collection of all L-length trends in historical simulations.
"""
import utils_management as mg
from utils_management.constants import depth_cm_new, year_cmip6, lat_median
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal_bylat, ls_trend
from misc.cmip6_utils import mrsol_availability
from glob import glob
import itertools as it
import time
import multiprocessing as mp


start = time.time()


land_mask = 'vanilla'
res = '0.5'
season_list = [str(i) for i in range(12)]
product_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
                'em_2cmip', 'em_all', 'mean_noncmip', 'mean_products']
L_range = range(10, 67)


noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['noSahel'].loc[np.array(lat_median).astype(int)].values)
else:
    suffix = ''
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['Normal'].loc[np.array(lat_median).astype(int)].values)


for base, limit, i, season, dist, lag in it.product(['historical', 'hist-GHG', 'hist-aer'],
                                                    [True, False], [0,1], season_list,
                                                    ['gmm','weibull'], [1, 3, 6]):
    #dcm = depth_cm[i]
    dcm = depth_cm_new[i]
    if limit:
        prefix = 'limit_'
    else:
        prefix = ''

    #####################
    # Load the first EOF
    #####################
    data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                    base, 'bylat', prefix + dist + '_eof_' + dcm + '_' + \
                                    str(lag) + '_' + season + '_' + res + suffix + '.csv'),
                       index_col = 0)
    data.index = data.index.astype(str)
    eof1 = data.loc[lat_median, 'Full'].values

    ################################
    # Load the list products
    ################################
    signal = pd.DataFrame(data = np.nan,
                          index = pd.date_range('1951-01-01', '2016-12-31',
                                                freq = 'YS'), 
                          columns = pd.MultiIndex.from_product([list(L_range),
                                                                product_list]))
    pc = pd.DataFrame(data = np.nan,
                      index = pd.date_range('1951-01-01', '2016-12-31',
                                            freq = 'YS'),
                      columns = product_list)

    for m_v in product_list:
        data = pd.read_csv(os.path.join(mg.path_out(),
                                        'products_spi_summary', dist, 
                                        m_v + '_' + dcm + '_' + \
                                        season + '_' + str(lag) + '_' + \
                                        res + '_g_lat_ts' + suffix + '.csv'),
                           index_col = 0, parse_dates = True)
        values = data.loc[pd.date_range('1951-01-01', '2016-12-31',
                                        freq = 'YS'), lat_median].values

        # Calculate the signal.
        wgts = lat_area_sqrt / np.mean(lat_area_sqrt)

        for L in L_range:
            if noSahel:
                tt, pp = calc_signal_bylat(eof1[:-1], values[:,:-1],
                                           wgts[:-1], L)
            else:
                tt, pp = calc_signal_bylat(eof1, values, wgts, L)
            signal.loc[:, (L, m_v)] = tt
        pc.loc[:, m_v] = pp

    #
    signal.to_csv(os.path.join(mg.path_out(), 'products_spi_signal',
                               'bylat', base + '_eofs', prefix + dist + \
                               '_signal_' + dcm + '_' + str(lag) + \
                               '_' + season + '_' + res + \
                               suffix + '.csv'))
    pc.to_csv(os.path.join(mg.path_out(), 'products_spi_signal',
                           'bylat', base + '_eofs', prefix + dist + \
                           '_pc_' + dcm + '_' + str(lag) + '_' + \
                           season + '_' + res + suffix + '.csv'))

end = time.time()
# Script finished in 37.292378644148506 minutes.
print('Script finished in ' + str((end - start) / 60) + ' minutes.')
