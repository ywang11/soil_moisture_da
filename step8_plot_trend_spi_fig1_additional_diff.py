"""
20200901
ywang254@utk.edu

Plot the product's trend, each CMIP6 model's trend, and the difference.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import matplotlib as mpl
import utils_management as mg
import itertools as it
from matplotlib import gridspec
from misc.cmip6_utils import mrsol_availability
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5.
mpl.rcParams['hatch.linewidth'] = 0.5
mpl.rcParams['axes.titlepad'] = 1
lab = 'abcdefghijklmnopqrstuvwxyz'
cmap = cmap_div()
hs = '/////'
levels = np.arange(-1.6e-2, 1.61e-2, 1e-3)
clist = ['k', '#2c39b8', 'r', '#31a354', '#fd8d3c', '#41b6c4', '#dd1c77']
pct = 0.85
yrng = [-1.2, 0.8]
pct2 = 0.85

res = '0.5'
land_mask = 'vanilla'
lat_eof = [float(x) for x in lat_median]
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')

season_list = [str(i) for i in range(12)] # ensure order of x-axis
season_list_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul',
                    'Aug','Sep','Oct','Nov','Dec']

folder = 'bylat'
suffix = ''
lag = 3

year_range_list = [range(1951, 1981), range(1956, 1986), range(1961, 1991),
                   range(1966, 1996), range(1971, 2001), range(1976, 2006),
                   range(1981, 2011), range(1987, 2017)]
year_list = [str(yr[0]) + '-' + str(yr[-1]) for yr in year_range_list]

expr = 'historical'
expr_name = 'ALL'

prod = 'mean_noncmip'
prod_name = 'Mean NonCMIP'


def plotter(mat, ax):
    h = ax.contourf(range(mat.shape[1]), range(mat.shape[0]),
                    mat.values, cmap = cmap, levels = levels,
                    extend = 'both')
    ax.set_yticks(range(2, mat.shape[0], 4))
    ax.set_yticklabels(lat_median_name[2::4])
    ax.set_xticks(np.linspace(0.35, 6.65, 8))
    ax.set_xticklabels(mat.columns)
    ax.tick_params(axis = 'y', length = 0.1, pad = 1)
    ax.tick_params(axis = 'x', length = 0.1, pad = 1)
    return h


for dist in ['gmm', 'weibull']:
    data_prod = {}
    data_cmip6 = {}
    for dcm in depth_cm_new:
        data_prod[dcm] = pd.read_csv(os.path.join(mg.path_out(),
                                                  'standard_diagnostics_spi',
                                                  'products', dist,
                                                  'bylat_trend_' + str(lag) + \
                                                  '_' + res + '_' + dcm + \
                                                  '_additional.csv'),
                                     index_col = 0, header = [0,1,2])
        data_prod[dcm].index = data_prod[dcm].index.values.astype(str)
        data_prod[dcm] = data_prod[dcm].loc[lat_median, prod]

        data_cmip6[dcm] = pd.read_csv(os.path.join(mg.path_out(),
                                                   'standard_diagnostics_spi',
                                                   expr, dist,
                                                   'bylat_trend_' + str(lag) +\
                                                   '_' + res + '_' + dcm +\
                                                   '_additional.csv'),
                                      index_col = 0, header = [0,1,2])
        data_cmip6[dcm].index = ['%d' % x for x in data_cmip6[dcm].index]
        data_cmip6[dcm] = data_cmip6[dcm].loc[lat_median, :]
        data_cmip6[dcm] = data_cmip6[dcm].reorder_levels([1,0,2], axis = 1)

    model_list = sorted(data_cmip6['0-10cm'].columns.levels[1])
    for model in model_list:
        fig = plt.figure(figsize = (6.5, 13))
        gs = gridspec.GridSpec(12, 6, hspace = 0.1, wspace = 0.05)
        for sind, ss in enumerate(season_list):
            for i, dcm in enumerate(depth_cm_new):
                #
                pdata = data_prod[dcm].loc[:, ss]
                cdata = data_cmip6[dcm].loc[:, (ss, model)]
                diff = pdata - cdata

                #
                ax = fig.add_subplot(gs[sind, i*3 + 0])
                h = plotter(pdata, ax)
                if sind != (len(season_list)-1):
                    ax.set_xticklabels([])
                else:
                    ax.set_xticklabels(year_list, rotation = 90)
                if sind == 0:
                    ax.text(0.5, 1.03, prod_name, fontsize = 6,
                            transform = ax.transAxes,
                            horizontalalignment = 'center')
                if i == 0:
                    ax.set_ylabel(season_list_name[sind])
                else:
                    ax.set_yticklabels([])
                ax.set_title('(' + lab[sind] + str(i*3+1) + ')', loc = 'left')

                #
                ax = fig.add_subplot(gs[sind, i*3 + 1])
                h = plotter(cdata, ax)
                if sind != (len(season_list)-1):
                    ax.set_xticklabels([])
                else:
                    ax.set_xticklabels(year_list, rotation = 90)
                if sind == 0:
                    ax.text(0.5, 1.03, model, fontsize = 6,
                            transform = ax.transAxes,
                            horizontalalignment = 'center')
                    ax.text(0.5, 1.11, dcm, fontsize = 6,
                            transform = ax.transAxes,
                            horizontalalignment = 'center')
                ax.set_yticklabels([])
                ax.set_title('(' + lab[sind] + str(i*3+2) + ')', loc = 'left')

                #
                ax = fig.add_subplot(gs[sind, i*3 + 2])
                h = plotter(diff, ax)
                if sind != (len(season_list)-1):
                    ax.set_xticklabels([])
                else:
                    ax.set_xticklabels(year_list, rotation = 90)
                if sind == 0:
                    ax.text(0.5, 1.03, 'Prod - Model', fontsize = 6,
                            transform = ax.transAxes,
                            horizontalalignment = 'center')
                ax.set_yticklabels([])
                ax.set_title('(' + lab[sind] + str(i*3+3) + ')', loc = 'left')

        cax = fig.add_axes([0.1, 0.045, 0.8, 0.015])
        plt.colorbar(h, cax = cax, orientation = 'horizontal',
                     label = 'Trend of ' + str(lag) + \
                     '-month SSI (year$^{-1}$)')
        fig.savefig(os.path.join(mg.path_out(), 'figures',
                                 'fig1_trend_' + str(lag) + '_' + res + \
                                 '_bymodel', 'additional_' + dist + suffix + \
                                 '_' + model + '.png'),
                    dpi = 600., bbox_inches = 'tight')
        plt.close(fig)
        ##break
    ##break
