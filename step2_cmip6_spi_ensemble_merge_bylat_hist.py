"""
2020/05/21
ywang254@utk.edu

Conduct the multi-model averaging of the CMIP6 historical and ssp585 results,
  to prepare the data for calculating the fingerprint.
"""
import os
import pandas as pd
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, year_cmip6
from misc.da_utils import get_model_list
import time
import itertools as it
import multiprocessing as mp
import numpy as np


land_mask = 'vanilla'
res = '0.5'
season_list = [str(i) for i in range(12)]
noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
else:
    suffix = ''


for limit, i, expr, dist, season, lag in it.product( \
    [True, False], range(len(depth_cm_new)),
    ['historical', 'hist-GHG', 'hist-aer', 'hist-nat',
     'NoNAT', 'GHGAER'], ['gmm','weibull'], season_list, [1, 3, 6]):
    dcm = depth_cm_new[i]

    ###########################################################################
    # The common list of models and ensemble members.
    ###########################################################################
    if expr == 'historical':
        if not limit:
            common = get_model_list(['historical', 'ssp585'], dcm, land_mask,
                                    res)
        else:
            common = get_model_list(['piControl', 'historical', 'ssp585'],
                                    dcm, land_mask, res)
        period = pd.date_range(start = '1951-01-01', end = '2100-12-31',
                               freq = 'YS')
    else:
        if not limit:
            common = get_model_list([expr], dcm, land_mask, res)
        else:
            common = get_model_list(['piControl', expr], dcm, land_mask, res)
        period = pd.date_range(start = '1951-01-01', end = '2020-12-31',
                               freq = 'YS')

    start = time.time()
    ###########################################################################
    # Average over the CMIP6 model realizations and then models.
    ###########################################################################
    # (historical is already concatenated to ssp585)
    for m_ind, m in enumerate(common.keys()):
        for r_ind, r in enumerate(common[m]):
            data = pd.read_csv(os.path.join(mg.path_out(), 
                                            'cmip6_spi_summary',
                                            expr, dist,
                                            m + '_' + r + '_' + dcm + \
                                            '_' + str(lag) + '_' + \
                                            season + '_' + res + \
                                            '_g_lat_ts' + suffix + '.csv'),
                               index_col = 0, parse_dates = True)

            if data.shape[1] < 25:
                print(dcm + ' ' + expr + ' ' + season + ' ' + str(lag) + \
                      ' ' + str(noSahel) + ' ' + m + ' ' + r + ' ' + \
                      str(data.shape[1]))

            if r_ind == 0:
                inner_avg = data
            else:
                inner_avg = data + inner_avg
        inner_avg = inner_avg / (r_ind + 1)
        if m_ind == 0:
            outer_avg = inner_avg
        else:
            outer_avg = outer_avg + inner_avg
    avg_hist = outer_avg / (m_ind + 1)

    ##    # ---- subset to 1950-2020
    ##    avg_concat = avg_concat.loc[pd.date_range(start = '1950-01-01',
    ##                                              end = '2020-12-31',
    ##                                              freq = 'MS'), :, :]

    if limit:
        avg_hist.to_csv(os.path.join(mg.path_out(),
                                     'cmip6_spi_ensemble_merge',
                                     'limit_' + dist + '_' + expr + \
                                     '_' + dcm + '_' + str(lag) + \
                                     '_' + season + '_' + res + suffix + \
                                     '.csv'))
    else:
        avg_hist.to_csv(os.path.join(mg.path_out(),
                                     'cmip6_spi_ensemble_merge',
                                     dist + '_' + expr + '_' + \
                                     dcm + '_' + str(lag) + '_' +\
                                     season + '_' + res + suffix + '.csv'))
    end = time.time()
#    print('Finished ' + res + '_' + dcm + '_' + expr + '_' + str(lag) + \
#          ' in ' + str((end - start)/60) + ' minutes.')


#pool = mp.Pool(6)
#pool.map_async(prepare, list(it.product(range(4), ['historical', 'hist-GHG', 
#                                                   'hist-aer', 'hist-nat'],
#                                        season_list, 
#                                        [3, 6, 12], [True, False])))
#pool.close()
#pool.join()
