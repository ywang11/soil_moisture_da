"""
20200917
ywang254@utk.edu

Plot the projection of the merged product on the HIST85 fingerprint.
Plot the S/N ratio of 15, 30, 45 years of the merged product on the HIST85
  fingerprint.
"""
import matplotlib.pyplot as plt
import pandas as pd
import xarray as xr
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median
from misc.da_utils import calc_signal_bylat
from misc.plot_utils import plot_ts_shade
import numpy as np
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil
import matplotlib.gridspec as gridspec


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titleweight'] = 'normal'
mpl.rcParams['axes.titlesize'] = 6


res = '0.5'
season_list = [str(i) for i in range(12)]

folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
lag = 3
base = 'historical' # Do not modify


prod = 'mean_noncmip' # 'mean_lsm', 'mean_products', 'mean_noncmip'
prod_name = 'Mean NonCMIP' # 'Mean ORS', 'Mean Products', 'Mean NonCMIP'


if suffix == '_noSahel':
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    ).loc[np.array(lat_median).astype(int), :].values[:, 1])
else:
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    ).loc[np.array(lat_median).astype(int), :].values[:, 0])


month_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep',
              'Oct','Nov','Dec']
cllist = ['#e41a1c', '#377eb8', '#4daf4a']
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


def plot_rep(ax_1, dist, dcm, sind, prefix): # ax_2, 
    flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                    prefix + 'flip_bylat.csv'),
                       index_col = [0,1,2,3])
    season = season_list[sind]
    h = [None] * 3

    ###########################################################################
    # Principal component
    ###########################################################################
    #
    pc = pd.read_csv(os.path.join(mg.path_out(), 'products_spi_signal',
                                  'bylat', base + '_eofs', prefix + \
                                  dist + '_pc_' + dcm + '_' + \
                                  str(lag) + '_' + season + \
                                  '_' + res + suffix + '.csv'),
                     index_col = 0, parse_dates = True)
    pc.index = pc.index.year
    pc = pc * flip.loc[(dcm, sind, dist, lag), base]
    h[0], = ax_1.plot(pc.index, pc.loc[:, prod].values, '-',
                      color = 'b', zorder = 3, lw = 0.8)

    #
    pc = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_signal',
                                  'bylat', base + '_eofs',  prefix + \
                                  dist + '_historical_pc_' + dcm + '_' + \
                                  str(lag) + '_' + season + \
                                  '_' + res + suffix + '.csv'),
                     index_col = 0, parse_dates = True)
    pc.index = pc.index.year
    pc = pc.loc[pc.index <= 2016, :] * flip.loc[(dcm, sind, dist, lag), base]
    # ---- need to weight the different models
    temp_model = [x.split('_')[0] for x in pc.columns]
    pc_mean = pc.groupby(temp_model, axis = 1).mean().mean(axis = 1)
    pc_std = pc.std(axis = 1)
    h[1], h[2] = plot_ts_shade(ax_1, pc.index,
                               ts = {'min': pc_mean.values - \
                                     pc_std.values * 1.96,
                                     'mean': pc_mean.values,
                                     'max': pc_mean.values + \
                                     pc_std.values * 1.96},
                               ts_col = 'k', ln_main = '-', ln_edge = '--',
                               shade_col = 'k')

    ax_1.set_xticks([1980, 1995, 2010]) # 1955, 

    ## https://ggweather.com/enso/oni.htm
    ## ---- volcanoes: Pinatubo, Agung, El Chichon
    #for yy in [1991, 1963, 1982]:
    #    h[0] = ax_1.axvline(yy, color = '#7570b3', lw = 0.5)
    ## ---- El Nino: 1972(-73), 1982(-83), 1997(-98), 2015(-16)
    ##      La Nina: 1973(-74), 1988(-89), 1999(-00), 2010(-11)
    #for yy in [1973, 1983, 1998, 2016]:
    #    h[1] = ax_1.axvline(yy, color = '#d95f02', lw = 0.5)
    #for yy in [1974, 1989, 2000, 2011]:
    #    h[2] = ax_1.axvline(yy, color = '#1b9e77', lw = 0.5)

    ax_1.set_xlim([1971, pc.index.values[-1]]) # pc.index.values[0]
    ax_1.set_yticks([-1, 0, 1])
    ax_1.set_ylim([-2, 2])

    ###########################################################################
    ## Moving-window signal
    ###########################################################################
    #noise_std = pd.read_csv(os.path.join(mg.path_out(),
    #                                     'cmip6_spi_noise', folder,
    #                                     base + '_eofs', prefix + \
    #                                     dist + '_trend_' + dcm + \
    #                                     '_' + str(lag) + '_' + \
    #                                     season + '_' + res + \
    #                                     suffix + '.csv'), index_col = 0)
    #
    #signal2 = pd.read_csv(os.path.join(mg.path_out(),
    #                                   'products_spi_signal', folder,
    #                                   base + '_eofs', prefix + \
    #                                   dist + '_signal_' + dcm + '_' + \
    #                                   str(lag) + '_' + season + '_' + \
    #                                   res + suffix + '.csv'),
    #                      header = [0,1] \
    #).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
    #signal2.index = pd.to_datetime(signal2.index).year
    #signal2.index.name = 'year'
    #signal2 = signal2.stack().loc[(slice(None), prod), :]
    #signal2.index = signal2.index.droplevel(1)
    #
    #for lind, L in enumerate([15, 30, 45]):
    #    nn = np.nanstd(noise_std.loc[:,str(L)].values, axis = 0)
    #    ss = signal2.loc[:, str(L)]
    #    h[2+lind], = ax_2.plot(ss.index,
    #                           ss.values / nn * flip.loc[(dcm, int(season),
    #                                                      dist, lag), base],
    #                           '-', color = cllist[lind], lw = 0.8)
    #ax_2.set_xticks([1955, 1971, 1985, 2000])
    #ax_2.set_xlim([1951, 2001])
    #ax_2.set_ylim([-3.8, 3.8])
    #h[5] = ax_2.axhline(1.96, ls = '-', color = '#984ea3', lw = 0.5)
    #h[6] = ax_2.axhline(2.57, ls = '-', color = '#984ea3', lw = 1.)
    return h


for limit, dist in it.product([False, True], ['gmm', 'weibull']):
    if limit:
        prefix = 'limit_'
    else:
        prefix = ''

    # ---- create figure
    fig = plt.figure(figsize = (5.6, 8))

    # ---- create grid
    gs = fig.add_gridspec(nrows = 3 * len(depth_cm_new),
                          ncols = len(season_list) // 3,
                          left = 0., right = 1., hspace = 0.15,
                          wspace = 0.1)

    for sind, dind in it.product(range(len(season_list)),
                                 range(len(depth_cm_new))):
        dcm = depth_cm_new[dind]
        # ---- add axes to grid
        #ax_1 = fig.add_subplot(gs[sind, dind * 2])
        #ax_2 = fig.add_subplot(gs[sind, dind * 2 + 1])
        ax = fig.add_subplot(gs[dind*3 + sind//4, np.mod(sind,4)])
        h = plot_rep(ax, dist, dcm, sind, prefix) # ax_1, ax_2, 

        #if dind == 0:
        #    ax_1.set_ylabel(month_name[sind])
        #if sind == 0:
        #    ax_1.text(1.2, 1.3, dcm, fontsize = 6,
        #              horizontalalignment = 'center',
        #              transform = ax_1.transAxes)
        #    ax_1.set_title('Projection')
        #    ax_2.set_title('S/N ratios')
        #if sind != (len(season_list)-1):
        #    ax_1.set_xticklabels([])
        #    ax_2.set_xticklabels([])
        #if (sind == (len(season_list)-1)) & (dind == 0):
        #    ax_1.legend(h, [prod_name, 'CMIP6 ALL'] + \
        #                ['15 yrs', '30yrs', '45yrs',
        #                 '95% Confidence', '99% Confidence'],
        #                loc = [0., -0.9], ncol = 7)
        if np.mod(sind, 4) == 0:
            ax.text(-0.3, 0.5, dcm, fontsize = 5, rotation = 90,
                    verticalalignment = 'center', transform = ax.transAxes)
        ax.set_title(month_name[sind], pad = 2)
        ax.text(0.1, 1.05, lab[sind], transform = ax.transAxes,
                weight = 'bold')
        if (dind == 1) & (sind >= 8):
            ax.set_xticklabels([1980, 1995, 2010])
        else:
            ax.tick_params('x', length = 0)
            ax.set_xticklabels([])
        if np.mod(sind,4) != 0:
            plt.setp(ax.get_yticklabels(), visible=False)

    ax.legend(h, [prod_name, 'ALL', 'Gaussian 95% CI'], loc = [-2, -0.5],
              ncol = 3)
    fig.savefig(os.path.join(mg.path_out(), 'figures', prefix + \
                             'fig3S_' + dist + '_pc_' + prod + '_' + \
                             str(lag) + '_' + res + suffix + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
