from utils_management.constants import depth_cm_new, drivers
import utils_management as mg
import os
import pandas as pd
import numpy as np
from glob import glob
import itertools as it


writer = pd.ExcelWriter(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                     'interp_cmip6_da_checksum.xlsx'),
                        engine = 'xlsxwriter')
#
for expr in ['piControl','historical', 'ssp585', 'hist-GHG', 'hist-aer',
             'hist-nat']:
    cmip6_model_path = mg.path_to_cmip6(expr, 'mrsol')
    collect = []
    for model in cmip6_model_path.keys():
        for j in cmip6_model_path[model].keys():
            collect.append( model + '_' + j)

    check = pd.DataFrame(data = 0, index = collect,
                         columns = pd.MultiIndex.from_product([['None','vanilla'], 
                                                               depth_cm_new]))
    for dcm, m_v, land_mask in it.product(depth_cm_new, collect,
                                          ['None','vanilla']):
        f = glob(os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', expr, m_v,
                              'mrsol_0.5_*_' + dcm + '.nc'))
        check.loc[m_v, (land_mask, dcm)] = len(f)
    check.to_excel(writer, sheet_name = expr + '_mrsol')
#
for expr, vv in it.product(['historical', 'hist-GHG', 'hist-aer'], drivers):
    cmip6_model_path = mg.path_to_cmip6(expr, vv)
    collect = []
    for model in cmip6_model_path.keys():
        for j in cmip6_model_path[model].keys():
            collect.append( model + '_' + j)

    check = pd.DataFrame(data = 0, index = collect,
                         columns = pd.MultiIndex.from_product([['None','vanilla'], 
                                                               depth_cm_new]))
    for dcm, m_v, land_mask in it.product(depth_cm_new, collect,
                                          ['None','vanilla']):
        f = glob(os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', expr, m_v, vv + '_0.5_*.nc'))
        check.loc[m_v, (land_mask, dcm)] = len(f)
    check.to_excel(writer, sheet_name = expr + '_' + vv)

writer.save()
