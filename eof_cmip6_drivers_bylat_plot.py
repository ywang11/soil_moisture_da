"""
20210708
ywang254@utk.edu

Plot the EOF of the fingerprint of the drivers of soil moisture.

The EOF is flipped already in the fingerprint calculation step to ensure the
  merged product projects positively unto them.

This version plots the EOF of all the experiments as a colormap. 
"""
import matplotlib.pyplot as plt
import pandas as pd
import xarray as xr
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median, drivers
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import numpy as np
import cartopy.crs as ccrs
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil
from matplotlib import gridspec


mpl.rcParams['font.size'] = 5.5
mpl.rcParams['axes.titleweight'] = 'normal'
mpl.rcParams['axes.titlesize'] = 5.5
mpl.rcParams['hatch.linewidth'] = 0.5
cmap = cmap_div()
levels = np.arange(-0.8, 0.81, 0.04)
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx', 'yy', 'zz', 'aaa', 'bbb', 'ccc', 'ddd', 'eee']

res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list = [str(i) for i in range(12)]
month_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
month_name2 = ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D']
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


# Setup
folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
season_name = 'bymonth'
lag = 3
expr = 'historical'
expr_name = 'ALL'


# 
fig = plt.figure(figsize = (6.5, 6.5))
axes = {}
gs = gridspec.GridSpec(1, 2, hspace = 0.12, width_ratios = [4, 1])

axes['pc'] = {}
sub_gs1 = gridspec.GridSpecFromSubplotSpec(len(drivers), 1, subplot_spec = gs[0], 
                                           hspace = 0.12, wspace = 0.)
for ii, vv in enumerate(drivers):
    sub_gs11 = gridspec.GridSpecFromSubplotSpec(4, 3, subplot_spec = sub_gs1[ii],
                                                hspace = 0.01, wspace = 0.01)
    axes['pc'][vv] = np.empty([4,3], dtype = object)
    for jj, kk in it.product(range(4), range(3)):
        axes['pc'][vv][jj,kk] = fig.add_subplot(sub_gs11[jj, kk],
                                                sharex = axes['pc'][vv][0,0],
                                                sharey = axes['pc'][vv][0,0])
axes['eof'] = {}
sub_gs2 = gridspec.GridSpecFromSubplotSpec(len(drivers), 1, subplot_spec = gs[1],
                                           hspace = 0.12, wspace = 0.)
for ii, vv in enumerate(drivers):
    axes['eof'][vv] = fig.add_subplot(sub_gs2[ii])

flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_drivers_fingerprint',
                                'flip_bylat.csv'), index_col = [0,1])
<<<<<<< Updated upstream
count = 0
for ii, vv in enumerate(drivers):
    #
    for ind, season in enumerate(season_list):
        ax = axes['pc'][vv].flat[ind]

        pc = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_drivers_fingerprint',
                                      expr, 'bylat', vv + '_pc_' + season + '.csv'),
                         index_col = 0, parse_dates = True)
        pc.index = pc.index.year
        pc = pc.loc[1971:2016, :]
        ax.plot(pc.index.values, pc['Full'].values * flip.loc[(int(season), vv), expr])
        ax.text(0.01, 0.75, lab[count], transform = ax.transAxes, weight = 'bold')
        ax.text(0.1, 0.75, month_name[ind], transform = ax.transAxes)
        ax.set_xlim([1971, 2016])
        if ~((ii == (len(drivers) - 1)) & (ind >= 8)):
            plt.setp(ax.get_xticklabels(), visible=False)
        if np.mod(ind, 3) != 0:
            plt.setp(ax.get_yticklabels(), visible=False)
        count += 1

    #
    ax = axes['eof'][vv]
    
    eof1_collection = np.full([len(lat_median), len(season_list)], np.nan)
    for sind, season in enumerate(season_list):
        eof = pd.read_csv(os.path.join(mg.path_out(),
                                       'cmip6_drivers_fingerprint', expr,
                                       'bylat', vv + '_eof_' + season + '.csv'),
=======
for ii, vv in enumerate(drivers):
    ax = axes[i, bind]

    #
    





    #
    eof1_collection = np.full([len(lat_median), len(season_list)], np.nan)
    for sind, season in enumerate(season_list):
        eof = pd.read_csv(os.path.join(mg.path_out(),
                                       'cmip6_spi_fingerprint',
                                       base, 'bylat', vv + '_eof_' + season + '.csv'),
>>>>>>> Stashed changes
                          index_col = 0)
        eof.index = eof.index.values.astype(str)
        eof = eof.loc[lat_median, :]
        eof1_collection[:,sind] = eof['Full'].values * flip.loc[(int(season), vv), expr]

<<<<<<< Updated upstream
=======
        eof1_collection[:,sind] = eof['Full'].values * flip.loc[(season, vv), expr]

>>>>>>> Stashed changes
    hb = ax.contourf(range(eof1_collection.shape[1]),
                     range(eof1_collection.shape[0]),
                     eof1_collection, cmap = cmap,
                     levels = levels, extend = 'both')
    ax.set_yticks(range(2, eof1_collection.shape[0], 4))
    ax.set_yticklabels(lat_median_name[2::4])
    ax.set_xticks(np.linspace(0.3,10.7,12))
    ax.tick_params(axis = 'both', length = 2., pad = 2.)
    ax.set_title(vv, pad = 2)
    if ii == (len(drivers) - 1):
        ax.set_xticklabels(month_name2)
    else:
        ax.set_xticklabels([])
    ax.text(0.1, 1.03, lab[count], transform = ax.transAxes, weight = 'bold')
    count += 1

cax = fig.add_axes([0.1, 0.04, 0.8, 0.015])
plt.colorbar(hb, cax = cax, orientation = 'horizontal',
<<<<<<< Updated upstream
             label = 'Fingerprints of the drivers')
fig.savefig(os.path.join(mg.path_out(), 'cmip6_drivers_fingerprint',
                         'eof_cmip6_drivers_bylat_plot_' + expr + '.png'),
=======
             label = 'Fingerprint of ' + str(lag) + '-month SSI')
fig.savefig(os.path.join(mg.path_out(), 'figures', 
                         'eof_cmip6_drivers_bylat.png'),
>>>>>>> Stashed changes
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
