"""
20200425
ywang254@utk.edu

Plot the trend in the Mean Products and ALL SPI.

20210623
Bit cosmetic change on the graph.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import matplotlib as mpl
import utils_management as mg
import itertools as it
from matplotlib import gridspec
from misc.cmip6_utils import mrsol_availability
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from scipy.stats import linregress
from matplotlib import font_manager
for font in font_manager.findSystemFonts('/ccs/home/yaoping/.local/share/fonts'):
    font_manager.fontManager.addfont(font)        

mpl.rcParams['font.size'] = 5.5
mpl.rcParams['axes.titlesize'] = 5.5
mpl.rcParams['axes.titleweight'] = 'bold'
mpl.rcParams['hatch.linewidth'] = 0.5
mpl.rcParams['axes.titlepad'] = 3
mpl.rcParams['font.family'] = 'arial'
mpl.rcParams['font.sans-serif'] = ['Arial']
lab = 'abcdefghijklmnopqrstuvwxyz'
cmap = cmap_div()
hs = '|||'
levels = np.linspace(-0.015, 0.015, 11) # np.arange(-1.6e-2, 1.61e-2, 1e-3)
clist = ['k', '#2c39b8', 'r', '#31a354', '#fd8d3c', '#41b6c4', '#dd1c77']
pct = 0.9
yrng = [-1.5, 1.]


res = '0.5'
land_mask = 'vanilla'
lat_eof = [float(x) for x in lat_median]
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


season_list = [str(i) for i in range(12)] # ensure order of x-axis
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']
half_year_name = ['ONDJFM', 'AMJJAS']
year = range(1971, 2101) # year = range(1951, 2101)


folder = 'bylat'
lag = 3
noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
else:
    suffix = ''


expr = 'historical'
expr_name = 'ALL'


prod = 'mean_noncmip' # 'mean_products', 'mean_noncmip' // 'mean_lsm'
prod_name = 'Mean NonCMIP' # 'Mean Products', 'Mean NonCMIP' // 'Mean ORS'


if prod == 'mean_products':
    limit = True
    prefix = 'limit_'
else:
    limit = False
    prefix = ''


def to_half_year(time_ind):
    year = time_ind.year
    # 0 - ONDJFM, 1 - AMJJAS
    half = np.where((time_ind.month.values >= 3) & \
                    (time_ind.month.values <= 8), 1, 0)
    return year * 100 + half


for dist in ['gmm', 'weibull']:
    fig = plt.figure(figsize = (6.5, len(depth_cm_new) * 2.5))
    gs = gridspec.GridSpec(2, 2, hspace = 0.1, wspace = 0.15,
                           width_ratios = [1.3, 1])

    for i, dcm in enumerate(depth_cm_new):
        # (1) Time series
        gs_1 = gridspec.GridSpecFromSubplotSpec(2, 1,
                                                subplot_spec = gs[i,0],
                                                hspace = 0.15, wspace = 0.)
        # ---- Mean Products
        prod_dt = pd.read_csv(os.path.join(mg.path_out(),
                                           'standard_diagnostics_spi',
                                           'products', dist, prod + '_' + \
                                           str(lag) + '_' + res + '_' + \
                                           dcm + '_g_ts' + suffix + '.csv'),
                              index_col = 0, parse_dates = True)
        prod_dt.loc[prod_dt.iloc[:,0] == np.inf, :] = np.nan
        prod_dt = prod_dt.loc[(prod_dt.index.year >= year[0]) & \
                              (prod_dt.index.year <= year[-1]), :]
        # -------- convert to six-month
        prod_dt = prod_dt.groupby( to_half_year(prod_dt.index) ).mean()
        
        # ---- CMIP6 products
        a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
        b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
        cmip6_list = sorted(set(a) & set(b))
        if limit:
            c, _ = mrsol_availability(dcm, land_mask, 'piControl', res)
            cmip6_list = sorted(set(c) & set(cmip6_list))
        cmip6_dt = pd.DataFrame(data = np.nan,
                                index = pd.date_range('1951-01-01', '2100-12-31',
                                                      freq = 'MS'),
                                columns = cmip6_list)
        for model in cmip6_list:
            dt = pd.read_csv(os.path.join(mg.path_out(),
                                          'standard_diagnostics_spi',
                                          expr, dist,
                                          model + '_' + str(lag) + '_' + \
                                          res + '_' + dcm + '_g_ts' + \
                                          suffix + '.csv'),
                             index_col = 0, parse_dates = True)
            cmip6_dt.loc[:, model] = dt
        cmip6_dt = cmip6_dt.loc[(cmip6_dt.index.year >= year[0]) & \
                                (cmip6_dt.index.year <= year[-1]), :]
        # ---- convert to six-month
        cmip6_dt = cmip6_dt.groupby( to_half_year(cmip6_dt.index) ).mean()
        # ---- calculate a weighted average and std
        cmip6_model = [x.split('_')[0] for x in cmip6_dt.columns]
        cmip6_mean = cmip6_dt.groupby(cmip6_model,
                                      axis = 1).mean().mean(axis = 1)
        cmip6_std = cmip6_dt.std(axis = 1)
        
        for j in range(2):
            ax = plt.subplot(gs_1[j])
            fig.add_subplot(ax)
            eqn = {}
            h = [None]*3

            prod_temp = prod_dt.loc[np.abs(np.mod(prod_dt.index.values, 2) - \
                                           j) < 1e-6, :]
            ##prod_regress = linregress(np.arange(len(prod_temp)),
            ##                          prod_temp.values.reshape(-1))
            ##prod_regress_slope = '%.3f' % (prod_regress.slope * \
            ##                               len(prod_temp))
            ##if prod_regress.pvalue < 0.1:
            ##    prod_regress_slope += '*'
            ##if prod_regress.pvalue < 0.05:
            ##    prod_regress_slope += '*'

            cmip6_mean_temp = cmip6_mean.loc[np.abs(np.mod( \
                cmip6_mean.index.values, 2) - j) < 1e-6]
            cmip6_regress = linregress(np.arange(len(prod_temp)),
                                       cmip6_mean_temp.values[:len(prod_temp)])
            ##cmip6_regress_slope = '%.3f' % (cmip6_regress.slope * \
            ##                                len(cmip6_regress))
            ##if cmip6_regress.pvalue < 0.1:
            ##    prod_regress_slope += '*'
            ##if cmip6_regress.pvalue < 0.05:
            ##    cmip6_regress_slope += '*'
            cmip6_std_temp = cmip6_std.loc[np.abs(np.mod( \
                cmip6_std.index.values, 2) - j) < 1e-6]

            #
            h[0], = ax.plot(range(year[0], 2017),
                            prod_temp.values, '-', lw = 1.5, color = clist[0])
            eqn[prod_name] = r', $\Delta$ = ' + \
                ('{:.2e}'.format(np.mean(prod_temp.values[-30:]) - \
                                 np.mean(prod_temp.values[:16])) \
                ).replace('e-0', 'x10$^{-') + '}$'
            ##eqn[prod_name] = r', $\rho$ = ' + prod_regress_slope

            #
            dmin = cmip6_mean_temp.values - 1.96 * cmip6_std_temp.values
            dmean = cmip6_mean_temp.values
            dmax = cmip6_mean_temp.values + 1.96 * cmip6_std_temp.values
            h[1], h[2] = plot_ts_shade(ax, year,
                                       ts = {'min': dmin, 'mean': dmean,
                                             'max': dmax},
                                       ts_col = clist[1], ln_edge = '-',
                                       shade_col = clist[1])
            eqn[expr] = r', $\Delta$ = ' + \
                ('{:.2e}'.format(np.mean(dmean[16:46]) - \
                                 np.mean(dmean[:16])) \
                ).replace('e-0', 'x10$^{-') + '}$'
            ##eqn[expr] = r', $\rho$ = ' + cmip6_regress_slope

            print(eqn)

            if not (j == 1):
                ax.tick_params('x', length = 0.)
                ax.set_xticklabels([])
            else:
                ax.tick_params('x', length = 2)
                if i == 0:
                    ax.set_xticklabels([])
            ax.tick_params('y', length = 2, pad = 2)
            ax.set_ylim(yrng)
            ax.set_xlim([year[0], year[-1]])
            ax.set_xticks(range(1971, 2101, 20))
            ax.set_ylabel(half_year_name[j])
            ax.set_title(lab[i*4+j], loc = 'left')

            #ax.legend(h, [prod_name + eqn[prod_name],
            #              'ALL' + eqn['historical'],
            #              'Gaussian 95% CI of ALL simulations'], ncol = 1,
            #          loc = 'lower left', fontsize = 5, labelspacing = 0.1,
            #          handlelength = 1.5, handletextpad = 0.5,
            #          columnspacing = 3., frameon = False)
            ax.text(0.02, 0.1, prod_name + eqn[prod_name] + '\n' + 'ALL' + eqn['historical'],
                    transform = ax.transAxes)
        ax.text(-0.2, 1., dcm, verticalalignment = 'center',
                rotation = 90, transform = ax.transAxes, fontsize = 5)

        # (2) Latitude
        # ---- Read data
        # -------- Mean Products
        data = pd.read_csv(os.path.join(mg.path_out(),
                                        'standard_diagnostics_spi',
                                        'products', dist, 'bylat_trend_' + \
                                        str(lag) + '_' + res + '_' + dcm + \
                                        suffix + '.csv'),
                           index_col = 0, header = [0, 1])
        data.index = data.index.values.astype(str)
        values = data.loc[:, prod].loc[lat_median, season_list].values

        data_p = pd.read_csv(os.path.join(mg.path_out(),
                                          'standard_diagnostics_spi',
                                          'products', dist, 'bylat_trend_p_' + \
                                          str(lag) + '_' + res + '_' + dcm + \
                                          suffix + '.csv'),
                             index_col = 0, header = [0, 1])
        data_p.index = data_p.index.values.astype(str)
        values_p = data_p.loc[:, prod].loc[lat_median, season_list].values

        # -------- CMIP6
        # ------------ trends of the individual model ensemble members
        data2 = pd.read_csv(os.path.join(mg.path_out(),
                                         'standard_diagnostics_spi',
                                         expr, dist, prefix + \
                                         'bylat_trend_' + \
                                         str(lag) + '_' + res + '_' + \
                                         dcm + suffix + '.csv'),
                            index_col = 0, header = [0,1])
        data2.index = data2.index.astype(str)
        data2_p = pd.read_csv(os.path.join(mg.path_out(),
                                           'standard_diagnostics_spi',
                                           expr, dist, prefix + \
                                           'bylat_trend_p_' + \
                                           str(lag) + '_' + res + '_' + \
                                           dcm + suffix + '.csv'),
                              index_col = 0, header = [0,1])
        data2_p.index = data2_p.index.astype(str)
        # ------------ weight of the individual ensemble members
        temp_model = [x.split('_')[0] for x in data2.columns.get_level_values(0)]
        data2_vote = np.full(data2.shape, np.nan)
        for ind, model in enumerate(temp_model):
            n_model = np.sum([x == model for x in temp_model])
            data2_vote[:, ind] = 1 / n_model
        data2_vote = data2_vote / \
            np.sum(data2_vote, axis = 1).reshape(-1,1) * \
            len(data2.columns.levels[1])
        # -------- weighted ensemble average of the trends
        weighted_avg = (data2 * data2_vote).groupby(level = 1, axis = 1 \
                                                    ).sum().loc[:, season_list]
        # -------- std of the trends
        weighted_std = data2.groupby(level = 1,
                                     axis = 1).std().loc[:, season_list]
        # -------- weighted ensemble average of whether p <= 0.05
        weighted_avg_p = ((data2_p <= 0.05)* data2_vote).groupby(level = 1, axis = 1 \
                                                                 ).sum().loc[:, season_list]

        # ---- Plot
        gs_2 = gridspec.GridSpecFromSubplotSpec(1, 2,
                                                subplot_spec = gs[i,1],
                                                hspace = 0., wspace = 0.05)
        # -------- Mean Products
        ax = fig.add_subplot(gs_2[0])
        h = ax.contourf(range(values.shape[1]), range(values.shape[0]),
                        values, cmap = cmap, levels = levels, extend = 'both')
        ax.set_yticks(range(2, values.shape[0], 4))
        ax.set_yticklabels(lat_median_name[2::4])
        ax.set_xticks(np.linspace(0.35, 10.65, 12))
        if i == 0:
            ax.set_xticklabels([])
        else:
            ax.set_xticklabels(season_list_name)
        ax.tick_params(axis = 'both', length = 2, pad = 2)
        if i == 0:
            ax.text(0.5, 1.03, prod_name, fontsize = 5,
                    transform = ax.transAxes, horizontalalignment = 'center')
        ax.set_title(lab[i*4+2], loc = 'left')
        # ------------ the product p > 0.05
        ax.contour(range(values_p.shape[1]), range(values_p.shape[0]),
                   values_p <= 0.05, levels = [0.5], colors = 'grey', linestyles = 'dashed',
                   linewidths = 0.5)
        ax.contourf(range(values_p.shape[1]), range(values_p.shape[0]),
                    values_p <= 0.05, levels = [0.5, 1.5], colors = 'grey', alpha = 0.36)
        # ------------ the product has the same sign as weighted_avg
        hatch1 = ((values > 0) & (weighted_avg.values > 0)) | \
            ((values < 0) & (weighted_avg.values < 0))
        stipple(ax, range(hatch1.shape[0]), range(hatch1.shape[1]),
                mask = hatch1, hatch = hs)
        # ------------ the product is outside the 95% uncertainty
        hatch2 = (~hatch1) & \
            (((weighted_avg + 1.96 * weighted_std) < values) | \
             ((weighted_avg - 1.96 * weighted_std) > values))
        stipple(ax, range(hatch2.shape[0]), range(hatch2.shape[1]),
                mask = hatch2, hatch = '---')

        # -------- CMIP6
        ax = plt.subplot(gs_2[1])
        fig.add_subplot(ax)
        h = ax.contourf(range(weighted_avg.shape[1]),
                        range(weighted_avg.shape[0]),
                        weighted_avg.values, cmap = cmap, levels = levels,
                        extend = 'both')
        ax.set_yticks(range(2, weighted_avg.shape[0], 4))
        ax.set_yticklabels( [] )
        ax.set_xticks(np.linspace(0.35, 10.65, 12)) # range(0, 12, 1))
        if i == 0:
            ax.set_xticklabels([])
        else:
            ax.set_xticklabels(season_list_name)
        ax.tick_params(axis = 'both', length = 2, pad = 2)
        if i == 0:
            ax.text(0.5, 1.03, expr_name, fontsize = 5,
                    transform = ax.transAxes, horizontalalignment = 'center')
        # ------------ weighted ensemble total of the consistency in trends
        pct_pos = ((data2 > 0.).astype(float) * data2_vote).groupby( \
            level = 1, axis = 1).sum().loc[:, season_list]
        pct_neg = ((data2 < 0.).astype(float) * data2_vote).groupby( \
            level = 1, axis = 1).sum().loc[:, season_list]
        hatch1 = ((weighted_avg.values > 0) & (pct_pos.values > pct)) | \
            ((weighted_avg.values < 0) & (pct_neg.values > pct))
        stipple(ax, range(hatch1.shape[0]), range(hatch1.shape[1]),
                mask = hatch1, hatch = hs)
        hatch2 = (~hatch1) & \
            (((weighted_avg.values > 0) & (pct_pos.values > (pct-0.1))) | \
             ((weighted_avg.values < 0) & (pct_neg.values > (pct-0.1))))
        stipple(ax, range(hatch2.shape[0]), range(hatch2.shape[1]),
                mask = hatch2, hatch = '---')
        # ------------ weighted_avg_p indicates percentage significant p values
        ax.contour(range(weighted_avg_p.shape[1]), range(weighted_avg_p.shape[0]),
                   weighted_avg_p >= 0.5, levels = [0.5], colors = 'grey', linestyles = 'dashed',
                   linewidths = 0.5)
        ax.contourf(range(weighted_avg_p.shape[1]), range(weighted_avg_p.shape[0]),
                    weighted_avg_p >= 0.5, levels = [0.5, 1.5], colors = 'grey', alpha = 0.36)
        ax.set_title(lab[i*4+3], loc = 'left')

    cax = fig.add_axes([0.1, 0.045, 0.8, 0.015])
    plt.colorbar(h, cax = cax, orientation = 'horizontal', ticks = levels, 
                 label = 'Trend of ' + str(lag) + '-month SSI (year$^{-1}$)')
    cax.text(-0.05, -1., 'Dry', fontsize = 5, transform = cax.transAxes,
             horizontalalignment = 'center') # , color = '#762a83')
    cax.text(1.05,  -1., 'Wet', fontsize = 5, transform = cax.transAxes,
             horizontalalignment = 'center') # , color = '#3288bd')

    figname = os.path.join(mg.path_out(), 'figures', prefix + \
                           'fig1_' + dist + '_trend_' + str(lag) + \
                           '_' + res + suffix + '.png')
    fig.savefig(figname, dpi = 600., bbox_inches = 'tight')

    figname = os.path.join(mg.path_out(), 'figures', prefix + \
                           'fig1_' + dist + '_trend_' + str(lag) + \
                           '_' + res + suffix + '.pdf')
    fig.savefig(figname, dpi = 600., bbox_inches = 'tight')

    plt.close(fig)
