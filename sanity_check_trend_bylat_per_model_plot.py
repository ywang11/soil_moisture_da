"""
20191126

Compare the trend between the latitudinal mean soil moisture of CMIP6
 models of different experiments.
"""
import matplotlib.pyplot as plt
from misc.da_utils import get_model_list
from scipy.stats import pearsonr
import utils_management as mg
from utils_management.constants import depth_cm
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
import itertools as it


lat_median = [str(x) for x in range(-45, 66, 10)]
expr_list = ['historical'] # 'hist-nat', 'hist-GHG', 'hist-aer',
season_list = ['annual', 'DJF', 'MAM', 'JJA', 'SON']
land_mask = 'vanilla'
res = '0.5'


#
for expr, dcm, ndvi in it.product(expr_list, depth_cm, [True, False]):
    if ndvi:
        suffix = '_ndvi_mask'
    else:
        suffix = ''

    if expr == 'historical':
        cmip6_list = get_model_list(['historical', 'ssp585'], dcm,
                                    land_mask, res)
        figsize = (10, 13)
    else:
        cmip6_list = get_model_list([expr], dcm, land_mask, res)
        figsize = (10, 10)

    model_list = list(cmip6_list.keys())

    fig, axes = plt.subplots(nrows = len(model_list), 
                             ncols = len(season_list), 
                             sharex = True, sharey = True, figsize = figsize)
    for i, j in it.product(range(len(model_list)), 
                           range(len(season_list))):
        model = model_list[i]
        season = season_list[j]

        data = pd.read_csv(os.path.join(mg.path_out(), 'sanity_check_trend',
                                        expr + '_' + model + '_' + res + \
                                        '_' + season + '_' + dcm + \
                                        suffix + '.csv'), index_col = 0)

        h1 = axes[i,j].plot(data.index, data.values, '-', color = 'grey')
        h2, = axes[i,j].plot(data.index, data.values.mean(axis = 1), '-',
                             color = 'r')

        if i == 0:
            axes[i,j].set_title(season)
        if j == 0:
            if expr == 'historical':
                axes[i,j].set_ylabel(model, rotation = 45.)
            else:
                axes[i,j].set_ylabel(model)

    axes[i,j].legend([h1[0], h2], ['Ensemble Members', 'Mean'], ncol = 2,
                     loc = [-3, -0.5])

    fig.savefig(os.path.join(mg.path_out(), 'sanity_check_trend', 
                             expr + '_' + dcm + '_' + res + suffix + '.png'),
                dpi = 600., bbox_tight = True)
    plt.close(fig)
