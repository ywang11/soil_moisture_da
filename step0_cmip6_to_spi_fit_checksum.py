"""
Ensure all files are generated in step0_cmip6_to_spi_fit.py
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new
import os
import numpy as np
from misc.da_utils import get_model_list
import pandas as pd
import itertools as it
import multiprocessing as mp
from glob import glob


land_mask = 'vanilla'
res = '0.5'
ref_expr = 'historical'
if ref_expr == 'piControl':
    ref_period = range(0, 201)
else:
    ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


mlist = ['BCC-CSM2-MR', 'BCC-ESM1', 'CESM2', 'CESM2-WACCM', 'CNRM-CM6-1',
         'CNRM-ESM2-1', 'CanESM5', 'EC-Earth3', 'EC-Earth3-Veg',
         'GISS-E2-1-G', 'GISS-E2-1-H', 'HadGEM3-GC31-LL', 'IPSL-CM6A-LR',
         'MIROC6', 'MPI-ESM1-2-HR', 'NorESM2-LM', 'SAM0-UNICON', 'UKESM1-0-LL']

ok = pd.DataFrame(data = ' ' * 100,
                  index = pd.MultiIndex.from_product([depth_cm_new,
                                                      ['gmm','weibull'],
                                                      [1, 3, 6],
                                                      range(11)]),
                  columns = mlist)

    
#for m in mlist:

def see(m):
    ok_temp = pd.Series(data = ' ' * 100,
                        index = pd.MultiIndex.from_product([depth_cm_new,
                                                            ['gmm','weibull'],
                                                            [1,3,6],
                                                            range(11)]))

    for dcm, dist, lag, month in it.product(depth_cm_new, ['gmm','weibull'],
                                            [1, 3, 6], range(12)):

        # All the necessary models for HIST85, piControl, hist-aer, hist-nat,
        # hist-GHG
        model_list = get_model_list(['historical'], dcm, land_mask, res)

        if not m in model_list.keys():
            ok_temp.loc[(dcm, lag, month)] = 'NA'
            continue

        full_path = os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                                 'fit_' + ref_expr + '_' + ref_period_str,
                                 dist, m + '_' + dcm + '_' + str(lag) + \
                                 '_' + res + '_' + str(month) + '.nc')

        if not os.path.exists(full_path):
            ok_temp.loc[(dcm, dist, lag, month)] = 'Bad'
        else:
            ok_temp.loc[(dcm, dist, lag, month)] = 'Good'

    return (ok_temp, m)

p = mp.Pool(6)
results = [p.apply_async(see, args =(m,)) for m in mlist]
p.close()
p.join()

results = [x.get() for x in results]

for m_ind in range(len(mlist)):
    ok.loc[:, results[m_ind][1]] = results[m_ind][0]


ok.to_csv(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                       'fit_checksum.csv'))
