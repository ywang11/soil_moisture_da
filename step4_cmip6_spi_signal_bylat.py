"""
20200514
ywang254@utk.edu

Obtain the collection of all L-length trends in "expr" simulations projected
 on the fingerprint of historical.
"""
import utils_management as mg
from utils_management.constants import depth_cm_new, year_cmip6, lat_median
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal_bylat, ls_trend
from misc.cmip6_utils import mrsol_availability
from glob import glob
import itertools as it
import time
import multiprocessing as mp


start = time.time()


land_mask = 'vanilla'
res = '0.5'
season_list = [str(i) for i in range(12)]
expr_list = ['historical', 'NoNAT', 'hist-nat', 'hist-GHG', 'hist-aer',
             'GHGAER']
L_range = range(10,67)


noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['noSahel'].loc[np.array(lat_median).astype(int)].values)
else:
    suffix = ''
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['Normal'].loc[np.array(lat_median).astype(int)].values)


base = 'REPLACE1'
limit = REPLACE2
i = REPLACE3


#for base, limit, expr, i, season, dist, lag in it.product(['historical',
#                                                           'hist-GHG',
#                                                           'hist-aer'],
#                                                          [True, False],
#                                                          expr_list, [0,1],
#                                                          season_list,
#                                                          ['gmm','weibull'],
#                                                          [1, 3, 6]):
def calc(option):
    #base, limit, expr, i, season, dist, lag = option
    expr, season, dist, lag = option

    if (expr == 'NoNAT') & (i == 1):
        return None

    dcm = depth_cm_new[i] # depth_cm[i]

    if limit:
        prefix = 'limit_'
    else:
        prefix = ''

    #####################
    # Load the first EOF
    #####################
    data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                    base, 'bylat', prefix + dist + '_eof_' +\
                                    dcm + '_' + str(lag) + '_' + season + \
                                    '_' + res + suffix + '.csv'),
                       index_col = 0)
    data.index = data.index.astype(str)
    eof1 = data.loc[lat_median, 'Full'].values

    ################################
    # Load the CMIP6 list of models
    ################################
    if expr == 'historical':
        list1, _ = mrsol_availability(dcm, land_mask, 'historical', res)
        list2, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
        model_version = sorted(set(list1) & set(list2))
    else:
        model_version, _ = mrsol_availability(dcm, land_mask, expr, res)

    if limit:
        mv2, _ = mrsol_availability(dcm, land_mask, 'piControl', res)
        model_version = sorted(list(set(model_version) & set(mv2)))
    else:
        model_version = sorted(model_version)

    if expr == 'historical':
        signal = pd.DataFrame(data = np.nan,
            index = pd.date_range(str(year_cmip6['historical'][1]) + \
                                  '-01-01', '2100-12-31', freq = 'YS'),
            columns = pd.MultiIndex.from_product([list(L_range),
                                                  model_version]))
        proj = pd.DataFrame(data = np.nan,
            index = pd.date_range(str(year_cmip6['historical'][1]) + \
                                  '-01-01', '2100-12-31', freq = 'YS'),
            columns = model_version)
    else:
        signal = pd.DataFrame(data = np.nan,
            index = pd.date_range(str(year_cmip6['historical'][1]) + \
                                  '-01-01', '2020-12-31', freq = 'YS'),
            columns = pd.MultiIndex.from_product([list(L_range),
                                                  model_version]))
        proj = pd.DataFrame(data = np.nan,
            index = pd.date_range(str(year_cmip6['historical'][1]) + \
                                  '-01-01', '2020-12-31', freq = 'YS'),
            columns = model_version)

    #
    for m_v in model_version:
        print(m_v)
        data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_summary',
                                        expr, dist, m_v + '_' + dcm + '_' + \
                                        str(lag) + '_' + season + \
                                        '_' + res + '_g_lat_ts' + \
                                        suffix + '.csv'),
                           index_col = 0, parse_dates = True)
        values = data.loc[:, lat_median].values

        # Calculate the signal.
        wgts = lat_area_sqrt / np.mean(lat_area_sqrt)

        for L in L_range:
            if noSahel:
                tt, pp = calc_signal_bylat(eof1[:-1], values[:,:-1],
                                           wgts[:-1], L)
            else:
                tt, pp = calc_signal_bylat(eof1, values, wgts, L)
            signal.loc[:, (L, m_v)] = tt
        proj.loc[:, m_v] = pp

    signal.to_csv(os.path.join(mg.path_out(), 'cmip6_spi_signal',
                               'bylat', base + '_eofs', prefix + \
                               dist + '_' + expr + \
                               '_signal_' + dcm + '_' + \
                               str(lag) + '_' + season + '_' + res + \
                               suffix + '.csv'))
    proj.to_csv(os.path.join(mg.path_out(), 'cmip6_spi_signal',
                             'bylat', base + '_eofs', prefix + \
                             dist + '_' + expr + '_pc_' + dcm + '_' + \
                             str(lag) + '_' + season + '_' + res + suffix + \
                             '.csv'))


#p = mp.Pool(8)
#p.map_async(calc, list(it.product(['historical', 'hist-GHG', 'hist-aer'],
#                                  [True, False], expr_list, [0,1], season_list,
#                                  ['gmm','weibull'],[1, 3, 6])))
#p.close()
#p.join()

p = mp.Pool(8)
p.map_async(calc, list(it.product(['historical', 'NoNAT'], #expr_list,
                                  season_list, 
                                  ['gmm','weibull'],[1, 3, 6])))
p.close()
p.join()

#calc(['historical', '7', 'gmm', 1])


end = time.time()
# Script finished in 323.43508274555205 minutes.
print('Script finished in ' + str((end - start) / 60) + ' minutes.')
