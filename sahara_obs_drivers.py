"""
2020/10/19

P,ET,P-ET,soil moisture trends for CRU, GLEAM, GRUN


!!!!!!!!!!!!!!!!!!!! TBC
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import utils_management as mg
from misc.analyze_utils import calc_seasonal_trend0
from misc.cmip6_utils import one_layer_availability, mrsol_availability
from utils_management.constants import depth_cm, depth_cm_new
import itertools as it


def get_flist2(lsm, varname):
    flist = []
    ylist = []
    for yy in year:
        if lsm in ['CRU_v3.20', 'CRU_v3.26', 'CRU_v4.03']:
            fname = os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                 land_mask, lsm, varname + '_' + str(yy) + \
                                 '.nc')
        else:
            fname = os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                 land_mask, lsm + '_' + varname,
                                 lsm + '_' + varname + '_' + str(yy) + \
                                 '.nc')
        if os.path.exists(fname):
            flist.append(fname)
            ylist.append(yy)
    return flist, ylist


def read_var(lsm, varname):
    if varname == 'pr':
        flist, ylist = get_flist2(lsm, 'pr')
        hr = xr.open_mfdataset(flist, decode_times = False)
        var = hr['pr'].copy(deep = True)
        var['time'] = pd.date_range(str(ylist[0]) + '-01-01',
                                    str(ylist[-1]) + '-12-31', freq = 'MS')
        hr.close()
    elif varname == 'evspsbl':
        hr = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                          land_mask, lsm + '_evspsbl',
                                          'evspsbl_0.5.nc'),
                             decode_times = True)
        var = hr['evspsbl'].copy(deep = True)
        var = var.loc[(var['time'].to_index().year >= year[0]) & \
                      (var['time'].to_index().year <= year[-1]), :, :]
        var = var.where((var >= 0.) | np.isnan(var), 0)
        ylist = list(np.unique(var['time'].to_index().year))
        hr.close()
    elif varname == 'mrro':
        flist = os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                             ', 'mrro_' + res + '.nc')
        hr = xr.open_mfdataset(flist, decode_times = False)
        hr['time'] = pd.date_range('1950-01-01', '2014-12-31', freq = 'MS')
        var = hr[varname][(hr['time'].to_index().year >= (yrng[0]-1)) & \
                          (hr['time'].to_index().year <= yrng[-1]) \
        ].copy(deep = True)
        hr.close()
    elif varname == 'snw':
        flist = os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                             'GlobSnow', 'snw_' + res + '.nc')
        hr = xr.open_mfdataset(flist, decode_times = False)
        hr['time'] = pd.date_range('1979-01-01', '2016-12-31', freq = 'MS')
        var = hr[varname][(hr['time'].to_index().year >= (yrng[0]-1)) & \
                          (hr['time'].to_index().year <= yrng[-1]) \
        ].copy(deep = True)
        hr.close()
    return var, ylist


def read_ssi(prod, dcm):
    ssi = np.full([(2016-1951+1)*12, 360, 720], np.nan, dtype = float)
    for m in range(12):
        hr = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'SPI_Products',
                                          'get_1970-2014', 'gmm',
                                          prod + '_' + dcm + '_3_0.5_' + \
                                          str(m) + '.nc'),
                             decode_times = False)
        ssi[m::12, :, :] = hr['spi'].values.copy()
        hr.close()
    ssi = xr.DataArray(ssi, dims = ['time','lat','lon'],
                       coords = {'time': pd.date_range('1951-01-01',
                                                       '2016-12-31',
                                                       freq = 'MS'),
                                 'lat': hr['lat'],
                                 'lon': hr['lon']})
    ssi = ssi.loc[(ssi['time'].to_index().year >= year[0]) & \
                  (ssi['time'].to_index().year <= year[-1]), :, :]
    return ssi, year


def laavg(var, bounding_box):
    lon2D, lat2D = np.meshgrid(var.lon.values, var.lat.values)
    values = np.nanmean(np.nanmean(var.where((lon2D >= bounding_box[0]) & \
                                             (lon2D <= bounding_box[1]) & \
                                             (lat2D >= bounding_box[2]) & \
                                             (lat2D <= bounding_box[3])),
                                   axis = 2), axis = 1)
    #return pd.Series(values,
    #                 index = var['time'].to_index().to_period(freq='Q-NOV'))
    return pd.Series(values, index = var['time'].to_index())


def get_bm(dcm, sign):
    if sign == 'dry':
        if dcm == '0-10cm':
            bounding_month = list(range(1,7)) # note month starts from 0
        elif dcm == '0-100cm':
            bounding_month = list(range(12))
    else:
        if dcm == '0-10cm':
            bounding_month = list(range(1, 5))
        else:
            bounding_month = [0,1,2,11]
    return bounding_month


def extract(var, bm_list):
    if sign == 'dry':
        if dcm == '0-10cm':
            # lon-1, lon-2, lat-1, lat-2
            bounding_box = [0, 55, 4, 28]
        elif dcm == '0-100cm':
            bounding_box = [5, 55, 15, 30]
    else:
        if dcm == '0-10cm':
            bounding_box = [-110, -90, 48, 55]
        else:
            bounding_box = [-180, 180, 55, 70]
    var1d = laavg(var, bounding_box)
    var1d = var1d.loc[[(mon-1) in bm_list for mon in var1d.index.month]]
    return var1d


lsm_all = ['CERA20C', 'CLASS-CTEM-N', 'CLM4', 'CLM4VIC',
           'CLM5.0', 'ERA20C', 'ERA-Interim', 'ERA5', 'ERA-Land', 'ESA-CCI',
           'GLDAS_Noah2.0', 'GLEAMv3.3a', 'ISAM', 'JSBACH', 'JULES',
           'LPX-Bern', 'ORCHIDEE', 'ORCHIDEE-CNP', 'SiBCASA']
met_to_lsm = {'CRU_v4.03': ['ESA-CCI', 'GLEAMv3.3a'],
              'CRU_v3.20': ['CLASS-CTEM-N', 'CLM4', 'CLM4VIC', 'SiBCASA'],
              'CRU_v3.26': ['CLM5.0', 'ISAM', 'JSBACH', 'JULES', 'LPX-Bern',
                            'ORCHIDEE', 'ORCHIDEE-CNP'],
              'GLDAS_Noah2.0': ['GLDAS_Noah2.0'],
              'ERA-Interim': ['ERA-Interim', 'ERA-Land'],
              'ERA5': ['ERA5'],
              'ERA20C': ['ERA20C'],
              'CERA20C': ['CERA20C']}
year = range(1971, 2017)
year_str = str(year[0]) + '-' + str(year[-1])
land_mask = 'vanilla'


for dcm, sign, varname in it.product(depth_cm_new, ['dry', 'wet'],
                                     ['pr', 'evspsbl', 'ssi']):
    if varname == 'pr':
        lsm_list = sorted(met_to_lsm.keys())
    elif varname == 'evspsbl':
        lsm_list = lsm_all
    else:
        lsm_list = ['mean_products', 'mean_noncmip']
    bm_list = get_bm(dcm, sign)
    var_collect = pd.DataFrame(np.nan, index = year,
        columns = pd.MultiIndex.from_product([lsm_list, bm_list]))
    for mind, lsm in enumerate(lsm_list):
        if (varname == 'evspsbl') & (lsm == 'ESA-CCI'):
            continue
        if varname != 'ssi':
            var, ylist = read_var(lsm, varname)
        else:
            var, ylist = read_ssi(lsm, dcm)
        var1d = extract(var, bm_list)
        for mon in bm_list:
            var_collect.loc[ylist, (lsm,mon)] = \
                var1d.loc[var1d.index.month == (mon+1)].values
    var_collect.to_csv(os.path.join(mg.path_out(), 'obs_drivers_summary',
                                    'sahara_lsm_avg_' + varname + \
                                    '_collect_' + sign + '_' + dcm + '.csv'))
