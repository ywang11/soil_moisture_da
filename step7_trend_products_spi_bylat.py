"""
2020/03/13

ywang254@utk.edu

Calculate the linear trend in the annual time series, and in the per month
  time series by latitude band during the full period for each CMIP6 model.

Ensure same time period as "standard_diagnostics.py", i.e. 1951-2016.
"""
import pandas as pd
from utils_management.constants import depth_cm_new, lat_median
import utils_management as mg
from misc.standard_diagnostics import get_bylat_trend
from misc.analyze_utils import get_ndvi_mask
import itertools as it
import os
import numpy as np


res = '0.5'


# Time period to calculate the trend on - MODIFY, which changes the 
# associated set of land surface models.
year = range(1971, 2017)
time_range = pd.date_range(start = str(year[0])+'-01-01',
                           end = str(year[-1])+'-12-31', freq = 'YS')


#prod_list = [x + '_' + y for x in ['mean', 'dolce', 'em'] \
#             for y in ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']]
prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all', 'mean_noncmip', 'mean_products']


noSahel = True
if noSahel:
    suffix = '_noSahel'
else:
    suffix = ''


#
for dist, lag, i in it.product(['gmm','weibull'], [1, 3, 6], range(2)):
    dcm = depth_cm_new[i]

    #
    trend_collection = pd.DataFrame(data = np.nan, index = lat_median,
        columns = pd.MultiIndex.from_product([prod_list, range(12)]))
    trend_p_collection = pd.DataFrame(data = np.nan, index = lat_median,
        columns = pd.MultiIndex.from_product([prod_list, range(12)]))
    for prod, month in it.product(prod_list, range(12)):
        fname = os.path.join(mg.path_out(), 'products_spi_summary', dist, 
                             prod + '_' + dcm + '_' + str(month) + '_' + \
                             str(lag) + '_' + res + '_g_lat_ts' + \
                             suffix + '.csv')
        trend, trend_p = get_bylat_trend(fname, time_range)
        trend_collection.loc[:, (prod, month)] = trend
        trend_p_collection.loc[:, (prod, month)] = trend_p

    trend_collection.to_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_spi', 'products', dist, 'bylat_trend_' + \
        str(lag) + '_' + res + '_' + dcm + suffix + '.csv'))
    trend_p_collection.to_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_spi', 'products', dist, 'bylat_trend_p_' + \
        str(lag) + '_' + res + '_' + dcm + suffix + '.csv'))
