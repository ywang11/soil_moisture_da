"""
2021/06/28
ywang254@utk.edu

Ridge regression coefficients between pr, evspsbl, mrro, and $\delta$snw_{t-(t-1)}.
"""
import os
import numpy as np
import xarray as xr
from utils_management.constants import *
import utils_management as mg
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import mean_squared_error, r2_score
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from analyze_cmip6_drivers_map import calc_pcorr, get_model_list
import itertools as it


def standard_read(expr, vv, model, yrng, season):
    data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_drivers_summary', vv, expr, 
                                    model + '_' + str(season) + '_' + res + \
                                    '_g_lat_ts.csv'),
                       index_col = 0, parse_dates = True)
    data.index = data.index.year
    data = data.loc[(data.index >= yrng[0]) & (data.index <= yrng[-1]), :]
    data = xr.DataArray(data.values, dims = ['year', 'lat'],
                        coords = {'year': data.index,
                                  'lat': [int(tt) for tt in data.columns]})
    return data


def standard_read2(expr, dcm, model, yrng, season):
    lag = 3
    data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_summary', 
                                    expr, 'gmm', model + '_' + dcm +  \
                                    '_' + str(lag) + '_' + \
                                    str(season) + '_' + res + '_g_lat_ts.csv'),
                       index_col = 0, parse_dates = True)
    data.index = data.index.year
    data = data.loc[(data.index >= yrng[0]) & (data.index <= yrng[-1]), :]
    data = xr.DataArray(data.values, dims = ['year', 'lat'],
                        coords = {'year': data.index,
                                  'lat': [int(tt) for tt in data.columns]})
    return data


if __name__ == '__main__':
    #
    ind = REPLACE1 # {0..72}
    expr = 'REPLACE2' # 'hist-GHG', 'hist-aer', 'historical'

    L = 45
    year_range_list = [range(1971,2017)] + \
                      [range(i, i+L) for i in [1975, 1995, 2015, 2035, 2045, 2050, 2055]]
    yrng = year_range_list[REPLACE3]
    land_mask = 'vanilla'
    res = '0.5'

    if yrng[0] == 1971:
        alpha_list = [0., 0.01, 0.1, 0.5, 0.9]
    else:
        alpha_list = [0.1] # The best based on selection
    year_str = str(yrng[0]) + '-' + str(yrng[-1])

    #
    for dcm, alpha in it.product(['0-10cm', '0-100cm'], alpha_list):
        cmip6_list = get_model_list(expr, drivers, dcm, land_mask, res)
        model = cmip6_list[ind]

        ds = xr.Dataset()
        for season in range(12):
            sm = standard_read2(expr, dcm, model, yrng, season)
            ##print(sm)
            pr = standard_read(expr, 'pr', model, yrng, season)
            tas = standard_read(expr, 'tas', model, yrng, season)
            lai = standard_read(expr, 'lai', model, yrng, season)
            snw = standard_read(expr, 'snw', model, yrng, season)

            coef, r2, mse, rpd = xr.apply_ufunc(calc_pcorr, 
                                                sm.chunk({'year':-1}), 
                                                pr.chunk({'year':-1}),
                                                tas.chunk({'year':-1}),
                                                lai.chunk({'year':-1}),
                                                snw.chunk({'year':-1}),
                                                input_core_dims = [['year']]*5,
                                                output_core_dims = [['var'], [], [], []],
                                                vectorize = True,
                                                kwargs = {'alpha': alpha,
                                                          'min_coef': np.array([-10, -10, -10,
                                                                                -10]),
                                                          'max_coef': np.array([10,10,0,10])},
                                                dask = 'parallelized',
                                                output_dtypes = (float,) * 4,
                                                dask_gufunc_kwargs = { \
                                                'output_sizes': {'var': 4}})
            coef.compute()
            coef = coef.transpose('var', 'lat')
            r2.compute()
            mse.compute()
            rpd.compute()

            if season == 0:
                coef_all = xr.DataArray(np.full([coef.shape[0], coef.shape[1],
                                                 12], np.nan),
                                        dims = ['var', 'lat', 'month'],
                                        coords = {'var': range(4),
                                                  'lat': coef['lat'],
                                                  'month': range(12)})
                r2_all = xr.DataArray(np.full([coef.shape[1], 12], np.nan),
                                      dims = ['lat', 'month'],
                                      coords = {'lat': coef['lat'],
                                                'month': range(12)})
                mse_all = xr.DataArray(np.full([coef.shape[1], 12], np.nan),
                                       dims = ['lat', 'month'],
                                       coords = {'lat': coef['lat'],
                                                 'month': range(12)})
                rpd_all = xr.DataArray(np.full([coef.shape[1], 12], np.nan),
                                       dims = ['lat', 'month'],
                                       coords = {'lat': coef['lat'],
                                                 'month': range(12)})
            coef_all[:, :, season] = coef
            r2_all[:, season] = r2
            mse_all[:, season] = mse
            rpd_all[:, season] = rpd
        ds.update({'coef': coef_all, 'r2': r2_all, 'mse': mse_all, 'rpd': rpd_all})
        for ii in ['coef', 'r2', 'mse', 'rpd']:
            ds[ii].encoding['_FillValue'] = -1e16
        ds['var'].encoding['_FillValue'] = None
        ds['lat'].encoding['_FillValue'] = None
        ds['month'].encoding['_FillValue'] = None
        ds.to_netcdf(os.path.join(mg.path_out(), 'cmip6_drivers_corr', 'bylat',
                                  expr, 'alpha=%0.2f' % alpha,
                                  model + '_' + dcm + '_' + year_str + '.nc'))
