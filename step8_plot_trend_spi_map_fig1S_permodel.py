"""
20210206
ywang254@utk.edu

Plot the trend in the individual models.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
from misc.analyze_utils import get_ndvi_mask
import matplotlib as mpl
import matplotlib.patches as mpatches
import utils_management as mg
import itertools as it
from misc.cmip6_utils import mrsol_availability
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from cartopy.feature import LAND
import cartopy.crs as ccrs
import multiprocessing as mp


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5.
mpl.rcParams['hatch.linewidth'] = 0.5


res = '0.5'
lab = 'abcdefghijklmnopqrstuvwxyz'


lag = 3
noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
    land_mask = 'vanilla'
    ndvi_mask = get_ndvi_mask(land_mask)
else:
    ndvi_mask = ''


expr = 'hist-aer'
if expr == 'historical':
    a, _ = mrsol_availability('0-10cm', 'vanilla', 'historical', res)
    b, _ = mrsol_availability('0-10cm', 'vanilla', 'ssp585', res)
    cmip6_list = sorted(list(set(a) & set(b)))
else:
    cmip6_list, _ = mrsol_availability('0-10cm', 'vanilla', expr, res)
dist = 'gmm'
season_list = ['DJF','MAM','JJA','SON']


cmap = cmap_div()
levels = np.arange(-4.2e-2, 4.21e-2, 1e-3)


##for model in cmip6_list:
def plotter(model):
    fig, axes = plt.subplots(figsize = (6.5, 6.5),
                             nrows = 4, ncols = 2,
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    for i, dcm in enumerate(depth_cm_new):
        for sind, ss in enumerate(season_list):
            fname = os.path.join(mg.path_out(),
                                 'standard_diagnostics_spi',
                                 expr, dist, model + '_' + \
                                 str(lag) + '_' + res + '_' + \
                                 dcm + '_g_map_trend_' + ss + '.nc')
            if not os.path.exists(fname):
                continue

            hr = xr.open_dataset(fname)
            if noSahel:
                val0 = hr['g_map_trend'].where(ndvi_mask).values.copy()
            else:
                val0 = hr['g_map_trend'].values.copy()
            hr.close()

            ax = axes[sind, i]
            ax.coastlines(lw = 0.1)
            gl = ax.gridlines(xlocs = np.arange(-180, 181, 360),
                              ylocs = np.arange(-40, 61, 5),
                              linestyle = '--',
                              linewidth = 0.5, draw_labels = True)
            gl.xlabels_top = False
            gl.xlabels_bottom = False
            gl.ylabels_right = False
            gl.xlocator = mticker.FixedLocator(np.arange(-180, 181, 360))
            gl.ylocator = mticker.FixedLocator(np.arange(-40, 61, 20))
            gl.xformatter = LONGITUDE_FORMATTER
            gl.yformatter = LATITUDE_FORMATTER
            gl.xlabel_style = {'color': 'black', 'weight': 'normal'}
            ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
            ax.outline_patch.set_visible(False)
            ax.spines['top'].set_visible(True)
            ax.spines['bottom'].set_visible(True)
            ax.spines['left'].set_visible(True)
            ax.spines['right'].set_visible(True)
            ax.spines['top'].set_lw(0.2)
            ax.spines['bottom'].set_lw(0.2)
            ax.spines['left'].set_lw(0.2)
            ax.spines['right'].set_lw(0.2)
            ax.add_feature(LAND, facecolor = [0.9, 0.9, 0.9])
            h = ax.contourf(hr.lon, hr.lat, val0,
                            cmap = cmap, levels = levels, extend = 'both',
                            transform = ccrs.PlateCarree())
            ax.text(-0.2, 0.5, ss, fontsize = 5, rotation = 90.,
                    verticalalignment = 'center', transform = ax.transAxes)
            ax.text(0.01, 0.05, lab[sind*2 + i],
                    transform = ax.transAxes, weight = 'bold')
            if sind == 0:
                ax.set_title(dcm)

    cax = fig.add_axes([0.1, 0.08, 0.8, 0.01])
    plt.colorbar(h, cax = cax, orientation = 'horizontal',
                 label = 'Trend of ' + str(lag) + '-month SSI (year$^{-1}$)')
    cax.text(.0, -1., 'Dry', fontsize = 5, transform = cax.transAxes,
             horizontalalignment = 'center') # , color = '#762a83')
    cax.text(1.,  -1., 'Wet', fontsize = 5, transform = cax.transAxes,
             horizontalalignment = 'center') # , color = '#3288bd')

    fig.savefig(os.path.join(mg.path_out(), 'figures',
                             'fig1S-2_trend_' + str(lag) + '_0.5' +
                             suffix + '_bymodel',
                             dist + '_' + expr, 
                             model + '_trend.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)


p = mp.Pool(4)
p.map_async(plotter, cmip6_list)
p.close()
p.join()
