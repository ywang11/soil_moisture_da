"""
2020/09/18

Global trend in the CMIP6 drivers of soil moisture.
1951-2014 <to accomodate GRUN runoff>
1980-2014 <to accomodate GlobSnow SWE>
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, target_lat, target_lon
import itertools as it
from misc.cmip6_utils import one_layer_availability, mrsol_availability
from misc.analyze_utils import calc_seasonal_trend0
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
import sys


res = '0.5'
land_mask = 'vanilla'


var_list = ['pr-evspsbl'] # ['pr', 'evspsbl', 'mrro', 'snw', 'pr-evspsbl',
#'pr-evspsbl-mrro', 'pr-evspsbl-mrro-dsnw',
#            'sm_0-10cm', 'sm_0-100cm']


year = range(1950, 2015) # 1950, 1980
# ----- note that the year in the trend skips the first year, because
#       of DJF.
year_str = str(year[1]) + '-' + str(year[-1])


def standard_read(vv, model, yrng):
    hr = xr.open_mfdataset(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                        land_mask, 'CMIP6', 'historical',
                                        model, vv + '_' + res + '*.nc'),
                           decode_times = False)
    hr['time'] = pd.date_range('1950-01-01', '2014-12-31', freq = 'MS')
    var = hr[vv][(hr['time'].to_index().year >= yrng[0]) & \
                 (hr['time'].to_index().year <= yrng[-1]) \
    ].copy(deep = True)
    hr.close()
    return var


model = 'CanESM5_r10i1p1f1'

for varname in var_list:
    if varname == 'pr-evspsbl-mrro':
        pr = standard_read('pr', model, year)
        evspsbl = standard_read('evspsbl', model, year)
        if (model == 'EC-Earth3') | (model == 'EC-Earth3-Veg'):
            evspsbl = - evspsbl
        mrro = standard_read('mrro', model, year)
        var = pr - evspsbl - mrro
    elif varname == 'pr-evspsbl-mrro-dsnw':
        pr = standard_read('pr', model, year)
        evspsbl = standard_read('evspsbl', model, year)
        if (model == 'EC-Earth3') | (model == 'EC-Earth3-Veg'):
            evspsbl = - evspsbl
        mrro = standard_read('mrro', model, year)
        snw = standard_read('snw', model, year)
        snw2 = standard_read('snw', model, range(year[0]-1, year[0]))

        dsnw = xr.concat([snw[1:, :, :] - snw[:-1, :, :].values,
                          xr.DataArray(np.full([1, snw.shape[1],
                                                snw.shape[2]], np.nan),
                                       coords = {'time': snw['time'][[0]],
                                                 'lat': snw['lat'],
                                                 'lon': snw['lon']},
                                       dims = ['time', 'lat', 'lon'])],
                         dim = 'time')
        # --- divide by the days in month
        dom = dsnw['time'].to_index().to_period('M').days_in_month.values
        dsnw = dsnw / np.reshape(dom, [-1, 1, 1])
        var = pr - evspsbl - mrro - dsnw
    elif varname == 'pr-evspsbl':
        pr = standard_read('pr', model, year)
        evspsbl = standard_read('evspsbl', model, year)
        if (model == 'EC-Earth3') | (model == 'EC-Earth3-Veg'):
            evspsbl = - evspsbl
        var = pr - evspsbl
    elif 'sm' in varname:
        dcm = varname.split('_')[-1]
        hr = xr.open_mfdataset(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                            land_mask, 'CMIP6', 'historical',
                                            model, 'mrsol_' + res + '_*_' + \
                                            dcm + '.nc'),
                               decode_times = False)
        hr['time'] = pd.date_range('1950-01-01', '2014-12-31', freq = 'MS')
        var = hr['sm'][(hr['time'].to_index().year >= year[0]) & \
                       (hr['time'].to_index().year <= year[-1]) \
        ].copy(deep = True)
        hr.close()
    else:
        var = standard_read(varname, model, year)
    
    # calculate trend
    for s in ['Annual'] # ['DJF', 'MAM', 'JJA', 'SON']:
        g_map_trend, g_p_values, g_intercepts = calc_seasonal_trend0(var, s)
        xr.Dataset({'g_map_trend': (['lat','lon'], g_map_trend),
                    'g_p_values': (['lat','lon'], g_p_values),
                    'g_intercepts': (['lat','lon'], g_intercepts)},
                   coords = {'lon':target_lon[res],
                             'lat':target_lat[res]} \
        ).to_netcdf(os.path.join(mg.path_out(), 'cmip6_drivers_summary',
                                 varname, model + '_' + res + \
                                 '_g_map_trend_' + s + '_' + year_str + '.nc'))
