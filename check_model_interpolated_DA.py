"""
2019/09/06
ywang254@utk.edu

Check the model and ensemble members in the "data" folder and the 
  "intermediate" folder.

// Print to .csv
// Nominal resolution uses the average difference between latitudes/longitudes

Project, Experiment, Model, Nominal resolution, Variable, Variant in "data", -
     Variant in "Interp_DA/None", Variant in "Interp_DA/vanilla"
"CMIP6", historical, , , "mrsol 0-10cm", , 
       , ssp585/rcp85, , "mrsol 10-30cm", ,
                         "mrsol 30-50cm", ,
                         "mrsol 50-100cm", ,
"""
import os
import pandas as pd
import numpy as np
import utils_management as mg
from utils_management.constants import depth_cm
from glob import glob
import xarray as xr


data = pd.DataFrame(data = np.nan, index = range(16),
                    columns = ['Project', 'Experiment', 'Model',
                               'Nominal resolution (lat)',
                               'Nominal resolution (lon)', 'Variable',
                               'Variant in .data.', 'Depth',
                               'Variant in .intermediate.Interp_DA.None',
                               'Variant in .intermediate.Interp_DA.vanilla'])


count = 0
proj = 'CMIP6'
var = 'mrsol'
for expr in ['piControl', 'historical', 'hist-aer', 'hist-nat',
             'hist-GHG', 'ssp585']:
    prefix = os.path.join(mg.path_data(), proj, expr, var)
    model_list = [x for x in os.listdir(prefix) if \
                  os.path.isdir(os.path.join(prefix, x))]
    for m in model_list:
        if ((expr == 'piControl') | (expr == 'historical')) & \
           ((m == 'GISS-E2-1-H') | (m == 'GISS-E2-1-G')):
            flist1 = glob(os.path.join(mg.path_data(), proj, expr, var, m,
                                       'mrlsl*' + m + '_' + expr + '*.nc'))
        else:
            flist1 = glob(os.path.join(mg.path_data(), proj, expr, var, m,
                                       var + '*' + m + '_' + expr + '*.nc'))
        for dcm in depth_cm:
            data.loc[count, 'Project'] = proj
            data.loc[count, 'Experiment'] = expr
            data.loc[count, 'Model'] = m

            if len(flist1) > 0:
                r = xr.open_dataset(flist1[0])
                data.loc[count, 'Nominal resolution (lat)'] = \
                    np.mean(r.lat.values[1:] - r.lat.values[:-1])
                data.loc[count, 'Nominal resolution (lon)'] = \
                    np.mean(r.lon.values[1:] - r.lon.values[:-1])
                r.close()

                data.loc[count, 'Variable'] = var

                list_variant = sorted(list(np.unique([x.split('_')[-3] \
                                                      for x in flist1])))
                data.loc[count, 'Variant in .data.'] = '; '.join(list_variant)

                data.loc[count, 'Depth'] = dcm

                for land_mask in ['None', 'vanilla']:
                    list_variant2 = []
                    for v in list_variant:
                        flist2 = glob(os.path.join(mg.path_intrim_out(), 
                                                   'Interp_DA', land_mask,
                                                   proj, expr, m + '_' + v,
                                                   var + '_*_' + dcm + '.nc'))
                        if len(flist2) > 0:
                            list_variant2.append(v)
                    data.loc[count, 'Variant in .intermediate.' + \
                             'Interp_DA.' + land_mask] = \
                             '; '.join(list_variant2)
            count += 1


data.to_csv(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                         'check_model_interpolated_DA.csv'), index = False)
