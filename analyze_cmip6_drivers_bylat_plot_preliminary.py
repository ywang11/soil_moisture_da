import os
import numpy as np
import xarray as xr
from utils_management.constants import *
import utils_management as mg
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from analyze_cmip6_drivers_map import get_model_list
from misc.plot_utils import cmap_div, stipple
import multiprocessing as mp
import itertools as it


def get_sm_trend(dcm, model, expr):
    res = '0.5'

    trend = pd.read_csv(os.path.join(mg.path_out(), 'standard_diagnostics_spi', expr,
                                     'gmm', 'bylat_trend_3_' + res + '_' + dcm + \
                                     '_noSahel.csv'),
                        index_col = 0, header = [0,1]).loc[:, model]
    trend = trend.loc[~np.isnan(trend.index), :]
    trend_p = pd.read_csv(os.path.join(mg.path_out(), 'standard_diagnostics_spi', expr,
                                       'gmm',
                                       'bylat_trend_p_3_' + res + '_' + dcm + '_noSahel.csv'),
                          index_col = 0, header = [0,1]).loc[:, model]
    trend_p = trend_p.loc[~np.isnan(trend_p.index), :]
    return trend, trend_p


def get_driver_trend(expr, var, model, year_str):
    res = '0.5'
    if year_str == '1971-2016':
        trend = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_drivers_summary', var,
                                         expr, 'bylat_trend_' + res + '.csv'),
                            header = [0,1], index_col = 0).loc[:, model]
        trend_p = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_drivers_summary', var,
                                           expr, 'bylat_p_trend_' + res + '.csv'),
                              header = [0,1], index_col = 0).loc[:, model]
    else:
        trend = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_drivers_summary', var,
                                         expr, 'bylat_fut_trend_' + res + '.csv'),
                            header = [0,1,2],
                            index_col = 0).loc[:, (model, slice(None), year_str)]
        trend_p = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_drivers_summary', var,
                                           expr, 'bylat_fut_p_trend_' + res + '.csv'),
                              header = [0,1,2],
                              index_col = 0).loc[:, (model, slice(None), year_str)]
    return trend, trend_p


year_str = '1971-2016'
##for ii,jj,kk,ll in it.product(range(2), range(25), [0, 0.01, 0.1, 0.5, 0.9],
##                              ['hist-aer']):
def plotter(option):
    ii, jj, kk, ll = option

    land_mask = 'vanilla'
    res = '0.5'
    dcm = depth_cm_new[ii]
    cmip6_list = get_model_list(ll, drivers, dcm, land_mask, res)
    model = cmip6_list[jj]
    alpha = kk

    hr = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_drivers_corr', 'bylat',
                                      ll, 'alpha=%.2f' % alpha,
                                      model + '_' + dcm + '_' + year_str + '.nc'))

    #
    fig, axes = plt.subplots(1, 3, figsize = (6.5, 4))
    for vind, vv in enumerate(['r2', 'mse', 'rpd']):
        ax = axes.flat[vind]
        cf = ax.contourf(hr['month'], hr['lat'], hr[vv],
                         cmap = 'Spectral')
        ax.set_title(vv)
        plt.colorbar(cf, ax = ax, shrink = 0.7)
    fig.savefig(os.path.join(mg.path_out(), 'cmip6_drivers_corr',
                             'bylat_plot', ll, 'alpha=%.2f' % alpha,
                             'metrics_' + model + '_' + dcm + '_' + year_str + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)


    #
    fig, axes = plt.subplots(2, 2, figsize = (6.5, 6.5))
    for vind, vv in enumerate(drivers):
        ax = axes.flat[vind]
        cf = ax.contourf(hr['month'], hr['lat'], hr['coef'][vind, :, :], 
                         cmap = cmap_div(), levels = np.linspace(-0.5, 0.5, 41),
                         extend = 'both')
        ax.set_title(vv + ' corr')
        plt.colorbar(cf, ax = ax, shrink = 0.7)
    fig.savefig(os.path.join(mg.path_out(), 'cmip6_drivers_corr',
                             'bylat_plot', ll, 'alpha=%.2f' % alpha,
                             'corr_' + model + '_' + dcm + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)

    # accounted-for trend in soil moisture by trend
    fig, axes = plt.subplots(4, 3, figsize = (6.5, 8.))
    for vind, vv in enumerate(drivers):
        trend, trend_p = get_driver_trend(ll, vv, model, year_str)
        coef = pd.DataFrame(hr['coef'][vind, :, :].values,
                            index = hr['lat'].values,
                            columns = [str(tt) for tt in hr['month'].values])
        ax = axes[vind, 0]
        if vv == 'pr':
            levels = np.linspace(-0.01, 0.01, 41)
        elif vv == 'tas':
            levels = np.linspace(-0.1, 0.1, 41)
        elif vv == 'lai':
            levels = np.linspace(-0.02, 0.02, 41)
        elif vv == 'snw':
            levels = np.linspace(-1., 1., 41)
        cf = ax.contourf(hr['month'], hr['lat'], trend,
                         cmap = cmap_div(), levels = levels,
                         extend = 'both')
        
        ax.set_title(vv + ' trend')
        plt.colorbar(cf, ax = ax, shrink = 0.7)
        stipple(ax, hr['lat'], hr['month'], trend_p <= 0.05)
        
        ax = axes[vind, 1]
        cf = ax.contourf(hr['month'], hr['lat'], trend * coef,
                         cmap = cmap_div(), levels = np.linspace(-0.006, 0.006, 41),
                         extend = 'both')
        ax.set_title(vv + '_accounted trend')
        plt.colorbar(cf, ax = ax, shrink = 0.7)

        stipple(ax, hr['lat'], hr['month'], trend_p <= 0.05)

    ax = axes[0, 2]
    trend, trend_p = get_sm_trend(dcm, model, ll)
    cf = ax.contourf(hr['month'], hr['lat'], trend,
                     cmap = cmap_div(), levels = np.linspace(-0.006, 0.006, 41),
                     extend = 'both')
    ax.set_title(dcm + ' trend')
    plt.colorbar(cf, ax = ax, shrink = 0.7)
    stipple(ax, hr['lat'], hr['month'], trend_p <= 0.05)

    fig.savefig(os.path.join(mg.path_out(), 'cmip6_drivers_corr', 'bylat_plot',
                             ll, 'alpha=%.2f' % alpha,
                             'accounted_trend_' + model + '_' + dcm + '_' + year_str + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)

    hr.close()


if __name__ == '__main__':
    p = mp.Pool(6)
    p.map_async(plotter,
                list(it.product(range(2), range(72), [0, 0.01, 0.1, 0.5, 0.9],
                                ['hist-GHG','hist-aer','historical'])))
    p.close()
    p.join()
