"""
2019/12/25
ywang254@utk.edu

Plot the start & end of the growing season.
"""
import xarray as xr
import os
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import utils_management as mg

suffix = '_interp' # or '' for the original data

data = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                    'Zhu_globalMonthlyGS' + suffix + '.nc'))
temp = data['growing_season'].values.copy()

start_month = np.full([data['growing_season'].shape[1],
                       data['growing_season'].shape[2]], np.nan)
end_month = np.full([data['growing_season'].shape[1],
                     data['growing_season'].shape[2]], np.nan)

temp = np.concatenate([temp, temp[0, :, :][np.newaxis, :, :]], axis = 0)

for month in range(12):
    start_month[(temp[month, :, :] < 0.5) & \
                (temp[month+1, :, :] > 0.5)] = month
    end_month[(temp[month, :, :] > 0.5) & \
              (temp[month+1, :, :] < 0.5)] = month

# No season
start_month[ np.sum(data['growing_season'].values > 0.5,
                    axis = 0) == 0 ] = np.nan
end_month[ np.sum(data['growing_season'].values > 0.5,
                  axis = 0) == 0 ] = np.nan

# All year
start_month[ np.sum(data['growing_season'].values > 0.5,
                    axis = 0) == 12 ] = 0.
end_month[ np.sum(data['growing_season'].values > 0.5,
                  axis = 0) == 12 ] = 11.

data.close()


#
levels = np.arange(-0.5, 11.6)

fig, ax = plt.subplots(figsize = (20, 10), nrows = 2, ncols = 1,
                       subplot_kw = {'projection': ccrs.PlateCarree()})
ax[0].coastlines()
h = ax[0].contourf(data.lon.values, data.lat.values, start_month,
                   levels = levels, cmap = 'Spectral')
cbar = plt.colorbar(h, ax = ax[0], shrink = 0.5, boundaries = levels)
cbar.set_ticks(range(12))
ax[0].set_title('Begin month of growing season')

ax[1].coastlines()
h = ax[1].contourf(data.lon.values, data.lat.values, end_month,
                   levels = levels, cmap = 'Spectral')
cbar = plt.colorbar(h, ax = ax[1], shrink = 0.5, boundaries = levels)
cbar.set_ticks(range(12))
ax[1].set_title('Last month of growing season')

fig.savefig(os.path.join(mg.path_data(), 'Global_Masks',
                         'Zhu_globalMonthlyGS_start_end' + suffix + '.png'),
            dpi = 600.)


#
