"""
2019/07/14

ywang254@utk.edu

Apply the common land mask from "../soil_moisture_merge" on the CMIP6 models
 that were interpolated to 0.5 deg, unmasked.
Need to run "land_mask_minimum_cmip6.py" "land_mask_minimum_obs_pr.py"
            "land_mask_minimum_cmip5.py" "land_mask_minimum_lsm.py"
            "land_mask_minimum_merge.py" first. The "_merge" is the final
            land mask.

2019/12/19

Use the vanilla land mask only because the minimum land mask removes too much
 land area.

2020/05/30

Mnimum land mask is fixed. Use it.
"""
import sys
import os
import xarray as xr
import numpy as np
import pandas as pd
from glob import glob
import utils_management as mg
from utils_management.constants import year_cmip6, depth_cm, depth_cm_new
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import multiprocessing as mp
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


land_mask = 'vanilla' # 'e1m', 'MODIS', 'vanilla'


# MODIFY
var = 'mrsol' # 'mrsol','mrsos','pr'
res = '0.5' # '0.5', '2.5', '5', '10'


###############################################################################
# Read the merged land mask.
###############################################################################
if (land_mask == 'MODIS') | (land_mask == 'vanilla'):
    if res == '0.5':
        data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                            'land_mask_minimum', 
                                            'merge_' + land_mask + '.nc'))
    else:
        data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                            'land_mask_minimum', 
                                            'merge_' + land_mask + '_' + \
                                            res + '.nc'))
    mask = data.mask.values.copy()
    data.close()
elif land_mask == 'elm':
    data = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                        'elm_half_degree_grid_negLon.nc'))
    mask = data.landfrac.values > 0.
    data.close()

hr = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                  'land_mask_minimum',
                                  'cmip6_da.nc'))
mask = mask & (data.mask.values.copy())
hr.close()


fig, ax = plt.subplots(figsize = (12, 12),
                       subplot_kw = {'projection': ccrs.PlateCarree()})
ax.coastlines(color = 'red')
ax.gridlines(draw_labels = True)
ax.contourf(hr.lon, hr.lat, mask, cmap = 'Greys')
fig.savefig(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                         'apply_merge_cmip6_da.png'),
            bbox_inches = 'tight', dpi = 600.)
plt.close(fig)



###############################################################################
# Apply on the CMIP6 models.
###############################################################################
def apply_mask(f):
    print(f)
    data = xr.open_dataset(f, decode_times = False)
    data2 = data.where(mask)
    data2.to_netcdf(f.replace('None', land_mask))
    data.close()


for expr in ['historical','piControl', 'hist-nat','hist-GHG','hist-aer',
             'ssp585']:
    if var == 'mrsol':
        for d_ind, dcm in enumerate(depth_cm + ['0-100cm']): 
            _, temp = mrsol_availability(dcm, 'None', expr, res)
            temp2 = []
            for f in temp.keys():
                temp2.extend(temp[f])
            if d_ind == 0:
                fpath = temp2
            else:
                fpath += temp2
    else:
        _, temp = one_layer_availability(var, 'None', expr, res)
        fpath = []
        for f in temp.keys():
            fpath.extend(temp)

    pool = mp.Pool(mp.cpu_count() // 2)
    r = pool.map(apply_mask, fpath)
    pool.close()
    pool.join()
