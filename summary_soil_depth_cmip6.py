"""
Extract the soil depth variable from the CMIP5 and CMIP6 models.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import os
import pandas as pd
from glob import glob
import numpy as np


land_mask = 'vanilla'
res = '0.5'


cmip6_depth_list = {}


cmip6_list, _ = mrsol_availability('0-10cm', land_mask, 'piControl', res)
##cmip6.remove('CanESM5_r1i1p2f1')
##cmip6.remove('IPSL-CM6A-LR_r1i2p1f1')
##cmip6 = sorted(cmip6)
cmip6 = list(np.unique([x.split('_')[0] for x in cmip6_list]))


for model in cmip6:
    flist = glob(os.path.join(mg.path_data(), 'CMIP6', 'piControl',
                              'mrsol', model, '*_' + model + '_*.nc'))

    hr = xr.open_dataset(flist[0])

    # Convert to cm
    if (model == 'CNRM-CM6-1') | (model == 'CNRM-ESM2-1'):
        depth_bnds = hr['sdepth_bounds'].values * 100
    elif (model == 'IPSL-CM6A-LR'):
        depth_bnds = np.array([[0., 0.001], [0.001, 0.004], [0.004, 0.01], \
                               [0.01, 0.022], [0.022, 0.045], [0.045, 0.092], \
                               [0.092, 0.186], [0.186, 0.374], [0.374, 0.749],\
                               [0.749, 1.499], [1.499, 1.999]]) * 100
    else:
        depth_bnds = hr['depth_bnds'].values * 100

    dbc = []
    for i in range(depth_bnds.shape[0]):
        a = ('%.3f' % depth_bnds[i,0]).rstrip('0')
        if a[-1] == '.':
            a = a[:-1]
        b = ('%.3f' % depth_bnds[i,1]).rstrip('0')
        if b[-1] == '.':
            b = b[:-1]

        dbc.append(a + '-' + b + 'cm')
    cmip6_depth_list[model] = dbc
    hr.close()


print(cmip6_depth_list)
