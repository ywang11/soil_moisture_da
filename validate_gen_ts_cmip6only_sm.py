"""
2020/08/04
ywang254@utk.edu

Create the SREX time series of the soil moisture products, and the validation
 datasets.

2021/05/26
ywang254@utk.edu

Add the SREX time series for the source datasets.

2021/07/02

Copied from soil_moisture_merge, limit to CMIP6 ESMs.
"""
import regionmask as rmk
import matplotlib.pyplot as plt
import os
import pandas as pd
import xarray as xr
import numpy as np
import utils_management as mg
from utils_management.constants import depth, depth_cm, depth_cm_new, path_to_final_prod
from misc.analyze_utils import decode_month_since, get_ndvi_mask
from misc.cmip6_utils import *
from time import time


# Grid area
hr = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                    'elm_half_degree_grid_negLon.nc'))
area = hr.area.copy(deep = True)
hr.close()


#
noSahel = True
land_mask = 'vanilla'
if noSahel:
    ndvi_mask = get_ndvi_mask(land_mask)


#
srex_mask = rmk.defined_regions.srex.mask(area)
def avg_srex(sm, which):
    """Average within SREX regions (all are over land)"""
    sm_srex = pd.DataFrame(data = np.nan,
                           index = sm['time'].to_index(),
                           columns = [str(sid) for sid in which])
    for sid in which:
        keep = (np.abs(srex_mask.values - sid) < 1e-6) & \
               (~np.isnan(sm.values[0,:,:]))
        sm_r = (sm.where(keep) * area.where(keep)).sum(dim = ['lat','lon']) \
               / np.sum(area.where(keep))
        sm_srex.loc[:, str(sid)] = sm_r.values
    return sm_srex


#
prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_srex_all = pd.DataFrame(data = np.nan,
                             index = pd.date_range('1970-01-01', '2016-12-31',
                                                   freq = 'MS'),
                             columns = pd.MultiIndex.from_product([ \
    prod_list, depth_cm_new, ['Global'] + [str(sid) for sid in range(1,27)]]))
for prod in prod_list:
    for d, dcm in zip(['0.00-0.10', '0.00-1.00'], depth_cm_new):
        if dcm == '0-10cm':
            path_prod = path_to_final_prod(prod, d, dcm)
            hr = xr.open_dataset(path_prod, decode_times = False)
            if 'em' in prod:
                sm = hr['predicted'].copy(deep = True)
            else:
                sm = hr['sm'].copy(deep = True)
            sm['time'] = pd.date_range('1950-01-01', '2016-12-31', freq = 'MS')
            sm = sm[sm['time'].to_index().year >= 1970, :, :]
            hr.close()
        else:
            for ind, dx, dcmx in zip(range(len(depth_cm)), depth, depth_cm):
                path_prod = path_to_final_prod(prod, dx, dcmx)
                hr = xr.open_dataset(path_prod, decode_times = False)
                if 'em' in prod:
                    sm = hr['predicted'].copy(deep = True)
                else:
                    sm = hr['sm'].copy(deep = True)
                sm['time'] = pd.date_range('1950-01-01', '2016-12-31', freq = 'MS')
                sm = sm[sm['time'].to_index().year >= 1970, :, :]
                hr.close()

                if ind == 0:
                    sm_all = sm * (float(dx.split('-')[1]) - float(dx.split('-')[0]))
                else:
                    sm_all = sm_all + sm * (float(dx.split('-')[1]) - float(dx.split('-')[0]))
                sm = sm_all
        sm = sm[(sm['time'].to_index().year >= 1970) & \
                (sm['time'].to_index().year <= 2016), :, :]
        if noSahel:
            sm = sm.where(ndvi_mask)
        prod_srex_all.loc[:, (prod, dcm,
                              'Global')] = (sm * area).sum(dim = ['lat','lon']) / np.sum(area)
        temp = avg_srex(sm, range(1,27))
        for sid in range(1,27):
            prod_srex_all.loc[:, (prod, dcm, str(sid))] = temp.loc[:, str(sid)]
prod_srex_all.to_csv(os.path.join(mg.path_out(), 
                                  'prod_srex_all_sm.csv'))



# Source datasets
res = '0.5'
a, _ = mrsol_availability('0-10cm', 'vanilla', 'historical', res)
b, _ = mrsol_availability('0-10cm', 'vanilla', 'ssp585', res)
cmip6_list = list(set(a) & set(b))
yrng = range(1970, 2017)


prod_source_all = pd.DataFrame(data = np.nan,
                               index = pd.date_range('1970-01-01', '2016-12-31', freq = 'MS'),
                               columns = pd.MultiIndex.from_product([cmip6_list,
    depth_cm_new, ['Global'] + [str(sid) for sid in range(1,27)]]))
for dcm in depth_cm_new:
    for nn in cmip6_list:
        start = time()

        flist = [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', 'historical', nn,
                              'mrsol_' + res + '_' + str(yy) + '_' + dcm + '.nc') \
                 for yy in range(max(1950, yrng[0]), min(2015, yrng[-1]+1))] + \
                [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', 'ssp585', nn,
                              'mrsol_' + res + '_' + str(yy) + '_' + dcm + '.nc') \
                 for yy in range(max(2015, yrng[0]), max(2015, yrng[-1]+1))]
        if len(flist) == 0:
            continue

        hr = xr.open_mfdataset(flist, decode_times = False)
        sm = hr['sm'].copy(deep = True)
        sm['time'] = pd.date_range(str(yrng[0]) + '-01-01',
                                   str(yrng[-1]) + '-12-31', freq = 'MS')
        sm = sm[(sm['time'].to_index().year >= 1970) & \
                (sm['time'].to_index().year <= 2016), :, :]
        hr.close()
        if noSahel:
            sm = sm.where(ndvi_mask)

        prod_source_all.loc[sm['time'].to_index(), (nn, dcm, 'Global')] = \
            ((sm * area).sum(dim = ['lat','lon']) / np.sum(area)).values
        temp = avg_srex(sm, range(1,27))
        for sid in range(1,27):
            prod_source_all.loc[sm['time'].to_index(),
                                (nn, dcm, str(sid))] = temp.loc[:, str(sid)]

        print(time() - start)
prod_source_all.to_csv(os.path.join(mg.path_out(), 
                                    'validate_gen_ts_cmip6only_sm.csv'))
