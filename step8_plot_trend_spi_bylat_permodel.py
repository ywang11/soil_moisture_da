"""
20200626
ywang254@utk.edu

Plot the trend in latitudinal mean SSI.
"""
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
from misc.cmip6_utils import mrsol_availability
import matplotlib as mpl
import utils_management as mg
import itertools as it


mpl.rcParams['font.size'] = 5.5
mpl.rcParams['axes.titlesize'] = 4.5


res = '0.5'
land_mask = 'vanilla'
lat_eof = [float(x) for x in lat_median]
season_list = [str(i) for i in range(12)]
expr = 'ANT'
folder = 'bylat'
suffix = '_noSahel'
lag = 3


lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


season_list = [str(i) for i in range(12)]
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']


cmap = cmap_div()
levels = np.arange(-1.6e-2, 1.61e-2, 1e-3)
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


for dist, dcm in it.product(['gmm', 'weibull'], depth_cm_new):
    if expr == 'historical':
        a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
        b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
        cmip6_list = list(set(a) & set(b))
    elif expr in ['hist-nat', 'hist-aer', 'hist-GHG']:
        cmip6_list, _ = mrsol_availability(dcm, land_mask, expr, res)
    elif expr == 'GHGAER':
        a, _ = mrsol_availability(dcm, land_mask, 'hist-aer', res)
        b, _ = mrsol_availability(dcm, land_mask, 'hist-GHG', res)
        cmip6_list = list(set(a) & set(b))
    elif expr == 'ANT':
        a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
        b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
        c, _ = mrsol_availability(dcm, land_mask, 'hist-nat', res)
        cmip6_list = list(set(a) & set(b) & set(c))

    cmip6_list = sorted(cmip6_list)

    for i, model in enumerate(cmip6_list):
        if (i == 0) | (i == 42):
            if len(cmip6_list) > 42:
                nrows = int(np.ceil(len(cmip6_list)/14))
            else:
                nrows = int(np.ceil(len(cmip6_list)/7))
            fig, axes = plt.subplots(nrows = nrows,
                                     ncols = 7, figsize = (7.2, 9),
                                     sharex = True, sharey = True)
            fig.subplots_adjust(wspace = 0.0, hspace = 0.15, top = 0.88)
        if i < 42:
            print(i)
            ax = axes.flat[i]
        else:
            ax = axes.flat[i - 42]

        if expr == 'GHGAER':
            d1 = pd.read_csv(os.path.join(mg.path_out(),
                                          'standard_diagnostics_spi',
                                          'hist-GHG', dist,
                                          'bylat_trend_' + \
                                          str(lag) + '_' + res + '_' + \
                                          dcm + '.csv'),
                             index_col = 0, header = [0,1])
            d2 = pd.read_csv(os.path.join(mg.path_out(),
                                          'standard_diagnostics_spi',
                                          'hist-aer', dist,
                                          'bylat_trend_' + str(lag) + '_' + \
                                          res + '_' + dcm + '.csv'),
                             index_col = 0, header = [0,1])
            data = d1 + d2
        elif expr == 'ANT':
            d1 = pd.read_csv(os.path.join(mg.path_out(),
                                          'standard_diagnostics_spi',
                                          'historical', dist,
                                          'bylat_trend_' + \
                                          str(lag) + '_' + res + '_' + \
                                          dcm + '.csv'),
                             index_col = 0, header = [0,1])
            d2 = pd.read_csv(os.path.join(mg.path_out(),
                                          'standard_diagnostics_spi',
                                          'hist-nat', dist,
                                          'bylat_trend_' + \
                                          str(lag) + '_' + res + '_' + \
                                          dcm + '.csv'),
                             index_col = 0, header = [0,1])
            data = d1 - d2
        else:
            data = pd.read_csv(os.path.join(mg.path_out(),
                                            'standard_diagnostics_spi',
                                            expr, dist, 'bylat_trend_' + \
                                            str(lag) + '_' + res + '_' + \
                                            dcm + '.csv'),
                               index_col = 0, header = [0,1])
        data.index = data.index.astype(str)

        if model in data.columns.levels[0]:
            values = data.loc[:, model].loc[lat_median, season_list].values
    
            h = ax.contourf(range(values.shape[1]), range(values.shape[0]),
                            values, cmap = cmap, levels = levels,
                            extend = 'both')
            ax.set_yticks(range(2, values.shape[0], 4))
            ax.set_yticklabels(lat_median_name[2::4])
            ax.set_xticks(np.linspace(0.35, 10.65, 12)) # range(0, 12, 1))
            ax.set_xticklabels(season_list_name)
            ax.tick_params(axis = 'y', length = 0.1)
            if (i < 35) | ((i >= 42) & (i < 77)):
                ax.tick_params('x', length = 0)

            ax.set_title('(' + lab[np.mod(i,42)] + ') ' + \
                         model.replace('_','\n'), pad = 1.)
        else:
            ax.axis('off')

        if (i == 41) | (i == 83) | (i == (len(cmip6_list)-1)):
            cax = fig.add_axes([0.1, 0.065, 0.8, 0.01])
            plt.colorbar(h, cax = cax, orientation = 'horizontal',
                 label = 'Trend of ' + str(lag) + '-month SSI (year$^{-1}$)')
            if i == 41:
                order = 1
            else:
                order = 2
            fig.savefig(os.path.join(mg.path_out(), 'figures',
                                     'fig1_trend_' + str(lag) + '_' + \
                                     res + suffix + '_bymodel',
                                     dist + '_' + expr + '_' + dcm + '_' + \
                                     str(order) + '.png'),
                        dpi = 600., bbox_inches = 'tight')
            plt.close(fig)
