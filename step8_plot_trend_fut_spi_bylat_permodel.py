"""
20200425
ywang254@utk.edu

Plot the trend in latitudinal mean SPI.
"""
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_gen, stipple
from misc.cmip6_utils import mrsol_availability
import matplotlib as mpl
import utils_management as mg
import itertools as it


mpl.rcParams['font.size'] = 5.5
mpl.rcParams['axes.titleweight'] = 'normal'
mpl.rcParams['axes.titlepad'] = 0.2


res = '0.5'
land_mask = 'vanilla'
lat_eof = [float(x) for x in lat_median]
season_list = [str(i) for i in range(12)]
year_list = [range(1951,1995), range(1971, 2015), range(1991, 2035),
             range(2011, 2055), range(2031, 2075), range(2051, 2095)]
year_str_list = [str(i[0])+'-'+str(i[-1]) for i in year_list]
expr = 'historical'
folder = 'bylat'
suffix = ''
lag = 3


dcm0 = '0-100cm'
if expr == 'historical':
    a, _ = mrsol_availability(dcm0, land_mask, 'historical', res)
    b, _ = mrsol_availability(dcm0, land_mask, 'ssp585', res)
    cmip6_list = list(set(a) & set(b))
else:
    cmip6_list, _ = mrsol_availability(dcm0, land_mask, expr, res)


lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


season_list = [str(i) for i in range(12)] # ensure order of x-axis
#lat_median = lat_median[::-1] # ensure order of y-axis
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']


cmap = cmap_gen('autumn', 'winter_r')
levels = np.arange(-1e-2, 1.1e-2, 1e-3)


for dist, model in it.product(['gmm','weibull'], cmip6_list):
    fig, axes = plt.subplots(nrows = len(depth_cm_new), ncols = 6,
                             figsize = (6.5, len(depth_cm_new)*2),
                             sharex = True, sharey = True)
    fig.subplots_adjust(wspace = 0.01, hspace = 0.1)
    for i,j in it.product(range(len(year_list)), range(len(depth_cm_new))):
        dcm = depth_cm_new[j]
        year = year_list[i]
        year_str = year_str_list[i]

        ax = axes.flat[j * len(year_list) + i]

        data = pd.read_csv(os.path.join(mg.path_out(),
                                        'standard_diagnostics_spi',
                                        expr, dist, 'bylat_trend_fut_' + \
                                        year_str + '_' + str(lag) + \
                                        '_' + res + '_' + dcm + '.csv'),
                            index_col = 0, header = [0,1])
        data.index = data.index.astype(str)

        if model in data.columns.levels[0]:
            values = data.loc[:, model].loc[lat_median, season_list].values

            h = ax.contourf(range(values.shape[1]), range(values.shape[0]),
                            values, cmap = cmap, levels = levels,
                            extend = 'both')
            ax.set_yticks(range(2, values.shape[0], 4))
            ax.set_yticklabels(lat_median_name[2::4])
            ax.set_xticks(np.linspace(0.35, 10.65, 12)) # range(0, 12, 1))
            ax.set_xticklabels(season_list_name)
            if j == 0:
                ax.tick_params('x', length = 0)
            ax.tick_params(axis = 'y', length = 0.1)
    
            ax.set_title(year_str)
            if int(np.mod(i,6)) == 0:
                ax.set_ylabel(dcm)
        else:
            ax.axis('off')

    cax = fig.add_axes([0.1, 0.03, 0.8, 0.015])
    plt.colorbar(h, cax = cax, orientation = 'horizontal',
                 label = 'Trend of ' + str(lag) + '-month SSI (year$^{-1}$)')
    fig.savefig(os.path.join(mg.path_out(), 'figures', 'fig5_trend_' + \
                             str(lag) + '_' + res + suffix + '_bymodel',
                             dist + '_' + model + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
