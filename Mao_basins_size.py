"""
20191218

Check basin area.
"""
import xarray as xr
import pandas as pd
import utils_management as mg
import os
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


data = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                    'elm_half_degree_grid_negLon.nc'))
area = data.area.values.copy()
data.close()


data = xr.open_dataset(os.path.join(mg.path_data(), 'Mao_basins',
                                    'basins_half.nc'))
basin = data.basin.values.copy()
data.close()


basin_list = [int(x) for x in np.unique(basin) if ~np.isnan(x)]


#
df = pd.DataFrame(np.nan, index = basin_list, columns = ['Area (km^2)'])
df.index.name = 'ID'
for i in basin_list:
    df.loc[i, 'Area (km^2)'] = np.nansum(area[np.abs(basin - i) < 1e-6])
df.to_csv(os.path.join(mg.path_data(), 'Mao_basins', 'basins_half_size.csv'))


# Plot the largest 20 basins.
basin_exclude_top20 = df.sort_values(by = 'Area (km^2)',
                                     ascending = False).index.values[20:]

basin_new = basin.copy()
for i in basin_exclude_top20:
    basin_new[np.abs(basin - i) < 1e-3] = np.nan

fig, ax = plt.subplots(subplot_kw = {'projection': ccrs.PlateCarree()})
ax.coastlines()
ax.contourf(data.lon.values, data.lat.values, basin_new, cmap = 'Spectral')
fig.savefig(os.path.join(mg.path_data(), 'Mao_basins',
                         'basins_half_top20.png'), dpi = 600.)
plt.close(fig)


# Plot selected basins, with balance in latitude. 
basin_exclude_select = basin_exclude_top20.tolist()
basin_exclude_select.remove(24)
basin_exclude_select.remove(19)

basin_new = basin.copy()
for i in basin_exclude_select:
    basin_new[np.abs(basin - i) < 1e-3] = np.nan

fig, ax = plt.subplots(subplot_kw = {'projection': ccrs.PlateCarree()})
ax.coastlines()
ax.contourf(data.lon.values, data.lat.values, basin_new, cmap = 'Spectral')
fig.savefig(os.path.join(mg.path_data(), 'Mao_basins',
                         'basins_half_select.png'), dpi = 600.)
plt.close(fig)


#
basin_select = np.append(df.sort_values(by = 'Area (km^2)',
                                        ascending = False \
).index.values[:20], [24, 19])

print(basin_select)
