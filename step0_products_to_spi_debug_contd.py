import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, depth_new, target_lat, \
     target_lon
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
from misc.spi_utils import *
import sys
import matplotlib.pyplot as plt
from scipy.stats import norm, gamma, exponweib


land_mask = 'vanilla'
res = '0.5'

product_list = ['mean_lsm', 'dolce_lsm',
                'em_lsm', 'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all']
period = pd.date_range('1951-01-01', '2016-12-31', freq = 'YS')


## MODIFY
i = 0
d = depth_new[i]
dcm = depth_cm_new[i]
m = 'mean_lsm'
month = 1
dist = 'weibull'
s = 1

## Inf points, found in file
aa, bb, cc = 64, 287, 120


##
data = xr.open_mfdataset(os.path.join(mg.path_intrim_out(),
                                      'interp_merged_products', m + \
                                      '_' + d + '_' + res + '.nc'),
                         decode_times = False)
data_array = data.sm.values.copy()
data.close()
data_array_apply = rolling_mean(data_array, s, month)[1:, :, :]


##
hr = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'SPI_Products', 'fit',
                                  dist, m + '_' + dcm + '_' + str(s) + '_' + \
                                  res + '_' + str(month) + '.nc'))
if dist == 'weibull':
    params = {'a': hr['a'].values[bb,cc], 'shape': hr['shape'].values[bb,cc],
              'loc': hr['loc'].values[bb,cc],
              'scale': hr['scale'].values[bb,cc],
              'loglik': hr['loglik'].values[bb,cc]}
hr.close()


##
vector = data_array_apply[:,bb,cc]
print( vector )
print( exponweib.cdf(vector, a = params['a'], c = params['shape'],
                     loc = params['loc'], scale = params['scale']) )

print( get_zscore(params, dist, vector) )


## Plot the distribution
fig, ax = plt.subplots()
y, x = np.histogram(vector, bins = 10, normed = True)
x = x[1:]
hb = ax.bar(x, y, width = np.mean(np.diff(x)))
if dist == 'weibull':
    pdf = exponweib.pdf(x, a = params['a'], c = params['shape'],
                        loc = params['loc'],
                        scale = params['scale'])
    h, = ax.plot(x, pdf, '-r')
    ax.legend([hb, h], ['Histogram', 'Fitted distribution'])
fig.savefig(os.path.join(mg.path_intrim_out(), 'SPI_Products', 'debug',
                         dist + '_' + m + '_' + dcm + '_' + str(s) + '_' + \
                         str(month) + '.png'), dpi = 600.,
            bbox_inches = 'tight')
