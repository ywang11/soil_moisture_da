"""
20210809
ywang254@utk.edu

Obtain the collection of 1971-2016/1981-2016 trends in the observed variables.
"""
import utils_management as mg
from utils_management.constants import depth_cm_new, year_cmip6, lat_median, drivers, var_to_name
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal_bylat, ls_trend
from misc.cmip6_utils import one_layer_availability, mrsol_availability
from glob import glob
import itertools as it
import time
import multiprocessing as mp
from analyze_cmip6_drivers_map import get_model_list


start = time.time()


land_mask = 'vanilla'
res = '0.5'

noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['noSahel'].loc[np.array(lat_median).astype(int)].values)
else:
    suffix = ''
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['Normal'].loc[np.array(lat_median).astype(int)].values)


expr = 'historical' # ['historical', 'hist-GHG', 'hist-aer']

for vv in var_to_name.keys():
    if vv == 'lai':
        yrng = range(1982, 2017) # no data before 1981, and 1st month dropped
    else:
        yrng = range(1971, 2017)

    signal = pd.DataFrame(data = np.nan, index = var_to_name[vv], 
                          columns = range(12))
    proj = pd.DataFrame(data = np.nan, index = yrng, 
                        columns = pd.MultiIndex.from_product([range(12), var_to_name[vv]], 
                                                             names = ['season', 'source']))
    for season, nn in it.product(range(12), var_to_name[vv]):
        # Load the first EOF
        data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_drivers_fingerprint',
                                        expr, 'bylat', vv + '_eof_' + str(season) + suffix + \
                                        '.csv'),
                           index_col = 0)
        data.index = data.index.astype(str)
        eof1 = data.loc[lat_median, 'Full'].values

        # Load the observed signal
        data = pd.read_csv(os.path.join(mg.path_out(), 'obs_drivers_summary', vv, 
                                        nn + '_' + str(season) + '_0.5_g_lat_ts' + \
                                        suffix + '.csv'),
                           index_col = 0, parse_dates = True)
        values = data.loc[(data.index.year >= yrng[0]) & \
                          (data.index.year <= yrng[-1]), lat_median].values

        # Calculate the signal.
        wgts = lat_area_sqrt / np.mean(lat_area_sqrt)

        if noSahel:
            tt, pp = calc_signal_bylat(eof1[:-1], values[:,:-1],
                                       wgts[:-1], values.shape[0])
        else:
            tt, pp = calc_signal_bylat(eof1, values, wgts, values.shape[0])

        signal.loc[nn, season] = tt[0]
        proj.loc[:, (season, nn)] = pp
    signal.to_csv(os.path.join(mg.path_out(), 'obs_drivers_signal',
                               'bylat', expr + '_eofs', 
                               vv + '_signal_' + res + suffix + '.csv'))
    proj.to_csv(os.path.join(mg.path_out(), 'cmip6_drivers_signal',
                             'bylat', expr + '_eofs', 
                             vv + '_pc_' + res + suffix + '.csv'))

end = time.time()
print('Script finished in ' + str((end - start) / 60) + ' minutes.')
