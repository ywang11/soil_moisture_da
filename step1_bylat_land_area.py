"""
2019/10/07
ywang254@utk.edu

Calculate the by-latitude (in 10-degrees, -50 to 70) land area fraction.
"""
from utils_management.constants import depth_cm, lat_median, lat_bnds
import utils_management as mg
from misc.analyze_utils import get_ndvi_mask
import os
import xarray as xr
import pandas as pd
import numpy as np


land_mask = 'vanilla'


lat_area = pd.DataFrame(data = np.nan, index = lat_median,
                        columns = ['Normal', 'noSahel'])


#
data = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                    'elm_half_degree_grid_negLon.nc'))
area = data.area.values.copy()
# ---- mask out ocean area for both.
mask0 = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                                     'merge_' + land_mask + '.nc'))
area[~mask0.mask.values] = np.nan
mask0.close()
data.close()


#
for i in range(len(lat_median)):
    lat_area.loc[lat_median[i], 'Normal'] = \
        np.nansum(area[(data.lat.values >= lat_bnds[i, 0]) & \
                       (data.lat.values < lat_bnds[i, 1]), :])



# ---- further mask out Sahel
ndvi_mask = get_ndvi_mask(land_mask)
area2 = np.where(ndvi_mask, area, np.nan)
for i in range(len(lat_median)):
    lat_area.loc[lat_median[i], 'noSahel'] = \
        np.nansum(area2[(data.lat.values >= lat_bnds[i, 0]) & \
                        (data.lat.values < lat_bnds[i, 1]), :])


#
lat_area.to_csv(os.path.join(mg.path_out(), 'bylat_area.csv'))
