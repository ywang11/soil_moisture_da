# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 20:24:05 2019

@author: ywang254

Aggregate the biomes & plot
"""
import time
import glob
import os
import xarray as xr
import numpy as np
import warnings
import collections as co
import utils_management as mg
import matplotlib.pyplot as plt


start = time.time()

# MODIFY
res = 2.5 # 0.5 is already there. Just manually copy.
target_lon = np.arange(-180 + res/2, 180.1 - res/2, res)
target_lat = np.arange(-90 + res/2, 90.1 - res/2, res)


# Source data
data = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                    'Biomes.nc'))
data = data.transpose('lat', 'lon')
data = data.sortby('lat', ascending=True)
long_name = data.biomes.attrs['long_name']

# Separate into different categories
xr_data = xr.DataArray(data = np.full([len(data.lat), len(data.lon), 9], 
                                      0, dtype = int),
                       dims = ('lat', 'lon', 'class'), 
                       coords = {'lat': data.lat, 'lon': data.lon,
                                 'class': np.array(range(1, 10), dtype=int)},
                       attrs = co.OrderedDict({'src': 'Biomes.nc'}))
for i in range(1, 10):
    xr_data.loc[:, :, i] = np.where(np.abs(data.biomes - i) < 1e-6,
                                    1, 0)

data.close()

# Aggregate to res
xr_data_aggr = xr.DataArray(data = np.empty([len(target_lat), 
                                             len(target_lon), 9]),
                            dims = ('lat', 'lon', 'class'), 
                            coords = {'lat': target_lat, 
                                      'lon': target_lon,
                                      'class': np.array(range(9))},
                            attrs = co.OrderedDict({'src': 'Biomes.nc'}))

debug = False
if debug:
    warnings.filterwarnings("error")

for x in range(len(target_lat)):
    for y in range(len(target_lon)):
        scale = int(np.around(res / 0.5))
        x_to = x * scale
        y_to = y * scale
        xr_data_aggr[x, y, :] = np.nanmean( np.nanmean( \
            xr_data.values[x_to:(x_to+scale), y_to:(y_to+scale), : ],
            axis=0), axis=0 )

warnings.filterwarnings("default")

# Find the highest in each grid cell.
xr_data_aggr_max = xr.DataArray(data = np.argmax(xr_data_aggr.values, 
                                                 axis=2).astype(int),
                                dims = ('lat', 'lon'),
                                coords = {'lat': target_lat,
                                          'lon': target_lon},
                                attrs = co.OrderedDict({'src': 'Biomes.nc',
                                                        'long_name': \
                                                        long_name}))

# Fill cells that do not have highest values with NaN
xr_data_aggr_max.values = np.where(np.abs(np.sum(xr_data_aggr.values, 
                                                 axis = 2)) < 1e-6,
                                   -999, xr_data_aggr_max.values)

xr_data_aggr_max.to_dataset(name = 'biomes').to_netcdf( \
    path = os.path.join(mg.path_intrim_out(), 'da_masks', 
                        'Biomes_' + str(res) + '.nc'),
    encoding = {'biomes': {'_FillValue': -999, 'dtype': int}})

end = time.time()

print(end - start)

data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'da_masks', 
                                    'Biomes_' + str(res) + '.nc'))
plt.figure()
data.biomes.plot()
plt.savefig(os.path.join(mg.path_intrim_out(), 'da_masks', 
                         'Biomes_' + str(res) + '.png'))
data.close()
