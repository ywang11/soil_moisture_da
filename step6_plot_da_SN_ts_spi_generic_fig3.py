"""
20190910
ywang254@utk.edu

Plot the times series of the signal, and threshold of 
   likely (S/N > 0.95, detectable at 66% confidence)
   very likely (S/N > 1.64, 90% confidence)
   virtually certain (S/N > 2.57, 99% confidence).

The thresholds follow Marvel et al. 2019 DOI: 10.1038/s41586-019-1149-8

The EOF is flipped already in the fingerprint calculation step to ensure most
  are negative values.

Fix the last year of the signal to 2016. Vary the beginning year.
"""
import matplotlib.pyplot as plt
import pandas as pd
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median # depth_cm
from misc.plot_utils import plot_ts_shade, cmap_gen
import numpy as np
import cartopy.crs as ccrs
##from cartopy.mpl.gridliner import Gridliner
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil


cmap = mpl.cm.get_cmap('nipy_spectral')
prod_col = [cmap(8.5/9)] # [cmap(6.5/9), cmap(8.5/9)]
#prod_col = [cmap(2.5/9), cmap(4.5/9), cmap(6.5/9), cmap(8.5/9)]
hist_col = ['r', 'b', 'g']
mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list_list = {'bymonth': [str(i) for i in range(12)],
                    'byyear': ['annual_mean', 'annual_maximum',
                               'annual_minimum',
                               'growing_season_mean', 'growing_season_maximum',
                               'growing_season_minimum']}


#Setup / MODIFY
## 'mean_products', 'mean_noncmip', 'mean_lsm', 'dolce_lsm', 'em_lsm']
prod_list = ['mean_noncmip'] # , 'mean_products'] 
## 'Mean Products', 'Mean NonCMIP'
prod_name_list = ['Mean NonCMIP']
folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
season_name = 'bymonth'
base = 'historical'
expr_list = ['historical', 'NoNAT', 'hist-nat', 'hist-GHG', 'hist-aer', 'GHGAER']
expr_name = {'historical': 'ALL', 'NoNAT': 'ANT', 'hist-nat': 'NAT',
             'hist-GHG': 'GHG', 'hist-aer': 'AER', 'GHGAER': 'GHGAER'}
season_list = season_list_list[season_name]
season_list_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug',
                    'Sep','Oct','Nov','Dec']
lag = 3


first_year = 1971
L_range = range(10, 2016 - first_year + 2)
L_range_str = [str(ll) for ll in L_range]


for limit, dist, dcm in it.product([False, True], ['gmm','weibull'],
                                   depth_cm_new):
    if limit:
        prefix = 'limit_'
    else:
        prefix = ''

    flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                    prefix + 'flip_bylat' + suffix + '.csv'),
                       index_col = [0,1,2,3])

    fig, axes = plt.subplots(ncols = len(expr_list), nrows = len(season_list),
                             figsize = (5.6, 8), sharex = True, sharey = True)
    fig.subplots_adjust(wspace = 0., hspace = 0.)

    for bind, expr in enumerate(expr_list):
        for sind, season in enumerate(season_list):

            ###################################################################
            # Signal/Noise ratio time series
            ###################################################################
            ax = axes[sind, bind]

            noise_std = pd.read_csv(os.path.join(mg.path_out(),
                                                 'cmip6_spi_noise', folder,
                                                 base + '_eofs', prefix + \
                                                 dist + '_trend_' + dcm + \
                                                 '_' + str(lag) + '_' + \
                                                 season + '_' + res + \
                                                 suffix + '.csv'),
                                    index_col = 0).loc[:, L_range_str]
            noise_std = np.nanstd(noise_std.values, axis = 0)

            # CMIP6 signals
            signal_hist = pd.read_csv(os.path.join(mg.path_out(),
                'cmip6_spi_signal', folder, base + '_eofs', prefix + \
                dist + '_' + expr + '_signal_' + dcm + '_' + \
                str(lag) + '_' + season + '_' + res + suffix + '.csv'),
                                      header = [0,1] \
            ).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
            signal_hist.index = pd.to_datetime(signal_hist.index).year
            signal_hist.index.name = 'year'
            signal_hist = signal_hist.reorder_levels([1, 0], axis = 1)
            signal_hist = signal_hist.loc[first_year,
                                          :].unstack().loc[:, L_range_str]
            signal_hist = signal_hist * \
                flip.loc[(dcm, int(season), dist, lag), base] \
                / noise_std.reshape(1, -1)
            # ---- need to weight the different models
            temp_model = [x.split('_')[0] for x in signal_hist.index]
            signal_mean = signal_hist.groupby(temp_model,
                                              axis = 0).mean().mean(axis = 0)
            signal_std = signal_hist.std(axis = 0)

            # Products signals
            signal2 = pd.read_csv(os.path.join(mg.path_out(), 
                'products_spi_signal', folder, base + '_eofs', prefix + \
                dist + '_signal_' + dcm + '_' + str(lag) + '_' + \
                season + '_' + res + suffix + '.csv'), header = [0,1] \
            ).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
            signal2.index = pd.to_datetime(signal2.index).year
            signal2.index.name = 'year'
            signal2 = signal2.stack().loc[:, L_range_str]
            signal2 = signal2.loc[first_year, :].loc[prod_list, :]

            #print(signal2)
            signal2 = signal2 * flip.loc[(dcm, int(season), dist, lag), base] \
                / noise_std.reshape(1, -1)

            #breakpoint()

            h = [None] * (5 + len(prod_list))
            yr_list = np.array(L_range) + first_year - 1

            # CMIP6 models
            h[0], h[1] = plot_ts_shade(ax, yr_list, 
                                       ts = {'min': signal_mean.values - \
                                             1.96 * signal_std.values,
                                             'mean': signal_mean.values,
                                             'max': signal_mean.values + \
                                             1.96 * signal_std.values},
                                       ts_col = 'k', ln_main = '-',
                                       shade_col = 'k')

            #
            ##h[1] = ax.axhline(0.95, linestyle = '-', 
            ##                  color = 'b', linewidth = 0.5) #fd8d3c
            ##h[2] = ax.axhline(1.64, linestyle = '-',
            ##                  color = 'b', linewidth = 0.5) #a63603
            ##h[3] = ax.axhline(2.57, linestyle = '-',
            ##                  color = 'b', linewidth = 1.) #a63603
            h[2] = ax.axhline(1.96, linestyle = '-',
                              color = 'b', linewidth = 0.5)
            h[3] = ax.axhline(2.57, linestyle = '-',
                              color = 'b', linewidth = 1.)
            #ax.axhline(- 0.95, linestyle = '-', 
            #           color = 'b', linewidth = 0.5) #fd8d3c
            #ax.axhline(- 1.64, linestyle = '-', 
            #           color = 'b', linewidth = 0.5) #a63603
            #ax.axhline(- 2.57, linestyle = '-', 
            #           color = 'b', linewidth = 1.) #a63603
            ax.axhline(0, linestyle = ':', color = 'b', linewidth = 0.5)

            # Product
            for pind, prod in enumerate(prod_list):
                h[pind+4], = ax.plot(yr_list, signal2.loc[prod, :].values,
                                     linewidth = 1.5,
                                     color = prod_col[pind], zorder = 5)


            if sind == 0:
                ax.set_title('$f_2$ = ' + expr_name[expr])
            if bind == 0:
                ax.set_yticks([-3, 0, 3, 6])
                ax.set_ylabel(season_list_name[int(season)])
            else:
                ax.tick_params(axis = 'y', length = 0.)
            #ax.set_ylim([-6, 11])
            ax.set_xlim([yr_list[0], yr_list[-1]])
            ax.set_xticks([first_year + L_range[0] + 5,
                           first_year + (L_range[0] + L_range[-1]) // 2,
                           first_year + L_range[-1] - 5])
            ax.set_xticklabels([first_year + L_range[0] + 5,
                                first_year + (L_range[0] + L_range[-1]) // 2,
                                first_year + L_range[-1] - 5])
            ax.text(0.1, 0.85, lab[bind]+str(sind+1),
                    transform = ax.transAxes, weight = 'bold')

            ##break
        ##break
    # '66% Confidence', '90% Confidence',
    ax.legend(h, ['Forcing $f_2$', 'Gaussian 95% CI',
                  '95% Confidence interval', '99% Confidence interval'] + \
              prod_name_list, ncol = 3, loc = 'lower center',
              bbox_to_anchor = (-2.1, -1.15))
    fig.savefig(os.path.join(mg.path_out(), 'figures',
                             prefix + 'fig3_' + str(first_year) + \
                             '_' + dist + '_' + dcm + '_' + \
                             str(lag) + '_' + season_name + \
                             '_' + res + suffix + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
    ##break
