"""
20221004

Plot the PC of the AER fingerprint. No CI or anything. 

Use flip of the EOF in the signal projection step.
"""
import matplotlib.pyplot as plt
import pandas as pd
import xarray as xr
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median
from misc.da_utils import calc_signal_bylat
from misc.plot_utils import plot_ts_shade
import numpy as np
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil
import matplotlib.gridspec as gridspec
from scipy.stats import linregress


mpl.rcParams['font.size'] = 5.5
mpl.rcParams['axes.titleweight'] = 'normal'
mpl.rcParams['axes.titlesize'] = 5.5


res = '0.5'
season_list_list = {'bymonth': [str(i) for i in range(12)],
                    'byyear': ['annual_mean', 'annual_maximum','annual_minimum',
                               'growing_season_mean', 'growing_season_maximum', 'growing_season_minimum']}


folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
season_name = 'bymonth'
lag = 3
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


if suffix == '_noSahel':
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    ).loc[np.array(lat_median).astype(int), :].values[:, 1])
else:
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    ).loc[np.array(lat_median).astype(int), :].values[:, 0])


season_list = season_list_list[season_name]


month_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']


def plot_rep(ax, limit, dist, sind, expr):
    if limit:
        prefix = 'limit_'
    else:
        prefix = ''

    flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                    prefix + 'flip_bylat' + suffix + '.csv'),
                       index_col = [0,1,2,3])

    season = season_list[sind]

    if limit & (expr == 'piControl'):
        expr = expr + '_historical' # Use this one

    clist = ['#ff7f00', '#377eb8']
    h = [None, None]
    for i, dcm in enumerate(depth_cm_new):
        pc = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                      expr, 'bylat', prefix + dist + '_pc_' + dcm + '_' + \
                                      str(lag) + '_' + season + '_' + res + suffix + '.csv'),
                         index_col = 0, parse_dates = True)
        if expr != 'piControl':
            pc.index = pc.index.year
        if flip.loc[(dcm, sind, dist, lag), expr] < 0:
            pc.loc[:, ['Full', 'Jackknife Estimate','Jackknife CI_lower', 'Jackknife CI_upper']] = - pc.loc[:, ['Full', 'Jackknife Estimate','Jackknife CI_lower', 'Jackknife CI_upper']]
            temp = pc.loc[:, 'Jackknife CI_lower'].values.copy()
            pc.loc[:, 'Jackknife CI_lower'] = pc.loc[:, 'Jackknife CI_upper'].values
            pc.loc[:, 'Jackknife CI_upper'] = temp

        h[i], = ax.plot(pc.index, pc['Full'].values, '--', color = clist[i], lw = 0.5)

        reg = linregress(np.arange(len(pc['Full'].values)), pc['Full'].values)
        slope = '%.3f' % reg.slope
        if reg.pvalue <= 0.05:
            fd = {'fontweight': 'bold'}
            color = clist[i]
        else:
            fd = {'fontweight': 'normal'}
            color = clist[i]
        ax.text(0.05, 0.9 - i * 0.15, f'――― {dcm} r = {slope}', color = color, fontdict = fd, transform = ax.transAxes)

        if expr == 'historical':
            ax.set_xticks(np.arange(1975, 2100, 25))
        else:
            ax.set_xticks(np.arange(1975, 2020, 10))
        ax.set_xlim([1971, pc.index.values[-1]])
        if sind < 8:
            ax.tick_params('x', length = 0)
        #    ax.set_xticklabels([])
    ax.set_title(month_name[sind])
    ax.text(0.02, 1.035, lab[sind], transform = ax.transAxes, weight = 'bold')
    if expr == 'historical':
        ax.set_ylim([-0.7, 4.7])
    else:
        ax.set_ylim([-0.6, 1.])

    return h


limit = False
dist = 'gmm'
expr = 'historical' # 'hist-aer'
if limit:
    prefix = 'limit_'
else:
    prefix = ''


fig, axes = plt.subplots(4, 3, figsize = (6.5, 5.5), sharex = True, sharey = True)
for sind in range(len(season_list)):
    ax = axes.flat[sind]
    plot_rep(ax, limit, dist, sind, expr)
fig.savefig(os.path.join(mg.path_out(), 'figures', prefix + 'fig2S' + expr + '_' + dist + \
                         '_pc_' + str(lag) + '_' + season_name + '_' + res + suffix + '.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
