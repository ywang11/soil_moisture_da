"""
2019/10/07
ywang254@utk.edu

Calculate the 
 : by-latitude (in 10-degrees, -50 to 70)
 land-area weighted mean soil moisture of the following LSM/merged products
 : Concatenated DOLCE LSM product
 : DOLCE-CMIP5 & DOLCE-CMIP6
 : EM-LSM & EM-CMIP5 & EM-CMIP6
 : Median of the 6 different products
 : Mean of all the source raw products
"""
from utils_management.constants import depth_cm_new
import utils_management as mg
from misc.analyze_utils import by_continent_avg, by_biome_avg, by_AI_avg, \
    by_lat_avg, by_subcontinent_avg, get_ndvi_mask
from misc.cmip6_utils import mrsol_availability
from glob import glob
import os
import xarray as xr
import pandas as pd
import multiprocessing as mp
import numpy as np
import itertools as it


# Build the list of products at this resolution.
res = '0.5'
land_mask = 'vanilla'
flist = []
for dcm in depth_cm_new:
    flist.append('mean_lsm_' + dcm)
    flist.append('dolce_lsm_' + dcm)
    flist.append('em_lsm_' + dcm)
    flist.append('em_cmip5_' + dcm)
    flist.append('em_cmip6_' + dcm)
    flist.append('em_2cmip_' + dcm)
    flist.append('em_all_' + dcm)
    flist.append('mean_noncmip_' + dcm)
    flist.append('mean_products_' + dcm)

season_list = [str(m) for m in range(12)]
ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])

noSahel = True
if noSahel:
    ndvi_mask = get_ndvi_mask(land_mask)
    suffix = '_noSahel'
else:
    suffix = ''

for dist, l, season, lag in it.product(['gmm', 'weibull'], flist,
                                       season_list, [3]): #  [1,3,6]):
    fn = os.path.join(mg.path_intrim_out(), 'SPI_Products', 
                      'get_' + ref_period_str, dist,
                      l + '_' + str(lag) + '_' + res + '_' + season + '.nc')

    data = xr.open_dataset(fn, decode_times = False)
    period = pd.date_range(start = '1951-01-01', end = '2016-12-31', freq = 'YS')
    if noSahel:
        spi = data.spi.where(ndvi_mask).values.copy()
    else:
        spi = data.spi.values.copy()
    data.close()

    prefix = os.path.join(mg.path_out(), 'products_spi_summary', dist,
                          l + '_' + season + '_' + str(lag))

    #by_continent_avg(spi, period, prefix, res)
    #by_subcontinent_avg(spi, period,
    #                    data.lat.values, data.lon.values, prefix, res)
    #by_biome_avg(spi, period, prefix, res)
    #by_AI_avg(spi, period, prefix, res)
    by_lat_avg(spi, period, data.lat.values, data.lon.values, prefix,
               res, suffix)

#    return None


#pool = mp.Pool(4) ## mp.Pool(mp.cpu_count()-1)
#pool.map_async(aggregate, list(it.product(flist, season_list, [3,6,12])))
#pool.close()
#pool.join()
