"""
20210728

Trends in the historical/hist-aer/hist-GHG simulations of SSI. 
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, target_lat, target_lon
import itertools as it
from misc.cmip6_utils import one_layer_availability, mrsol_availability
from misc.analyze_utils import calc_reg, get_ndvi_mask
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
import sys
from map_cmip6_drivers_trend import get_model_list
import dask.array as dsa


land_mask = 'vanilla'
res = '0.5'
res_to = '5'
lag = 3
noSahel = True
dcm = depth_cm_new[REPLACE1]
expr = 'REPLACE2'


if noSahel:
    ndvi_mask = get_ndvi_mask(land_mask)
    suffix = '_noSahel'
else:
    suffix = ''
if expr == 'historical':
    a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
    b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
    cmip6_list = sorted(set(a) & set(b))
else:
    cmip6_list, _ = mrsol_availability(dcm, land_mask, expr, res)
cmip6_list = sorted(cmip6_list)
try:
    model = cmip6_list[REPLACE3]
except IndexError:
    sys.exit('Not so many models.')


def read_expr(dcm, model, expr, season):
    if expr == 'historical':
        data_time = pd.date_range(start = '1951-01-01',
                                  end = '2100-12-31', freq = 'YS')
    else:
        data_time = pd.date_range(start = '1951-01-01',
                                  end = '2020-01-01', freq = 'YS')
    fn = os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                      'get_historical_1970-2014', 'gmm', expr,
                      model + '_' + dcm + '_' + str(lag) + '_' + res + \
                      '_' + season + '.nc')
    hr = xr.open_dataset(fn, decode_times = False)
    if noSahel:
        spi = hr.spi.where(ndvi_mask).copy(deep = True)
    else:
        spi = hr.spi.copy(deep = True)
    spi['time'] = data_time
    hr.close()
    return spi


def nanmean(x):
    if sum(np.isnan(x.reshape(-1))) < (len(x.reshape(-1)) * 0.95):
        return np.array([[[np.nanmean(x)]]])
    else:
        return np.array([[[np.nan]]])


def interp(var):
    """ To 5 degrees. """
    da = dsa.from_array(var.values, chunks = (1, 10, 10))
    da2 = da.map_blocks(lambda x: nanmean(x), dtype = np.float64,
                        chunks = (1,1,1)).compute()
    var2 = xr.DataArray(da2, dims = ['time', 'lat', 'lon'],
                        coords = {'time': var['time'], 'lon': target_lon[res_to],
                                  'lat': target_lat[res_to]}).load()
    return var2


# calculate trend
g_map_trend = xr.DataArray(np.full([12, len(target_lat[res_to]),
                                    len(target_lon[res_to])], np.nan),
                           dims = ['month', 'lat', 'lon'],
                           coords = {'month': range(12),
                                     'lat': target_lat[res_to],
                                     'lon': target_lon[res_to]})
g_p_values = xr.DataArray(np.full([12, len(target_lat[res_to]),
                                    len(target_lon[res_to])], np.nan),
                           dims = ['month', 'lat', 'lon'],
                           coords = {'month': range(12),
                                     'lat': target_lat[res_to],
                                     'lon': target_lon[res_to]})
g_intercepts = xr.DataArray(np.full([12, len(target_lat[res_to]),
                                    len(target_lon[res_to])], np.nan),
                           dims = ['month', 'lat', 'lon'],
                           coords = {'month': range(12),
                                     'lat': target_lat[res_to],
                                     'lon': target_lon[res_to]})
for season in [str(ii) for ii in range(12)]:
    spi = interp(read_expr(dcm, model, expr, season))
    spi = spi[(spi['time'].to_index().year >= 1971) & \
              (spi['time'].to_index().year <= 2017), :, :]
    temp = xr.apply_ufunc(calc_reg, spi.chunk({'time':-1}),
                          input_core_dims = [['time']], vectorize = True,
                          dask = 'parallelized', output_core_dims = [['new']],
                          dask_gufunc_kwargs = {'output_sizes': {'new': 3}})
    temp.compute()
    g_map_trend[int(season), :, :] = temp[:,:,1]
    g_p_values[int(season), :, :] = temp[:,:,2]
    g_intercepts[int(season), :, :] = temp[:,:,0]
    print(season)
ds = xr.Dataset({'g_map_trend': g_map_trend,
                 'g_p_values': g_p_values,
                 'g_intercepts': g_intercepts})
ds['lon'].encoding['_FillValue'] = None
ds['lat'].encoding['_FillValue'] = None
ds['month'].encoding['_FillValue'] = None
ds.to_netcdf(os.path.join(mg.path_out(), 'cmip6_spi_signal', 'bygrid',
                          'gmm_' + expr + '_' + model + '_' + res_to + \
                          '_' + dcm + '_' + str(lag) + suffix + '.nc'))
