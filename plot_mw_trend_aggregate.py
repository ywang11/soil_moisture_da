"""
2019/10/12

Plot the 30-year moving window linear trend in latitudinal mean soil moisture
 of different historical experiments: 
 : historical (concatenated by ssp585), hist-aer, hist-GHG, and hist-nat, 
 and with the following dataset for comparison:
 : dolce_lsm, dolce_cmip5, dolce_cmip6, em_lsm, em_cmip5, em_cmip6, 
   median of products

Following the practice of previous papers, subtract mean (1980-2010, for
 compatibility across ddatasets), and normalize by standard deviation.
"""
import matplotlib.pyplot as plt
import pandas as pd
import os
import numpy as np
import utils_management as mg
from matplotlib import cm
from utils_management.constants import depth, depth_cm, style, region, \
    region_names
import matplotlib as mpl
import itertools as it
import multiprocessing as mp


# MODIFY
res = '0.5' # '2.5', '5', '10'
# Number of years in the moving window to calculate trend over.
n_year = 50


def plotter(option):
    i, season = option
    d = depth[i]
    dcm = depth_cm[i]

    # Plot parameters
    expr = 'historical' # , 'hist-aer', 'hist-GHG', 'hist-nat']
    mpl.rcParams['font.size'] = 6
    mpl.rcParams['axes.titleweight'] = 'normal'
    ##mpl.rcParams['text.usetex'] = True
    cmap = cm.get_cmap('Spectral', 12)
    product_color = [cmap((i+0.5)/7) for i in range(7)]
    expr_color = ['k', '#525252', '#08306b', '#2171b5']
    
    # Collect the individual products
    product = ['dolce_lsm', 'dolce_cmip5', 'dolce_cmip6', 'em_lsm', 'em_cmip5',
               'em_cmip6', 'median_products']
    
    
    # Make the plots
    data_product = pd.read_csv(os.path.join(mg.path_out(), 
                                            'aggregate_ts_trend',
                                            'bylat_product_' + d + '_' + \
                                            res + '_' + season + '_L=' + \
                                            str(n_year) + '.csv'), 
                               index_col = 0, header = [0,1])
    data_cmip6 = pd.read_csv(os.path.join(mg.path_out(), 'aggregate_ts_trend',
                                          'bylat_' + expr + '_cmip6_' + d + \
                                          '_' + res + '_' + season + '_L=' + \
                                          str(n_year) + '.csv'), 
                             index_col = 0, header = [0,1])
    
    s = 'lat'
    fig = plt.figure(figsize = (12, 12))
    axes = np.empty([len(region[s]), 2], dtype = object)
    for r_ind in range(len(region[s])):
        if r_ind == 0:
            axes[r_ind, 0] = plt.subplot(len(region[s]), 2, r_ind*2+1)
        else:
            axes[r_ind, 0] = plt.subplot(len(region[s]), 2, r_ind*2+1,
                                         sharex = axes[0,0])
        axes[r_ind, 1] = plt.subplot(len(region[s]), 2, r_ind*2+2,
                                     sharex = axes[0,0], 
                                     sharey = axes[r_ind,0])
    fig.subplots_adjust(wspace = 0.2, hspace = 0.1)
    for r_ind, r in enumerate(region[s]):
        # (1) Plot the individual products
        h = [None] * len(product)
        ax = axes[r_ind, 0]
    
        for p_ind, p in enumerate(product):
            h[p_ind], = ax.plot(data_product.index, 
                                data_product.loc[:, (p, r)].values,
                                color = product_color[p_ind])
        ##ax.set_title(region_names[s][r_ind])
        ax.set_ylabel(region_names[s][r_ind], multialignment='center')
        ##ax.set_yticklabels(ax.get_yticks())

        # (2) Collect and plot the shaded region of CMIP6
        ax = axes[r_ind, 1]
    
        # ---- display quartiles because min/max have outliers and the
        #      range is too large compared to the range/trend in the mean
        h2 = ax.fill_between(data_cmip6.index,
                             np.percentile(data_cmip6.loc[:, (slice(None),
                                                              r)].values, 
                                           25, axis = 1),
                             np.percentile(data_cmip6.loc[:, (slice(None),
                                                              r)].values,
                                           75, axis = 1),
                             color = expr_color[0], alpha = 0.5)
        ##ax.set_yticklabels([])
        ax.plot(data_cmip6.index, 
                data_cmip6.loc[:, (slice(None), r)].mean(axis = 1),
                color = expr_color[0], linewidth = 2)
    ax.legend(h + [h2], product + [expr], ncol = 4, # len(product),
              bbox_to_anchor = (0., -0.7), loc = 'lower center')
    fig.savefig(os.path.join(mg.path_out(), 'plot_trend_aggregate',
                             'L=' + str(n_year) + '_' + season + '_' + \
                             d + '_' + s + '_' + res + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)


pool = mp.Pool(4)
pool.map(plotter, it.product(range(4),
                             ['annual', 'DJF', 'MAM', 'JJA', 'SON']))
pool.close()
pool.join()
