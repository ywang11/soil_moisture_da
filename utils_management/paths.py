# -*- coding: utf-8 -*-
"""
Created on Sat Dec  8 12:13:38 2018

@author: ywang254

Return the root path to all data and output files, and other paths within the
root path.
"""
import os
import sys
import numpy as np
from glob import glob
from .constants import cmip6_list


def path_root():
    if sys.platform == 'win32':
        path = 'D:/Projects/2018 Soil Moisture'
    elif sys.platform == 'linux':
        if 'cori' in os.environ['HOSTNAME']:
            path = os.path.join(os.environ['SCRATCH'], 'Soil_Moisture')
        else:
            #path = '/lustre/haven/user/ywang254/Soil_Moisture'
            path = os.path.join(os.environ['PROJDIR'], 'Soil_Moisture')
    return path

def path_data():
    return os.path.join(path_root(), 'data')

def path_intrim_out():
    return os.path.join(path_root(), 'intermediate')

def path_out():
    return os.path.join(path_root(), 'output_da')

def path_merge():
    return os.path.join(path_root(), 'output_products')


def path_to_cmip6(expr, var):
    """
    Directory to all the existing directories that have CMIP6 data. 
    """
    cmip6 = cmip6_list(expr, var)

    model = np.unique([x.split('_')[0] for x in cmip6])
    model_path = {}
    for m in model:
        model_path[m] = {}

        # get unique version numbers
        # '_' to prevent models whose names is contained in another model.
        version_number = [x.split('_')[1] for x in \
                          [y for y in cmip6 if m+'_' in y]]
        for v in version_number:
            model_path[m][v] = glob(os.path.join(path_data(), 'CMIP6', expr,
                                                 var, m, var + '*' + m + \
                                                 '_' + expr + '_' + v + \
                                                 '*.nc'))
    return model_path


def path_to_srex():
    if sys.platform == 'linux':
        return '/nics/c/home/ywang254/Git/IPCC-SREX_regions_netCDF/SREX_regions_mask_0.5x0.5.nc'
    else:
        return 'C:/Users/ywang254/Documents/Git/IPCC-SREX_regions_netCDF/SREX_regions_mask_0.5x0.5.nc'
