"""
20190730
ywang254@utk.edu
"""
import os
import sys
from glob import glob
import numpy as np


"""
def build_cori_path(doc):
    ##Construct the path to CMIP6 soil moisture files using the json dictionary
    ##of one file from esgf search. Works on cori path structure.
    path_cmip6 = '/global/cscratch1/sd/cmip6/CMIP6/'
    file_folder = os.path.join(path_cmip6, doc['activity_id'][0],
                               doc['institution_id'][0], doc['source_id'][0],
                               doc['experiment_id'][0], doc['member_id'][0],
                               doc['table_id'][0], doc['variable'][0],
                               doc['grid_label'][0], 'v' + doc['version'])
    return file_folder
"""


def mrsol_availability(dcm, land_mask, expr, res):
    """
    Find the availability of interpolated soil moisture at given depth, 
    return the list of ModelName_EnsembleID.
    """
    if sys.platform == 'win32':
        path = 'D:/Projects/2018 Soil Moisture'
    elif sys.platform == 'linux':
        if 'cori' in os.environ['HOSTNAME']:
            path = os.path.join(os.environ['SCRATCH'], 'Soil_Moisture')
        else:
            path = os.path.join(os.environ['PROJDIR'], 'Soil_Moisture')

    if (expr == 'NoNAT') | (expr == 'OTH') | (expr == 'GHGAER'):
        if expr == 'NoNAT':
            ee_list = ['historical', 'ssp585', 'hist-nat']
        elif expr == 'GHGAER':
            ee_list = ['hist-GHG', 'hist-aer']
        else:
            ee_list = ['historical', 'ssp585', 'hist-GHG', 'hist-aer', 'hist-nat']

        for eind, ee in enumerate(ee_list):
            # Model name + ensemble member name
            folders_ee = os.listdir(os.path.join(path, 'intermediate',
                                                 'Interp_DA', land_mask,
                                                 'CMIP6', ee))
            if eind == 0:
                #models_ee = set([x.split('_')[0] for x in folders_ee])
                models_ee = set(folders_ee)
            else:
                #models_ee = models_ee & \
                #    set([x.split('_')[0] for x in folders_ee])
                models_ee = models_ee & set(folders_ee)
        folder2 = models_ee

#       folder2 = []
#       for mod in models_ee:            
#           n = 1
#           for ee in ee_list:
#               count = 0
#               f_list = glob(os.path.join(path, 'intermediate',
#                                          'Interp_DA', land_mask,
#                                          'CMIP6', ee, mod + '_*'))
#               if expr == 'NoNAT':
#                   if ee == 'ssp585':
#                       continue
#                   elif ee == 'historical':
#                       f_list2 = glob(os.path.join(path, 'intermediate',
#                                                   'Interp_DA', land_mask,
#                                                   'CMIP6', 'ssp585',
#                                                   mod + '_*'))
#                       f_list = [x for x in f_list if \
#                                 os.path.join(path, 'intermediate',
#                                              'Interp_DA', land_mask,
#                                              'CMIP6', 'ssp585',
#                                              x.split('/')[-1]) in f_list2]
#               for f in f_list:
#                   if dcm == '0-100cm':
#                       if (len(glob(os.path.join(f, 'mrsol_' + res + '_*_' + \
#                                                 '0-10cm.nc'))) > 0) & \
#                          (len(glob(os.path.join(f, 'mrsol_' + res + '_*_' + \
#                                                 '10-30cm.nc'))) > 0) & \
#                          (len(glob(os.path.join(f, 'mrsol_' + res + '_*_' + \
#                                                 '30-50cm.nc'))) > 0) & \
#                          (len(glob(os.path.join(f, 'mrsol_' + res + '_*_' + \
#                                                 '50-100cm.nc'))) > 0):
#                           count += 1
#                   else:
#                       if len(glob(os.path.join(f, 'mrsol_' + res + '_*_' + \
#                                                dcm + '.nc'))) > 0:
#                           count += 1
#               n = n * count
#           folder2 += [mod + '_r' + str(ni+1) + 'i1p1f1' for ni in range(n)]
        folder2_path = None
    else:
        folders = os.listdir(os.path.join(path, 'intermediate', 'Interp_DA',
                                          land_mask, 'CMIP6', expr))
        folder2 = []
        folder2_path = {}
        for f in folders:
            f_list = glob(os.path.join(path, 'intermediate', 'Interp_DA',
                                       land_mask, 'CMIP6', expr, f,
                                       'mrsol_' + res + '_*_' + dcm + \
                                       '.nc'))
            if len(f_list) > 0:
                folder2.append(f)
                folder2_path[f] = f_list
    return folder2, folder2_path


def one_layer_availability(var, land_mask, expr, res):
    """
    Find the availability of one-layer variables (pr, tas, evspsbl, mrsos)
    return the list of ModelName_EnsembleID.
    """
    if sys.platform == 'win32':
        path = 'D:/Projects/2018 Soil Moisture'
    elif sys.platform == 'linux':
        if 'cori' in os.environ['HOSTNAME']:
            path = os.path.join(os.environ['SCRATCH'], 'Soil_Moisture')
        else:
            path = os.path.join(os.environ['PROJDIR'], 'Soil_Moisture')

    folders = os.listdir(os.path.join(path, 'intermediate', 'Interp_DA',
                                      land_mask, 'CMIP6', expr))
    folder2 = []
    folder2_path = {}
    for f in folders:
        f_list = glob(os.path.join(path, 'intermediate', 'Interp_DA',
                                   land_mask, 'CMIP6', expr, f, var + '_' + \
                                   res + '_*.nc'))
        if ((expr == 'hist-GHG') | (expr == 'hist-aer')) and \
           (('CESM2' in f) | ('GISS-E2-1-G' in f) | ('NorESM2-LM' in f)):
            continue # skip these because ends in 2014 instead of 2020
                     # NorESM2-LM was because 2015 starts with Feb instead of Jan
        if len(f_list) > 0:
            folder2.append(f)
            folder2_path[f] = f_list
    return folder2, folder2_path
