from .analyze_utils import get_lat_avg
from .analyze_utils import calc_seasonal_trend0, calc_ts_trend
import numpy as np
import sys
import os
import xarray as xr
import pandas as pd
from glob import glob


if sys.platform == 'linux':
    path_mask = '/lustre/haven/proj/UTK0134/Soil_Moisture/data/' + \
                'Global_Masks/elm_half_degree_grid_negLon.nc'
    path_intrim = '/lustre/haven/proj/UTK0134/Soil_Moisture/intermediate'
    path_srex = '/nics/c/home/ywang254/Git/IPCC-SREX_regions_netCDF/SREX_regions_mask_0.5x0.5.nc'
else:
    path_mask = 'D:/Projects/2018 Soil Moisture/data/' + \
                'Global_Masks/elm_half_degree_grid_negLon.nc'
    path_intrim = 'D:/Projects/2018 Soil Moisture/intermediate'
    path_srex = 'C:/Users/ywang254/Documents/Git/IPCC-SREX_regions_netCDF/SREX_regions_mask_0.5x0.5.nc'


def standard_diagnostics(array, time, lat, lon, path_out, prefix):
    # Assumed dimension of array: time, lat, lon.
    #
    # Computed results: 
    # 1. Global maps (averaged over time, and by season over time).
    # 2. Global trends (all months, annual mean, seasonal mean).
    #
    # Note: this is not the same as the soil_moisture_merge because
    # (1) lat_bnds are 5-degree instead of 0.5-degree
    # (2) climatological period is 1981-2010
    # (3) trend period is rather 1967-2016, i.e. L = 50 years

    # Identical with ../utils_management/constants.py
    lat_bnds = np.concatenate([np.arange(-52.5, 67.6, 5).reshape(-1,1),
                               np.arange(-47.5, 72.6, 5).reshape(-1,1)],
                              axis = 1)
    lat_median = [('%d' % x) for x in np.arange(-50, 71, 5)]

    #standard_period = pd.date_range('1981-01-01', '2010-12-31', freq = 'MS')
    #standard_period2 = pd.date_range('1951-01-01', '2016-12-31', freq = 'MS')
    standard_period = time
    standard_period2 = time

    # km^2 of land in each grid cell.
    data = xr.open_dataset(path_mask)
    land_area = data.area.values * data.landfrac.values
    data.close()

    # ---- pre-multiply because this will be used multiple times
    array2 = np.empty_like(array)
    for t in range(array.shape[0]):
        array2[t,:,:] = array[t,:,:] * land_area

    # ---- make sure that the ocean grid cells match between array2 and
    #      land_area
    #      ESA-CCI has "land area" that varies over time because it has
    #      different missing values. Therefore, makd land area a time-
    #      varying variable.
    land_area = np.tile(land_area, (array2.shape[0], 1, 1))
    for la in range(land_area.shape[0]):
        land_area[la,:,:] = np.where(np.isnan(array2[la,:,:]), np.nan, 
                                     land_area[la,:,:])

    array3 = array[(time >= standard_period[0]) & \
                   (time <= standard_period[-1]), :, :]

    # 1. Climatology of the standard time period.
    g_map = np.nanmean(array3, axis=0)
    # ---- remove grids with insufficient data.
    g_map[np.sum(~np.isnan(array3), axis = 0) < 3] = np.nan

    xr.DataArray(data=g_map, dims=['lat', 'lon'],
                 coords={'lat':lat, 'lon':lon}).to_dataset(name='g_map' \
                 ).to_netcdf(os.path.join(path_out, prefix + \
                                          '_g_map_annual.nc'))

    for s1, s2 in zip(['DJF', 'MAM', 'JJA', 'SON'], 
                      [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]):
        temp = np.logical_or( np.logical_or(standard_period.month == s2[0],
                                            standard_period.month == s2[1]),
                              standard_period.month == s2[2] )

        temp4 = array3[temp, :, :]
        g_map_season = np.nanmean(temp4, axis=0)
        # ---- remove grids with insufficient data
        g_map_season[np.sum(~np.isnan(temp4), axis = 0) < 3] = np.nan

        xr.DataArray(data=g_map_season, dims=['lat', 'lon'],
                     coords={'lat':lat, 'lon':lon} \
        ).to_dataset(name='g_map' \
        ).to_netcdf(os.path.join(path_out, prefix + '_g_map_' + s1 + '.nc'))

    # 2. Trend of the input time period.
    array4 = array[(time >= standard_period2[0]) & \
                   (time <= standard_period2[-1]), :, :]
    sm_data = xr.DataArray(array4, coords={'time':standard_period2,
                                           'lat':lat, 'lon':lon},
                           dims = ['time', 'lat', 'lon'])
    for s in ['All', 'Annual', 'DJF', 'MAM', 'JJA', 'SON']:
        g_map_trend, g_p_values, g_intercepts = \
            calc_seasonal_trend0(sm_data, s)
        xr.Dataset({'g_map_trend': (['lat','lon'], g_map_trend),
                    'g_p_values': (['lat','lon'], g_p_values), 
                    'g_intercepts': (['lat','lon'], g_intercepts)},
                   coords = {'lon':lon, 'lat':lat} \
        ).to_netcdf(os.path.join(path_out, prefix + '_g_map_trend_' + \
                                 s +'.nc'))


def get_bylat_trend(fname, time_range):
    """
    Given prefix to the standard diagnostics by-latitude time series,
    calculate the trend over time_range for each latitude band.

    The original time series is per month. Therefore, just one month.
    """
    data = pd.read_csv(fname, index_col = 0, parse_dates = True)
    data = data.loc[time_range, :] # Subset to the required time range.

    trend = pd.Series(data = np.nan, index = data.columns)
    trend_p = pd.Series(data = np.nan, index = data.columns)

    for lat in data.columns:
        ts = data.loc[:, lat]

        slope, intercept, p_value = \
            calc_ts_trend(range(len(ts.values)), ts.values)

        trend.loc[lat] = slope
        trend_p.loc[lat] = p_value

    return trend, trend_p


def get_bylat_std(fname, time_range):
    data = pd.read_csv(fname, index_col = 0, parse_dates = True)
    data = data.loc[time_range, :] # Subset to the required time range.
    return data.std(axis = 0), data.mean(axis = 0)


def seasonal_avg(pd_series):
    """
    Calculate the seasonal average of a pandas data series.
    """
    result = {}
    result['annual'] = pd_series.groupby(pd_series.index.year).mean()

    temp = pd_series.index.to_period(freq = 'Q-NOV')
    temp2 = pd_series.groupby(temp).mean()

    # Assuming the time series starts from Jan and ends in Dec: 
    # set the seasonal average of 2 months to NaN, and remove the
    # last season (only contains a December)
    temp2.iloc[0] = np.nan
    temp2 = temp2.iloc[:-1]

    for qtr, season in enumerate(['DJF', 'MAM', 'JJA', 'SON'], 1):
        result[season] = temp2.loc[temp2.index.quarter == qtr]
        result[season].index = result[season].index.year

    return pd.DataFrame(result)


def seasonal_avg2(pd_frame):
    """
    Calculate the seasonal average of a pandas data frame.
    """
    result = {}
    result['annual'] = pd_frame.groupby(pd_frame.index.year).mean()

    temp = pd_frame.index.to_period(freq = 'Q-NOV')
    temp2 = pd_frame.groupby(temp).mean()

    # Assuming the time series starts from Jan and ends in Dec: 
    # set the seasonal average of 2 months to NaN, and remove the
    # last season (only contains a December)
    temp2.iloc[0] = np.nan
    temp2 = temp2.iloc[:-1]

    for qtr, season in enumerate(['DJF', 'MAM', 'JJA', 'SON'], 1):
        result[season] = temp2.loc[temp2.index.quarter == qtr]
        result[season].index = result[season].index.year

    return result
