import numpy as np
#from sklearn.model_selection import GridSearchCV
#from sklearn.neighbors import KernelDensity
#import statsmodels.api as stats
#from scipy.stats import gaussian_kde
#from KDEpy import FFTKDE
from fastkde import fastKDE
import itertools as it
from scipy.stats import norm, gamma, exponweib
from sklearn.mixture import GaussianMixture
#import pickle
import multiprocessing as mp
from scipy.interpolate import interp1d


def rolling_mean(data_array, s, month = None):
    # s = size of moving window; leave leading NaN's.
    if len(data_array.shape) == 3:
        if type(month) == type(None):
            data_new = np.full(data_array.shape, np.nan)
            for i in range(s, data_array.shape[0] + 1):
                data_new[i-1, :] = np.mean(data_array[(i-s):i, :, :],
                                           axis = 0)
        elif isinstance(month, int):
            data_new = np.full((round(data_array.shape[0] / 12),
                                data_array.shape[1], data_array.shape[2]),
                               np.nan)

            data_array_extend = np.concatenate([np.full((s-1,
                                                         data_array.shape[1],
                                                         data_array.shape[2]),
                                                        np.nan), data_array],
                                               axis = 0)
            for i_ind, i in enumerate(range(month, 
                                            data_array_extend.shape[0] - \
                                            s + 1, 12)):
                data_new[i_ind, :, :] = np.mean(data_array_extend[i:(i+s),
                                                                  :, :],
                                                axis = 0)
        else:
            raise Exception('Unrecognized month format')
    else:
        if type(month) == type(None):
            data_new = np.full(data_array.shape, np.nan)
            for i in range(s, data_array.shape[0] + 1):
                data_new[i-1, :] = np.mean(data_array[(i-s):i, :], axis = 0)
        elif isinstance(month, int):
            data_new = np.full((round(data_array.shape[0] / 12),
                                data_array.shape[1]), np.nan)

            data_array_extend = np.concatenate([np.full((s-1,
                                                         data_array.shape[1]),
                                                        np.nan), data_array],
                                               axis = 0)
            for i_ind, i in enumerate(range(month, 
                                            data_array_extend.shape[0] - \
                                            s + 1, 12)):
                data_new[i_ind, :] = np.mean(data_array_extend[i:(i+s), :],
                                             axis = 0)
        else:
            raise Exception('Unrecognized month format')

    return data_new


#def fit_samples(vector, order):    
#    vector = vector.reshape(-1) # array split preserved 2-D    
#    if np.std(vector) < 1e-6:
#        return (np.array([np.nan, np.nan]).reshape(1, 2), order)
#    else:
#        vector = vector[~np.isnan(vector)]
#        pdf, coord = fastKDE.pdf(vector, doApproximateECF = False,
#                                 numPointsPerSigma = 20)
#
#        """
#        Some processing on the pdf & coord to make later calculation of
#        z-score easier. Save the CDF, instead of pdf.
#        """
#        # Ensure that the pdf >= 0. and coords are between 0 and 1.
#        temp = (pdf >= 0.) & (coord > 0.) & (coord < 1.)
#        pdf = pdf[temp]
#        coord = coord[temp]
#
#        # Remove leading and trailing zeros from the pdf, because need to
#        # ensure that soil moisture values in (0, 1) cannot map to
#        # quantile = 0 or 1.
#        nonzero = np.where(pdf > 0.)[0]
#        start = nonzero.min()
#        end = nonzero.max() + 1
#        pdf = pdf[start:end]
#        coord = coord[start:end]
#
#        # Add one leading and one training zero to the pdf.
#        coord = np.insert(np.insert(coord, 0, 0.), len(coord) + 1, 1.)
#        pdf = np.insert(np.insert(pdf, 0, 0.), len(pdf) + 1, 0.)
#
#        # Normalize the trapezoid integration on [0, 1] to 1.
#        tot = 0.5 * (pdf[:-1] + pdf[1:]) * (coord[1:] - coord[:-1])
#        pdf = pdf / np.sum(tot)
#        tot2 = 0.5 * (pdf[:-1] + pdf[1:]) * (coord[1:] - coord[:-1])
#
#        #
#        cdf = np.insert(np.cumsum(tot2), 0, 0.)
#
#        return (np.concatenate([coord.reshape(-1, 1), cdf.reshape(-1, 1)],
#                               axis = 1), order)
def fit_samples(vector, dist, order):
    vector = vector.reshape(-1) # array split preserved 2-D
    if np.std(vector) < 1e-6:
        # Set np.nan flags to later use uniform distribution on [0,1]
        if dist == 'gamma':
            return ({'shape': np.nan, 'loc': np.nanmean(vector),
                     'scale': np.nan, 'll': np.nan}, order)
        elif dist == 'weibull':
            return ({'a': np.nan, 'shape': np.nan, 'loc': np.nanmean(vector),
                     'scale': np.nan, 'll': np.nan}, order)
        elif dist == 'gmm':
            return ({'weights': np.array([np.nan,np.nan]),
                     'means': np.array([np.nanmean(vector),
                                        np.nanmean(vector)]),
                     'sd': np.array([np.nan,np.nan]), 'll': np.nan}, order)
    else:
        vector = vector[~np.isnan(vector)]
        if dist == 'gamma':
            shape, loc, scale = gamma.fit(vector)
            # calculate log-likelihood
            ll = sum(np.log(gamma.pdf(vector, a=shape, loc=loc, scale=scale)))
            return ({'shape': shape, 'loc': loc, 'scale': scale,
                     'll': ll}, order)
        elif dist == 'weibull':
            ain = 1
            locin = 0
            aout, shape, locout, scale = exponweib.fit(vector, floc=locin,
                                                       f0=ain)
            # calculate log-likelihood
            ll = sum(np.log(exponweib.pdf(vector, a=aout, c=shape, loc=locout,
                                          scale = scale)))
            return ({'a': aout, 'shape': shape, 'loc': locout, 'scale': scale,
                     'll': ll}, order)
        elif dist == 'gmm':
            vector = vector.reshape(-1, 1)
            ##print(vector)
            mod = GaussianMixture(n_components = 2, random_state = 999)
            mod.fit(vector)
            # calculate log-likelihood
            ll = sum(mod.score_samples(vector))
            ##print('gmm')
            ##print({'weights': mod.weights_, 'means': mod.means_,
            ##       'sd': np.sqrt(mod.covariances_), 'll': ll})
            return ({'weights': mod.weights_, 'means': mod.means_.reshape(-1),
                     'sd': np.sqrt(mod.covariances_.reshape(-1)),
                     'll': ll}, order)


def fit_pdf(data_array, dist, thread_count = mp.cpu_count() - 1):
    # thread_count = int(0.875 * mp.cpu_count())

    # Manual coord does not work well.

    # get the pdf at the coordinates
    p = mp.Pool(thread_count)
    data_array_list = np.array_split(data_array, data_array.shape[1], axis = 1)
    result = [p.apply_async(fit_samples, args = (da, dist, r)) \
              for r, da in enumerate(data_array_list)]
    p.close()
    p.join()

    result = [r.get() for r in result]

    if dist == 'gamma':
        shape = np.full(data_array.shape[1], np.nan)
        loc = np.full(data_array.shape[1], np.nan)
        scale = np.full(data_array.shape[1], np.nan)
        loglik = np.full(data_array.shape[1], np.nan)
        for rind, r in enumerate(result):
            shape[r[1]] = r[0]['shape']
            loc[r[1]] = r[0]['loc']
            scale[r[1]] = r[0]['scale']
            loglik[r[1]] = r[0]['ll']
        return shape, loc, scale, loglik
    elif dist == 'weibull':
        a = np.full(data_array.shape[1], np.nan)
        shape = np.full(data_array.shape[1], np.nan)
        loc = np.full(data_array.shape[1], np.nan)
        scale = np.full(data_array.shape[1], np.nan)
        loglik = np.full(data_array.shape[1], np.nan)
        for rind, r in enumerate(result):
            #print(rind)
            #print(r)
            a[r[1]] = r[0]['a']
            shape[r[1]] = r[0]['shape']
            loc[r[1]] = r[0]['loc']
            scale[r[1]] = r[0]['scale']
            loglik[r[1]] = r[0]['ll']
        return a, shape, loc, scale, loglik
    elif dist == 'gmm':
        weights = np.full([2, data_array.shape[1]], np.nan)
        means = np.full([2, data_array.shape[1]], np.nan)
        sd = np.full([2, data_array.shape[1]], np.nan)
        loglik = np.full(data_array.shape[1], np.nan)
        for rind, r in enumerate(result):
            weights[:, r[1]] = r[0]['weights']
            means[:, r[1]] = r[0]['means']
            sd[:, r[1]] = r[0]['sd']
            loglik[r[1]] = r[0]['ll']
        return weights, means, sd, loglik


#def get_zscore(coord, cdf, vector):
#    """
#    Interpolate the cdf to the data points of data_array_apply.
#    """
#    if np.all(np.isnan(coord.reshape(-1))):
#        # calibration period has very little soil moisture variability.
#        return np.zeros(len(vector))
#
#    else:
#        f0 = interp1d(coord, cdf, kind = 'linear')
#
#        #print(np.nanmin(f0(vector)))
#        #print(np.nanmax(f0(vector)))
#
#        # Find the quantile and convert the quantile to z-score on
#        # standard normal distribution.
#        temp = np.where(~np.isnan(vector))[0]
#        score = np.full(len(vector), np.nan)
#        score[temp] = norm.ppf(f0(vector[temp]), 0, 1)
#
#        return score
def get_zscore(params, dist, vector):
    """
    Convert given values to z-score in Gaussian distribution.
    """
    if np.isnan(params['loglik']):
        # Apply a simple Gaussian distribution
        if dist != 'gmm':
            pct = norm.cdf(vector, loc = params['loc'], scale = 1)
        else:
            pct = norm.cdf(vector, loc = params['means'][0], scale = 1)
    else:
        if dist == 'gamma':
            pct = gamma.cdf(vector, a = params['shape'],
                            loc = params['loc'], scale = params['scale'])
        elif dist == 'weibull':
            pct = exponweib.cdf(vector, a = params['a'], c = params['shape'],
                                loc = params['loc'], scale = params['scale'])
        elif dist == 'gmm':
            pct = params['weights'][0] * norm.cdf(vector,
                                                  loc = params['means'][0],
                                                  scale = params['sd'][0]) + \
                  params['weights'][1] * norm.cdf(vector,
                                                  loc = params['means'][1],
                                                  scale = params['sd'][1])

    # sometimes the pct = 1., resulting in np.inf
    pct = np.clip(pct, 1e-8, 1 - 1e-8)

    return norm.ppf(pct, 0, 1)
