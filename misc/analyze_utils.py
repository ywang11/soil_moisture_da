import numpy as np
import sys
import os
import xarray as xr
import pandas as pd
from glob import glob
import statsmodels.api as sm
import multiprocessing as mp


if sys.platform == 'linux':
    #path_root = os.environ['SCRATCHDIR'] + '/Soil_Moisture/'
    path_root = os.environ['PROJDIR'] + '/Soil_Moisture/'
else:
    path_root = 'D:/Projects/2018 Soil Moisture/'


def decode_month_since(time):
    ref = pd.Timestamp(time.attrs['units'].split(' ')[2])
    start = ref + pd.Timedelta(time.values[0], unit = 'M')
    start = start.replace(day = 1, hour = 0, minute = 0, second = 0)
    return pd.date_range(start, periods = len(time), freq = 'MS')


def by_global_avg(array, time, prefix, res):
    """
    Assumed dimension of array: time, lat, lon.
    """
    # km^2 of land in each grid cell.
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 
                                        'land_mask_minimum',
                                        'landfrac_' + res + '.nc'))
    land_area = data.area.values.copy()
    data.close()

    # (numpy broadcasting adds additional left axis)
    g_global_ts = np.nansum(np.nansum(array * land_area, axis = 2),
                            axis = 1) / np.nansum(land_area)

    pd.DataFrame(data = g_global_ts.reshape(-1,1), index = time,
                 columns = ['global']).to_csv(os.path.join(prefix + '_' + \
                                                           res + \
                                                           '_g_global_ts.csv'))


def by_hemisphere_avg(array, time, prefix, res):
    """
    Assumed dimension of array: time, lat, lon.
    """
    # km^2 of land in each grid cell.
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 
                                        'land_mask_minimum',
                                        'landfrac_' + res + '.nc'))
    land_area = data.area.values.copy()
    data.close()

    # (numpy broadcasting adds additional left axis)
    g_hemisphere_ts = np.full((array.shape[0], 2), np.nan)

    # 'NH'
    mask = np.broadcast_to(data.lat.values[:, np.newaxis] > 0.,
                           land_area.shape)
    g_hemisphere_ts[:, 0] = np.nansum(np.nansum(array * land_area * mask,
                                                axis = 2),
                                      axis = 1) / np.nansum(land_area * mask)

    # 'SH'
    mask = np.broadcast_to(data.lat.values[:, np.newaxis] < 0.,
                           land_area.shape)
    g_hemisphere_ts[:, 1] = np.nansum(np.nansum(array * land_area * mask,
                                                axis = 2),
                                      axis = 1) / np.nansum(land_area * mask)

    pd.DataFrame(data = g_hemisphere_ts, index = time, columns = ['NH', 'SH'] \
    ).to_csv(os.path.join(prefix + '_' + res + '_g_hemisphere_ts.csv'))


def by_continent_avg(array, time, prefix, res):
    """
    Assumed dimension of array: time, lat, lon.
    """
    # km^2 of land in each grid cell.
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 
                                        'land_mask_minimum',
                                        'landfrac_' + res + '.nc'))
    land_area = data.area.values.copy()
    data.close()

    # Continent mask
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 'da_masks',
                                        'natural_earth_continent_mask_' + \
                                        res + '.nc'))
    # ---- ID and Abbreviation:
    levels = data.comment.split('; ')
    ids = [int(x.split(' - ')[0]) for x in levels[1:-1]]
    abbrev = [x.split(' - ')[1] for x in levels[1:-1]]

    print(ids)
    print(abbrev)

    mask = data.region.values.copy()
    data.close()

    # Apply the continent mask
    g_continent_ts = np.full([array.shape[0], len(ids)], np.nan)
    for c_ind, c in enumerate(ids):
        temp = np.where(np.abs(mask - c) < 1e-6, 1, np.nan)
        g_continent_ts[:, c_ind] = np.nansum(np.nansum(array * land_area * \
                                                       temp, axis = 2),
                                             axis = 1) / \
                                             np.nansum(land_area * temp)

    pd.DataFrame(data = g_continent_ts, index = time,
                 columns = abbrev).to_csv(os.path.join(prefix + '_' + res + \
                                                       '_g_continent_ts.csv'))


def by_subcontinent_avg(array, time, lat, lon, prefix, res):
    """
    Assumed dimension of array: time, lat, lon.
    Use the regions in Cheng & Huang 2016 DOI: 10.1002/2015JD024559
    (The bounds are in the order: bottom latitude, top latitude,
     west longitude, east longitude).
    North America (NAM): [15, 70, -180, -25]
    South America (SAM): [-60, 15, -180, -25]
    Europe (EU): [30, 70, -25, 60]
    West Asia (WAS): [30, 70, 60, 90]
    East Asia (EAS): [30, 70, 90, 180]
    South Asia (SAS): [12, 30, 60, 180]
    North Africa (NAF): [0, 30, -25, 60]
    South Africa (SAF): [-60, 0, -25, 60]
    Australia (AUS): [-60, -12, 60, 180]
    """
    # km^2 of land in each grid cell.
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 
                                        'land_mask_minimum',
                                        'landfrac_' + res + '.nc'))
    land_area = data.area.values.copy()
    data.close()

    region = {'NAM': [15, 70, -180, -25],
              'SAM': [-60, 15, -180, -25],
              'EU': [30, 70, -25, 60],
              'WAS': [30, 70, 60, 90],
              'EAS': [30, 70, 90, 180],
              'SAS': [12, 30, 60, 180],
              'NAF': [0, 30, -25, 60],
              'SAF': [-60, 0, -25, 60],
              'AUS': [-60, -12, 60, 180]}
    region_names = list(region.keys())

    lon2D, lat2D = np.meshgrid(lon, lat)

    g_region_ts = np.full([array.shape[0], len(region.keys())], np.nan)
    for r_ind, r in enumerate(region_names):
        mask = np.where((lat2D >= region[r][0]) & (lat2D <= region[r][1]) & \
                        (lon2D >= region[r][2]) & (lon2D <= region[r][3]),
                        1, np.nan)
        g_region_ts[:, r_ind] = np.nansum(np.nansum(array * land_area * mask,
                                                    axis = 2), axis = 1) / \
                                np.nansum(land_area * mask)

    pd.DataFrame(data=g_region_ts, index=time, columns=region_names \
    ).to_csv(prefix + '_' + res + '_g_region_ts.csv')



def by_biome_avg(array, time, prefix, res):
    """
    Assumed dimension of array: time, lat, lon.
    """
    # km^2 of land in each grid cell.
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 
                                        'land_mask_minimum',
                                        'landfrac_' + res + '.nc'))
    land_area = data.area.values.copy()
    data.close()

    # Biome mask
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 'da_masks',
                                        'Biomes_' + res + '.nc'))
    # ---- ID and Abbreviation:
    levels = data.biomes.attrs['long_name'].split('; ')
    ids = [int(x.split('-')[0]) for x in levels[:-1]]
    abbrev = [x.split('-')[1] for x in levels[:-1]]

    mask = data.biomes.values.copy()
    data.close()

    # ---- re-order because lon was before lat
    if res == '0.5':
        mask = np.transpose(mask, axes = [1, 0])

    # Apply the Biomes mask
    g_biomes_ts = np.full([array.shape[0], len(ids)], np.nan)
    for c_ind, c in enumerate(ids):
        temp = np.where(np.abs(mask - c) < 1e-6, 1, np.nan)
        g_biomes_ts[:, c_ind] = np.nansum(np.nansum(array * land_area * \
                                                    temp, axis = 2),
                                          axis = 1) / \
                                          np.nansum(land_area * temp)

    pd.DataFrame(data = g_biomes_ts, index = time,
                 columns = abbrev).to_csv(os.path.join(prefix + '_' + res + \
                                                       '_g_biomes_ts.csv'))


def by_AI_avg(array, time, prefix, res):
    """
    Assumed dimension of array: time, lat, lon.
    """
    # km^2 of land in each grid cell.
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 
                                        'land_mask_minimum',
                                        'landfrac_' + res + '.nc'))
    land_area = data.area.values.copy()
    data.close()

    # Aridity Index mask:  <0.05 hyper-arid, 0.05-0.2 arid,
    #   0.2-0.5 semiarid, 0.5-0.65 dry subhumid, >=0.65 humid,
    #   following https://www.nature.com/articles/nclimate2837
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 'da_masks',
                                        'Aridity_Index_discrete_' + res + \
                                        '.nc'))
    # ---- ID and Abbreviation:
    levels = data.ai.attrs['Key'].split('; ')
    ids = [int(x.split(': ')[0]) for x in levels[:-1]]
    abbrev = [x.split(': ')[1] for x in levels[:-1]]

    mask = data.ai.values.copy()
    data.close()

    # Apply the Aridity Index mask
    g_ai_ts = np.full([array.shape[0], len(ids)], np.nan)
    for c_ind, c in enumerate(ids):
        temp = np.abs(mask - c) < 1e-6
        g_ai_ts[:, c_ind] = np.nansum(np.nansum(array * land_area * \
                                                temp, axis = 2), axis = 1) / \
                                      np.nansum(land_area * temp)
    pd.DataFrame(data = g_ai_ts, index = time,
                 columns = abbrev).to_csv(os.path.join(prefix + '_' + res + \
                                                       '_g_ai_ts.csv'))

def get_lat_avg(matrix, lat, lat_rng):
    """
    Calculate the latitudinal average of a matrix.
    """
    if len(matrix.shape) == 2:
        return np.nanmean(matrix[(lat >= lat_rng[0]) & (lat <= lat_rng[1]), :])
    elif len(matrix.shape) == 3:
        return np.nanmean(np.nanmean(matrix[:, (lat >= lat_rng[0]) & \
                                            (lat <= lat_rng[1]), :],
                                     axis=2), axis=1)
    else:
        raise Exception('Matrix has too many dimensions')


def by_lat_avg(array, time, lat, lon, prefix, res, suffix = ''):
    """
    Assumed dimension of array: time, lat, lon.
    """
    # km^2 of land in each grid cell.
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 
                                        'land_mask_minimum',
                                        'landfrac_' + res + '.nc'))
    land_area = data.area.values.copy()
    data.close()

    lat_bnds = np.concatenate([np.arange(-52.5, 67.6, 5).reshape(-1,1),
                               np.arange(-47.5, 72.6, 5).reshape(-1,1)],
                              axis = 1)
    lat_median = [('%d' % x) for x in np.arange(-50, 71, 5)]

    g_lat_ts = np.full([array.shape[0], lat_bnds.shape[0]], np.nan)
    for lb in range(lat_bnds.shape[0]):
        g_lat_ts[:, lb] = get_lat_avg(array * land_area, lat,
                                      lat_bnds[lb,:]) / \
                          get_lat_avg(land_area, lat, lat_bnds[lb,:])

    pd.DataFrame(data = g_lat_ts, index=time, columns=lat_median \
    ).to_csv(os.path.join(prefix + '_' + res + '_g_lat_ts' + suffix + '.csv'))


def get_summary_cmip5(path_out, target_lat, target_lon, dcm, 
                      kind = {'mean', 'trend'}):
    """
    The mean, min, max of the mean/trend of global soil moisture of CMIP5
    models.
    """
    if kind == 'mean':
        model_list = glob(os.path.join(path_out,
                                       'standard_diagnostics_cmip5',
                                       '*' + dcm + '_g_map_annual.nc'))
    else:
        model_list = glob(os.path.join(path_out,
                                       'standard_diagnostics_cmip5',
                                       '*' + dcm + '_g_map_trend_Annual.nc'))

    collect_data = np.empty([len(model_list), len(target_lat),
                             len(target_lon)])
    collect_data[:,:,:] = np.nan


    if kind == 'trend':
        collect_pvalue = np.empty([len(model_list), len(target_lat),
                                   len(target_lon)])
        collect_pvalue[:,:,:] = np.nan
    else:
        collect_pvalue = None


    for model_ind, model_path in enumerate(model_list):
        if kind == 'mean':
            var_name = 'g_map'
        elif kind == 'trend':
            var_name = ['g_map_trend', 'g_p_values']

        data = xr.open_dataset(model_path)

        if kind == 'mean':
            collect_data[model_ind, :, :] = data[var_name].values.copy()
        else:
            # Average over all trends, instead of the significant trends only.
            # data[var_name[0]].where(data[var_name[1]] <= 0.05)
            collect_data[model_ind, :, :] = data[var_name[0]].values.copy()
            collect_pvalue[model_ind, :, :] = data[var_name[1]].values.copy()

        data.close()

    collect_mean = np.nanmean(collect_data, axis=0)
    collect_max = np.nanmax(collect_data, axis=0)
    collect_min = np.nanmin(collect_data, axis=0)
    return collect_mean, collect_max, collect_min, collect_data, \
        collect_pvalue


def get_summary_cmip6(path_out, model_version, prefix, target_lat,
                      target_lon, year, kind = {'mean', 'trend'}):
    """
    The mean, min, max of the mean/trend of global soil moisture of CMIP6
    models.
    """
    collect_data = np.empty([len(model_version), len(target_lat),
                             len(target_lon)])
    collect_data[:,:,:] = np.nan


    if kind == 'trend':
        collect_pvalue = np.empty([len(model_version), len(target_lat),
                                   len(target_lon)])
        collect_pvalue[:,:,:] = np.nan
    else:
        collect_pvalue = None


    for model_ind, model in enumerate(model_version):
        if kind == 'mean':
            suffix = '_g_map_annual.nc'
            var_name = 'g_map'
        elif kind == 'trend':
            suffix = '_g_map_trend_Annual.nc'
            var_name = ['g_map_trend', 'g_p_values']

        data = xr.open_dataset(os.path.join(path_out,
                                            'standard_diagnostics_cmip6',
                                            prefix + '_' + model + '_' + \
                                            str(year[0]) + '-' + \
                                            str(year[-1]) + suffix))

        if kind == 'mean':
            collect_data[model_ind, :, :] = data[var_name].values.copy()
        else:
            # Average over all trends, instead of the significant trends only.
            # data[var_name[0]].where(data[var_name[1]] <= 0.05)
            collect_data[model_ind, :, :] = data[var_name[0]].values.copy()
            collect_pvalue[model_ind, :, :] = data[var_name[1]].values.copy()

        data.close()

    collect_mean = np.nanmean(collect_data, axis=0)
    collect_max = np.nanmax(collect_data, axis=0)
    collect_min = np.nanmin(collect_data, axis=0)

    return collect_mean, collect_max, collect_min, collect_data, \
        collect_pvalue


def calc_trend(vector, rank):
    vector = vector.reshape(-1)
    mod = sm.OLS(vector, sm.add_constant(range(len(vector))))
    reg = mod.fit()
    _, slope = reg.params
    p_slope = reg.pvalues[1]
    return (slope, p_slope, rank)


def calc_ts_trend(X, y):
    mod = sm.OLS(y, sm.add_constant(X))
    reg = mod.fit()
    slope = reg.params[1]
    intercept = reg.params[0]
    p_value = reg.pvalues[1]
    return slope, intercept, p_value


def calc_seasonal_trend(array, nthread = 0.5 * mp.cpu_count()):
    nlat = array.shape[1]
    nlon = array.shape[2]

    array = np.reshape(array, [array.shape[0], nlat * nlon])
    retain = np.where(~np.isnan(array[0, :]))[0]
    array_list = np.array_split(array[:, retain], len(retain), axis = 1)


    pool = mp.Pool(nthread)
    results = [pool.apply_async(calc_trend, args = (var1, ind)) \
               for ind, var1 in enumerate(array_list)]
    results = [xxx.get() for xxx in results]
    pool.close()
    pool.join()


    trend = np.full(nlat * nlon, np.nan)
    trend_p = np.full(nlat * nlon, np.nan)
    for n in range(len(results)):
        trend[retain[results[n][-1]]] = results[n][0]
        trend_p[retain[results[n][-1]]] = results[n][1]
    trend = trend.reshape(nlat, nlon)
    trend_p = trend_p.reshape(nlat, nlon)

    return trend, trend_p


def calc_reg(vector):
    if len(vector) < 2:
        return np.full(3, np.nan)

    y = vector
    #print(y)
    x = np.arange(len(vector))
    temp = ~(np.isnan(y) | np.isnan(x))

    if sum(temp) < 2: # less than two values cannot perform linear regress
        return np.full(3, np.nan)
    else:
        y = y[temp]
        x = x[temp]
        #print(y)
        #print(x)
        
        reg = sm.OLS(y, sm.add_constant(x)).fit()
        return np.array([reg.params[0], reg.params[1], reg.pvalues[1]])


def calc_seasonal_trend0(sm_data, season = ['All', 'Annual', 'DJF', 'MAM',
                                            'JJA', 'SON']):
    # Convert to time series of annual or seasonal averages
    if season=='All':
        # ---- Skip the 1st year to be consistent with DJF.
        season_annual = sm_data[12:, :, :]

        # Extract the numpy array of the seasonal values
        year = np.arange(0., (len(season_annual.time)-1)/12 + 1e-6, 1/12.)
    elif season=='Annual':
        # ---- Skip the 1st year to be consistent with DJF.
        sm_data = sm_data[12:, :, :]
        season_annual = sm_data.groupby('time.year').mean(dim='time',
                                                          skipna = False)#True
        # do not calculate the annual mean, if <3 observations
        seasonal_annual_count = (~np.isnan(sm_data) \
        ).groupby('time.year').sum(dim = 'time')
        season_annual = season_annual.where(seasonal_annual_count >= 3,
                                            np.nan)

        # Extract the numpy array of the seasonal values
        year = season_annual.year.values
    else:
        # ---- Skip the 1st year to be consistent with DJF.
        period = sm_data['time'].to_index().to_period(freq = 'Q-NOV')
        if season == 'DJF':
            qtr = 1
        elif season == 'MAM':
            qtr = 2
        elif season == 'JJA':
            qtr = 3
        else:
            qtr = 4
        sm_data = sm_data[period.quarter == qtr, :, :]
        sm_data['time'] = period[period.quarter == qtr].year
        season_annual = sm_data.groupby('time').mean(dim = 'time',
                                                     skipna = False)

        if season == 'DJF':
            season_annual = season_annual[1:-1, :, :]
        else:
            season_annual = season_annual[1:, :, :]

        season_annual = season_annual.rename({'time': 'year'})

        # Extract the numpy array of the seasonal values
        year = season_annual.year.values

    temp = xr.apply_ufunc(calc_reg,
                          season_annual.chunk({'year':-1}),
                          input_core_dims = [['year']],
                          vectorize = True,
                          dask = 'parallelized',
                          output_core_dims = [['new']],
                          dask_gufunc_kwargs = {'output_sizes': {'new': 3}})
    temp.compute()
    intercepts = temp[:, :, 0]
    trends = temp[:, :, 1]
    p_values = temp[:, :, 2]

    return trends, p_values, intercepts


def calc_seasonal_mean(sm_data, season = ['All', 'Annual', 'DJF', 'MAM',
                                          'JJA', 'SON']):
    # Convert to time series of annual or seasonal averages
    if season=='All':
        # ---- Skip the 1st year to be consistent with DJF.
        season_annual = sm_data[12:, :, :]

        # Extract the numpy array of the seasonal values
        year = np.arange(0., (len(season_annual.time)-1)/12 + 1e-6, 1/12.)
    elif season=='Annual':
        # ---- Skip the 1st year to be consistent with DJF.
        sm_data = sm_data[12:, :, :]
        season_annual = sm_data.groupby('time.year').mean(dim='time',
                                                          skipna = False)#True
        # do not calculate the annual mean, if <3 observations
        seasonal_annual_count = (~np.isnan(sm_data) \
        ).groupby('time.year').sum(dim = 'time')
        season_annual = season_annual.where(seasonal_annual_count >= 2,
                                            np.nan)

        # Extract the numpy array of the seasonal values
        year = season_annual.year.values
    else:
        # ---- Skip the 1st year to be consistent with DJF.
        period = sm_data['time'].to_index().to_period(freq = 'Q-NOV')
        if season == 'DJF':
            qtr = 1
        elif season == 'MAM':
            qtr = 2
        elif season == 'JJA':
            qtr = 3
        else:
            qtr = 4
        sm_data = sm_data[period.quarter == qtr, :, :]
        sm_data['time'] = period[period.quarter == 1].year
        season_annual = sm_data.groupby('time').mean(dim = 'time',
                                                     skipna = False)

        if season == 'DJF':
            season_annual = season_annual[1:-1, :, :]
        else:
            season_annual = season_annual[1:, :, :]

        season_annual = season_annual.rename({'time': 'year'})

        # Extract the numpy array of the seasonal values
        year = season_annual.year.values

    return season_annual.mean(dim = 'year')


def get_land_mask(land_mask):
    """ True = retain values; False = discard values """
    hr = xr.open_dataset(os.path.join(path_root, 'intermediate',
                                      'land_mask_minimum',
                                      'merge_' + land_mask + '.nc'))
    mask = hr.mask.values.copy()
    hr.close()
    hr = xr.open_dataset(os.path.join(path_root, 'intermediate',
                                      'land_mask_minimum', 'cmip6_da.nc'))
    mask = mask & (hr.mask.values.copy())
    hr.close()
    return mask


def get_ndvi_mask(land_mask):
    hr = xr.open_dataset(os.path.join(path_root, 'intermediate',
                                      'land_mask_minimum',
                                      'ndvi_mask_0.5_0.125.nc'))
    mask = get_land_mask(land_mask) & hr['mask'].values.copy()
    hr.close()

    return mask                                      
