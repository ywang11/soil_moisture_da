import os
import pandas as pd
import sys
import numpy as np
from statsmodels.sandbox.regression.predstd import wls_prediction_std
import statsmodels.api as stats
import xarray as xr
import itertools as it
import scipy.stats as scistats


if sys.platform == 'linux':
    #path_root = os.environ['SCRATCHDIR'] + '/Soil_Moisture/'
    path_root = os.environ['PROJDIR'] + '/Soil_Moisture/'
else:
    path_root = 'D:/Projects/2018 Soil Moisture/'

path_out = os.path.join(path_root, 'output_da')


"""
def collect_SN_model(prf, sfx, fld, base, expr, dist, lag, res,
                     season_list, depth_cm_new, strt, wd, strt2, model_list = None):
    # Collect the signal and noise
    flip = pd.read_csv(os.path.join(path_out, 'cmip6_spi_fingerprint',
                                    prf + 'flip_bylat' + sfx + '.csv'),
                       index_col = [0,1,2,3])

    signal_self_collect = {}
    signal_mean_collect = {}
    #signal_std_collect = {} # STD around the weighted average
    signal_weights_collect = {} # inversely proportional to the variance
                                # of each ESM, i.e., proportional to the
                                # number of samples in each ESM
    for sind, season in enumerate(season_list):
        for dcm in depth_cm_new:
            noise_std = pd.read_csv(os.path.join(path_out,
                'cmip6_spi_noise', fld, base + '_eofs', prf + dist + \
                '_trend_' + dcm + '_' + str(lag) + '_' + season + '_' + \
                res + sfx + '.csv'), index_col = 0).std(axis = 0)

            # CMIP6 signals
            signal_hist = pd.read_csv(os.path.join(path_out,
                'cmip6_spi_signal', fld, base + '_eofs', prf + \
                dist + '_' + expr + '_signal_' + dcm + '_' + \
                str(lag) + '_' + season + '_' + res + sfx + '.csv'),
                                      header = [0,1] \
            ).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
            signal_hist.index = pd.to_datetime(signal_hist.index).year
            signal_hist0 = signal_hist.loc[[strt], str(wd)]
            signal_hist = signal_hist.loc[strt2, :]

            #print(noise_std)
            #print(signal_hist0)
            #print(signal_hist.unstack())
            #print(noise_std.index)
            #print(signal_hist.unstack().index)
            #print(signal_hist.unstack().T / noise_std)
            #dummy()

            signal_hist = signal_hist.unstack().T * flip.loc[(dcm, sind, dist, lag),
                                                              base] / noise_std
            signal_hist = pd.concat([ signal_hist0 * \
                flip.loc[(dcm, sind, dist, lag), base] / noise_std.loc[str(wd)], 
                signal_hist.T], axis = 0)
            if not model_list is None:
                signal_hist = signal_hist.loc[:, model_list]
            signal_hist.index = [strt] + [strt2+int(ii)-1 for ii in signal_hist.index[1:]]

            #print(signal_hist)
            #dummy()

            signal_self_collect[(season, dcm)] = signal_hist

            # ---- need to weight the different models
            temp_model = [x.split('_')[0] for x in signal_hist.columns]
            signal_mean = signal_hist.groupby(temp_model, axis = 1).mean()

            ## Standard deviation around the mean of each ESM
            #signal_std = signal_hist.copy(deep = True)
            #for mind, m in enumerate(signal_std.columns):
            #    temp = signal_std.loc[:, m] - \
            #        signal_mean.loc[:, m.split('_')[0]]
            #    temp = np.where(np.abs(temp) < 1e-7, np.nan, temp)
            #    signal_std.loc[:, m] = temp
            #signal_std = np.nanstd(signal_std, axis = 1)
            signal_weights = signal_mean.copy(deep = True)
            for mind, m in enumerate(signal_weights.columns):
                signal_weights.loc[:, m] = np.sum( \
                    [tm == m for tm in temp_model] )

            signal_mean_collect[(season, dcm)] = signal_mean
            #signal_std_collect[(season, dcm)] = pd.Series(signal_std,
            #    index = signal_mean.index)
            signal_weights_collect[(season, dcm)] = signal_weights

    ##signal_std_collect = pd.DataFrame.from_dict(signal_std_collect)
    ##return signal_mean_collect, signal_std_collect, signal_self_collect
    return signal_mean_collect, signal_weights_collect, signal_self_collect
"""


def collect_SN_model(prf, sfx, fld, base, expr, dist, lag, res,
                     season_list, depth_cm_new, strt, wd, strt2 = None, wd2 = None, model_list = None):
    # Collect the signal and noise
    if strt2 is None:
        strt2 = strt
    if wd2 is None:
        wd2 = wd

    flip = pd.read_csv(os.path.join(path_out, 'cmip6_spi_fingerprint',
                                    prf + 'flip_bylat' + sfx + '.csv'),
                       index_col = [0,1,2,3])

    signal_self_collect = {}
    signal_mean_collect = {}
    #signal_std_collect = {} # STD around the weighted average
    signal_weights_collect = {} # inversely proportional to the variance
                                # of each ESM, i.e., proportional to the
                                # number of samples in each ESM
    for sind, season in enumerate(season_list):
        for dcm in depth_cm_new:
            noise_std = np.nanstd(pd.read_csv(os.path.join(path_out,
                'cmip6_spi_noise', fld, base + '_eofs', prf + dist + \
                '_trend_' + dcm + '_' + str(lag) + '_' + season + '_' + \
                res + sfx + '.csv'), index_col = 0 \
            ).loc[:, str(wd)].values, axis = 0)
            noise_std2 = np.nanstd(pd.read_csv(os.path.join(path_out,
                'cmip6_spi_noise', fld, base + '_eofs', prf + dist + \
                '_trend_' + dcm + '_' + str(lag) + '_' + season + '_' + \
                res + sfx + '.csv'), index_col = 0 \
            ).loc[:, str(wd2)].values, axis = 0)

            # CMIP6 signals
            signal_hist = pd.read_csv(os.path.join(path_out,
                'cmip6_spi_signal', fld, base + '_eofs', prf + \
                dist + '_' + expr + '_signal_' + dcm + '_' + \
                str(lag) + '_' + season + '_' + res + sfx + '.csv'),
                                      header = [0,1] \
            ).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
            signal_hist.index = pd.to_datetime(signal_hist.index).year


            temp = signal_hist.loc[[strt], str(wd)] * \
                flip.loc[(dcm, sind, dist, lag), base] / noise_std
            #print(temp) # debug

            temp2 = signal_hist.loc[strt2:(2100-wd2+1), str(wd2)] * \
                flip.loc[(dcm, sind, dist, lag), base] / noise_std2
            #print(temp2) # debug

            signal_hist = pd.concat([temp, temp2], axis = 0)
            #print(signal_hist) # debug

            if not model_list is None:
                signal_hist = signal_hist.loc[:, model_list]

            signal_self_collect[(season, dcm)] = signal_hist

            # ---- need to weight the different models
            temp_model = [x.split('_')[0] for x in signal_hist.columns]
            signal_mean = signal_hist.groupby(temp_model, axis = 1).mean()

            ## Standard deviation around the mean of each ESM
            #signal_std = signal_hist.copy(deep = True)
            #for mind, m in enumerate(signal_std.columns):
            #    temp = signal_std.loc[:, m] - \
            #        signal_mean.loc[:, m.split('_')[0]]
            #    temp = np.where(np.abs(temp) < 1e-7, np.nan, temp)
            #    signal_std.loc[:, m] = temp
            #signal_std = np.nanstd(signal_std, axis = 1)
            signal_weights = signal_mean.copy(deep = True)
            for mind, m in enumerate(signal_weights.columns):
                signal_weights.loc[:, m] = np.sum( \
                    [tm == m for tm in temp_model] )

            signal_mean_collect[(season, dcm)] = signal_mean
            #signal_std_collect[(season, dcm)] = pd.Series(signal_std,
            #    index = signal_mean.index)
            signal_weights_collect[(season, dcm)] = signal_weights
    
    ##signal_std_collect = pd.DataFrame.from_dict(signal_std_collect)
    ##return signal_mean_collect, signal_std_collect, signal_self_collect
    return signal_mean_collect, signal_weights_collect, signal_self_collect


def collect_drivers_model(drvs, sfx, fld, expr, res, season_list, strt2 = None, wd2 = None, 
                          model_list = None):
    """ Collect the signal and noise. Note using 1982-2016 for LAI and
        1971-2016 for the rest. """
    flip = pd.read_csv(os.path.join(path_out, 'cmip6_drivers_fingerprint',
                                    'flip_bylat' + sfx + '.csv'), index_col = [0,1])

    signal_mean_collect = {}
    #signal_std_collect = {} # STD around the weighted average
    signal_weights_collect = {} # inversely proportional to the variance
                                # of each ESM, i.e., proportional to the
                                # number of samples in each ESM
    signal_self_collect = {}
    for sind, season in enumerate(season_list):
        for vv in drvs:
            # CMIP6 signals
            signal_hist = pd.read_csv(os.path.join(path_out, 'cmip6_drivers_signal',
                                                   fld, expr + '_eofs', vv + '_signal_' + \
                                                   str(season) + '_' + res + sfx + '.csv'),
                                      header = [0,1] \
            ).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
            signal_hist.index = pd.to_datetime(signal_hist.index).year
            if vv != 'lai':
                strt = 1971
                wd = 2016 - strt + 1
            else:
                strt = 1982
                wd = 2016 - strt + 1
            if strt2 is None:
                strt2 = 1971
            if wd2 is None:
                wd2 = 2016 - strt2 + 1
            signal_hist = pd.concat([signal_hist.loc[[strt], str(wd)], 
                                     signal_hist.loc[strt2:(2100-wd2+1), str(wd2)]],
                                    axis = 0)
            signal_hist = signal_hist * flip.loc[(sind, vv), expr]
            signal_hist = signal_hist.loc[:, model_list]

            signal_self_collect[(season, vv)] = signal_hist

            # ---- need to weight the different models
            temp_model = [x.split('_')[0] for x in signal_hist.columns]
            signal_mean = signal_hist.groupby(temp_model, axis = 1).mean()

            signal_weights = signal_mean.copy(deep = True)
            for mind, m in enumerate(signal_weights.columns):
                signal_weights.loc[:, m] = np.sum([tm == m for tm in temp_model] )
    
            signal_mean_collect[(season, vv)] = signal_mean
            signal_weights_collect[(season, vv)] = signal_weights
    
    return signal_mean_collect, signal_weights_collect, signal_self_collect


def collect_SN_obs(prf, sfx, fld, base, prod, dist, lag, res,
                   season_list, depth_cm_new, strt, wd):
    """ Collect the signal to noise ratio. """
    flip = pd.read_csv(os.path.join(path_out, 'cmip6_spi_fingerprint',
                                    prf + 'flip_bylat' + sfx + '.csv'),
                       index_col = [0,1,2,3])
    signal_obs_collect = pd.DataFrame(np.nan,
                                      index = season_list,
                                      columns = depth_cm_new)
    for sind, season in enumerate(season_list):
        for dcm in depth_cm_new:
            noise_std = np.nanstd(pd.read_csv(os.path.join(path_out,
                'cmip6_spi_noise', fld, base + '_eofs', prf + dist + \
                '_trend_' + dcm + '_' + str(lag) + '_' + season + '_' + \
                res + sfx + '.csv'), index_col = 0 \
            ).loc[:, str(wd)].values, axis = 0)

            signal2 = pd.read_csv(os.path.join(path_out,
                'products_spi_signal', fld, base + '_eofs', prf + \
                dist + '_signal_' + dcm + '_' + str(lag) + '_' + \
                season + '_' + res + sfx + '.csv'), header = [0,1] \
            ).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
            signal2.index = pd.to_datetime(signal2.index).year
            signal2.index.name = 'year'

            print(signal2.loc[strt, (str(wd), prod)])

            signal_obs_collect.loc[season, dcm] = \
                signal2.loc[strt, (str(wd), prod)] * \
                flip.loc[(dcm, int(season), dist, lag), base] / noise_std

    return signal_obs_collect


def collect_drivers_obs(drvs, sfx, fld, expr, res, season_list):
    """ Collect the signal and noise. Note the LAI is 1982-2016 and
        the rest are 1971-2016. """
    flip = pd.read_csv(os.path.join(path_out, 'cmip6_drivers_fingerprint',
                                    'flip_bylat' + sfx + '.csv'), index_col = [0,1])

    signal_obs_collect = pd.DataFrame(np.nan, index = season_list,
                                      columns = drvs)
    for vv in drvs:
        signal = pd.read_csv(os.path.join(path_out, 'obs_drivers_signal',
                                          'bylat', expr + '_eofs',
                                          vv + '_signal_' + res + sfx + '.csv'),
                            index_col = 0)
        signal_obs_collect.loc[:, vv] = signal.mean(axis = 0)
    return signal_obs_collect


def collect_SN_em(dist, season_list, depth_cm_new):
    a = pd.read_csv(os.path.join(path_out, 'figures', 'fig3_em_calc', 'SSI',
                                 dist + '_signal_collect_constrained.csv'),
                    index_col = 0, header = [0,1])
    b = pd.read_csv(os.path.join(path_out, 'figures', 'fig3_em_calc', 'SSI',
                                 dist + '_signal_collect_se.csv'),
                    index_col = 0, header = [0,1])
    ##c = pd.read_csv(os.path.join(path_out, 'figures', 'fig3_em_calc',
    ##                             dist + '_signal_collect_std.csv'),
    ##                index_col = 0, header = [0,1])
    d = pd.read_csv(os.path.join(path_out, 'figures', 'fig3_em_calc', 'SSI',
                                 dist + '_signal_collect_p.csv'),
                    index_col = 0, header = [0,1])

    signal_em = {}
    for season, dcm in it.product(season_list, depth_cm_new):
        signal_em[(season, dcm)] = pd.DataFrame.from_dict( \
            {'mean': a.loc[:, (season,dcm)],
             'se': b.loc[:, (season,dcm)],
             'p': d.loc[:, (season,dcm)]}) #'std': c.loc[:, (season,dcm)],
    return signal_em


def collect_SN_em_interaction(dist, season_list, depth_cm_new): # method, 
    a = pd.read_csv(os.path.join(path_out, 'figures', 'fig3_em_calc', 'SSI',
                                 dist + '_signal_collect_constrained.csv'), # '_' + method +
                    index_col = 0, header = [0,1])
    b = pd.read_csv(os.path.join(path_out, 'figures', 'fig3_em_calc', 'SSI',
                                 dist + '_signal_collect_se.csv'), # '_' + method + 
                    index_col = 0, header = [0,1])

    signal_em = {}
    for season, dcm in it.product(season_list, depth_cm_new):
        signal_em[(season, dcm)] = pd.DataFrame.from_dict( \
            {'mean': a.loc[:, (season,dcm)],
             'CI95_lower': a.loc[:, (season,dcm)] - scistats.t.ppf(0.95, 12) * b.loc[:, (season,dcm)], # 12 ESMs in historical
             'CI95_upper': a.loc[:, (season,dcm)] + scistats.t.ppf(0.95, 12) * b.loc[:, (season,dcm)]})
    return signal_em


def collect_eof_model(prf, sfx, base, expr, dist, lag, res,
                      lat_median, season_list, depth_cm_new):
    flip = pd.read_csv(os.path.join(path_out, 'cmip6_spi_fingerprint',
                                    prf + 'flip_bylat' + sfx + '.csv'),
                       index_col = [0,1,2,3])
    eof1_collection = xr.DataArray(np.full([len(lat_median), len(season_list),
                                            len(depth_cm_new)], np.nan),
                                   dims = ['lat_median', 'season', 'dcm'],
                                   coords = {'lat_median': lat_median,
                                             'season': season_list,
                                             'dcm': depth_cm_new})

    for sind, season in enumerate(season_list):
        for dind, dcm in enumerate(depth_cm_new):
            eof1 = pd.read_csv(os.path.join(path_out,
                                            'cmip6_spi_fingerprint', base,
                                            'bylat', prf + \
                                            dist + '_eof_' + dcm + '_' + \
                                            str(lag) + '_' + \
                                            season + '_' + res + \
                                            sfx + '.csv'),
                               index_col = 0).loc[[int(y) for y in lat_median],
                                                  'Full']
            eof1_collection[:,sind,dind] = eof1.values * \
                flip.loc[(dcm, int(season), dist, lag), base]
    return eof1_collection


def collect_noise(prf, sfx, fld, base, dist, lag, res,
                  season_list, depth_cm_new, wd):
    noise_std_collect = pd.DataFrame(np.nan, index = season_list,
                                     columns = depth_cm_new)
    for sind, season in enumerate(season_list):
        for dcm in depth_cm_new:
            noise_std_collect.loc[season, dcm] = \
                np.nanstd(pd.read_csv(os.path.join(path_out,
                'cmip6_spi_noise', fld, base + '_eofs', prf + dist + \
                '_trend_' + dcm + '_' + str(lag) + '_' + season + '_' + \
                res + sfx + '.csv'), index_col = 0 \
            ).loc[:, str(wd)].values, axis = 0)
    return noise_std_collect


def get_constrained(reg, obs_value):
    ##if reg.pvalues[0] <= 0.05:
    ##    if reg.pvalues[1] <= 0.05:
    ##        exog = np.array([1, obs_value])
    ##        predict_obs = reg.predict(exog)
    ##        predict_se, _, _ = wls_prediction_std(reg, exog)
    ##    else:
    ##        reg2 = stats.OLS(reg.model.endog,
    ##                         reg.model.exog[:,0]).fit()
    ##        exog = np.ones(1)
    ##        predict_obs = reg2.predict(exog)
    ##        predict_se, _, _ = wls_prediction_std(reg2, exog)
    ##else:
    ##    if reg.pvalues[1] <= 0.05:
    ##        reg2 = stats.OLS(reg.model.endog,
    ##                         reg.model.exog[:,1]).fit()
    ##        exog = np.array([obs_value])
    ##        predict_obs = reg2.predict(exog)
    ##        predict_se, _, _ = wls_prediction_std(reg2, exog)
    ##    else:
    ##        predict_obs = np.array([np.mean(reg.model.endog)])
    ##        predict_se = np.array([np.std(reg.model.endog)])
    if (type(obs_value) == float) | (type(obs_value) == np.float64):
        exog = np.array([1, obs_value])
    else:
        exog = np.hstack([np.ones([obs_value.shape[0],1]), obs_value])
    predict_obs = reg.predict(exog)
    ###predict_se, _, _ = wls_prediction_std(reg, exog, weights = 1)
    predict_se = np.std(reg.resid, ddof = 1) # for comparison with GAM results
    
    if (type(obs_value) == float) | (type(obs_value) == np.float64):
        return predict_obs[0], predict_se, reg.pvalues[1] # predict_se[0], predidct_weights
    else:
        return predict_obs, predict_se, reg.pvalues
