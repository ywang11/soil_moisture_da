# -*- coding: utf-8 -*-
"""
20190910

@author: ywang254
"""
import numpy as np
import os
import sys
import matplotlib.pyplot as plt
from matplotlib import colors
from matplotlib import cm
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
import xarray as xr
import skill_metrics as SM


def plot_ts_shade(ax, time, ts = {'min': np.array([]), 'mean': np.array([]),
                                  'max': np.array([])}, ts_label = '',
                  ts_col = 'red', ln_main = '-', ln_edge = '--',
                  shade_col = 'red', alpha = 0.2, lw_main = 1.5,
                  lw_edge = 1):
    """
    Plot a shaded ensemble.
    """
    h, = ax.plot(time, ts['mean'], ln_main, color = ts_col,
                 linewidth = lw_main, label = ts_label)
    ##h2, = ax.plot(time, ts['min'], ln_edge, color = ts_col, linewidth = 1)
    ##ax.plot(time, ts['max'], ln_edge, color = ts_col, linewidth = 1)
    h2 = ax.fill_between(time, ts['min'], ts['max'],
                         where = ts['max'] > ts['min'],
                         edgecolor = ts_col, ls = ln_edge,
                         lw = lw_edge, facecolor = shade_col, alpha = alpha)
    return h, h2


def plot_ts_bracket(ax, time, ts = {'min': np.array([]), 'mean': np.array([]),
                                    'max': np.array([])}, ts_label = '',
                    ts_col = 'red', ln_main = '-', ln_edge = '--',
                    edge_col = 'red', lw_main = 1.5, lw_edge = 1):
    h, = ax.plot(time, ts['mean'], ln_main, color = ts_col,
                 linewidth = lw_main, label = ts_label)
    h2, = ax.plot(time, ts['min'], ln_edge, color = edge_col,
                  linewidth = lw_edge, alpha = 0.1)
    h3, = ax.plot(time, ts['max'], ln_edge, color = edge_col,
                  linewidth = lw_edge, alpha = 0.1)
    h4 = ax.fill_between(time, ts['min'], ts['max'],
                         where = ts['max'] > ts['min'],
                         lw = 0., facecolor = edge_col, alpha = 0.05)
    return h, h2


def plot_ts_shadex(ax, time, ts = {'min': np.array([]), 'mean': np.array([]),
                                   'max': np.array([])}, ts_label = '',
                   ts_col = 'red', ln_main = '-', ln_edge = '--',
                   shade_col = 'red'):
    """
    Plot a shaded ensemble in the vertical sense.
    """
    h, = ax.plot(ts['mean'], time, ln_main, color = ts_col, linewidth = 1.5,
                 label = ts_label)
    ##h2, = ax.plot(ts['min'], time, ln_edge, color = ts_col, linewidth = 1)
    ##ax.plot(ts['max'], time, ln_edge, color = ts_col, linewidth = 1)
    h2 = ax.fill_betweenx(time, ts['min'], ts['max'],
                          where = ts['max'] > ts['min'],
                          edgecolor = ts_col, ls = ln_edge, lw = 1., 
                          facecolor = shade_col, alpha = 0.2)
    return h, h2


def plot_ts_trend(ax, time, ts, txt_x, txt_y,
                  ts_kw = {'color': 'k', 'marker': '.', 'markersize': 3.},
                  ln_kw = {'color': 'k', 'linestyle': '-', 'linewidth': 1.5}):
    """
    Plot the time series with trend.
    """
    h1, = ax.plot(time, ts, **ts_kw)

    model = lm.OLS(ts, add_constant(np.arange(0, len(time))))
    result = model.fit()
    params_CI = result.conf_int()

    """
    ax.text(txt_x, txt_y + 0.025, 'Slope = %.2E (%.2E, %.2E)'  % \
            (result.params[1], params_CI[1,0], params_CI[1,1]),
            transform = ax.transAxes, color = ln_kw['color'],
            fontsize = 8)
    ax.text(txt_x, txt_y - 0.025, 'Intercept = %.2E (%.2E, %.2E)' % \
            (result.params[0], params_CI[0,0], params_CI[0,1]),
            transform = ax.transAxes, color = ln_kw['color'],
            fontsize = 8)
    """

    slope = result.params[1]
    intercept = result.params[0]

    h2, = ax.plot(time, np.arange(0, len(time)) * slope + intercept,
                  **ln_kw)

    if intercept < 0:
        symbol = '-'
        intercept = -intercept
    else:
        symbol = '+'
    format_str = 'y = %.6f * x ' + symbol + ' %.6f (%.3f, %.3f)'

    ax.text(txt_x, txt_y, format_str % \
            (slope, intercept, result.pvalues[1], result.pvalues[0]),
            transform = ax.transAxes, color = ln_kw['color'],
            fontdict = {'fontsize': 8, 'family' : 'serif',
                        'weight': 'bold'})
    ##print(np.arange(0, len(time)) * slope + intercept)
    return h1, h2


"""def cmap_div():
    c = cm.get_cmap('gist_ncar_r', 256)
    new1 = c(np.linspace(0.1, 0.5, 128))
    new2 = c(np.linspace(0.6, 1., 128))
    newcmp = colors.ListedColormap(np.concatenate([new1, new2], axis = 0))
    return newcmp
"""
def cmap_div():
    c = cm.get_cmap('PiYG', 256)
    new1 = c(np.linspace(0., 0.46, 128))
    new2 = c(np.linspace(0.54, 1., 128))
    newcmp = colors.ListedColormap(np.concatenate([new1, new2], axis = 0))
    return newcmp

def cmap_gen(cmap_1, cmap_2):
    """
    Generate a new colormap by stacking two color maps.
    """
    c1 = cm.get_cmap(cmap_1, 128)
    c2 = cm.get_cmap(cmap_2, 128)
    new1 = c1(np.linspace(0, 1, 128))
    new2 = c2(np.linspace(0, 1, 128))
    newcmp = colors.ListedColormap(np.concatenate([new1, new2], axis = 0))
    return newcmp


def stipple(ax, lat, lon, mask, transform=None,
            hatch = '//////////', alpha = 0):
    """
    Stipple points using plt.scatter for where mask = True.
    """
    xData, yData = np.meshgrid(lon, lat)
    ax.contourf(xData, yData, mask, hatches = ['', hatch], 
                transform = transform, zorder = 3, alpha = alpha, levels = [-.5, 0.5, 1.5])


def plot_map_w_stipple(ax, lat, lon, array, hatch_mask, add_cyc=False,
                       contour_style = {}):
    """
    Plot global map with certain areas shaded.
    """
    ax.set_extent([-180, 180, -70, 90])
    ax.coastlines()

    if add_cyc:
        Z_cyc, x_cyc = add_cyclic_point(array, lon)
    else:
        Z_cyc = array
        x_cyc = lon

    # **kwargs examples:
    # levels= np.arange(-0.0004, 0.00041, 0.00002)
    # cmap = 'PiYG'
    # vmin= -0.0004
    # vmax= 0.0004
    cf = ax.contourf(x_cyc, lat, Z_cyc, transform=ccrs.PlateCarree(),
                     **contour_style)
    #cb = plt.colorbar(cf, ax=ax, extend='both', **cbar_style)
    # ---- two-sided t-test at p = 0.05; need to ensure p = two-sided.
    stipple(ax, lat, lon, hatch_mask, transform=ccrs.PlateCarree())
    return ax, cf #, cb


def plot_convenient_taylor(ax, obs, sim, color_list, label_list, **kwargs):
    """
    Given two numpy arrays (1-D or 2-D, in which case the 1st dimension
    is taken as the time dimension and must match), plot the Taylor diagram.

    https://github.com/PeterRochford/SkillMetrics/wiki

    **kwargs pass into SM.taylor_diagram() options other than 'markerfacecolor'
     and 'markerLabel'
    """
    if len(obs.shape) == 1:
        if len(sim.shape) == 1:
            taylor_stats = SM.taylor_statistics(sim, obs)
            SD = taylor_stats['sdev']
            uRMSE = taylor_stats['crmsd']
            Corr = taylor_stats['ccoef']
        else:
            SD = np.empty(1 + sim.shape[1])
            uRMSE = np.empty(1 + sim.shape[1])
            Corr = np.empty(1 + sim.shape[1])
            for i in range(sim.shape[1]):
                taylor_stats = SM.taylor_statistics(sim[:,i], obs)
                ##print(taylor_stats)
                if i == 0:
                    SD[0] = taylor_stats['sdev'][0]
                    uRMSE[0] = taylor_stats['crmsd'][0]
                    Corr[0] = taylor_stats['ccoef'][0]
                SD[i+1] = taylor_stats['sdev'][1]
                uRMSE[i+1] = taylor_stats['crmsd'][1]
                Corr[i+1] = taylor_stats['ccoef'][1]
    else:
        SD = np.empty(1 + sim.shape[1])
        uRMSE = np.empty(1 + sim.shape[1])
        Corr = np.empty(1 + sim.shape[1])
        for i in range(sim.shape[1]):
            taylor_stats = SM.taylor_statistics(sim[:,i], obs[:,i])
            if i == 0:
                SD[0] = taylor_stats['sdev'][0]
                uRMSE[0] = taylor_stats['crmsd'][0]
                Corr[0] = taylor_stats['ccoef'][0]
            SD[i] = taylor_stats['sdev'][1]
            uRMSE[i] = taylor_stats['crmsd'][1]
            Corr[i] = taylor_stats['ccoef'][1]

    plt.sca(ax)
    if (len(obs.shape) == 1) & (len(sim.shape) == 1):
        SM.taylor_diagram(SD, uRMSE, Corr, markercolor = color_list,
                          markerLabel = [label_list], markerObs = 's',
                          titleOBS = label_list[0], rmsLabelFormat = '0:.2f',
                          **kwargs)
    else:
        for i in range(1, sim.shape[1]+1):
            if i == 1:
                SM.taylor_diagram(SD[[0,i]], uRMSE[[0,i]], Corr[[0,i]],
                                  colOBS = color_list[0],
                                  markercolor = color_list[i],
                                  markerLabel = [label_list[0],
                                                 label_list[i]],
                                  markerObs = 's', titleOBS = label_list[0],
                                  rmsLabelFormat = '0:.2f',
                                  **kwargs)
            else:
                SM.taylor_diagram(SD[[0,i]], uRMSE[[0,i]], Corr[[0,i]],
                                  colOBS = color_list[0],
                                  markercolor = color_list[i],
                                  markerLabel = [label_list[0],
                                                 label_list[i]],
                                  overlay = 'on', markerObs = 's',
                                  rmsLabelFormat = '0:.2f',
                                  titleOBS = label_list[0], **kwargs)
