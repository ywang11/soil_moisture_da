import statsmodels.api as sm
import numpy as np
from .cmip6_utils import mrsol_availability
import os
import sys
import xarray as xr


if sys.platform == 'linux':
    path_root = os.environ['PROJDIR'] + '/Soil_Moisture/'
else:
    path_root = 'D:/Projects/2018 Soil Moisture/'


def get_model_list(expr_list, dcm, land_mask, res):
    list_common = set()
    for expr_ind, expr in enumerate(expr_list):
        m_v, _ = mrsol_availability(dcm, land_mask, expr, res)
        if expr_ind == 0:
            list_common = set(m_v)
        else:
            list_common = list_common & set(m_v)
    list_common = list(list_common)
    common = {}
    for i in list_common:
        i2 = i.split('_')
        if not i2[0] in common.keys():
            common[i2[0]] = [i2[1]]
        else:
            common[i2[0]].append(i2[1])
    return common


def ls_trend(vector):
    """
    Least squares trend, regardless whether significant.
    """
    model = sm.OLS(vector, sm.add_constant(np.arange(len(vector))))
    result = model.fit()
    return result.params[1]


def calc_signal(eof1, data_array, L):
    """
    Obtain the linear trend signal of data_array using eof1.
    """
    # Project the time series unto the EOF with weights by square-root of
    # cosine of latitude.
    coslat = np.cos(np.deg2rad(data_array.lat.values)).clip(0., 1.)
    wgts = np.tile(np.sqrt(coslat)[..., np.newaxis], (1, eof1.shape[1]))
    wgts[np.isnan(eof1)] = np.nan
    wgts = wgts / np.nanmean(wgts)

    pc = np.empty(len(data_array.time))
    for t in range(len(data_array.time)):
        pc[t] = np.nansum(data_array[t, :, :] * eof1 * wgts)

    # Obtain the least squares trend time series.
    trend = np.empty(len(data_array.time))
    trend[:] = np.nan
    for t in range(L, len(data_array.time)+1):
        trend[t-1] = ls_trend(pc[(t-L):t])

    return trend


def calc_signal_bylat(eof1, data_array, wgts, L):
    """
    Obtain the linear trend signal of data_array using eof1. But 
    data_array is a 2D matrix with the first dimension being time, and the
    second dimension in eof coordinates, and eof1 is a vector.

    If pc = True, stop at calculating the projection.
    """
    pc = np.zeros(data_array.shape)
    for t in range(data_array.shape[1]):
        pc[:, t] = data_array[:, t] * eof1[t] * wgts[t]
    pc = np.nanmean(pc, axis = 1) * pc.shape[1]

    # Obtain the least squares trend time series.
    trend = np.empty(data_array.shape[0])
    trend[:] = np.nan
    for t in range(0, data_array.shape[0]-L+1):
        trend[t] = ls_trend(pc[t:(t+L)])

    return trend, pc


def by_hemisphere_mask(res):
    """
    Return a dictionary of the boolean mask of each hemisphere.
    """
    res = float(res)
    lat = np.arange(-90 + res/2, 90.1 - res/2, res)
    lon = np.arange(-180 + res/2, 180.1 - res/2, res)
    _, lat2D = np.meshgrid(lon, lat)
    return {'NH': xr.DataArray(lat2D > 0., dims = ['lat', 'lon'],
                               coords = {'lat': lat, 'lon': lon}),
            'SH': xr.DataArray(lat2D < 0., dims = ['lat', 'lon'], 
                               coords = {'lat': lat, 'lon': lon})}


def by_lat_mask(res):
    """
    Return a dictionary of the boolean mask of 10-degree latitude bands.
    """
    lat_median = np.arange(-60, 71, 5)
    lat_str = ['%d' % x for x in lat_median]

    res = float(res)
    lat = np.arange(-90 + res/2, 90.1 - res/2, res)
    lon = np.arange(-180 + res/2, 180.1 - res/2, res)
    lon2D, lat2D = np.meshgrid(lon, lat)

    mask_gen = {}
    for r_ind, r in enumerate(lat_median):
        mask = (lat2D >= (r - 5)) & (lat2D < (r + 5))
        mask_gen[lat_str[r_ind]] = xr.DataArray(mask, dims = ['lat', 'lon'],
                                                coords = {'lat': lat, 
                                                          'lon': lon})
    return mask_gen    


def by_continent_mask(res):
    """
    Return a dictionary of the boolean masks for each continent.
    """
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 'da_masks',
                                        'natural_earth_continent_mask_' + \
                                        res + '.nc'))
    # ---- ID and Abbreviation:
    levels = data.comment.split('; ')
    ids = [int(x.split(' - ')[0]) for x in levels[1:-1]]
    abbrev = [x.split(' - ')[1] for x in levels[1:-1]]
    mask_gen = {}
    for c_ind, c in enumerate(ids):
        mask_gen[abbrev[c_ind]] = np.abs(data.region - c) < 1e-6
    data.close()
    return mask_gen


def by_subcontinent_mask(res):
    """
    Return a dictionary of the boolean masks for each region.
    """
    region = {'NAM': [15, 70, -180, -25],
              'SAM': [-60, 15, -180, -25],
              'EU': [30, 70, -25, 60],
              'WAS': [30, 70, 60, 90],
              'EAS': [30, 70, 90, 180],
              'SAS': [12, 30, 60, 180],
              'NAF': [0, 30, -25, 60],
              'SAF': [-60, 0, -25, 60],
              'AUS': [-60, -12, 60, 180]}
    region_names = list(region.keys())

    res = float(res)
    lat = np.arange(-90 + res/2, 90.1 - res/2, res)
    lon = np.arange(-180 + res/2, 180.1 - res/2, res)
    lon2D, lat2D = np.meshgrid(lon, lat)
    mask_gen = {}
    for r_ind, r in enumerate(region_names):
        mask = (lat2D >= region[r][0]) & (lat2D <= region[r][1]) & \
               (lon2D >= region[r][2]) & (lon2D <= region[r][3])
        mask_gen[r] = xr.DataArray(mask, dims = ['lat', 'lon'],
                                   coords = {'lat': lat, 'lon': lon})
    return mask_gen


def by_biome_mask(res):
    """
    Return a dictionary of the boolean masks for each biome.
    """
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 'da_masks',
                                        'Biomes_' + res + '.nc'))
    # ---- ID and Abbreviation:
    levels = data.biomes.attrs['long_name'].split('; ')
    ids = [int(x.split('-')[0]) for x in levels[:-1]]
    abbrev = [x.split('-')[1] for x in levels[:-1]]

    mask = data.biomes.values

    # ---- re-order because lon was before lat
    if res == '0.5':
        mask = np.transpose(mask, axes = [1,0])

    mask_gen = {}
    for c_ind, c in enumerate(ids):
        mask_gen[abbrev[c_ind]] = xr.DataArray(np.abs(mask - c) < 1e-6,
            dims = ['lat', 'lon'], coords = {'lat': data.lat.values.copy(), 
                                             'lon': data.lon.values.copy()})
    data.close()
    return mask_gen


def by_AI_mask(res):
    """
    Return a dictionary of the aridity index masks.
    """
    # Aridity Index mask:  <0.05 hyper-arid, 0.05-0.2 arid,
    #   0.2-0.5 semiarid, 0.5-0.65 dry subhumid, >=0.65 humid,
    #   following https://www.nature.com/articles/nclimate2837
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 'da_masks',
                                        'Aridity_Index_discrete_' + res + \
                                        '.nc'))
    # ---- ID and Abbreviation:
    levels = data.ai.attrs['Key'].split('; ')
    ids = [int(x.split(': ')[0]) for x in levels[:-1]]
    abbrev = [x.split(': ')[1] for x in levels[:-1]]
    mask_gen = {}
    for c_ind, c in enumerate(ids):
        mask_gen[abbrev[c_ind]] = np.abs(data.ai - c) < 1e-6
    data.close()
    return mask_gen


def CONUS_mask(res):
    """
    Return a bool array that is True in CONUS, and False elsewhere.
    """
    data = xr.open_dataset(os.path.join(path_root, 'intermediate', 'da_masks',
                                        'US_' + res + '.nc'))
    conus = data.region.values.copy()
    # remove Alaska
    conus[:, data.lon.values <= -140.] = np.nan
    # convert to boolean
    conus = ~np.isnan(conus)
    data.close()

    return conus


def read_basin_mask(name, basin_list):
    data = xr.open_dataset(os.path.join(path_root, 'data', 'hydroSHEDS',
                                        name + '.nc'))
    mask = data.region.values.copy()
    data.close()
    mask_list = []
    for b in basin_list:
        mask_list.append( np.abs(mask - b) < 1e-6 )
    return mask_list


def read_Mao_basin_mask(basin_list):
    data = xr.open_dataset(os.path.join(path_root, 'data', 'Mao_basins',
                                        'basins_half.nc'))
    mask = data.basin.values.copy()
    data.close()
    mask_list = []
    for b in basin_list:
        mask_list.append( np.abs(mask - b) < 1e-6 )
    return mask_list
