"""
2020/09/18

Global trend in the observed drivers of soil moisture, 1981-2010 to 
 accomodate GLEAM ET, 1971-2016 for CRU-based P & PET.

2021/02/14
Added global trend 1982-2016 for LAI.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, target_lat, target_lon
import itertools as it
from misc.cmip6_utils import one_layer_availability, mrsol_availability
from misc.analyze_utils import calc_seasonal_trend0, calc_seasonal_mean
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
import sys


def get_flist2(lsm, varname, yrng):
    flist = []
    ylist = []
    for yy in yrng:
        if lsm in ['CRU_v3.20', 'CRU_v3.26', 'CRU_v4.03']:
            fname = os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                 land_mask, lsm, varname + '_' + str(yy) + \
                                 '.nc')
        else:
            fname = os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                 land_mask, lsm + '_' + varname,
                                 lsm + '_' + varname + '_' + str(yy) + \
                                 '.nc')
        if os.path.exists(fname):
            flist.append(fname)
            ylist.append(yy)
    return flist, ylist


def standard_read(lsm, varname, yrng, skip = True):
    # ----- note that the year in the trend skips the first year, because
    #       of DJF.
    if varname == 'pr':
        flist, ylist = get_flist2(lsm, 'pr', range(yrng[0]-1, yrng[-1]+1))
        hr = xr.open_mfdataset(flist, decode_times = False)
        var = hr['pr'].copy(deep = True)
        var['time'] = pd.date_range(str(ylist[0]) + '-01-01',
                                    str(ylist[-1]) + '-12-31', freq = 'MS')
        hr.close()
    elif varname == 'evspsbl':
        hr = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                          land_mask, lsm + '_evspsbl',
                                          'evspsbl_0.5.nc'),
                             decode_times = True)
        var = hr['evspsbl'].copy(deep = True)
        if skip & (lsm == 'GLDAS_Noah2.0'):

            var = var.loc[(var['time'].to_index().year >= (yrng[0]-1)) & \
                          (var['time'].to_index().year <= 2010), :, :]
        else:
            var = var.loc[(var['time'].to_index().year >= (yrng[0]-1)) & \
                          (var['time'].to_index().year <= yrng[-1]), :, :]
        ##var = var.where((var >= 0.) | np.isnan(var), 0)
        ylist = list(np.unique(var['time'].to_index().year))
        hr.close()
    elif varname == 'mrro':
        # ---- Need to add the MsTMIP & TRENDY mrro

        if lsm != 'GRUN':
            raise Exception('Only accept lsm = GRUN')
        flist = os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                             lsm, 'mrro_' + res + '.nc')
        hr = xr.open_mfdataset(flist, decode_times = False)
        hr['time'] = pd.date_range('1950-01-01', '2014-12-31', freq = 'MS')
        var = hr[varname][(hr['time'].to_index().year >= (yrng[0]-1)) & \
                          (hr['time'].to_index().year <= yrng[-1]) \
        ].copy(deep = True)
        hr.close()
    elif varname == 'snw':
        if lsm != 'GlobSnow':
            raise Exception('Only accept lsm = GlobSnow')
        flist = os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                             lsm, 'snw_' + res + '.nc')
        hr = xr.open_mfdataset(flist, decode_times = False)
        hr['time'] = pd.date_range('1979-01-01', '2016-12-31', freq = 'MS')
        var = hr[varname][(hr['time'].to_index().year >= (yrng[0]-1)) & \
                          (hr['time'].to_index().year <= yrng[-1]) \
        ].copy(deep = True)
        hr.close()
    else:
        flist = os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                             lsm, 'lai_' + res + '.nc')
        hr = xr.open_mfdataset(flist, decode_times = False)
        hr['time'] = pd.date_range('1981-07-01', '2016-12-31', freq = 'MS')
        var = hr[varname][(hr['time'].to_index().year >= (yrng[0]-1)) & \
                          (hr['time'].to_index().year <= yrng[-1]) \
        ].copy(deep = True)
        if yrng[0] == 1982:
            # extend to full year for compatibility
            var = xr.DataArray( \
                np.concatenate([np.full([6, var.shape[1], var.shape[2]],
                                        np.nan),
                                var.values], axis = 0),
                                dims = var.dims,
                                coords = {'time': pd.date_range(\
                                    str(yrng[0]-1) + '-01-01',
                                    str(yrng[-1]) + '-12-31', freq = 'MS'),
                                          'lat': hr['lat'],
                                          'lon': hr['lon']})
        hr.close()
    return var


def calc_trend(lsm, var, varname, year_str):
    for s in ['Annual', 'DJF', 'MAM', 'JJA', 'SON']:
        g_map_trend, g_p_values, g_intercepts = calc_seasonal_trend0(var, s)
        xr.Dataset({'g_map_trend': (['lat','lon'], g_map_trend),
                    'g_p_values': (['lat','lon'], g_p_values),
                    'g_intercepts': (['lat','lon'], g_intercepts)},
                   coords = {'lon':target_lon[res], 'lat':target_lat[res]} \
        ).to_netcdf(os.path.join(mg.path_out(), 'obs_drivers_summary',
                                 lsm + '_' + varname + '_' + res + \
                                 '_g_map_trend_' + s + '_' + year_str + '.nc'))


def calc_mean(lsm, var, varname, year_str):
    for s in ['Annual', 'DJF', 'MAM', 'JJA', 'SON']:
        g_map_mean = calc_seasonal_mean(var, s)
        xr.Dataset({'g_map_mean': (['lat','lon'], g_map_mean)},
                   coords = {'lon':target_lon[res], 'lat':target_lat[res]} \
        ).to_netcdf(os.path.join(mg.path_out(), 'obs_drivers_summary',
                                 lsm + '_' + varname + '_' + res + \
                                 '_g_map_mean_' + s + '_' + year_str + '.nc'))


#
res = '0.5'
land_mask = 'vanilla'
lsm_all = ['CERA20C', 'CLASS-CTEM-N', 'CLM4', 'CLM4VIC',
           'CLM5.0', 'ERA20C', 'ERA-Interim', 'ERA5', 'ERA-Land', 'ESA-CCI',
           'GLDAS_Noah2.0', 'GLEAMv3.3a', 'ISAM', 'JSBACH', 'JULES',
           'LPX-Bern', 'ORCHIDEE', 'ORCHIDEE-CNP', 'SiBCASA']
met_to_lsm = {'CRU_v4.03': ['ESA-CCI', 'GLEAMv3.3a'],
              'CRU_v3.20': ['CLASS-CTEM-N', 'CLM4', 'CLM4VIC', 'SiBCASA'],
              'CRU_v3.26': ['CLM5.0', 'ISAM', 'JSBACH', 'JULES', 'LPX-Bern',
                            'ORCHIDEE', 'ORCHIDEE-CNP'],
              'GLDAS_Noah2.0': ['GLDAS_Noah2.0'],
              'ERA-Interim': ['ERA-Interim', 'ERA-Land'],
              'ERA5': ['ERA5'],
              'ERA20C': ['ERA20C'],
              'CERA20C': ['CERA20C']}
lsm_to_met = {}
for a,b in met_to_lsm.items():
    for bb in b:
        lsm_to_met[bb] = a
lsm_to_met = dict(lsm_to_met)
var_list = ['pr', 'evspsbl', 'pr-evspsbl', 'pr-evspsbl-mrro', 'snw']


### 1981-2010 trend
##year = range(1981, 2011)
##year_str = str(year[0]) + '-' + str(year[-1])
##for vv in var_list:
##    if vv == 'snw':
##        var = standard_read('GlobSnow', vv, year)
##        calc_trend('GlobSnow', var, vv, year_str)
##    elif vv == 'pr-evspsbl-mrro':
##        pr = standard_read('CRU_v4.03', 'pr', year)
##        et = standard_read('GLEAMv3.3a', 'evspsbl', year)
##        mrro = standard_read('GRUN', 'mrro', year)
##        var = pr - et - mrro
##        calc_trend('CRU_v4.03', var, vv, year_str)
##    else:
##        if vv == 'pr':
##            lsm_list = sorted(met_to_lsm.keys())
##        else:
##            lsm_list = lsm_all
##        if vv == 'pr-evspsbl':
##            for lsm in lsm_list:
##                if lsm == 'ESA-CCI':
##                    continue
##                pr = standard_read(lsm_to_met[lsm], 'pr', year)
##                et = standard_read(lsm, 'evspsbl', year)
##                var = pr - et
##                calc_trend(lsm, var, vv, year_str)
##        else:
##            for lsm in lsm_list:
##                if (vv == 'evspsbl') & (lsm == 'ESA-CCI'):
##                    continue
##                var = standard_read(lsm, vv, year)
##                calc_trend(lsm, var, vv, year_str)
##
##
### Additionally calculate 1981-2014 trend, focusing on the observed data.
##year = range(1981, 2015)
##year_str = str(year[0]) + '-' + str(year[-1])
##for vv in var_list:
##    if vv == 'pr':
##        lsm = 'CRU_v4.03'
##        var = standard_read(lsm, 'pr', year)
##    elif vv == 'evspsbl':
##        lsm = 'GLEAMv3.3a'
##        var = standard_read(lsm, 'evspsbl', year)
##    elif vv == 'pr-evspsbl':
##        lsm = 'CRU_v4.03'
##        pr = standard_read(lsm, 'pr', year)
##        et = standard_read('GLEAMv3.3a', 'evspsbl', year)
##        var = pr - et
##    elif vv == 'pr-evspsbl-mrro':
##        lsm = 'CRU_v4.03'
##        pr = standard_read(lsm, 'pr', year)
##        et = standard_read('GLEAMv3.3a', 'evspsbl', year)
##        mrro = standard_read('GRUN', 'mrro', year)
##        var = pr - et - mrro
##    elif vv == 'snw':
##        lsm = 'GlobSnow'
##        var = standard_read(lsm, vv, year)
##    calc_trend(lsm, var, vv, year_str)
##
##
### Additionally 1971-2016 trend, focusing on precipitation & P-ET
##year = range(1971, 2017)
##year_str = str(year[0]) + '-' + str(year[-1])
##pr = standard_read('CRU_v4.03', 'pr', year)
##et = standard_read('GLDAS_Noah2.0', 'evspsbl', year, skip = False)
##calc_trend('CRU_v4.03', pr, 'pr', year_str)
##calc_trend('GLDAS_Noah2.0', et, 'evspsbl', year_str)
##calc_trend('CRU_v4.03', pr-et, 'pr-evspsbl', year_str)
##
###
##year = range(1971, 2015)
##year_str = str(year[0]) + '-' + str(year[-1])
##pr = standard_read('CRU_v4.03', 'pr', year)
##et = standard_read('GLDAS_Noah2.0', 'evspsbl', year, skip = False)
##mrro = standard_read('GRUN', 'mrro', year)
##calc_trend('CRU_v4.03', pr, 'pr', year_str)
##calc_trend('GLDAS_Noah2.0', et, 'evspsbl', year_str)
##calc_trend('CRU_v4.03', pr-et, 'pr-evspsbl', year_str)
##calc_trend('CRU_v4.03', pr-et-mrro, 'pr-evspsbl-mrro', year_str)
##
##
##year = range(1980, 2017)
##year_str = str(year[0]) + '-' + str(year[-1])
##pr = standard_read('CRU_v4.03', 'pr', year)
##et = standard_read('GLEAMv3.3a', 'evspsbl', year)
##snw = standard_read('GlobSnow', 'snw', year)
##calc_trend('GLEAMv3.3a', et, 'evspsbl', year_str)
##calc_trend('CRU_v4.03', pr-et, 'pr-evspsbl', year_str)
##calc_trend('GlobSnow', snw, 'snw', year_str)

year = range(1982, 2017)
year_str = str(year[0]) + '-' + str(year[-1])
lai = standard_read("CDR_LAI", "lai", year)
##lai[(lai['time'].to_index().year == 1994) & \
##    (lai['time'].to_index().month >= 10), :, :] = np.nan
calc_trend('CDR_LAI', lai, 'lai', year_str)
calc_mean('CDR_LAI', lai, 'lai', year_str)
