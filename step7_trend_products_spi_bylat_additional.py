"""
20200901

ywang254@utk.edu

Calculate the linear trend in the annual time series, and in the per month
  time series by latitude band during the full period for each CMIP6 model.

1951-1980, 1971-2000, 1987-2016
"""
import pandas as pd
from utils_management.constants import depth_cm_new, lat_median
import utils_management as mg
from misc.standard_diagnostics import get_bylat_trend
import itertools as it
import os
import numpy as np
import multiprocessing as mp


year_str0 = '1950-2016'
res = '0.5'


# Time period to calculate the trend on - MODIFY, which changes the 
# associated set of land surface models.
##year_range_list = [range(1951, 1981), range(1956, 1986), range(1961, 1991),
##                   range(1966, 1996), range(1971, 2001), range(1976, 2006),
##                   range(1981, 2011), range(1987, 2017)]
##year_range_list = [range(1951, 1971), range(1971, 1981), range(1981, 2017)]
year_range_list = [range(1951, 1971), range(1971, 2017),
                   range(1951, 1976), range(1976, 2017),
                   range(1951, 1981), range(1981, 2017),
                   range(1951, 1986), range(1986, 2017)]
year_list = [str(yr[0]) + '-' + str(yr[-1]) for yr in year_range_list]


#
prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all', 'mean_noncmip', 'mean_products']


#
#for dist, lag, i in it.product(['gmm','weibull'], [1, 3, 6], range(2)):
def calc(option):
    dist, lag, i = option
    dcm = depth_cm_new[i]

    #
    trend_collection = pd.DataFrame(data = np.nan, index = lat_median,
        columns = pd.MultiIndex.from_product([prod_list, range(12),
                                              year_list]))
    trend_p_collection = pd.DataFrame(data = np.nan, index = lat_median,
        columns = pd.MultiIndex.from_product([prod_list, range(12),
                                              year_list]))
    for prod, month, yind in it.product(prod_list, range(12),
                                        range(len(year_list))):
        yearstr = year_list[yind]
        year = year_range_list[yind]
        time_range = pd.date_range(start = str(year[0])+'-01-01',
                                   end = str(year[-1])+'-12-31', freq = 'YS')

        fname = os.path.join(mg.path_out(), 'products_spi_summary', dist, 
                             prod + '_' + dcm + '_' + str(month) + '_' + \
                             str(lag) + '_' + res + '_g_lat_ts.csv')
        trend, trend_p = get_bylat_trend(fname, time_range)
        trend_collection.loc[:, (prod, month, yearstr)] = trend
        trend_p_collection.loc[:, (prod, month, yearstr)] = trend_p

    trend_collection.to_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_spi', 'products', dist, 'bylat_trend_' + \
        str(lag) + '_' + res + '_' + dcm + '_additional.csv'))
    trend_p_collection.to_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_spi', 'products', dist, 'bylat_trend_p_' + \
        str(lag) + '_' + res + '_' + dcm + '_additional.csv'))


#
p = mp.Pool(4)
p.map_async(calc, list(it.product(['gmm', 'weibull'], [1,3,6], range(2))))
p.close()
p.join()

#calc(['gmm', 1, 1])
