"""
20210210
ywang254@utk.edu

Use emergent constraint to modify the future signal time series.

Plot the times series of the signal, and threshold of 
   likely (S/N > 0.95, detectable at 66% confidence)
   very likely (S/N > 1.64, 90% confidence)
   virtually certain (S/N > 2.57, 99% confidence).

The thresholds follow Marvel et al. 2019 DOI: 10.1038/s41586-019-1149-8

The EOF is flipped already in the fingerprint calculation step to ensure most
  are negative values.

Historical + RCP85 only. Until 2100.

20210713

Apply GAM or interaction to fit one model for all future preiods.
"""
import matplotlib.pyplot as plt
import pandas as pd
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median # depth_cm
from misc.plot_utils import plot_ts_shade, cmap_gen
import numpy as np
import cartopy.crs as ccrs
from misc.cmip6_utils import mrsol_availability
##from cartopy.mpl.gridliner import Gridliner
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil
import statsmodels.api as stats
import statsmodels.formula.api as smf
#from statsmodels.stats.outliers_influence import summary_table
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from matplotlib.cm import get_cmap
from misc.em_utils import collect_SN_model, collect_SN_obs, get_constrained
import scipy.stats as scistats
import patsy
from pygam import LinearGAM, s, l, te
from mpl_toolkits import mplot3d
from linearmodels import PanelOLS
from numpy import linalg as la


def plot_diagnostic(x1, x2, y, Z, nspace = 12):
    fig, axes = plt.subplots(1, 2, figsize = (6.5, 6.5), 
                             subplot_kw = {'projection': '3d'})
    norm = plt.Normalize(Z.min(), Z.max())
    norm2 = plt.Normalize(-max(abs(Z.max()), abs(Z.min())) * 0.5,
                          max(abs(Z.max()), abs(Z.min())) * 0.5)

    x1_ind = np.lexsort((x1.reshape(-1), x2.reshape(-1)))
    x1 = x1.reshape(-1)[x1_ind].reshape(x2.shape)
    x2 = x2.reshape(-1)[x1_ind].reshape(x1.shape)
    y = y[x1_ind]
    Z = Z.reshape(-1)[x1_ind].reshape(x1.shape)

    #
    ax = axes[0]
    colors = mpl.cm.viridis(norm(y)).reshape(-1, 4)
    ax.scatter3D(x1.reshape(-1), x2.reshape(-1), y, c = colors, s = 2)
    colors = mpl.cm.viridis(norm(Z[::nspace, :]))
    rcount, ccount, _ = colors.shape
    surf = ax.plot_surface(x1[::nspace, :], x2[::nspace, :], Z[::nspace, :],
                           rcount = rcount, ccount = ccount, 
                           facecolors = colors, shade = False)
    surf.set_facecolor((0, 0, 0, 0))
    ax.set_xlabel('Historical S/N ratio')
    ax.set_ylabel('Year')
    ax.set_zlabel('Future S/N ratio')
    ax.view_init(40, 60)

    #
    ax = axes[1]
    Z_resid = y.reshape(x1.shape) - Z
    colors = mpl.cm.RdBu(norm2(Z_resid)).reshape(-1, 4)
    ax.scatter3D(x1, x2, Z_resid, c = colors, s = 2)
    ax.set_xlabel('Historical S/N ratio')
    ax.set_ylabel('Year')
    ax.set_zlabel('Residual of predicted future S/N ratio')
    ax.view_init(40, 60)

    return fig

def nearestPD(A):
    """Find the nearest positive-definite matrix to input

    A Python/Numpy port of John D'Errico's `nearestSPD` MATLAB code [1], which
    credits [2].

    [1] https://www.mathworks.com/matlabcentral/fileexchange/42885-nearestspd

    [2] N.J. Higham, "Computing a nearest symmetric positive semidefinite
    matrix" (1988): https://doi.org/10.1016/0024-3795(88)90223-6
    """

    B = (A + A.T) / 2
    _, s, V = la.svd(B)

    H = np.dot(V.T, np.dot(np.diag(s), V))

    A2 = (B + H) / 2

    A3 = (A2 + A2.T) / 2

    if isPD(A3):
        return A3

    spacing = np.spacing(la.norm(A))
    # The above is different from [1]. It appears that MATLAB's `chol` Cholesky
    # decomposition will accept matrixes with exactly 0-eigenvalue, whereas
    # Numpy's will not. So where [1] uses `eps(mineig)` (where `eps` is Matlab
    # for `np.spacing`), we use the above definition. CAVEAT: our `spacing`
    # will be much larger than [1]'s `eps(mineig)`, since `mineig` is usually on
    # the order of 1e-16, and `eps(1e-16)` is on the order of 1e-34, whereas
    # `spacing` will, for Gaussian random matrixes of small dimension, be on
    # othe order of 1e-16. In practice, both ways converge, as the unit test
    # below suggests.
    I = np.eye(A.shape[0])
    k = 1
    while not isPD(A3):
        mineig = np.min(np.real(la.eigvals(A3)))
        A3 += I * (-mineig * k**2 + spacing)
        k += 1

    return A3


def isPD(B):
    """Returns true when input is positive-definite, via Cholesky"""
    try:
        _ = la.cholesky(B)
        return True
    except la.LinAlgError:
        return False


#Setup / MODIFY
folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
base = 'historical'
expr = 'historical'
expr_name = 'ALL'
season_list = [str(i) for i in range(12)]
season_list_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug',
                    'Sep','Oct','Nov','Dec']
prod = 'mean_noncmip'
lag = 3
start = 1971
L = 2016 - start + 1
limit = False # Not enough ensemble members to estimate the
              # variation around each ESM's mean.
if limit:
    prefix = 'limit_'
else:
    prefix = ''
form = 'gam' # ['lm1', 'lm2', 'gam', 'gls']
dist = 'gmm' # ['gmm', 'weibull']
if form == 'gam':
    spline_order_time = 3 # cubic splines
    spline_order_x = 1 # linear
    n_splines_time_rng = np.arange(spline_order_time + 1, 41, 4)
    n_splines_x_rng = [2] # np.arange(spline_order_x + 1, 11, 2)


res = '0.5'
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'
mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titleweight'] = 'normal'
a, _ = mrsol_availability('0-10cm', 'vanilla', expr, res)
b, _ = mrsol_availability('0-10cm', 'vanilla', 'ssp585', res)
cmip6_list = list(set(a) & set(b))
cmip6_list_models = list(np.unique([t.split('_')[0] for t in cmip6_list]))
cmap = get_cmap('jet')
clist = {model: cmap((1+ind)/len(cmip6_list_models)) for ind, model in enumerate(cmip6_list_models)}
# mlist = ['o', 's', '^', 'v'] # Too few


#
signal_mean_collect, signal_weights_collect, _ = \
    collect_SN_model(prefix, suffix, folder, base, expr, dist,
                     lag, res, season_list, depth_cm_new, start, L, start + 1)
signal_obs_collect = collect_SN_obs(prefix, suffix, folder, base, prod, dist,
                                    lag, res, season_list, depth_cm_new, start, L)


ind = signal_mean_collect[(season_list[0], depth_cm_new[0])].index
col = pd.MultiIndex.from_product([season_list, depth_cm_new])
signal_collect_constrained = pd.DataFrame(np.nan, index = ind, columns = col)
signal_collect_se = pd.DataFrame(np.nan, index = ind, 
                                 columns = pd.MultiIndex.from_product([season_list,
                                                                       depth_cm_new]))
if form == 'lm1':
    signal_collect_params = pd.DataFrame(np.nan, 
                                         index = ['Intercept', 'esm', 'time', 'time:esm'],
                                         columns = col)
    signal_collect_pvals = pd.DataFrame(np.nan, 
                                        index = ['Intercept', 'esm', 'time', 'time:esm'],
                                        columns = col)
elif form == 'lm2':
    signal_collect_params = pd.DataFrame(np.nan, 
                                         index = ['Intercept', 'esm', 'time', 'time:esm',
                                                  'time2', 'time2:esm'],
                                         columns = col)
    signal_collect_pvals = pd.DataFrame(np.nan, 
                                        index = ['Intercept', 'esm', 'time', 'time:esm', 
                                                 'time2', 'time2:esm'],
                                        columns = col)
elif form == 'gam':
    signal_collect_ci = pd.DataFrame(np.nan, index = ind, 
                                     columns = pd.MultiIndex.from_product([season_list,
                                                                           depth_cm_new,
                                                                           ['CI95_lower',
                                                                            'CI95_upper',
                                                                            'CI90_lower',
                                                                            'CI90_upper',
                                                                            'CI80_lower',
                                                                            'CI80_upper']]))
    signal_collect_coef = pd.DataFrame(np.nan, index = range(401),  columns = col)
    signal_collect_coef_se = pd.DataFrame(np.nan,  index = range(401), columns = col)
elif form == 'gls':
    signal_collect_params = pd.DataFrame(np.nan, 
                                         index = ['Break', 'Intercept-1', 'esm-1', 'time-1',
                                                  'time:esm-1',
                                                  'Intercept-2', 'esm-2', 'time-2',
                                                  'time:esm-2'],
                                         columns = col)
    signal_collect_pvals = pd.DataFrame(np.nan, 
                                        index = ['Break', 'Intercept-1', 'esm-1', 'time-1',
                                                 'time:esm-1',
                                                 'Intercept-2', 'esm-2', 'time-2',
                                                 'time:esm-2'],
                                        columns = col)


for season, dcm in it.product(season_list, depth_cm_new):
    if form == 'lm1':
        # (1) prepare data
        y = signal_mean_collect[(season, dcm)].iloc[1:, :].values.reshape(-1, 1)
        x1, x2 = np.meshgrid(signal_mean_collect[(season, dcm)].iloc[0, :].values,
                             signal_mean_collect[(season, dcm)].index[1:].values)
        df = pd.DataFrame( np.concatenate([y, x1.reshape(-1,1), x2.reshape(-1,1)],
                                          axis = 1),
                           columns = ['target', 'esm', 'time'] )

        # (2) fit model
        reg = smf.ols(formula = 'target ~ time * esm', data = df).fit()

        # (3) apply future constraint
        obs_value = np.array([[signal_obs_collect.loc[season, dcm]]])
        obs_t = signal_mean_collect[(season, dcm)].index.values.reshape(-1,1)
        x1_obs, x2_obs = np.meshgrid(obs_value, obs_t)
        X_obs = pd.DataFrame( np.concatenate([x1_obs, x2_obs], axis = 1), 
                              columns = ['esm', 'time'] )
        predict_obs = reg.predict(X_obs)
        predict_se = np.std(reg.resid.values.reshape(-1, len(cmip6_list_models)),
                            axis = 1, ddof = 1)
        predict_params = reg.params
        predict_pvals  = reg.pvalues

        # first value is observation
        predict_obs[0] = signal_obs_collect.loc[season, dcm]
        predict_se = np.insert(predict_se, 0, np.array([0]))

        # (4) save the results
        signal_collect_constrained.loc[:, (season, dcm)] = predict_obs.values
        signal_collect_se.loc[:, (season, dcm)] = predict_se
        signal_collect_params.loc[:, (season, dcm)] = predict_params
        signal_collect_pvals.loc[:, (season, dcm)] = predict_pvals

        # (5) plot the results
        fig = plot_diagnostic(x1, x2, y, Z = reg.predict().reshape(x1.shape), nspace = 12)
        fig.savefig(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc', 'SSI', dist + '_' + \
                                 form + '_fit_' + season + '_' + dcm + '.png'), dpi = 600.,
                    bbox_inches = 'tight')
        plt.close(fig)

    elif form == 'lm2':
        # (1) prepare data
        y = signal_mean_collect[(season, dcm)].iloc[1:, :].values.reshape(-1, 1)
        x1, x2 = np.meshgrid(signal_mean_collect[(season, dcm)].iloc[0, :].values,
                             signal_mean_collect[(season, dcm)].index[1:].values)
        df = pd.DataFrame( np.concatenate([y, x1.reshape(-1,1), x2.reshape(-1,1)],
                                          axis = 1), 
                           columns = ['target', 'esm', 'time'] )
        df['time:esm'] = df['time'] * df['esm']
        df['time2'] = df['time'] * df['time'] / 2000.
        df['time2:esm'] = df['time2'] * df['esm']

        # (2) fit model
        reg = smf.ols(formula = 'target ~ esm + time + time2 + time:esm + time2:esm',
                      data = df).fit()

        # (3) apply future constraint
        obs_value = np.array([[signal_obs_collect.loc[season, dcm]]])
        obs_t = signal_mean_collect[(season, dcm)].index.values.reshape(-1,1)
        x1_obs, x2_obs = np.meshgrid(obs_value, obs_t)
        X_obs = pd.DataFrame( np.concatenate([x1_obs, x2_obs], axis = 1), 
                              columns = ['esm', 'time'] )
        X_obs['time:esm'] = X_obs['time'] * X_obs['esm']
        X_obs['time2'] = X_obs['time'] * X_obs['time'] / 2000.
        X_obs['time2:esm'] = df['time2'] * df['esm']
        predict_obs = reg.predict(X_obs)
        predict_se = np.std(reg.resid.values.reshape(-1, len(cmip6_list_models)),
                            axis = 1, ddof = 1)
        predict_params = reg.params
        predict_pvals  = reg.pvalues

        # first value is observation
        predict_obs[0] = signal_obs_collect.loc[season, dcm]
        predict_se = np.insert(predict_se, 0, np.array([0]))

        # (4) save the results
        signal_collect_constrained.loc[:, (season, dcm)] = predict_obs.values
        signal_collect_se.loc[:, (season, dcm)] = predict_se
        signal_collect_params.loc[:, (season, dcm)] = predict_params
        signal_collect_pvals.loc[:, (season, dcm)] = predict_pvals

        # (5) plot the results
        fig = plot_diagnostic(x1, x2, y, Z = reg.predict().reshape(x1.shape), nspace = 12)
        fig.savefig(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc', 'SSI', dist + '_' + \
                                 form + '_fit_' + season + '_' + dcm + '.png'), dpi = 600.,
                    bbox_inches = 'tight')
        plt.close(fig)
    elif form == 'gam':
        # (1) prepare the data
        y = signal_mean_collect[(season, dcm)].iloc[1:, :].values.reshape(-1, 1)
        x1, x2 = np.meshgrid(signal_mean_collect[(season, dcm)].iloc[0, :].values,
                             signal_mean_collect[(season, dcm)].index[1:].values)
        X = np.concatenate([x1.reshape(-1,1), x2.reshape(-1,1)], axis = 1)

        # (2) fit the model, test multiple options & find the 
        #     smallest AIC
        aic_collect = []
        param_collect = []
        for n_splines_time, n_splines_x in it.product(n_splines_time_rng, n_splines_x_rng):
            gam = LinearGAM(te(0, 1, n_splines = (n_splines_x, n_splines_time),
                               spline_order = (spline_order_x, spline_order_time))).fit(X, y)
            param_collect.append([n_splines_time, n_splines_x])
            aic_collect.append(gam.statistics_['AIC'])
        param = param_collect[np.argmin(aic_collect)]
        gam = LinearGAM(te(0, 1, n_splines = (param[1], param[0]),
                           spline_order = (spline_order_x, spline_order_time))).fit(X, y)

        # (3) apply future constraint
        obs_value = np.array([[signal_obs_collect.loc[season, dcm]]])
        obs_t = signal_mean_collect[(season, dcm)].index.values.reshape(-1,1)
        x1_obs, x2_obs = np.meshgrid(obs_value, obs_t)
        X_obs = np.concatenate([x1_obs, x2_obs], axis = 1)
        predict_obs = gam.predict(X_obs)
        # ---- as linear GAM, this is the same as (y.reshape(-1) - gam.predict(X))
        predict_se = np.std(gam.deviance_residuals(X, y).reshape(-1,
                                                                 len(cmip6_list_models)),
                            axis = 1, ddof = 1)
        predict_ci95 = gam.prediction_intervals(X_obs, width = 0.95)
        predict_ci90 = gam.prediction_intervals(X_obs, width = 0.90)
        predict_ci80 = gam.prediction_intervals(X_obs, width = 0.80)
        predict_params = gam.coef_
        predict_params_se = gam.statistics_['se']

        # first value is observation
        predict_obs[0] = signal_obs_collect.loc[season, dcm]
        predict_se = np.insert(predict_se, 0, np.array([0]))
        predict_ci95[0,:] = [0,0]
        predict_ci90[0,:] = [0,0]
        predict_ci80[0,:] = [0,0]

        # (4) save the results
        signal_collect_constrained.loc[:, (season, dcm)] = predict_obs
        signal_collect_se.loc[:, (season, dcm)] = predict_se
        signal_collect_ci.loc[:, (season, dcm, 'CI95_lower')] = predict_ci95[:,0]
        signal_collect_ci.loc[:, (season, dcm, 'CI95_upper')] = predict_ci95[:,1]
        signal_collect_ci.loc[:, (season, dcm, 'CI90_lower')] = predict_ci90[:,0]
        signal_collect_ci.loc[:, (season, dcm, 'CI90_upper')] = predict_ci90[:,1]
        signal_collect_ci.loc[:, (season, dcm, 'CI80_lower')] = predict_ci80[:,0]
        signal_collect_ci.loc[:, (season, dcm, 'CI80_upper')] = predict_ci80[:,1]
        signal_collect_coef.loc[:(len(predict_params)-1), (season, dcm)] = predict_params
        signal_collect_coef_se.loc[:(len(predict_params)-1), (season, dcm)] = predict_params_se

        # (5) plot the results
        fig = plot_diagnostic(x1, x2, y, gam.predict(X).reshape(x1.shape), nspace = 12)
        fig.savefig(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc', 'SSI', dist + '_' + \
                                 form + '_fit_' + season + '_' + dcm + '_' + \
                                 str(param[0]) + '_' + str(spline_order_time) + \
                                 '_' + str(param[1]) + '_' + str(spline_order_x) + \
                                 '.png'), dpi = 600., bbox_inches = 'tight')
        plt.close(fig)

    elif form == 'gls':
        aic_list = []
        for skip in range(1980, 2025):
            def fit_gls_with_skip(skip, interact_fore = True, interact_aft = True):
                # Before skip
                # (1) prepare data
                y = signal_mean_collect[(season, dcm)].loc[:(skip-1), :].values.reshape(-1, 1)
                x1, x2 = np.meshgrid(signal_mean_collect[(season, dcm)].iloc[0, :].values,
                                     signal_mean_collect[(season, dcm)].loc[:(skip-1), 
                                                                            :].index.values)
                df = pd.DataFrame( np.concatenate([y, x1.reshape(-1,1), x2.reshape(-1,1)],
                                                  axis = 1), 
                                   columns = ['target', 'esm', 'time'] )
                if interact_fore:
                    df['time:esm'] = df['time'] * df['esm']
    
                # (2) fit model
                reg = stats.OLS(df['target'].values,
                                stats.add_constant(df.iloc[:,1:].values)).fit()
                # assumption: the residuals were not correlated over time or across the models.
                omega_temp = np.diagflat(np.diag(np.cov(reg.resid.reshape(x1.shape))))
                omega = np.kron( omega_temp,  np.eye(x1.shape[1]))
                reg = stats.GLS(reg.model.endog, reg.model.exog, sigma = omega).fit()
                aic_fore = reg.aic
                reg_fore = reg

                # After skip
                # (1) prepare data
                y = signal_mean_collect[(season, dcm)].loc[skip:, :].values.reshape(-1, 1)
                x1, x2 = np.meshgrid(signal_mean_collect[(season, dcm)].iloc[0, :].values,
                                     signal_mean_collect[(season, dcm)].loc[skip:,
                                                                            :].index.values)
                df = pd.DataFrame( np.concatenate([y, x1.reshape(-1,1), x2.reshape(-1,1)],
                                                  axis = 1), 
                                   columns = ['target', 'esm', 'time'] )
                if interact_aft:
                    df['time:esm'] = df['time'] * df['esm']
        
                # (2) fit model
                reg = stats.OLS(df['target'].values,
                                stats.add_constant(df.iloc[:,1:].values)).fit()
                # assumption: the residuals were not correlated over time or across the models.
                omega_temp = np.diagflat(np.diag(np.cov(reg.resid.reshape(x1.shape))))
                omega = np.kron( omega_temp,  np.eye(x1.shape[1]))
                reg = stats.GLS(reg.model.endog, reg.model.exog, sigma = omega).fit()
                aic_aft = reg.aic
                reg_aft = reg

                return aic_fore, aic_aft, reg_fore, reg_aft
            aic_fore, aic_aft, _, _ = fit_gls_with_skip(skip)
            aic_list.append(aic_fore + aic_aft)
        skip = 1980 + np.argmin(aic_list)

        if ((season == '7') and (dcm == '0-10cm')) or (season == '8') or \
           ((season == '10') and (dcm == '0-100cm')) or \
           ((season == '11') and (dcm == '0-100cm')):
            _, _, reg_fore, reg_aft = fit_gls_with_skip(skip, interact_fore = False)
        elif ((season == '2') and (dcm == '0-100cm')) or \
             ((season == '3') and (dcm == '0-10cm')) or (season == '4'):
            _, _, reg_fore, reg_aft = fit_gls_with_skip(skip, interact_aft = False)
        else:
            _, _, reg_fore, reg_aft = fit_gls_with_skip(skip)
        signal_collect_params.loc['Break'] = skip
        signal_collect_pvals.loc['Break'] = skip

        # Before skip
        # (3) apply future constraint
        obs_value = np.array([[signal_obs_collect.loc[season, dcm]]])
        obs_t = signal_mean_collect[(season, dcm)].loc[:(skip-1),:].index.values.reshape(-1,1)
        x1_obs, x2_obs = np.meshgrid(obs_value, obs_t)
        X_obs = pd.DataFrame( np.concatenate([x1_obs, x2_obs], axis = 1), 
                              columns = ['esm', 'time'] )
        predict_params = reg_fore.params
        predict_pvals  = reg_fore.pvalues
        if len(predict_params) == 4:
            X_obs['time:esm'] = X_obs['time'] * X_obs['esm']
        predict_obs = reg_fore.predict(stats.add_constant(X_obs.values, has_constant = 'add'))
        predict_se = np.std(reg_fore.resid.reshape(-1, len(cmip6_list_models)), axis = 1,
                            ddof = 1)

        # first value is observation
        predict_obs[0] = signal_obs_collect.loc[season, dcm]
        predict_se[0,:] = 0.

        # (4) save the results
        signal_collect_constrained.loc[:(skip-1), (season, dcm)] = predict_obs
        signal_collect_se.loc[:(skip-1), (season, dcm)] = predict_se
        if len(predict_params) == 3:
            signal_collect_params.loc[['Intercept-1', 'esm-1', 'time-1'],
                                      (season, dcm)] = predict_params
            signal_collect_pvals.loc[['Intercept-1', 'esm-1', 'time-1'],
                                     (season, dcm)] = predict_pvals
        else:
            signal_collect_params.loc[['Intercept-1', 'esm-1', 'time-1',
                                       'time:esm-1'], (season, dcm)] = predict_params
            signal_collect_pvals.loc[['Intercept-1', 'esm-1', 'time-1',
                                      'time:esm-1'], (season, dcm)] = predict_pvals

        # After skip
        # (3) apply future constraint
        obs_value = np.array([[signal_obs_collect.loc[season, dcm]]])
        obs_t = signal_mean_collect[(season, dcm)].loc[skip:,:].index.values.reshape(-1,1)
        x1_obs, x2_obs = np.meshgrid(obs_value, obs_t)
        X_obs = pd.DataFrame( np.concatenate([x1_obs, x2_obs], axis = 1), 
                              columns = ['esm', 'time'] )
        predict_params = reg_aft.params
        predict_pvals  = reg_aft.pvalues
        if len(predict_params) == 4:
            X_obs['time:esm'] = X_obs['time'] * X_obs['esm']
        predict_obs = reg_aft.predict(stats.add_constant(X_obs.values, has_constant = 'add'))
        predict_se = np.std(reg_aft.resid.reshape(-1, len(cmip6_list_models)), axis = 1,
                            ddof = 1)

        # (4) save the results
        signal_collect_constrained.loc[skip:, (season, dcm)] = predict_obs
        signal_collect_se.loc[skip:, (season, dcm)] = predict_se
        if len(predict_params) == 3:
            signal_collect_params.loc[['Intercept-2', 'esm-2', 'time-2'],
                                      (season, dcm)] = predict_params
            signal_collect_pvals.loc[['Intercept-2', 'esm-2', 'time-2'],
                                     (season, dcm)] = predict_pvals
        else:
            signal_collect_params.loc[['Intercept-2', 'esm-2', 'time-2',
                                       'time:esm-2'], (season, dcm)] = predict_params
            signal_collect_pvals.loc[['Intercept-2', 'esm-2', 'time-2',
                                      'time:esm-2'], (season, dcm)] = predict_pvals

        # (5) plot the results
        y = signal_mean_collect[(season, dcm)].values.reshape(-1, 1)
        x1, x2 = np.meshgrid(signal_mean_collect[(season, dcm)].iloc[0, :].values,
                             signal_mean_collect[(season, dcm)].index.values)
        fig = plot_diagnostic(x1, x2, y, 
                              Z = y.reshape(x1.shape) - \
                              signal_collect_se.loc[:, (season, dcm)].values, 
                              nspace = 12)
        fig.savefig(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc', 'SSI',
                                 dist + '_' + \
                                 form + '_fit_' + season + '_' + dcm + '.png'), dpi = 600.,
                    bbox_inches = 'tight')
        plt.close(fig)


signal_collect_constrained.to_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc',
                                               'SSI', prefix + dist + '_' + form + \
                                               '_signal_collect_constrained.csv'))
signal_collect_se.to_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc',
                                      'SSI', prefix + dist + '_' + form + \
                                      '_signal_collect_se.csv'))
if form != 'gam':
    signal_collect_params.to_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc',
                                              'SSI', prefix + dist + '_' + form + \
                                              '_signal_collect_params.csv'))
    signal_collect_pvals.to_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc',
                                             'SSI', prefix + dist + '_' + form + \
                                             '_signal_collect_pvals.csv'))
else:
    signal_collect_ci.to_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc',
                                          'SSI', prefix + dist + '_' + form + \
                                          '_signal_collect_ci.csv'))
    signal_collect_coef.to_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc',
                                            'SSI', prefix + dist + '_' + form + \
                                            '_signal_collect_coef.csv'))
    signal_collect_coef_se.to_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc',
                                               'SSI', prefix + dist + '_' + form + \
                                               '_signal_collect_coef_se.csv'))
