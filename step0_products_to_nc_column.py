"""
20190910

ywang254@utk.edu

Sum the products soil moisture to 0-100cm.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm, depth, target_lat, target_lon
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
from misc.spi_utils import *
from scipy.integrate import quad
import sys


land_mask = 'vanilla'
res = '0.5'
depth_weight = [0.1, 0.2, 0.2, 0.5]


product_list = ['mean_lsm'] # , 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
                # 'em_2cmip', 'em_all']
period = pd.date_range('1950-01-01', '2016-12-31', freq = 'MS')
year = range(1951, 2017)


for prod in product_list:
    for dind, dcm in enumerate(depth_cm):
        h = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                         'interp_merged_products',
                                         prod + '_' + depth[dind] + \
                                         '_' + res + '.nc'),
                            decode_times = False)
        if dind == 0:
            sm = h['sm'].values.copy() * depth_weight[dind]
        else:
            sm += h['sm'].values.copy() * depth_weight[dind]
        h.close()

        #
        sm = xr.DataArray(sm, coords = {'time': period,
                                          'lat': target_lat[res],
                                          'lon': target_lon[res]},
                           dims = ['time', 'lat', 'lon'])

        # Save to file.
        sm.to_dataset(name = 'sm').to_netcdf( os.path.join( \
            mg.path_intrim_out(), 'interp_merged_products',
            prod + '_0.00-1.00_' + res + '.nc') )
