"""
2020/09/15
ywang254@utk.edu

Calculate the by-latitude land-area weighted mean drivers from observations.
"""
from utils_management.constants import drivers, target_lat, target_lon, var_to_name
import utils_management as mg
from misc.analyze_utils import by_lat_avg, by_subcontinent_avg, get_ndvi_mask
from misc.cmip6_utils import one_layer_availability
from glob import glob
import os
import xarray as xr
import pandas as pd
import multiprocessing as mp
import itertools as it
import numpy as np


res = '0.5'
land_mask = 'vanilla'
season_list = [str(m) for m in range(12)]
lag = 3
noSahel = True


if noSahel:
    ndvi_mask = get_ndvi_mask(land_mask)
    suffix = '_noSahel'
else:
    suffix = ''

for vv in sorted(var_to_name.keys()):
    for nn in var_to_name[vv]:
        hr = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'Interp_DA', 'vanilla',
                                          nn, vv + '_' + res + '.nc'))
        for season in season_list:
            if noSahel:
                var = hr[vv].where(ndvi_mask)
            else:
                var = hr[vv]
            
            # Average over the previous two months and the current month.
            if season == '0':
                var = var[int(season)::12, :, :].load()
            elif season == '1':
                var = var[int(season)::12, :, :].load() / 2 + \
                    var[(int(season)-1)::12, :, :].values / 2
            else:
                var = var[int(season)::12, :, :].load() / 3 + \
                    var[(int(season)-1)::12, :, :].values / 3 + \
                    var[(int(season)-2)::12, :, :].values / 3

            if (season == '0') | (season == '1'):
                var[0, :, :] = np.nan

            prefix = os.path.join(mg.path_out(), 'obs_drivers_summary', vv, 
                                  nn + '_' + season)
            by_lat_avg(var, var['time'].to_index(), hr.lat.values, hr.lon.values,
                       prefix, res, suffix)
        hr.close()
