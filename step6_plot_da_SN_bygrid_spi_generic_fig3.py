"""
2021/08/27
ywang254@utk.edu

1. Calculate the STD of noise, across all the piControl models.
2. Calculate the ratio of the products signal to the STD of the noise.
3. Calculate the envelope of the ratio of the CMIP6 models to the noise (95% Gaussian assumption).
4. Plot the ratio, showing only where > 1.96 and > 2.72.
5. Add another layer of alpha shade where the products ratio are outside the envelope of the ratios
   of the CMIP6 models.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, target_lat, target_lon
import itertools as it
from misc.cmip6_utils import one_layer_availability, mrsol_availability
from misc.analyze_utils import calc_reg, get_ndvi_mask
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
import sys
import matplotlib.pyplot as plt
from misc.plot_utils import cmap_div, stipple
import cartopy.crs as ccrs
import matplotlib as mpl


land_mask = 'vanilla'
##prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all']
month_list = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx', 'yy', 'zz', 'aaa', 'bbb', 'ccc', 'ddd', 'eee']
prod_list = ['mean_noncmip', 'mean_products']
lag = 3
noSahel = True
if noSahel:
    ndvi_mask = get_ndvi_mask(land_mask)
    suffix = '_noSahel'
else:
    suffix = ''
dist = 'gmm'
res = '0.5'
res_to = '5'


def calc_noise(dcm):
    cmip6_list, _ = mrsol_availability(dcm, land_mask, 'piControl', res)
    for ind, model in enumerate(cmip6_list):
        if ind == 0:
            sum_of_squares = np.zeros((12, 36, 72))
            sum_of_means = np.zeros((12, 36, 72))
        for season in range(12):
            hr = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_spi_noise', 'bygrid',
                                              model + '_' + res_to + '_' + dcm + '_' + str(lag) + \
                                              '_' + str(season) + suffix + '.nc'))
            sum_of_squares[season,:,:] += np.mean(np.power(hr['g_map_trend'].values, 2), 0)
            sum_of_means[season,:,:] += (np.mean(hr['g_map_trend'].values, axis = 0) / len(cmip6_list))
            hr.close()
    std_noise = np.sqrt( sum_of_squares / len(cmip6_list)  - np.power(sum_of_means,2) )
    std_noise = xr.DataArray(std_noise, dims = ['month', 'lat', 'lon'],
                             coords = {'month': range(12), 'lon': hr['lon'], 'lat': hr['lat']})
    return std_noise


def calc_prod_ratio(dcm, noise):
    hr = xr.open_dataset(os.path.join(mg.path_out(), 'products_spi_signal', 'bygrid',
                                      'gmm_mean_noncmip_' + res_to + '_' + dcm + '_' + str(lag) + suffix + '.nc'))
    SN_prod = hr['g_map_trend'].copy(deep = True) / noise
    hr.close()
    return SN_prod


def calc_cmip6_ratio_envelop(dcm, noise):
    a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
    b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
    cmip6_list = sorted(set(a) & set(b))
    for ind, model in enumerate(cmip6_list):
        if ind == 0:
            ratio_min = np.ones((12, 36, 72)) * 9999
            ratio_max = - np.ones((12, 36, 72)) * 9999
        hr = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_spi_signal', 'bygrid',
                                          'gmm_historical_' + model + '_' + res_to + '_' + dcm + '_' + \
                                          str(lag) + suffix + '.nc'))
        ##print(hr['g_map_trend'].shape)        
        ratio_min = np.minimum(ratio_min, hr['g_map_trend'].values)
        ratio_max = np.maximum(ratio_max, hr['g_map_trend'].values)
        hr.close()
    SN_cmip6_lower = xr.DataArray(ratio_min / noise, dims = ['month', 'lat', 'lon'],
                                  coords = {'month': range(12), 'lon': hr['lon'], 'lat': hr['lat']})
    SN_cmip6_upper = xr.DataArray(ratio_max / noise, dims = ['month', 'lat', 'lon'],
                                  coords = {'month': range(12), 'lon': hr['lon'], 'lat': hr['lat']})
    return SN_cmip6_lower, SN_cmip6_upper


def plot_prod_ratio(ax, SN_ratio):
    cf = ax.contourf(SN_ratio['lon'], SN_ratio['lat'], SN_ratio,
                     cmap = cmap_div(), extend = 'both',
                     levels = [-2.57, -1.96, -1., 0, 1., 1.96, 2.57])
    return cf


def plot_cmip6_shade(ax, SN_ratio, SN_cmip6_lower, SN_cmip6_upper):
    mask = (SN_ratio < SN_cmip6_lower) | (SN_ratio > SN_cmip6_upper)
    ##cf = ax.contourf(mask['lon'], mask['lat'], mask,
    ##                 levels = [0.5, 1.5], colors = 'w', alpha = 0.5)
    stipple(ax, mask['lat'], mask['lon'], mask, transform = ccrs.PlateCarree())
    return None # cf


mpl.rcParams['font.size'] = 6.
mpl.rcParams['axes.titlesize'] = 6
for dcm in depth_cm_new:
    std_noise = calc_noise(dcm)    
    SN_prod = calc_prod_ratio(dcm, std_noise)
    SN_cmip6_lower, SN_cmip6_upper = calc_cmip6_ratio_envelop(dcm, std_noise)

    fig, axes = plt.subplots(4, 3, figsize = (6.5, 4),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    for ii in range(12):
        ax = axes.flat[ii]
        ax.coastlines(lw = 0.5)
        ax.set_extent([-179.75, 179.75, -60, 90])
        cf = plot_prod_ratio(ax, SN_prod[ii, :, :])
        plot_cmip6_shade(ax, SN_prod[ii, :, :], SN_cmip6_lower[ii, :, :], SN_cmip6_upper[ii, :, :])
        ax.set_title(month_list[ii])
        ax.text(0.05, 1.05, lab[ii], fontdict = {'weight': 'bold', 'size': 6}, transform = ax.transAxes)
    cax = fig.add_axes([0.1, 0.05, 0.8, 0.01])
    plt.colorbar(cf, cax = cax, orientation = 'horizontal', label = 'S/N ratio')
    fig.savefig(os.path.join(mg.path_out(), 'figures', 'fig3_' + dist + '_' + str(lag) + \
                             '_bygrid_L=46_' + dcm + '_' + res + suffix + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
