"""
20200514

The SPI of NorESM1-M seems to be much more negative than the other models.

Check if there is a problem with its piControl soil moisture.
"""
import pandas as pd
import numpy as np
import os
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm, year_cmip6
from glob import glob
import matplotlib.pyplot as plt


land_mask = 'vanilla'
model = 'NorESM2-LM'
N = 200
res = '0.5'


fig, axes = plt.subplots(nrows = 2, ncols = 2, figsize = (8, 8))

for dind, dcm in enumerate(depth_cm):
    ###########################################################################
    # Read the piControl soil moisture, globally average.
    ###########################################################################
    fpath = glob(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                              land_mask, 'CMIP6', 'piControl',
                              model + '_r1i1p1f1', 'mrsol_' + res + \
                              '_*_' + dcm + '.nc'))
    # Make sure things are ordered by year.
    fpath = sorted(fpath, key = lambda x: int(x.split('_')[-2]))
    # Subset to the last 201 years.
    fpath = fpath[-N:]

    hr = xr.open_mfdataset(fpath, decode_times = False)
    hr['time'] = pd.date_range('2001-01-01', '2200-12-31', freq = 'MS')
    sm = hr['sm'].resample({'time': '1Y'}).mean(dim = 'time')
    sm_piC = pd.DataFrame(sm.mean(dim = ['lat', 'lon'],
                                  skipna = True).values.copy(),
                          index = range(200), 
                          columns = ['r1i1p1f1'])
    hr.close()


    ###########################################################################
    # Read the historical soil moisture, globally average.
    ###########################################################################
    fpath = [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                          land_mask, 'CMIP6', 'historical',
                          model + '_r1i1p1f1',
                          'mrsol_' + res + '_' + str(y) + \
                          '_' + dcm + '.nc') \
             for y in year_cmip6['historical']]

    hr = xr.open_mfdataset(fpath, decode_times = False)
    hr['time'] = pd.date_range(str(year_cmip6['historical'][0])+'-01-01',
                               str(year_cmip6['historical'][-1])+'-12-31',
                               freq = 'MS')
    sm = hr['sm'].resample({'time': '1Y'}).mean(dim = 'time')
    sm_hist = pd.DataFrame(sm.mean(dim = ['lat', 'lon'],
                                   skipna = True).values.copy(),
                           index = sm['time'].to_index(),
                           columns = ['r1i1p1f1'])
    hr.close()


    ###########################################################################
    # Plot the two in juxtaposition
    ###########################################################################
    ax = axes.flat[dind]
    ax.set_title(dcm)

    h1, = ax.plot(range(200), sm_piC['r1i1p1f1'].values, '-b')
    h2, = ax.plot(range(65), sm_hist['r1i1p1f1'].values, '-r')

ax.legend([h1, h2], ['piControl', 'historical'], ncol = 2,
          loc = (-0.5, -0.3))
fig.savefig(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                         'noise_debug.png'), dpi = 600., bbox_inches = 'tight')
plt.close()
