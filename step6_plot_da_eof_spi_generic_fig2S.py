"""
20190910
ywang254@utk.edu

Plot the EOF of the fingerprint with uncertainty intervals.

Solid with shade - jackknife estimation mean & 95% CI
Dashed - full estimation for comparison
Title - average percentage explained variance and std of jackknife estimation
        (the full estimation already shown in main text)

Use flip of the EOF in the signal projection step.
"""
import matplotlib.pyplot as plt
import pandas as pd
import xarray as xr
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shadex
import numpy as np
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titleweight'] = 'normal'
mpl.rcParams['axes.titlesize'] = 5
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list_list = {'bymonth': [str(i) for i in range(12)],
                    'byyear': ['annual_mean', 'annual_maximum',
                               'annual_minimum',
                               'growing_season_mean', 'growing_season_maximum',
                               'growing_season_minimum']}


folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
season_name = 'bymonth'
lag = 3


season_list = season_list_list[season_name]

# 'hist-nat', 'piControl'
expr_list = ['historical', 'historical-2', 'hist-GHG',
             'hist-aer'] # , 'NoNAT',  'GHGAER', 'hist-nat']
# 'NAT', 'piControl'
expr_list2 = ['ALL', 'ALL-2', 'GHG', 'AER'] # 'ANT', 'GHGAER', 'NAT']


lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


month_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
for limit, dist in it.product([False, True], ['gmm','weibull']):
    if limit:
        prefix = 'limit_'
    else:
        prefix = ''

    flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                    prefix + 'flip_bylat.csv'),
                       index_col = [0,1,2,3])

    fig, axes = plt.subplots(nrows = len(season_list),
                             ncols = len(expr_list) * len(depth_cm_new),
                             figsize = (7.5, len(season_list) / 1.5),
                             sharex = True, sharey = True)
    fig.subplots_adjust(wspace = 0.0, hspace = 0.0)

    for eind, sind, dind in it.product(range(len(expr_list)),
                                       range(len(season_list)),
                                       range(len(depth_cm_new))):
        expr = expr_list[eind]
        season = season_list[sind]
        dcm = depth_cm_new[dind]

        ax = axes[sind, eind + dind * len(expr_list)]

        eof = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                       expr, 'bylat', prefix + \
                                       dist + '_eof_' + dcm + '_' + \
                                       str(lag) + '_' + season + \
                                       '_' + res + suffix + '.csv'),
                          index_col = 0)
        eof.index = eof.index.values.astype(str)
        eof = eof.loc[lat_median, :]
        if flip.loc[(dcm, int(season), dist, lag), expr] < 0:
            eof.loc[:, ['Full', 'Jackknife Estimate','Jackknife CI_lower',
                        'Jackknife CI_upper']] = \
                - eof.loc[:, ['Full', 'Jackknife Estimate',
                              'Jackknife CI_lower', 'Jackknife CI_upper']]
            temp = eof.loc[:, 'Jackknife CI_lower'].values.copy()
            eof.loc[:, 'Jackknife CI_lower'] = \
                eof.loc[:, 'Jackknife CI_upper'].values
            eof.loc[:, 'Jackknife CI_upper'] = temp

        pct = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                       expr, 'bylat', prefix + \
                                       dist + '_pct_' + dcm + \
                                       '_' + str(lag) + '_' + season + \
                                       '_' + res + suffix + '.csv'),
                          index_col = 0)

        h1, h12 = plot_ts_shadex(ax, range(len(lat_median)),
                                 {'min': eof['Jackknife CI_lower'].values,
                                  'mean': eof['Jackknife Estimate'].values,
                                  'max': eof['Jackknife CI_upper'].values},
                                 ts_col = 'r', shade_col = 'r')
        h2, = ax.plot(eof['Full'].values, range(len(lat_median)), '--',
                      color = 'k') # #2b8cbe
        ax.text(0.25, 0.86, ('%.1f' % np.around(pct.loc['Pct',
                                                       'Jackknife Estimate'],
                                               1)) + '%(' \
                + ('%.1f' % np.around(pct.loc['Pct',
                                              'Jackknife Std Err'],1)) \
                + '%)', transform = ax.transAxes, color = 'r')
        ax.axvline(x = 0, ymin = 0, ymax = len(lat_median)-1, color = 'b',
                   ls = ':', lw = 0.5)

        ax.set_xticks([-0.5, 0, 0.5])
        ax.set_yticks(range(2, len(lat_median), 8))
        if not (eind == 0 and dind == 0):
            ax.tick_params('y', length = 0)
        else:
            ax.set_yticklabels(lat_median_name[2::8])
            ax.set_ylabel(month_name[sind])
        if sind != (len(expr_list)-1):
            ax.tick_params('x', length = 0)
        if sind == 0:
            ax.set_title(expr_list2[eind] + ' ' + dcm)
        ax.set_xlim([-0.68, 0.68])
        ax.set_ylim([0, len(lat_median) * 1.2])

        ax.text(0.02, 0.86, lab[eind + dind *len(expr_list)] + str(sind+1),
                transform = ax.transAxes, weight = 'bold')

        ##if (eind == 2) & (sind == 0):
        ##    ax.text(-0.5, 1.3, dcm, transform = ax.transAxes)
        
    ax.legend([h1, h12, h2], ['Jackknife Mean', 'Jackknife 95% CI', 'Main'],
              loc = [-4, -0.5], ncol = 3)
    fig.savefig(os.path.join(mg.path_out(), 'figures', prefix + 'fig2S_' + \
                             dist + '_eof_' + str(lag) + '_' + season_name + \
                             '_' + res + suffix + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
