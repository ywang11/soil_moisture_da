"""
Ensure all files are generated in "step1_cmip6_spi_to_bylat_noSahel.py" & 
                                  "step1_cmip6_spi_to_bylat.py"
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new
import os
import numpy as np
from misc.da_utils import get_model_list
import pandas as pd
import itertools as it
from glob import glob


land_mask = 'vanilla'
res = '0.5'

expr_list = ['historical', 'hist-GHG', 'hist-aer', 'hist-nat', 'piControl',
             'GHGAER', 'NoNAT']
noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
else:
    suffix = ''

f = pd.ExcelWriter(os.path.join(mg.path_out(), 'cmip6_spi_summary',
                                'bylat_checksum' + suffix + '.xlsx'))

for expr in expr_list:
    ok = pd.DataFrame(data = ' ' * 100, index = [],
                      columns = pd.MultiIndex.from_product([depth_cm_new,
                                                            [1, 3, 6],
                                                            ['gmm',
                                                             'weibull']]))

    for dcm, lag, dist in it.product(depth_cm_new, [1, 3, 6],
                                     ['gmm', 'weibull']):
        if expr == 'historical':
            model_list = get_model_list([expr, 'ssp585'], dcm, land_mask, res)
        else:
            model_list = get_model_list([expr], dcm, land_mask, res)

        prefix_path = os.path.join(mg.path_out(), 'cmip6_spi_summary', expr,
                                   dist)
        for m in sorted(model_list.keys()):
            for r in model_list[m]:
                temp = glob(os.path.join(prefix_path, m + '_' + r + \
                                         '_' + dcm + '_' + str(lag) + '_*_' + \
                                         res + '*_g_lat_ts' + suffix + '.csv'))
                if len(temp) != 12:
                    ok.loc[m + '_' + r, (dcm, lag, dist)] = 'Bad'
                else:
                    ok.loc[m + '_' + r, (dcm, lag, dist)] = 'Good'

    ok.to_excel(f, sheet_name = expr)
f.close()
