"""
2020/08/04
ywang254@utk.edu

Create the SREX time series of the soil moisture products, and the validation
 datasets.

2021/05/26
ywang254@utk.edu

Add the SREX time series for the source datasets.

2021/07/02

Copied from soil_moisture_merge, limit to CMIP6 ESMs.

20210706

Use SSI isntead of soil moisture.
"""
import regionmask as rmk
import matplotlib.pyplot as plt
import os
import pandas as pd
import xarray as xr
import numpy as np
import utils_management as mg
from utils_management.constants import depth, depth_cm, depth_cm_new, path_to_final_prod, \
    target_lat, target_lon
from misc.analyze_utils import decode_month_since, get_ndvi_mask
from misc.cmip6_utils import *
from time import time
import itertools as it


# Grid area
hr = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                    'elm_half_degree_grid_negLon.nc'))
area = hr.area.copy(deep = True)
hr.close()


#
srex_mask = rmk.defined_regions.srex.mask(area)
def avg_srex(ssi, which):
    """Average within SREX regions (all are over land)"""
    ssi_srex = pd.DataFrame(data = np.nan,
                           index = ssi['time'].to_index(),
                           columns = [str(sid) for sid in which])
    for sid in which:
        keep = (np.abs(srex_mask.values - sid) < 1e-6) & \
               (~np.isnan(ssi.values[0,:,:]))
        ssi_r = (ssi.where(keep) * area.where(keep)).sum(dim = ['lat','lon']) \
               / np.sum(area.where(keep))
        ssi_srex.loc[:, str(sid)] = ssi_r.values
    return ssi_srex

#
lag = 3
land_mask = 'vanilla'
res = '0.5'
dist = 'gmm'
ref_period = range(1970, 2015)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])
tvec = {'prod': pd.date_range('1951-01-01', '2016-12-31', freq = 'MS'),
        'historical': pd.date_range(start = '1951-01-01',
                                    end = '2100-12-31', freq = 'YS'),
        'hist-GHG': pd.date_range(start = '1951-01-01',
                                  end = '2020-12-31', freq = 'YS'),
        'hist-aer': pd.date_range(start = '1951-01-01',
                                  end = '2020-12-31', freq = 'YS')}
noSahel = True
if noSahel:
    ndvi_mask = get_ndvi_mask(land_mask)


#
"""
prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all']
prod_srex_all = pd.DataFrame(data = np.nan,
                             index = pd.date_range('1970-01-01', '2016-12-31',
                                                   freq = 'MS'),
                             columns = pd.MultiIndex.from_product([ \
    prod_list, depth_cm_new, ['Global'] + [str(sid) for sid in range(1,27)]]))
for prod, dcm in it.product(prod_list, depth_cm_new):
    ssi = xr.DataArray(np.full([len(tvec['prod']), len(target_lat[res]),
                                 len(target_lon[res])], np.nan),
                        dims = ['time', 'lat', 'lon'],
                        coords = {'time': tvec['prod'], 'lat': target_lat[res],
                                  'lon': target_lon[res]})
    for season in range(12):
        fn = os.path.join(mg.path_intrim_out(), 'SPI_Products',
                          'get_' + ref_period_str, dist, prod + '_' + dcm + '_' + \
                          str(lag) + '_' + res + '_' + str(season) + '.nc')
        hr = xr.open_dataset(fn, decode_times = False)
        if noSahel:
            ssi[season::12, :, :] = hr['spi'].where(ndvi_mask).values.copy()
        else:
            ssi[season::12, :, :] = hr['spi'].values.copy()
        hr.close()
    ssi = ssi[(ssi['time'].to_index().year >= 1970) & \
              (ssi['time'].to_index().year <= 2016), :, :]

    prod_srex_all.loc[:, (prod, dcm,
                          'Global')] = (ssi * area).sum(dim = ['lat','lon']) / np.sum(area)
    temp = avg_srex(ssi, range(1,27))
    for sid in range(1,27):
        prod_srex_all.loc[:, (prod, dcm, str(sid))] = temp.loc[:, str(sid)]
prod_srex_all.to_csv(os.path.join(mg.path_out(), 'prod_srex_all_ssi.csv'))
"""

# Source datasets
a, _ = mrsol_availability('0-10cm', 'vanilla', 'historical', res)
b, _ = mrsol_availability('0-10cm', 'vanilla', 'ssp585', res)
cmip6_list = list(set(a) & set(b))


prod_source_all = pd.DataFrame(data = np.nan,
                               index = pd.date_range('1970-01-01', '2016-12-31', freq = 'MS'),
                               columns = pd.MultiIndex.from_product([cmip6_list,
    depth_cm_new, ['Global'] + [str(sid) for sid in range(1,27)]]))
for dcm, nn in it.product(depth_cm_new, cmip6_list):
    start = time()
        
    ssi = xr.DataArray(np.full([len(tvec['historical']), len(target_lat[res]),
                                len(target_lon[res])], np.nan),
                       dims = ['time', 'lat', 'lon'],
                       coords = {'time': tvec['historical'],
                                 'lat': target_lat[res], 
                                 'lon': target_lon[res]})
    for season in range(12):
        fn = os.path.join(mg.path_intrim_out(), 'SPI_Products',
                          'get_' + ref_period_str, dist, nn + '_' + dcm + '_' + \
                          str(lag) + '_' + res + '_' + str(season) + '.nc')
        hr = xr.open_dataset(fn, decode_times = False)
        if noSahel:
            ssi[season::12, :, :] = hr['spi'].where(ndvi_mask).values.copy()
        else:
            ssi[season::12, :, :] = hr['spi'].values.copy()
        hr.close()

    ssi = ssi[(ssi['time'].to_index().year >= 1970) & \
              (ssi['time'].to_index().year <= 2016), :, :]

    prod_source_all.loc[ssi['time'].to_index(), (nn, dcm, 'Global')] = \
        ((ssi * area).sum(dim = ['lat','lon']) / np.sum(area)).values
    temp = avg_srex(ssi, range(1,27))
    for sid in range(1,27):
        prod_source_all.loc[ssi['time'].to_index(),
                            (nn, dcm, str(sid))] = temp.loc[:, str(sid)]
    print(time() - start)
prod_source_all.to_csv(os.path.join(mg.path_out(), 
                                    'validate_gen_ts_cmip6only_ssi.csv'))
