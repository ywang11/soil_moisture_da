"""
20200425
ywang254@utk.edu

Plot the trend in latitudinal mean SPI.
"""
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import lat_median, drivers
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import matplotlib as mpl
from matplotlib import gridspec
import utils_management as mg
import itertools as it


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 6.
mpl.rcParams['hatch.linewidth'] = 0.5


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list = [str(i) for i in range(12)]


folder = 'bylat'
suffix = ''
lag = 3


lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


season_list = [str(i) for i in range(12)]
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']


hs = '|||'
pct = 0.9
lab = 'abcdefghijklmnopqrstuvwxyz'

drivers_name = ['Precipitation', 'Temperature', 'Leaf Area Index', 'Snow Water Equivalent']
drivers_unit = ['mm day$^-1$ year$^{-1}$', '$o$C year$^{-1}$', 'year$^{-1}$', 
                'mm day$^-1$ year$^{-1}$']
expr = 'hist-GHG' # 'hist-GHG', 'hist-aer'

fig = plt.figure(figsize = (8, 3))
gs = gridspec.GridSpec(1, 4, wspace = 0.5, hspace = 0.)
for vind, vv in enumerate(drivers):
    ax = fig.add_subplot(gs[vind])

    if vv == 'pr':
        cmap = 'RdBu'
        levels = np.arange(-0.008, 0.0081, 0.001)
    elif vv == 'tas':
        cmap = 'RdBu_r'
        levels = np.arange(-0.06, 0.061, 0.005)
    elif vv == 'lai':
        cmap = 'RdBu'
        levels = np.arange(-0.006, 0.0061, 0.0005)
    elif vv == 'snw':
        cmap = 'RdBu'
        levels = np.arange(-0.4, 0.41, 0.025)

    # ---- trends of the individual model ensemble members
    data2 = pd.read_csv(os.path.join(mg.path_out(),
                                     'cmip6_drivers_summary', vv, expr,
                                     'bylat_trend_' + res + '.csv'),
                        index_col = 0, header = [0,1])
    data2.index = data2.index.astype(str)
    data2 = data2.dropna(axis = 1)

    # ---- weight of the individual ensemble members
    temp_model = [x.split('_')[0] for x in \
                  data2.columns.get_level_values(0)]
    data2_vote = np.full(data2.shape, np.nan)
    for ind, model in enumerate(temp_model):
        n_model = np.sum([x == model for x in temp_model])
        data2_vote[:, ind] = 1 / n_model
    data2_vote = data2_vote / np.sum(data2_vote, axis = 1).reshape(-1,1) * \
        len(data2.columns.levels[1])

    # ---- weighted ensemble average of the trends
    weighted_avg = (data2 * data2_vote).groupby(level = 1, axis = 1 \
    ).sum().loc[:, season_list]
    h = ax.contourf(range(weighted_avg.shape[1]),
                    range(weighted_avg.shape[0]),
                    weighted_avg.values, cmap = cmap, levels = levels,
                    extend = 'both')
    ax.set_title(drivers_name[vind])
    ax.set_yticks(range(2, weighted_avg.shape[0], 4))
    if vind == 0:
        ax.set_yticklabels(lat_median_name[2::4])
    else:
        ax.set_yticklabels([])
    ax.set_xticks(np.linspace(0.35, 10.65, 12)) # range(0, 12, 1))
    ax.set_xticklabels(season_list_name)
    ax.tick_params(axis = 'y', length = 0.1)
    ax.text(-0.1, 1.03, lab[vind], fontweight = 'bold', transform = ax.transAxes)

    # ---- weighted ensemble total of the consistency in trends
    pct_pos = ((data2 > 0.).astype(float) * data2_vote).groupby( \
        level = 1, axis = 1).sum().loc[:, season_list]
    pct_neg = ((data2 < 0.).astype(float) * data2_vote).groupby( \
        level = 1, axis = 1).sum().loc[:, season_list]
    hatch1 = ((weighted_avg.values > 0) & (pct_pos.values > pct)) | \
        ((weighted_avg.values < 0) & (pct_neg.values > pct))
    stipple(ax, range(hatch1.shape[0]), range(hatch1.shape[1]),
            mask = hatch1, hatch = hs)
    hatch2 = (~hatch1) & \
        (((weighted_avg.values > 0) & (pct_pos.values > (pct-0.1))) | \
         ((weighted_avg.values < 0) & (pct_neg.values > (pct-0.1))))
    stipple(ax, range(hatch2.shape[0]), range(hatch2.shape[1]),
            mask = hatch2, hatch = '---')
    plt.colorbar(h, ax = ax, orientation = 'vertical', label = drivers_unit[vind],
                 shrink = 0.9)
fig.savefig(os.path.join(mg.path_out(), 'figures',
                         'aggregate_cmip6_drivers_bylat_trend_' + expr + '_' + res + \
                         suffix + '.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
