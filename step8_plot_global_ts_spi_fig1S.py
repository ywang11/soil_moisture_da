"""
20200518

Six-month average time series.
"""
import pandas as pd
import numpy as np
import os
from misc.cmip6_utils import mrsol_availability
from utils_management.constants import depth_cm_new
import utils_management as mg
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.cm import get_cmap
from misc.plot_utils import plot_ts_shade
import matplotlib.gridspec as gridspec
from scipy.stats import linregress


def to_half_year(time_ind):
    year = time_ind.year
    # 0 - ONDJFM, 1 - AMJJAS
    half = np.where((time_ind.month.values >= 3) & \
                    (time_ind.month.values <= 8), 1, 0)
    return year * 100 + half


expr_list = ['NoNAT', 'hist-nat', 'hist-GHG', 'hist-aer', 
             'GHGAER']
expr_name = ['ANT', 'NAT', 'GHG', 'AER', 'GHGAER']
prod = 'mean_products'
prod_name = ['Mean Products']
lag = 3
res = '0.5'
land_mask = 'vanilla'
year = range(1971, 2021)
clist = ['r', '#31a354', '#fd8d3c', '#41b6c4', '#dd1c77']
half_year_name = ['ONDJFM', 'AMJJAS']


noSahel = True
if noSahel:
    suffix = '_noSahel'
else:
    suffix = ''


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
yrng = [-1.5, 1.]
lab = 'abcdefghijklmnopqrstuvwxyz'


for dist in ['gmm', 'weibull']:
    fig = plt.figure(figsize = (6.5, 6.5))
    gs = fig.add_gridspec(nrows = 4, ncols = 2, hspace = 0.15,
                          wspace = 0.05)
    for i in range(2):
        dcm = depth_cm_new[i]

        #######################################################################
        # Read the CMIP6 global mean time series.
        #######################################################################
        for eind, expr in enumerate(expr_list):
            if expr == 'historical':
                a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
                b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
                cmip6_list = sorted(set(a) & set(b))
                cmip6_dt = pd.DataFrame(data = np.nan,
                                        index = pd.date_range('1951-01-01',
                                                              '2100-12-31',
                                                              freq = 'MS'),
                                        columns = cmip6_list)
            else:
                cmip6_list, _ = mrsol_availability(dcm, land_mask, expr, res)
                cmip6_list = sorted(cmip6_list)
                cmip6_dt = pd.DataFrame(data = np.nan,
                                        index = pd.date_range('1951-01-01',
                                                              '2020-12-31',
                                                              freq = 'MS'),
                                        columns = cmip6_list)
            for model in cmip6_list:
                dt = pd.read_csv(os.path.join(mg.path_out(),
                                              'standard_diagnostics_spi',
                                              expr, dist,
                                              model + '_' + str(lag) + '_' + \
                                              res + '_' + dcm + '_g_ts' + \
                                              suffix + '.csv'),
                                 index_col = 0, parse_dates = True)
                #dt = dt.loc[dt.index.year <= 2020, :]
                cmip6_dt.loc[:, model] = dt

            cmip6_dt = cmip6_dt.loc[(cmip6_dt.index.year >= year[0]) & \
                                    (cmip6_dt.index.year <= year[-1]), :]

            # ---- convert to six-month
            cmip6_dt = cmip6_dt.groupby( to_half_year(cmip6_dt.index) ).mean()
            # ---- calculate a weighted average and std
            temp_model = [x.split('_')[0] for x in cmip6_dt.columns]
            temp_mean = cmip6_dt.groupby(temp_model,
                                         axis = 1).mean().mean(axis = 1)
            temp_std = cmip6_dt.std(axis = 1)
    
            if eind == 0:
                cmip6_mean = pd.DataFrame(data = np.nan,
                                          index = temp_mean.index,
                                          columns = expr_list)
                cmip6_std = pd.DataFrame(data = np.nan, index = temp_std.index,
                                         columns = expr_list)
            cmip6_mean.loc[temp_mean.index, expr] = temp_mean
            cmip6_std.loc[temp_std.index, expr] = temp_std

        #######################################################################
        # Plot for the winter months and summer months separately.
        #######################################################################
        for j in range(2):
            h = [None] * (len(expr_list))
            eqn = {} # least squares fit equations

            cmip6_mean_temp = cmip6_mean.loc[np.abs(np.mod( \
                cmip6_mean.index.values, 2) - j) < 1e-6, :]
            cmip6_std_temp = cmip6_std.loc[np.abs(np.mod( \
                cmip6_std.index.values, 2) - j) < 1e-6, :]

            #
            for eind, expr in enumerate(expr_list):
                if expr in ['NoNAT', 'hist-nat']:
                    if expr == 'NoNAT':
                        # order is important; hist-nat continues to use this ax
                        ax = fig.add_subplot(gs[i*2+j, 0])
                        lab_temp = lab[(i*2+j)*2 + 0]
                else:
                    if expr == 'hist-GHG':
                        # order is important; hist-aer & GHGAER continues to
                        # use this ax
                        ax = fig.add_subplot(gs[i*2+j, 1])
                        lab_temp = lab[(i*2+j)*2 + 1]
    
                dmin = cmip6_mean_temp.loc[:, expr].values - \
                    1.96 * cmip6_std_temp.loc[:, expr].values
                dmean = cmip6_mean_temp.loc[:, expr].values
                dmax = cmip6_mean_temp.loc[:, expr].values + \
                    1.96 * cmip6_std_temp.loc[:, expr].values
                h[eind], h2 = plot_ts_shade(ax, year,
                                            ts = {'min': dmin, 'mean': dmean,
                                                  'max': dmax},
                                            ts_col = clist[eind],
                                            ln_edge = '-',
                                            shade_col = clist[eind])
                print(dmean)
                eqn[expr] = r', $\Delta$ = ' + \
                    ('{:.2e}'.format(np.mean(dmean[16:46]) - \
                                     np.mean(dmean[:16])) \
                    ).replace('e-0', 'x10$^{-') + '}$'
                ax.text(0.01, 1.04, lab_temp, weight = 'bold',
                        transform = ax.transAxes)
                ax.set_ylim(yrng)
                ax.set_xlim([1971, 2020])
                ax.set_xticks(range(1980, 2011, 10))
                ax.tick_params('both', length = 2., pad = 2.)
                if not ((i == 1) & (j == 1)):
                    ax.set_xticklabels([])
                if eind >= 2:
                    ax.set_yticklabels([])
                else:
                    ax.set_yticks(np.arange(-1., 0.8, 0.5))
                    ax.set_ylabel(half_year_name[j])
                    if j == 0:
                        ax.text(-0.25, 0, dcm, fontsize = 6,
                                rotation = 90,
                                verticalalignment = 'center',
                                transform = ax.transAxes)
    
                if (expr == 'NoNAT') | (expr == 'hist-GHG'):
                    h1 = [h[eind]]
                else:
                    h1.append(h[eind])
                if expr == 'hist-nat':
                    ax.legend(h1 + [h2],
                              ['ANT' + eqn['NoNAT'],
                               'NAT' + eqn['hist-nat'],
                               'Gaussian 95% CI'], ncol = 2,
                              loc = 'lower left',
                              fontsize = 6, labelspacing = 0.1,
                              handlelength = 0.8, handletextpad = 0.5,
                              columnspacing = 1.)
                elif expr == 'GHGAER':
                    ax.legend(h1 + [h2], ['GHG' + eqn['hist-GHG'],
                                          'AER' + eqn['hist-aer'],
                                          'GHGAER' + eqn['GHGAER'],
                                          'Gaussian 95% CI'], ncol = 2,
                              loc = 'lower left',
                              fontsize = 6, labelspacing = 0.1,
                              handlelength = 0.8, handletextpad = 0.5, 
                              columnspacing = 1.)
    fig.savefig(os.path.join(mg.path_out(), 'figures', 'fig1S_' + dist + \
                             '_ts_' + str(lag) + '_' + res + suffix + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
