"""
Ensure all files are generated in step0_cmip6_to_spi_fit.py
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new
import os
import numpy as np
from misc.da_utils import get_model_list
import pandas as pd
import itertools as it
from glob import glob


land_mask = 'vanilla'
res = '0.5'
ref_expr = 'historical'
if ref_expr == 'piControl':
    ref_period = range(0, 201)
else:
    ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


expr_list = ['historical', 'piControl', 'hist-aer', 'hist-nat', 'hist-GHG']

f = pd.ExcelWriter(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                                'get_checksum.xlsx'))

for expr in expr_list:
    ok = pd.DataFrame(data = ' ' * 100,
                      index = [],
                      columns = pd.MultiIndex.from_product([depth_cm_new,
                                                            ['gmm','weibull'],
                                                            [1,3,6]]))

    for dcm, dist, lag in it.product(depth_cm_new, ['gmm', 'weibull'],
                                     [1,3,6]):
        if expr == 'historical':
            model_list = get_model_list([expr, 'ssp585'], dcm, land_mask, res)
        else:
            model_list = get_model_list([expr], dcm, land_mask, res)

        prefix_path = os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                                   'get_'  + ref_expr + '_' + ref_period_str,
                                   dist, expr)

        for m in sorted(model_list.keys()):
            for r in model_list[m]:
                temp = glob(os.path.join(prefix_path, m + '_' + r + \
                                         '_' + dcm + '_' + str(lag) + '_' + \
                                         res + '*'))
                if len(temp) != 12:
                    ok.loc[m + '_' + r, (dcm, dist, lag)] = 'Bad'
                else:
                    ok.loc[m + '_' + r, (dcm, dist, lag)] = 'Good'

    ok.to_excel(f, sheet_name = expr)
f.close()
