"""
202205025
ywang254@utk.edu

Plot the inter-model relationship between temperature trend and S/N ratio
"""
import matplotlib.pyplot as plt
import pandas as pd
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median # depth_cm
from misc.plot_utils import plot_ts_shade, cmap_gen
import numpy as np
import matplotlib as mpl
import itertools as it
import shutil
from scipy.stats import linregress


def read_SN_ratio(sind, season, year_fut):
    flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                    prefix + 'flip_bylat' + suffix + '.csv'),
                       index_col = [0,1,2,3])

    signal_hist = {}
    signal_fut  = {}
    for dcm in depth_cm_new:
        noise_std = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_noise', folder,
                                             base + '_eofs', prefix + dist + '_trend_' + dcm + \
                                             '_' + str(lag) + '_' + season + '_' + res + \
                                             suffix + '.csv'),
                                index_col = 0).loc[:, str(L)]
        noise_std = np.nanstd(noise_std.values, axis = 0)
        
        signal = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_signal', folder,
                                          base + '_eofs', prefix + dist + '_' + expr + \
                                          '_signal_' + dcm + '_' + str(lag) + '_' + \
                                          season + '_' + res + suffix + '.csv'),
                             header = [0,1] \
                             ).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
        signal.index = pd.to_datetime(signal.index).year
        signal_hist[dcm] = signal.loc[year_hist[0], str(L)] * flip.loc[(dcm, sind, dist, lag),
                                                                       base] / noise_std
        signal_fut[dcm] = signal.loc[year_fut[0], str(L)] * flip.loc[(dcm, sind, dist, lag),
                                                                     base] / noise_std
    signal_hist = pd.DataFrame(signal_hist)
    signal_fut  = pd.DataFrame(signal_fut)
    return signal_hist, signal_fut


def read_temperature_trends(season, model_list, year_fut):
    bylat_area = pd.read_csv(os.path.join(mg.path_out(), 'bylat_area.csv'),
                             index_col = 0).loc[:, suffix.replace('_', '')]

    trends = pd.Series(np.nan, index = model_list)
    for mod in model_list:
        data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_drivers_summary', 'tas', 'historical',
                                        mod + '_' + season + '_' + res + '_g_lat_ts.csv'),
                           index_col = 0, parse_dates = True)
        data.index = data.index.year
        data = data.loc[year_fut, :]
        data = (data * bylat_area.values.reshape(1, -1)).sum(axis = 1) / bylat_area.values.sum()
        result = linregress(year_fut, data.values)
        trends.loc[mod] = result.slope
    return trends


def calc_ratios(signal_hist, signal_fut, trends):
    ratios_hist = signal_hist / trends.values.reshape(-1,1)
    ratios_fut  = signal_fut  / trends.values.reshape(-1,1)

    models = [x.split('_')[0] for x in ratios_hist.index]

    ratios_hist_mean = ratios_hist.groupby(models).mean()
    ratios_hist_sd   = ratios_hist.groupby(models).std ()

    ratios_fut_mean  = ratios_fut .groupby(models).mean()
    ratios_fut_sd    = ratios_fut .groupby(models).std ()

    return ratios_hist_mean, ratios_hist_sd, ratios_fut_mean, ratios_fut_sd


#Setup / MODIFY
prod = 'mean_noncmip' # , 'mean_products'] # 'mean_lsm'
prod_name = 'Mean NonCMIP' # , 'Mean Products'] # 'Mean ORS'
folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
base = 'historical'
expr = 'historical'
year_hist = range(1971, 2017)
year_fut_list = [range(1971 + i, 2017 + i) for i in range(10, 84)]
L = year_hist[-1] - year_hist[0] + 1
limit = False
dist = 'gmm'

# Other constants
res = '0.5'
lag = 3
lat_eof = [float(x) for x in lat_median]
season_list = [str(i) for i in range(12)]
season_list_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
expr_name = {'historical': 'ALL', 'NoNAT': 'ANT', 'hist-nat': 'NAT',
             'hist-GHG': 'GHG', 'hist-aer': 'AER', 'GHGAER': 'GHGAER'}
depth_clist = ['b', 'r']
depth_clist2 = ['#756bb1', '#c51b8a']
mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titleweight'] = 'normal'
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


for year_fut in [range(2025, 2071)]: # year_fut_list:
    if limit:
        prefix = 'limit_'
    else:
        prefix = ''
    year_fut_str = str(year_fut[0]) + '-' + str(year_fut[-1])

    fig, axes = plt.subplots(3, 4, figsize = (8, 8), sharex = True, sharey = True)
    for sind, season in enumerate(season_list):
        ax = axes.flat[sind]

        # read the S/N ratio
        signal_hist, signal_fut = read_SN_ratio(sind, season, year_fut)

        # read the global average temperature trends (degC/year)
        trends = read_temperature_trends(season, signal_hist.index, year_fut)

        # calculate the S/N ratio divided by temperature trends, averaged by model
        ratios_hist_mean, ratios_hist_sd, ratios_fut_mean, ratios_fut_sd = \
            calc_ratios(signal_hist, signal_fut, trends)

        # make the plot
        h = [None] * len(depth_cm_new)
        for dind, dcm in enumerate(depth_cm_new):
            h[dind], = ax.plot(ratios_hist_mean[dcm], ratios_fut_mean[dcm], 
                               'o', color = depth_clist2[dind], ms = 2)
            ax.errorbar(ratios_hist_mean[dcm], ratios_fut_mean[dcm], ratios_fut_sd[dcm], 
                        ratios_hist_sd[dcm], ecolor = depth_clist2[dind], elinewidth = 0.5, lw = 0)
            result = linregress(ratios_hist_mean[dcm], ratios_fut_mean[dcm])

            ax.plot([ratios_hist_mean[dcm].min(), ratios_hist_mean[dcm].max()],
                    [result.slope * ratios_hist_mean[dcm].min() + result.intercept, 
                     result.slope * ratios_hist_mean[dcm].max() + result.intercept], '--', 
                    color = depth_clist[dind], lw = 1)

        # adjust the panel appearances
        if sind >= 8:
            ax.set_xlabel('Historical SN-ratio/T$_{trend}$')
        if np.mod(sind, 4) == 0:
            ax.set_ylabel('Future SN-ratio/T$_{trend}$')
        ax.set_title(season_list_name[sind])
        ax.text(-0.05, 1.03, lab[sind], fontweight = 'bold', transform = ax.transAxes)
        if sind == 0:
            ax.legend(h, depth_cm_new)
        ax.set_aspect('equal',adjustable='box')

    # save the figure
    fig.savefig(os.path.join(mg.path_out(), 'figures',
                             prefix + 'SN_to_dT_' + year_fut_str + suffix + '.png'),
                dpi = 600, bbox_inches = 'tight')
    plt.close(fig)
