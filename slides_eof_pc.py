"""
20220130
ywang254@utk.edu

Plot the EOF and PC of the ALL fingerprint.
"""
import matplotlib.pyplot as plt
import pandas as pd
import xarray as xr
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import numpy as np
import cartopy.crs as ccrs
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil
from matplotlib import gridspec


mpl.rcParams['font.size'] = 16
mpl.rcParams['axes.titleweight'] = 'normal'
mpl.rcParams['axes.titlesize'] = 16
mpl.rcParams['hatch.linewidth'] = 0.5
cmap = cmap_div()
cmap2 = mpl.cm.get_cmap('jet')
clist = [cmap2((i+0.5)/12) for i in range(12)]
levels = np.linspace(-0.016, 0.016, 41)
levels2 = np.arange(-0.84, 0.86, 0.04)
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list = [str(i) for i in range(12)]
month_name = ['J','F','M','A','M','J','J','A','S','O','N','D']
month_name2 = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
lag = 3
prefix = '' # 'limit_', ''
base = 'historical'
expr = 'historical'
dist = 'gmm'
pct = 0.9
hs = '|||'


fig = plt.figure(figsize = (8, 8))

gs = gridspec.GridSpec(len(depth_cm_new), 2, hspace = 0.12, wspace = 0.12, width_ratios = [3, 1.5])
axes = {}
axes['fingerprint'] = np.empty(shape=(len(depth_cm_new)), dtype=object)
for ii, dcm in enumerate(depth_cm_new):
    axes['fingerprint'][ii] = fig.add_subplot(gs[ii,1])
axes['pc'] = np.empty(shape = (2), dtype = object)
for ii, dcm in enumerate(depth_cm_new):
    axes['pc'][ii] = fig.add_subplot(gs[ii,0],
                                     sharex = axes['pc'][0],
                                     sharey = axes['pc'][0])

flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                prefix + 'flip_bylat' + suffix + '.csv'),
                   index_col = [0,1,2,3])

# PC
for i, dcm in enumerate(depth_cm_new):
    ax = axes['pc'][i]
    for sind, season in enumerate(season_list):
        pc = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint', expr, 'bylat',
                                      prefix + dist + '_pc_' + dcm + '_' + str(lag) + \
                                      '_' + season + '_' + res + suffix + '.csv'),
                         index_col = 0, parse_dates = True)
        ax.plot(pc.index.year, pc['Full'] * flip.loc[(dcm, int(season), dist, lag), base],
                color = clist[sind])
        ax.set_ylabel(dcm)
        ax.text(0., 1.03, lab[i],
                transform = ax.transAxes, weight = 'bold')
        ax.set_xticks(pc.index.year[::30])
        if i == 0:
            plt.setp(ax.get_xticklabels(), visible=False)
            ax.set_title('PC')
    ax.axvline(1982, lw = 0.5, color = '#2ca25f')
    ax.axvline(1991, lw = 0.5, color = '#2ca25f')
ax.legend(month_name2, loc = [0., -0.5], ncol = 4)

# EOF
for ii in range(len(depth_cm_new)):
    dcm = depth_cm_new[ii]
    ax = axes['fingerprint'][ii]
    eof1_collection = np.full([len(lat_median), len(season_list)], np.nan)
    for sind, season in enumerate(season_list):
        eof = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                       base, 'bylat', prefix + dist + '_eof_' + dcm + '_' + \
                                       str(lag) + '_' + season + '_' + res + suffix + '.csv'),
                          index_col = 0)
        eof.index = eof.index.values.astype(str)
        eof = eof.loc[lat_median, :]

        eof1_collection[:,sind] = eof['Full'].values * \
            flip.loc[(dcm, int(season), dist, lag), base]
    hb = ax.contourf(range(eof1_collection.shape[1]),
                     range(eof1_collection.shape[0]),
                     eof1_collection, cmap = cmap,
                     levels = levels2, extend = 'both')
    ax.set_yticks(range(2, eof1_collection.shape[0], 4))
    ax.set_yticklabels([])
    ax.set_xticks(np.linspace(0.3,10.7,12))
    ax.tick_params(axis = 'both', length = 2., pad = 2.)
    if ii == 1:
        ax.set_xticklabels(month_name)
    else:
        ax.set_xticklabels([])
    if ii == 0:
        if expr == 'hist-nat':
            ax.set_title('NAT Forcings')
        else:
            ax.set_title('ALL Forcings')
    ax.text(0., 1.03, lab[ii+2],
            transform = ax.transAxes, weight = 'bold')
cax = fig.add_axes([0.95, 0.1, 0.01, 0.8])
plt.colorbar(hb, cax = cax, orientation = 'vertical',
             label = 'Fingerprint of ' + str(lag) + '-month SSI')
fig.savefig(os.path.join(mg.path_out(), 'figures',
                         prefix + 'slide_' + dist + '_' + str(lag) + \
                         '_' + res + suffix + '.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)