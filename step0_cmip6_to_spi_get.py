"""
20200529

ywang254@utk.edu

Convert the CMIP6 historical soil moisture to 1-, 3-, and 6-month SSI, 
 following Hao & AghaKouchak 2013 Multivariate Standardized Drought Index:
                                  A parametric multi-index model.
# For the soil moisture pdf, use KDE implemented in sklearn and 5-fold
# cross-validation to find the optimal bandwidth.
Use the fitted parameters for Weibull and GMM distributions to find SPI for
 models that exist in piControl, historical, hist-aer, hist-nat, hist-GHG.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, year_cmip6, target_lat, \
    target_lon
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
from misc.spi_utils import *
from glob import glob
import sys

#
land_mask = 'vanilla'
res = '0.5'
dist_list = ['weibull', 'gmm']


#
ref_expr = 'historical'
if ref_expr == 'piControl':
    ref_period = range(0, 201)
else:
    ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


#
i = REPLACE1 # [0,1,2,3]
dcm = depth_cm_new[i]
expr = 'REPLACE2'
s = REPLACE3 # lag, [1, 3, 6]


print(dcm)
print(expr)
print('lag ' + str(s))


# All the necessary models for HIST85, piControl, hist-aer, hist-nat, hist-GHG
# model_list = get_model_list(['historical'], dcm, land_mask, res)
# sort(model_list.keys())
mlist = ['BCC-CSM2-MR', 'BCC-ESM1', 'CESM2', 'CESM2-WACCM', 'CNRM-CM6-1',
         'CNRM-ESM2-1', 'CanESM5', 'EC-Earth3', 'EC-Earth3-Veg',
         'GISS-E2-1-G', 'GISS-E2-1-H', 'HadGEM3-GC31-LL', 'IPSL-CM6A-LR',
         'MIROC6', 'MPI-ESM1-2-HR', 'NorESM2-LM', 'SAM0-UNICON', 'UKESM1-0-LL']
m_ind = REPLACE4 # {0..17}
m = mlist[m_ind]


# The CMIP6 list of models for individual experiments.
if expr == 'historical':
    model_list = get_model_list(['historical', 'ssp585'], dcm, 
                                land_mask, res)
else:
    model_list = get_model_list([expr], dcm, land_mask, res)

if not m in model_list.keys():
    sys.exit()


# Use 50 years for historical, and 200 years for piControl, for later
# omission of 1st year.
if expr == 'historical':
    period = pd.date_range(str(year_cmip6['historical'][1]) + '-01-01', 
                           str(year_cmip6['ssp585'][-1]) + '-12-31',
                           freq = 'YS')
elif expr == 'piControl':
    period = pd.date_range('2001-01-01', '2200-12-31', freq = 'YS')
elif (expr == 'hist-aer') | (expr == 'hist-nat') | (expr == 'hist-GHG'):
    period = pd.date_range('1951-01-01', '2020-12-31', freq = 'YS')


#
def apply_spi(params, dist, vector, order):
    return (get_zscore(params, dist, vector), order)


#
for r in model_list[m]:
    print(m + '_' + r)

    start = time.time()

    # Handle CMIP6 data reading
    if expr == 'historical':
        fpath = [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                              land_mask, 'CMIP6', expr, m + '_' + r,
                              'mrsol_' + res + '_' + str(y) + \
                              '_' + dcm + '.nc') \
                 for y in year_cmip6['historical']] + \
                [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                              land_mask, 'CMIP6', 'ssp585', m + '_' + r,
                              'mrsol_' + res + '_' + str(y) + \
                              '_' + dcm + '.nc') \
                 for y in year_cmip6['ssp585']]
    elif expr == 'piControl':
        # Use N+1 year in the calculation, so that the first year
        # can be discarded.
        N = 201
        fpath = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                         land_mask, 'CMIP6', expr, m + '_' + r,
                                         'mrsol_' + res + '_*_' + dcm + \
                                         '.nc')),
                       key = lambda x: int(x.split('_')[-2]))
        fpath = fpath[-N:]
    else:
        fpath = [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                              land_mask, 'CMIP6', expr, m + '_' + r,
                              'mrsol_' + res + '_' + str(y) + \
                              '_' + dcm + '.nc') \
                 for y in range(1950, 2021)]

    data = xr.open_mfdataset(fpath, decode_times = False)
    data_array = data.sm.values.copy()
    data_array = data_array.reshape(data_array.shape[0], -1)
    # ---- subset to within the land mask.
    retain = np.where(~np.all(np.isnan(data_array), axis = 0))[0]
    data_array = data_array[:, retain]
    data.close()

    # ---- convert to moving average.
    data_array_apply = rolling_mean(data_array, s)
    # ---- remove the first year.
    data_array_apply = data_array_apply[12:, :]
    # ---- deal with small negative & large positive values
    data_array_apply[(data_array_apply > -1) & \
                     (data_array_apply <= 0.)] = 1e-8
    data_array_apply[(data_array_apply >= (1. - 1e-8))] = 1. - 1e-8


    #######################################################################
    # Handle SSI calculation
    #######################################################################
    for month in range(12):
        print(month)
        data_array_apply2 = data_array_apply[month::12, :]
        data_array_apply2 = np.array_split(data_array_apply2,
                                           data_array_apply2.shape[1],
                                           axis = 1)

        #
        for dist in dist_list:
            # Parameters
            hr = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                              'SPI_CMIP6', 
                                              'fit_' + ref_expr + '_' + \
                                              ref_period_str, dist,
                                              m + '_' + dcm + '_' + str(s) + \
                                              '_' + res + '_' + \
                                              str(month) + '.nc'))
            params = [None] * len(retain)
            if dist == 'weibull':
                a = hr['a'].values.reshape(-1).copy()
                shape = hr['shape'].values.reshape(-1).copy()
                loc = hr['loc'].values.reshape(-1).copy()
                scale = hr['scale'].values.reshape(-1).copy()
                loglik = hr['loglik'].values.reshape(-1).copy()
                for ind, rr in enumerate(retain):
                    params[ind] = {'a': a[rr], 'shape': shape[rr],
                                   'loc': loc[rr], 'scale': scale[rr],
                                   'loglik': loglik[rr]}
            else:
                weights = hr['weights'].values.reshape(2,-1).copy()
                means = hr['means'].values.reshape(2,-1).copy()
                sd = hr['sd'].values.reshape(2,-1).copy()
                loglik = hr['loglik'].values.reshape(-1).copy()
                for ind, rr in enumerate(retain):
                    params[ind] = {'weights': weights[:,rr],
                                   'means': means[:,rr],
                                   'sd': sd[:,rr], 'loglik': loglik[rr]}
            hr.close()

            # Container for output
            spi = np.full([len(data_array_apply2[0]), 
                           len(data.lat) * len(data.lon)], np.nan)

            # Apply z-score calculation
            #p = mp.Pool(min(mp.cpu_count()-1, 16))
            # ---- limit the number of threads for memory issue
            p = mp.Pool(3)
            results = [p.apply_async(apply_spi,
                                     args = (params[order], dist,
                                             data_array_apply2[order],
                                             order)) \
                       for order in range(len(data_array_apply2))]
            p.close()
            p.join()

            #
            results = [result.get() for result in results]
    
            for a in range(len(data_array_apply2)):
                spi[:, retain[results[a][1]]] = results[a][0].reshape(-1)

            spi = spi.reshape(-1, len(target_lat[res]), len(target_lon[res]))

            #
            spi = xr.DataArray(spi, coords = {'time': period,
                                              'lat': target_lat[res],
                                              'lon': target_lon[res]},
                               dims = ['time', 'lat', 'lon'])
    
            # Save to file.
            spi.to_dataset(name = 'spi').to_netcdf( os.path.join( \
                mg.path_intrim_out(), 'SPI_CMIP6', 'get_' + ref_expr + \
                '_' + ref_period_str, dist, expr,
                m + '_' + r + '_' + dcm + '_' + str(s) + '_' + res + '_' + \
                str(month) + '.nc') )

    end = time.time()
    print(('Spent %.6f minutes reading ' + m + '_' + r) % ((end - start)/60))
