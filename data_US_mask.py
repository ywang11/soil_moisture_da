"""
2019/10/31

Create a mask for the contiguous US

https://regionmask.readthedocs.io/en/stable/defined_countries.html
"""
import xarray as xr
import regionmask
import numpy as np
import regionmask
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import os
import utils_management as mg
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


def create(res):
    res_flt = float(res)

    lon = np.arange(-180 + res_flt/2, 180 - res_flt/2 + 0.01, res_flt)
    lat = np.arange(-90 + res_flt/2, 90 - res_flt/2 + 0.01, res_flt)

    ds = xr.Dataset(coords = {'lon': lon, 'lat': lat})

    states = regionmask.defined_regions.natural_earth.us_states_50.mask(ds)
    states[:, :] = np.where(np.isnan(states), -9999, states)
    states = states.astype(int)
    states.attrs = {'_FillValue': -9999}
    
    ds = states.to_dataset()
    
    # attach attribute information
    abbrevs = regionmask.defined_regions.natural_earth.us_states_50.abbrevs
    names = regionmask.defined_regions.natural_earth.us_states_50.names
    numbers = regionmask.defined_regions.natural_earth.us_states_50.numbers
    description = 'Region ID - Abbrevation - Longname; '
    for i in range(len(numbers)):
        description += str(numbers[i]) + ' - ' + str(abbrevs[i]) + ' - ' + \
                       str(names[i]) + '; '
    
    # 
    ds.attrs['description'] = description    
    
    #
    ds.to_netcdf(os.path.join(mg.path_intrim_out(), 'da_masks',
                              'US_' + res + '.nc'))

    #
    ds = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                      'da_masks',
                                      'US_' + res + '.nc'))
    fig, ax = plt.subplots(subplot_kw = {'projection': ccrs.PlateCarree()})
    ax.coastlines()
    ax.set_extent([-180, 0, 0, 90])
    gl = ax.gridlines(draw_labels = True)
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator(np.arange(-180, 1., 20.))
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    ax.contourf(ds.lon, ds.lat, ds.region)
    fig.savefig(os.path.join(mg.path_intrim_out(), 'da_masks',
                             'US_' + res + '.png'))
    ds.close()


# '2.5', '5', '10'
for res in ['0.5']:
    create(res)
