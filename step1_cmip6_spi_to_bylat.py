"""
2020/06/23
ywang254@utk.edu

Calculate the 
 : by-latitude (in 5-degrees, -50 to 70)
 land-area weighted mean soil moisture of the CMIP6 experiments.
 : historical
 : hist-GHG
 : hist-nat
 : hist-aer
 : piControl
"""
from utils_management.constants import depth_cm_new
import utils_management as mg
from misc.analyze_utils import by_continent_avg, by_biome_avg, by_AI_avg, \
    by_lat_avg, by_subcontinent_avg, get_ndvi_mask
from misc.cmip6_utils import mrsol_availability
from glob import glob
import os
import xarray as xr
import pandas as pd
import multiprocessing as mp
import itertools as it
import numpy as np


# MODIFY
# historical, hist-GHG, hist-aer, hist-nat, piControl, GHGAER, NoNAT
expr = 'REPLACE1' 
i = REPLACE2 # [0,1]
dcm = depth_cm_new[i]
s = REPLACE3 # [1 3 6]
dist = 'REPLACE4' # 'gmm', 'weibull'


res = '0.5'
land_mask = 'vanilla'
noSahel = True # True, False


if noSahel:
    ndvi_mask = get_ndvi_mask(land_mask)
    suffix = '_noSahel'
else:
    suffix = ''


ref_expr = 'historical'
if ref_expr == 'piControl':
    ref_period = range(0, 201)
else:
    ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


if expr == 'historical':
    a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
    b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
    cmip6_list = list(set(a) & set(b))
else:
    cmip6_list, _ = mrsol_availability(dcm, land_mask, expr, res)


season_list = [str(m) for m in range(12)]


## Uncomment to DEBUG
#def aggregate(option):
#    l, season = option
for l, season in it.product(cmip6_list, season_list):
    if expr == 'piControl':
        data_time = pd.date_range(start = '2001-01-01',
                                  end = '2200-12-31', freq = 'YS')
    elif expr == 'historical':
        data_time = pd.date_range(start = '1951-01-01', 
                                  end = '2100-12-31', freq = 'YS')
    else:
        data_time = pd.date_range(start = '1951-01-01', 
                                  end = '2020-01-01', freq = 'YS')

    if ('growing_season' in season) | ('annual' in season):
        fn = os.path.join(mg.path_out(), 'cmip6_spi_summary', expr, 
                          l + '_' + dcm + '_' + season + '_' + \
                          str(s) + '_' + res + '.nc')
    else:
        fn = os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                          'get_' + ref_expr + '_' + ref_period_str, dist,
                          expr, l + '_' + dcm + '_' + str(s) + '_' + res + \
                          '_' + season + '.nc')

    if not os.path.exists(fn):
        print(fn + ' does not exist?')
#        return None
        continue

    data = xr.open_dataset(fn, decode_times = False)
    if noSahel:
        spi = data.spi.where(ndvi_mask).values.copy()
    else:
        spi = data.spi.values.copy()
    data.close()

    prefix = os.path.join(mg.path_out(), 'cmip6_spi_summary', expr, dist,
                          l + '_' + dcm + '_' + str(s) + '_' + season)

    by_lat_avg(spi, data_time, data.lat.values, data.lon.values,
               prefix, res, suffix)

#    return None


#pool = mp.Pool(2) #mp.cpu_count() - 2)
#pool.map_async(aggregate, list(it.product(cmip6_list, season_list)))
#pool.close()
#pool.join()
