"""
2019/10/09
ywang254@utk.edu

Calculate the 1981-2010 climatology of the Aridity Index using 
 CRUv4.03 data at 0.5 degrees.
"""
import os
import utils_management as mg
import xarray as xr
from glob import glob
from calendar import monthrange
import itertools as it
import numpy as np
import time


start = time.time()


flist = sorted(glob(os.path.join(mg.path_data(), 'CRU_v4.03',
                                 'cru_ts4.03.*.pet.dat.nc')))
data = xr.open_mfdataset(flist)
pet = data.pet
pet.load()
# ---- unit conversion: mm/day to mm/month
days_in_month = np.array([monthrange(x.year, x.month)[1] for x in \
                          pet.time.indexes['time']])
for i,j in it.product(range(len(pet.lat)), range(len(pet.lon))):
    if np.isnan(pet.values[0, i, j]):
        print('here')
        continue
    pet[:, i, j] *= days_in_month
pet = pet.loc[(data.time.indexes['time'].year >= 1981) & \
              (data.time.indexes['time'].year <= 2010), :, :].mean(dim='time')
# ---- fill the PET = 0 grids with NaN
pet2 = pet.copy(deep = True)
pet2.values = np.where(pet2.values < 1e-6, np.nan, pet2.values)


flist2 = sorted(glob(os.path.join(mg.path_data(), 'CRU_v4.03',
                                  'cru_ts4.03.*.pre.dat.nc')))
data2 = xr.open_mfdataset(flist2)
pre = data2.pre.loc[(data.time.indexes['time'].year >= 1981) & \
                    (data.time.indexes['time'].year <= 2010), :, 
                    :].mean(dim = 'time')
pre.load()

ai = pre / pet2


ai.to_dataset(name = 'ai').to_netcdf(os.path.join(mg.path_intrim_out(), 
                                                  'da_masks',
                                                  'Aridity_Index_0.5.nc'),
                                     encoding = {'ai': {'_FillValue': 1e20}})

data.close()
data2.close()


end = time.time()
print( (end - start) / 3600 )
