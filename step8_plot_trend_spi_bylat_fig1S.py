"""
20200425
ywang254@utk.edu

Plot the trend in latitudinal mean SPI.
"""
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import matplotlib as mpl
import utils_management as mg
import itertools as it


mpl.rcParams['font.size'] = 6.
mpl.rcParams['axes.titlesize'] = 6.
mpl.rcParams['hatch.linewidth'] = 0.5


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list = [str(i) for i in range(12)]


folder = 'bylat'
suffix = '_noSahel' # ['', '_noSahel']
lag = 3


expr_list = ['NoNAT','hist-nat','hist-GHG','hist-aer','GHGAER']
expr_list_name = ['ANT','NAT','GHG','AER','GHGAER']


lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


season_list = [str(i) for i in range(12)]
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']


cmap = cmap_div()
levels = np.arange(-1.5e-2, 1.51e-2, 3e-3)
hs = '|||'
pct = 0.9
lab = 'abcdefghijklmnopqrstuvwxyz'


for limit, dist in it.product([False, True], ['gmm', 'weibull']):
    if limit:
        prefix = 'limit_'
    else:
        prefix = ''

    fig, axes = plt.subplots(nrows = len(depth_cm_new),
                             ncols = len(expr_list),
                             figsize = (6.5, len(depth_cm_new) * 3),
                             sharex = True, sharey = True)
    fig.subplots_adjust(wspace = 0.01, hspace = 0.1)
    for i, dcm in enumerate(depth_cm_new):
        for j, expr in enumerate(expr_list):
            ax = axes[i, j]

            # ---- trends of the individual model ensemble members
            data2 = pd.read_csv(os.path.join(mg.path_out(),
                                             'standard_diagnostics_spi',
                                             expr, dist, prefix + \
                                             'bylat_trend_' + \
                                             str(lag) + '_' + res + '_' + \
                                             dcm + suffix + '.csv'),
                                index_col = 0, header = [0,1])
            data2.index = data2.index.astype(str)
    
            # ---- weight of the individual ensemble members
            temp_model = [x.split('_')[0] for x in \
                          data2.columns.get_level_values(0)]
            data2_vote = np.full(data2.shape, np.nan)
            for ind, model in enumerate(temp_model):
                n_model = np.sum([x == model for x in temp_model])
                data2_vote[:, ind] = 1 / n_model
            data2_vote = data2_vote / \
                np.sum(data2_vote, axis = 1).reshape(-1,1) * \
                len(data2.columns.levels[1])

            # ---- weighted ensemble average of the trends
            weighted_avg = (data2 * data2_vote).groupby(level = 1, axis = 1 \
            ).sum().loc[:, season_list]
            h = ax.contourf(range(weighted_avg.shape[1]),
                            range(weighted_avg.shape[0]),
                            weighted_avg.values, cmap = cmap, levels = levels,
                            extend = 'both')
            ax.set_yticks(range(2, weighted_avg.shape[0], 4))
            ax.set_yticklabels(lat_median_name[2::4])
            ax.set_xticks(np.linspace(0.35, 10.65, 12)) # range(0, 12, 1))
            if i == 0:
                ax.set_xticklabels([])
            else:
                ax.set_xticklabels(season_list_name)
            ax.tick_params(axis = 'y', length = 0.1)
            if i == 0:
                ax.set_title(expr_list_name[j])
            ax.text(0.1, 1.03, lab[i*len(expr_list)+j],
                    transform = ax.transAxes, weight = 'bold')
            if j == 0:
                ax.set_ylabel(dcm)

            # ---- weighted ensemble total of the consistency in trends
            pct_pos = ((data2 > 0.).astype(float) * data2_vote).groupby( \
                level = 1, axis = 1).sum().loc[:, season_list]
            pct_neg = ((data2 < 0.).astype(float) * data2_vote).groupby( \
                level = 1, axis = 1).sum().loc[:, season_list]
            hatch1 = ((weighted_avg.values > 0) & (pct_pos.values > pct)) | \
                ((weighted_avg.values < 0) & (pct_neg.values > pct))
            stipple(ax, range(hatch1.shape[0]), range(hatch1.shape[1]),
                    mask = hatch1, hatch = hs)
            hatch2 = (~hatch1) & \
                (((weighted_avg.values > 0) & (pct_pos.values > (pct-0.1))) | \
                 ((weighted_avg.values < 0) & (pct_neg.values > (pct-0.1))))
            stipple(ax, range(hatch2.shape[0]), range(hatch2.shape[1]),
                    mask = hatch2, hatch = '---')

    cax = fig.add_axes([0.1, 0.045, 0.8, 0.015])
    plt.colorbar(h, cax = cax, orientation = 'horizontal', ticks = levels,
                 label = 'Trend of ' + str(lag) + '-month SSI (year$^{-1}$)')
    cax.text(-0.05, -1., 'Dry', fontsize = 6, transform = cax.transAxes,
             horizontalalignment = 'center') # , color = '#762a83')
    cax.text(1.05,  -1., 'Wet', fontsize = 6, transform = cax.transAxes,
             horizontalalignment = 'center') # , color = '#3288bd')
    fig.savefig(os.path.join(mg.path_out(), 'figures', prefix + \
                             'fig1S_bylat_' + dist + '_trend_' + str(lag) + \
                             '_' + res + suffix + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
