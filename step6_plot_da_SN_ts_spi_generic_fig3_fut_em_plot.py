"""
2021/02/19
ywang254@utk.edu

Plot the S/N ratio of the signal, and threshold of 
   likely (S/N > 0.95, detectable at 66% confidence)
   very likely (S/N > 1.64, 90% confidence)
   virtually certain (S/N > 2.57, 99% confidence).

Before and after emergent constraint.

The thresholds follow Marvel et al. 2019 DOI: 10.1038/s41586-019-1149-8

The EOF is flipped already in the fingerprint calculation step to ensure most
  are negative values.

Historical + RCP85 only. Until 2100.
"""
import matplotlib.pyplot as plt
import pandas as pd
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median # depth_cm
from misc.plot_utils import plot_ts_bracket, cmap_gen # plot_ts_shade
import numpy as np
import cartopy.crs as ccrs
##from cartopy.mpl.gridliner import Gridliner
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil
from misc.em_utils import collect_SN_em, collect_SN_em_interaction
import scipy.stats as scistats
from matplotlib import font_manager
for font in font_manager.findSystemFonts('/ccs/home/yaoping/.local/share/fonts'):
    font_manager.fontManager.addfont(font)

mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titleweight'] = 'normal'
mpl.rcParams['font.family'] = 'arial'
mpl.rcParams['font.sans-serif'] = ['Arial']

res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list_list = {'bymonth': [str(i) for i in range(12)],
                    'byyear': ['annual_mean', 'annual_maximum',
                               'annual_minimum',
                               'growing_season_mean', 'growing_season_maximum',
                               'growing_season_minimum']}


#clist = [(0.00392156862745098, 0.45098039215686275, 0.6980392156862745),
#         (0.8352941176470589, 0.3686274509803922, 0.0)] # ['#377eb8', '#e41a1c']
#clist2 = [(0.00784313725490196, 0.6196078431372549, 0.45098039215686275),
#          (0.8, 0.47058823529411764, 0.7372549019607844)] # ['#984ea3', '#4daf4a']
clist = ['#08519c', '#810f7c']
clist2 = ['#4292c6', '#fc9272']
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


#Setup / MODIFY
folder = 'bylat'
suffix = '_noSahel'
season_name = 'bymonth'
base = 'historical'
expr = 'historical'
expr_name = 'ALL'
season_list = season_list_list[season_name]
season_list_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
lag = 3
start = 1971
L = 2016 - start + 1
#method = 'gam'
limit = False
if limit:
    prefix = 'limit_'
else:
    prefix = ''


for dist in ['gmm']: # ['gmm', 'weibull']:
    #
    signal_em = collect_SN_em_interaction(dist, season_list, depth_cm_new)

    #
    flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                    prefix + 'flip_bylat' + suffix + '.csv'),
                       index_col = [0,1,2,3])

    #
    fig, axes = plt.subplots(ncols = 4, nrows = 3, figsize = (6.5, 5.5),
                             sharex = False, sharey = True)
    fig.subplots_adjust(wspace = 0.05, hspace = 0.15)
    for sind, season in enumerate(season_list):
        ax = axes.flat[sind]

        ###################################################################
        # Signal/Noise ratio time series
        ###################################################################
        h = [None] * 6
        for dind, dcm in enumerate(depth_cm_new):
            # CMIP6 noises
            noise_std = np.nanstd(pd.read_csv(os.path.join(mg.path_out(),
                'cmip6_spi_noise', folder, base + '_eofs', prefix + dist + \
                '_trend_' + dcm + '_' + str(lag) + '_' + season + '_' + \
                res + suffix + '.csv'), index_col = 0 \
            ).loc[:, str(L)].values, axis = 0)

            # CMIP6 signals
            signal_hist = pd.read_csv(os.path.join(mg.path_out(),
                'cmip6_spi_signal', folder, base + '_eofs', prefix + \
                dist + '_' + expr + '_signal_' + dcm + '_' + \
                str(lag) + '_' + season + '_' + res + suffix + '.csv'),
                                      header = [0,1] \
            ).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
            signal_hist.index = pd.to_datetime(signal_hist.index).year
            signal_hist = signal_hist.loc[start:(2100-L+1), str(L)]
            signal_hist = signal_hist * flip.loc[(dcm, sind, dist, lag), base] / noise_std
            # ---- need to weight the different models
            temp_model = [x.split('_')[0] for x in signal_hist.columns]
            signal_mean = signal_hist.groupby(temp_model, axis = 1).mean().mean(axis = 1)
            signal_std = signal_hist.std(axis = 1)
    
            # CMIP6 models
            ##h[dind + len(depth_cm_new)], _ = \
            ##    plot_ts_shade(ax, signal_hist.index.values, 
            ##                  ts = {'min': signal_mean.values - \
            ##                        1.96 * signal_std.values,
            ##                        'mean': signal_mean.values,
            ##                        'max': signal_mean.values + \
            ##                        1.96 * signal_std.values},
            ##                  ts_col = clist2[dind], ln_main = '--',
            ##                  ln_edge = '--', shade_col = clist2[dind],
            ##                  alpha = 0, lw_main = 1)
            h[dind + len(depth_cm_new)], _ = plot_ts_bracket(ax, signal_hist.index.values, 
                                                             ts = {'min': signal_mean.values - \
                                                                   1.96 * signal_std.values,
                                                                   'mean': signal_mean.values,
                                                                   'max': signal_mean.values + \
                                                                   1.96 * signal_std.values},
                                                             ts_col = clist2[dind], ln_main = '--',
                                                             ln_edge = '--', edge_col = clist2[dind],
                                                             lw_main = 1, lw_edge = 1)


            # Constrained results
            signal_em_mean = signal_em[(season, dcm)].loc[:, 'mean']
            signal_em_lower = signal_em[(season, dcm)].loc[:, 'CI95_lower']
            signal_em_upper = signal_em[(season, dcm)].loc[:, 'CI95_upper']
            ##h[dind], h[2*len(depth_cm_new)] = \
            ##    plot_ts_shade(ax, signal_em_mean.index.values,
            ##                  ts = {'min': signal_em_lower,
            ##                        'mean': signal_em_mean,
            ##                        'max': signal_em_upper},
            ##                  ts_col = clist[dind], ln_main = '-',
            ##                  ln_edge = '-', shade_col = clist[dind],
            ##                  alpha = 0.07, lw_main = 1)
            h[dind], h[2*len(depth_cm_new)] = plot_ts_bracket(ax, signal_em_mean.index.values, 
                                                              ts = {'min': signal_em_lower,
                                                                    'mean': signal_em_mean,
                                                                    'max': signal_em_upper},
                                                              ts_col = clist[dind], ln_main = '-',
                                                              ln_edge = '-', edge_col = clist[dind],
                                                              lw_main = 1, lw_edge = 1)

            
            ### Add a layer where constrained slope significant
            ##temp = signal_em_mean.copy()
            ##temp[signal_em_p > 0.05] = np.nan
            ##h[dind], = ax.plot(signal_hist.index.values,
            ##                   temp, color = clist[dind], lw = 2)

        h[1+2*len(depth_cm_new)] = ax.axhline(1.96, linestyle = '-', color = 'k',
                                              linewidth = 0.5, zorder = 1)
        #h[-1] = ax.axhline(2.57, linestyle = '-', color = 'k',
        #                   linewidth = 1., zorder = 1)
        ax.axhline(0, linestyle = '--', color = 'k', linewidth = 0.5)

        ax.set_title(season_list_name[int(season)], pad = 3.)
        if (sind == 0) | (sind == 4) | (sind == 8):
            #ax.set_ylabel(dcm)
            ax.tick_params(axis = 'y', length = 2., pad = 2.)
        else:
            ax.tick_params(axis = 'y', length = 2.)
        ax.set_xlim([signal_hist.index[0],
                     signal_hist.index[-1]])
        ax.set_xticks([1980, 2000, 2020, 2040])
        ax.tick_params(axis = 'x', length = 2., pad = 2.)
        if (sind <= 7):
            ax.set_xticklabels([])
        else:
            ax.set_xticklabels([1980, 2000, 2020, 2040])
        ax.text(0.01, 1.03, lab[sind], transform = ax.transAxes,
                weight = 'bold')
    # [r'Constrained ALL ' + x + ' (p $\leq$ 0.05)' for x in depth_cm_new] + \
    # ['Constrained ALL ' + x + ' (p > 0.05)' for x in depth_cm_new] + \
    ax.legend(h, ['Constrained ALL ' + x for x in depth_cm_new] + ['ALL ' + x for x in depth_cm_new] + \
              ['95% CI of models/constrained', 'p = 0.05 threshold'],
              ncol = 3, loc = 'lower center', bbox_to_anchor = (-1.1, -0.4))
    fig.savefig(os.path.join(mg.path_out(), 'figures', prefix + 'fig3_fut_' + \
                             dist + '_' + str(lag) + '_' + season_name + \
                             '_L=' + str(L) + '_' + res + suffix + '.png'), # '_' + method
                dpi = 600., bbox_inches = 'tight')
    fig.savefig(os.path.join(mg.path_out(), 'figures', prefix + 'fig3_fut_' + \
                             dist + '_' + str(lag) + '_' + season_name + \
                             '_L=' + str(L) + '_' + res + suffix + '.pdf'), # '_' + method
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
