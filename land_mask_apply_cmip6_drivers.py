"""
20200914
ywang254@utk.edu

Apply the common land mask of the interpolated CMIP6 models after their
interpolation to 0.5oC.
"""
import sys
import os
import xarray as xr
import numpy as np
import pandas as pd
from glob import glob
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import utils_management as mg
from utils_management.constants import year_cmip6, drivers
from misc.cmip6_utils import one_layer_availability
import multiprocessing as mp
import gc
import time


res = '0.5'
target_lat = np.arange(-89.75, 89.76, 0.5)
target_lon = np.arange(-179.75, 179.76, 0.5)


hr = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'land_mask_minimum', 'cmip6_da.nc'))
mask_check = hr['mask'].copy(deep = True)
hr.close()


# 
for expr in ['historical', 'ssp585', 'hist-GHG', 'hist-aer']:
    for vv in drivers:
        ##if not vv == 'tas':
        ##    continue

        cmip6_list, _ = one_layer_availability(vv, 'None', expr, res)
        cmip6_list = sorted(cmip6_list)

        for model in cmip6_list:
            ##if not 'BCC-CSM2-MR' in model:
            ##    continue

            flist = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                             'None', 'CMIP6', expr, model,
                                             vv + '_' + res + '_*.nc')))

            if not os.path.exists(os.path.join(mg.path_intrim_out(),
                                               'Interp_DA', 'vanilla', 'CMIP6',
                                               expr, model)):
                os.mkdir(os.path.join(mg.path_intrim_out(),
                                      'Interp_DA', 'vanilla', 'CMIP6',
                                      expr, model))

            #for ff in flist:
            def mask(ff):
                try:
                    hr = xr.open_dataset(ff, decode_times = False)
                    var = hr[vv].where(mask_check).load()
                    hr.close()
                    var.to_dataset(name = vv).to_netcdf(ff.replace('None',
                                                                   'vanilla'))
                except:
                    print(ff + ' was not successful')
                time.sleep(0.5)
                gc.collect()
            p = mp.Pool(5)
            p.map_async(mask, flist)
            p.close()
            p.join()
