""" Check future trends of the drivers of soil moisture."""
import numpy as np
import statsmodels.api as stats
import pandas as pd
import os
import matplotlib.pyplot as plt
from glob import glob
import itertools as it
from utils_management.constants import *
import utils_management as mg
from analyze_cmip6_drivers_map import get_model_list
from misc.plot_utils import cmap_div
import multiprocessing as mp


def calc_trend(data):
    data.index = data.index.year
    trend = pd.DataFrame(np.nan, index = range(2016, 2101), columns = data.columns)
    for ii in range(2016, 2101):
        for jj in data.columns:
            res = stats.OLS(data.loc[(ii-45):ii, jj].values, stats.add_constant(range(46))).fit()
            trend.loc[ii, jj] = res.params[1]
    return trend


def calc_mean(data):
    data.index = data.index.year
    mean = pd.DataFrame(np.nan, index = range(2016, 2101), columns = data.columns)
    for ii in range(2016, 2101):
        mean.loc[ii, :] = data.loc[(ii-45):ii, :].values.mean(axis = 0)
    return mean


def plot_trend(ax, trend, levels):
    cf = ax.contourf(trend.columns, trend.index, trend, 
                     levels = levels,
                     extend = 'both', cmap = cmap_div())
    ax.set_xticks(trend.columns[::10])
    return cf


def plotter(option):
    vv, season, opt = option

    fig, axes = plt.subplots(3, 3, sharex = True, sharey = True, figsize = (6.5, 6.5))
    for mind, model in enumerate(cmip6_list):
        flist = glob(os.path.join(mg.path_out(), 'cmip6_drivers_summary', 
                                  vv, 'historical',  model + '_*_' + \
                                  str(season) + '_0.5_g_lat_ts.csv'))
        for count, ff in enumerate(flist):
            data = pd.read_csv(ff, index_col = 0, parse_dates = True)
            if count == 0:
                if opt == 'trend':
                    trend = calc_trend(data) / len(flist)
                else:
                    mean = calc_mean(data) / len(flist)
            else:
                if opt == 'trend':
                    trend += calc_trend(data) / len(flist)
                else:
                    mean += calc_mean(data) / len(flist)
            count += 1
        ax = axes.flat[mind]
        if opt == 'trend':
            if vv == 'lai':
                levels = np.linspace(-0.012, 0.012, 31)
            elif vv == 'pr':
                levels = np.linspace(-0.016, 0.016, 31)
            elif vv == 'tas':
                levels = np.linspace(-0.16, 0.16, 31)
            else:
                levels = np.linspace(-0.8, 0.8, 31)
        else:
            if vv == 'lai':
                levels = np.linspace(0., 5., 31)
            elif vv == 'pr':
                levels = np.linspace(0, 20, 31)
            elif vv == 'tas':
                levels = np.linspace(-30, 30, 31)
            else:
                levels = np.linspace(0., 20, 31)

        ##cf = plot_trend(ax, trend, levels)
        cf = plot_trend(ax, mean, levels)

        ax.set_title(model)
    cax = fig.add_axes([0.1, 0.01, 0.8, 0.01])
    plt.colorbar(cf, cax = cax, orientation = 'horizontal')

    fig.savefig(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc', 'examine',
                             'mean_' + vv + '_' + str(season) + '.png'),
                dpi = 600., bbox_inches= 'tight')
    plt.close(fig)


cmip6_list = np.unique([xx.split('_')[0] for xx in get_model_list('historical', drivers, 
                                                                  '0-10cm', 'vanilla', '0.5')])
plotter([drivers[0], 0, 'mean'])


p = mp.Pool(6)
p.map_async(plotter, list(it.product(drivers, range(12), ['mean'])))
p.close()
p.join()


