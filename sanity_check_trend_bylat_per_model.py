"""
20191126

Compare the trend between the latitudinal mean soil moisture of CMIP6
 models of different experiments.
"""
import matplotlib.pyplot as plt
from misc.da_utils import get_model_list
from scipy.stats import pearsonr
import utils_management as mg
from utils_management.constants import depth_cm
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
import itertools as it


lat_median = [str(x) for x in range(-45, 66, 10)]
expr_list = ['historical', 'hist-nat', 'hist-GHG', 'hist-aer']
season_list = ['annual', 'DJF', 'MAM', 'JJA', 'SON']
land_mask = 'vanilla'


#
## DEBUG
## option = ['hist-nat', '0.5', True, 'annual', '0-10cm']
def calc(option):
    expr, res, ndvi_mask, season, dcm = option

    if ndvi_mask:
        suffix = '_ndvi_mask'
    else:
        suffix = ''

    if expr == 'historical':
        cmip6_list = get_model_list(['historical', 'ssp585'], dcm, 
                                    land_mask, res)
    else:
        cmip6_list = get_model_list([expr], dcm, land_mask, res)

    model_list = list(cmip6_list.keys())

    for m in model_list:
        trend = pd.DataFrame(data = np.nan, index = lat_median,
                             columns = cmip6_list[m])

        for r in cmip6_list[m]:
            if ndvi_mask:
                data = pd.read_csv(os.path.join(mg.path_out(), 
                                                'aggregate_ts_cmip6', expr,
                                                'ndvi_mask_' + m + '_' + r + \
                                                '_' + dcm + '_' + \
                                                season + '_' + res + \
                                                '_g_lat_ts.csv'),
                                   index_col = 0).values
            else:
                data = pd.read_csv(os.path.join(mg.path_out(), 
                                                'aggregate_ts_cmip6', expr,
                                                m + '_' + r + '_' + dcm + \
                                                '_' + \
                                                season + '_' + res + \
                                                '_g_lat_ts.csv'),
                                   index_col = 0).values
            if expr == 'historical':
                data = data[:71, :]

            for c_ind, c in enumerate(lat_median):
                coef, p = pearsonr(range(data.shape[0]), data[:, c_ind])
                trend.loc[c, r] = coef

        trend.to_csv(os.path.join(mg.path_out(), 'sanity_check_trend',
                                  expr + '_' + m + '_' + res + '_' + \
                                  season + '_' + dcm + suffix + '.csv'))


# DEBUG
# calc(['historical', '0.5', True, 'DJF', '0-10cm'])


pool = mp.Pool(mp.cpu_count()-1)
pool.map_async(calc, list(it.product(expr_list, ['0.5'], [True, False],
                                     season_list, depth_cm)))
pool.close()
pool.join()
