"""
20220130
ywang254@utk.edu

Plot the trend in the Mean Products and ALL SPI.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import matplotlib as mpl
import utils_management as mg
import itertools as it
from matplotlib import gridspec
from misc.cmip6_utils import mrsol_availability
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from scipy.stats import linregress


mpl.rcParams['font.size'] = 16
mpl.rcParams['axes.titlesize'] = 16
mpl.rcParams['hatch.linewidth'] = 0.5
mpl.rcParams['axes.titlepad'] = 3
lab = 'abcdefghijklmnopqrstuvwxyz'
cmap = cmap_div()
hs = '|||'
levels = np.arange(-1.6e-2, 1.61e-2, 1e-3)
clist = ['k', '#2c39b8', 'r', '#31a354', '#fd8d3c', '#41b6c4', '#dd1c77']
pct = 0.9
yrng = [-1.5, 1.]


res = '0.5'
land_mask = 'vanilla'
lat_eof = [float(x) for x in lat_median]
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


season_list = [str(i) for i in range(12)] # ensure order of x-axis
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']
half_year_name = ['ONDJFM', 'AMJJAS']
year = range(1971, 2101) # year = range(1951, 2101)


folder = 'bylat'
lag = 3
noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
else:
    suffix = ''
dist = 'gmm'

expr = 'historical'
expr_name = 'ALL'


prod = 'mean_noncmip' # 'mean_products', 'mean_noncmip' // 'mean_lsm'
prod_name = 'Mean NonCMIP' # 'Mean Products', 'Mean NonCMIP' // 'Mean ORS'


if prod == 'mean_products':
    limit = True
    prefix = 'limit_'
else:
    limit = False
    prefix = ''


def to_half_year(time_ind):
    year = time_ind.year
    # 0 - ONDJFM, 1 - AMJJAS
    half = np.where((time_ind.month.values >= 3) & \
                    (time_ind.month.values <= 8), 1, 0)
    return year * 100 + half


fig = plt.figure(figsize = (6, 8))
gs = gridspec.GridSpec(2, 2, hspace = 0.1, wspace = 0.15,
                        width_ratios = [1, 1])

for i, dcm in enumerate(depth_cm_new):
    # Latitude
    # ---- Read data
    # -------- Mean Products
    data = pd.read_csv(os.path.join(mg.path_out(),
                                    'standard_diagnostics_spi',
                                    'products', dist, 'bylat_trend_' + \
                                    str(lag) + '_' + res + '_' + dcm + \
                                    suffix + '.csv'),
                        index_col = 0, header = [0, 1])
    data.index = data.index.values.astype(str)
    values = data.loc[:, prod].loc[lat_median, season_list].values

    data_p = pd.read_csv(os.path.join(mg.path_out(),
                                        'standard_diagnostics_spi',
                                        'products', dist, 'bylat_trend_p_' + \
                                        str(lag) + '_' + res + '_' + dcm + \
                                        suffix + '.csv'),
                            index_col = 0, header = [0, 1])
    data_p.index = data_p.index.values.astype(str)
    values_p = data_p.loc[:, prod].loc[lat_median, season_list].values

    # -------- CMIP6
    # ------------ trends of the individual model ensemble members
    data2 = pd.read_csv(os.path.join(mg.path_out(),
                                        'standard_diagnostics_spi',
                                        expr, dist, prefix + \
                                        'bylat_trend_' + \
                                        str(lag) + '_' + res + '_' + \
                                        dcm + suffix + '.csv'),
                        index_col = 0, header = [0,1])
    data2.index = data2.index.astype(str)
    data2_p = pd.read_csv(os.path.join(mg.path_out(),
                                        'standard_diagnostics_spi',
                                        expr, dist, prefix + \
                                        'bylat_trend_p_' + \
                                        str(lag) + '_' + res + '_' + \
                                        dcm + suffix + '.csv'),
                            index_col = 0, header = [0,1])
    data2_p.index = data2_p.index.astype(str)
    # ------------ weight of the individual ensemble members
    temp_model = [x.split('_')[0] for x in data2.columns.get_level_values(0)]
    data2_vote = np.full(data2.shape, np.nan)
    for ind, model in enumerate(temp_model):
        n_model = np.sum([x == model for x in temp_model])
        data2_vote[:, ind] = 1 / n_model
    data2_vote = data2_vote / \
        np.sum(data2_vote, axis = 1).reshape(-1,1) * \
        len(data2.columns.levels[1])
    # -------- weighted ensemble average of the trends
    weighted_avg = (data2 * data2_vote).groupby(level = 1, axis = 1 \
                                                ).sum().loc[:, season_list]
    # -------- std of the trends
    weighted_std = data2.groupby(level = 1,
                                    axis = 1).std().loc[:, season_list]
    # -------- weighted ensemble average of whether p <= 0.05
    weighted_avg_p = ((data2_p <= 0.05)* data2_vote).groupby(level = 1, axis = 1 \
                                                                ).sum().loc[:, season_list]

    # ---- Plot
    # -------- Mean Products
    ax = fig.add_subplot(gs[i,0])
    h = ax.contourf(range(values.shape[1]), range(values.shape[0]),
                    values, cmap = cmap, levels = levels, extend = 'both')
    ax.set_yticks(range(2, values.shape[0], 4))
    ax.set_yticklabels(lat_median_name[2::4])
    ax.set_xticks(np.linspace(0.35, 10.65, 12))
    if i == 0:
        ax.set_xticklabels([])
    else:
        ax.set_xticklabels(season_list_name)
    ax.tick_params(axis = 'both', length = 2, pad = 2)
    if i == 0:
        ax.text(0.5, 1.03, prod_name,
                transform = ax.transAxes, horizontalalignment = 'center')
    ax.set_title(lab[i], loc = 'left', weight = 'bold')
    # ------------ the product p > 0.05
    ax.contour(range(values_p.shape[1]), range(values_p.shape[0]),
                values_p > 0.05, levels = [0.5], colors = 'grey', linestyles = 'dashed',
                linewidths = 0.5)
    ax.contourf(range(values_p.shape[1]), range(values_p.shape[0]),
                values_p > 0.05, levels = [0.5, 1.5], colors = 'w', alpha = 0.4)

    # -------- CMIP6
    ax = plt.subplot(gs[i,1])
    fig.add_subplot(ax)
    h = ax.contourf(range(weighted_avg.shape[1]),
                    range(weighted_avg.shape[0]),
                    weighted_avg.values, cmap = cmap, levels = levels,
                    extend = 'both')
    ax.set_yticks(range(2, weighted_avg.shape[0], 4))
    ax.set_yticklabels( [] )
    ax.set_xticks(np.linspace(0.35, 10.65, 12)) # range(0, 12, 1))
    if i == 0:
        ax.set_xticklabels([])
    else:
        ax.set_xticklabels(season_list_name)
    ax.tick_params(axis = 'both', length = 2, pad = 2)
    if i == 0:
        ax.text(0.5, 1.03, expr_name + ' Forcings',
                transform = ax.transAxes, horizontalalignment = 'center')
    # ------------ weighted_avg_p indicates at least 90%
    ax.contour(range(weighted_avg_p.shape[1]), range(weighted_avg_p.shape[0]),
                weighted_avg_p < 0.5, levels = [0.5], colors = 'grey', linestyles = 'dashed',
                linewidths = 0.5)
    ax.contourf(range(weighted_avg_p.shape[1]), range(weighted_avg_p.shape[0]),
                weighted_avg_p < 0.5, levels = [0.5, 1.5], colors = 'w', alpha = 0.4)
    ax.set_title(lab[i+2], loc = 'left', weight = 'bold')

cax = fig.add_axes([0.95, 0.1, 0.01, 0.8])
plt.colorbar(h, cax = cax, orientation = 'vertical',
                label = 'Trend of ' + str(lag) + '-month SSI (year$^{-1}$)')
cax.text(3., 0., 'Dry', fontsize = 12, transform = cax.transAxes,
            verticalalignment = 'center', rotation = 90) # , color = '#762a83')
cax.text(3., 1., 'Wet', fontsize = 12, transform = cax.transAxes,
            verticalalignment = 'center', rotation = 90) # , color = '#3288bd')

figname = os.path.join(mg.path_out(), 'figures', prefix + \
                        'slide_' + dist + '_trend_' + str(lag) + \
                        '_' + res + suffix + '.png')
fig.savefig(figname, dpi = 600., bbox_inches = 'tight')
plt.close(fig)
