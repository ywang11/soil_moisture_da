"""
20220130
ywang254@utk.edu

Plot the S/N ratio of the signal, and threshold of 
   likely (S/N > 0.95, detectable at 66% confidence)
   very likely (S/N > 1.64, 90% confidence)
   virtually certain (S/N > 2.57, 99% confidence).

Before and after emergent constraint.

The thresholds follow Marvel et al. 2019 DOI: 10.1038/s41586-019-1149-8

The EOF is flipped already in the fingerprint calculation step to ensure most
  are negative values.

Historical + RCP85 only. Until 2100.
"""
import matplotlib.pyplot as plt
import pandas as pd
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median # depth_cm
from misc.plot_utils import plot_ts_shade, cmap_gen
import numpy as np
import cartopy.crs as ccrs
##from cartopy.mpl.gridliner import Gridliner
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil
from misc.em_utils import collect_SN_em, collect_SN_em_interaction
import scipy.stats as scistats


mpl.rcParams['font.size'] = 16
mpl.rcParams['axes.titleweight'] = 'normal'


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list_list = {'bymonth': [str(i) for i in range(12)],
                    'byyear': ['annual_mean', 'annual_maximum',
                               'annual_minimum',
                               'growing_season_mean', 'growing_season_maximum',
                               'growing_season_minimum']}

lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'
month_name2 = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']

cmap2 = mpl.cm.get_cmap('jet')
clist = [cmap2((i+0.5)/12) for i in range(12)]

#Setup / MODIFY
folder = 'bylat'
suffix = '_noSahel'
season_name = 'bymonth'
base = 'historical'
expr = 'historical'
expr_name = 'ALL'
season_list = season_list_list[season_name]
season_list_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug',
                    'Sep','Oct','Nov','Dec']
lag = 3
start = 1971
L = 2016 - start + 1
method = 'gam'
limit = False
if limit:
    prefix = 'limit_'
else:
    prefix = ''
dist = 'gmm'


signal_em = collect_SN_em_interaction(dist, method, season_list, depth_cm_new)

#
flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                prefix + 'flip_bylat' + suffix + '.csv'),
                    index_col = [0,1,2,3])

#
fig, axes = plt.subplots(ncols = 2, nrows = 1, figsize = (12,6),
                         sharex = True, sharey = True)
fig.subplots_adjust(wspace = 0.05, hspace = 0.15)
for dind, dcm in enumerate(depth_cm_new):
    ax = axes.flat[dind]

    ###################################################################
    # Signal/Noise ratio time series
    ###################################################################
    h = [None] * 12
    for sind, season in enumerate(season_list):
        # CMIP6 noises
        noise_std = np.nanstd(pd.read_csv(os.path.join(mg.path_out(),
            'cmip6_spi_noise', folder, base + '_eofs', prefix + dist + \
            '_trend_' + dcm + '_' + str(lag) + '_' + season + '_' + \
            res + suffix + '.csv'), index_col = 0 \
        ).loc[:, str(L)].values, axis = 0)

        # CMIP6 signals
        signal_hist = pd.read_csv(os.path.join(mg.path_out(),
            'cmip6_spi_signal', folder, base + '_eofs', prefix + \
            dist + '_' + expr + '_signal_' + dcm + '_' + \
            str(lag) + '_' + season + '_' + res + suffix + '.csv'),
                                    header = [0,1] \
        ).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
        signal_hist.index = pd.to_datetime(signal_hist.index).year
        signal_hist = signal_hist.loc[start:(2100-L+1), str(L)]
        signal_hist = signal_hist * flip.loc[(dcm, sind, dist, lag),
                                                base] / noise_std
        # ---- need to weight the different models
        temp_model = [x.split('_')[0] for x in signal_hist.columns]
        signal_mean = signal_hist.groupby(temp_model,
                                            axis = 1).mean().mean(axis = 1)
        signal_std = signal_hist.std(axis = 1)

        # CMIP6 models
        h[sind],  = ax.plot(signal_hist.index.values, signal_mean.values, lw = 2,
                            color = clist[sind])

    h[-2] = ax.axhline(1.96, linestyle = '-', color = 'k',
                        linewidth = 0.5, zorder = 1)
    h[-1] = ax.axhline(2.57, linestyle = '-', color = 'k',
                        linewidth = 1., zorder = 1)
    ax.axhline(0, linestyle = ':', color = 'k', linewidth = 0.5)

    ax.set_title(dcm, pad = 3.)
    ax.set_xlim([signal_hist.index[0], signal_hist.index[-1]])
    ax.set_xticks([1980, 2000, 2020, 2040])
    ax.text(0.01, 1.03, lab[dind], transform = ax.transAxes,
            weight = 'bold')
    if dind == 0:
        ax.set_ylabel('Constrained S/N Ratio')
    ax.set_xlabel('Year')
ax.legend(h, month_name2, ncol = 1, loc = 'lower center', bbox_to_anchor = (1.3, 0.1))
fig.savefig(os.path.join(mg.path_out(), 'figures', prefix + 'slide_fut_' + \
                            dist + '_' + method + '_' + str(lag) + '_' + season_name + \
                            '_L=' + str(L) + '_' + res + suffix + '.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
