"""
20210803
ywang254@utk.edu

Obtain the collection of all L-length trends in "expr" simulations projected
 on the fingerprint of historical.
"""
import utils_management as mg
from utils_management.constants import depth_cm_new, year_cmip6, lat_median, drivers
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal_bylat, ls_trend
from misc.cmip6_utils import one_layer_availability, mrsol_availability
from glob import glob
import itertools as it
import time
import multiprocessing as mp
from analyze_cmip6_drivers_map import get_model_list


start = time.time()


land_mask = 'vanilla'
res = '0.5'
season_list = [str(i) for i in range(12)]
expr_list = ['historical', 'hist-GHG', 'hist-aer']
L_range = range(10,67)


noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['noSahel'].loc[np.array(lat_median).astype(int)].values)
else:
    suffix = ''
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['Normal'].loc[np.array(lat_median).astype(int)].values)


expr = 'historical'
for varname, season in it.product(drivers, range(12)):
    print(varname, season)

    #####################
    # Load the first EOF
    #####################
    data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_drivers_fingerprint',
                                    expr, 'bylat', varname + '_eof_' + \
                                    str(season) + suffix + '.csv'),
                       index_col = 0)
    data.index = data.index.astype(str)
    eof1 = data.loc[lat_median, 'Full'].values

    ################################
    # Load the CMIP6 list of models
    ################################
    model_version = get_model_list(expr, drivers, '0-10cm', land_mask, res)

    if expr == 'historical':
        signal = pd.DataFrame(data = np.nan,
            index = pd.date_range('1971-01-01', '2100-12-31', freq = 'YS'),
            columns = pd.MultiIndex.from_product([list(L_range),
                                                  model_version]))
        proj = pd.DataFrame(data = np.nan,
            index = pd.date_range('1971-01-01', '2100-12-31', freq = 'YS'),
            columns = model_version)
    else:
        signal = pd.DataFrame(data = np.nan,
            index = pd.date_range('1971-01-01', '2020-12-31', freq = 'YS'),
            columns = pd.MultiIndex.from_product([list(L_range),
                                                  model_version]))
        proj = pd.DataFrame(data = np.nan,
            index = pd.date_range('1971-01-01', '2020-12-31', freq = 'YS'),
            columns = model_version)

    #
    for m_v in model_version:
        print(m_v)
        data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_drivers_summary', varname,
                                        expr, m_v + '_' + str(season) + \
                                        '_' + res + '_g_lat_ts' + suffix + '.csv'),
                           index_col = 0, parse_dates = True)
        values = data.loc[data.index.year >= 1971, lat_median].values

        # Calculate the signal.
        wgts = lat_area_sqrt / np.mean(lat_area_sqrt)

        for L in L_range:
            if noSahel:
                tt, pp = calc_signal_bylat(eof1[:-1], values[:,:-1],
                                           wgts[:-1], L)
            else:
                tt, pp = calc_signal_bylat(eof1, values, wgts, L)
            signal.loc[:, (L, m_v)] = tt
        proj.loc[:, m_v] = pp
    signal.to_csv(os.path.join(mg.path_out(), 'cmip6_drivers_signal',
                               'bylat', expr + '_eofs', varname + \
                               '_signal_' + str(season) + '_' + res + suffix + '.csv'))
    proj.to_csv(os.path.join(mg.path_out(), 'cmip6_drivers_signal',
                             'bylat', expr + '_eofs', varname + '_pc_' + \
                             str(season) + '_' + res + suffix + '.csv'))

end = time.time()
print('Script finished in ' + str((end - start) / 60) + ' minutes.')
