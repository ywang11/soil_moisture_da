import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec
import itertools as it
import utils_management as mg
from utils_management.constants import depth_cm, depth_cm_new
from matplotlib.cm import get_cmap
from misc.cmip6_utils import one_layer_availability, mrsol_availability
import statsmodels.api as stats
import matplotlib as mpl


#
def fit_lr(df, ax, xlim = (1981,2010),
           posx = 0.02, posy = 0.85, color = 'k'):
    df.columns = df.columns.get_level_values(0)
    dfx = (df - df.mean(axis = 0))
    dfx = dfx.loc[xlim[0]:xlim[1], :].stack()
    x = dfx.index.get_level_values(0)
    y = dfx.values
    temp = (~(np.isnan(x) | np.isnan(y)))
    x = x[temp]
    y = y[temp]
    mod = stats.OLS(y, stats.add_constant(x)).fit()
    h, = ax.plot([1971,2016],
                 [mod.params[0] + mod.params[1]*1971,
                  mod.params[0] + mod.params[1]*2016], '-',
                 color = color, lw = 0.5)
    #if mod.pvalues[0] <= 0.05:
    #    p1 = '$^*$'
    #else:
    #    p1 = ''
    if mod.pvalues[1] <= 0.05:
        p2 = '$^*$'
    else:
        p2 = ''
    ax.text(posx, posy, #('%.2f' % mod.params[0]) + p1 + ' + ' + \
            'r = ' + ('%.6f' % mod.params[1]) + p2, # + '$x$',
            transform = ax.transAxes, fontsize = 6, color = color)
    return h


def get_bm2(dcm, sign):
    if sign == 'dry':
        if dcm == '0-10cm':
            bounding_month = list(range(1,7)) # note month starts from 0
        elif dcm == '0-100cm':
            bounding_month = list(range(12))
    else:
        if dcm == '0-10cm':
            bounding_month = list(range(1, 5))
        else:
            bounding_month = [0,1,2,11]
    return [str(bm) for bm in bounding_month]


#
cmap = get_cmap('jet')
land_mask = 'vanilla'
a, _ = mrsol_availability('0-10cm', land_mask, 'historical', '0.5')
b, _ = mrsol_availability('0-10cm', land_mask, 'ssp585', '0.5')
cmip6_list = sorted(set(a) & set(b))
lsm_all = ['CERA20C', 'CLASS-CTEM-N', 'CLM4', 'CLM4VIC',
           'CLM5.0', 'ERA20C', 'ERA-Interim', 'ERA5', 'ERA-Land', 'ESA-CCI',
           'GLDAS_Noah2.0', 'GLEAMv3.3a', 'ISAM', 'JSBACH', 'JULES',
           'LPX-Bern', 'ORCHIDEE', 'ORCHIDEE-CNP', 'SiBCASA']
met_to_lsm = {'CRU_v4.03': ['ESA-CCI', 'GLEAMv3.3a'],
              'CRU_v3.20': ['CLASS-CTEM-N', 'CLM4', 'CLM4VIC', 'SiBCASA'],
              'CRU_v3.26': ['CLM5.0', 'ISAM', 'JSBACH', 'JULES', 'LPX-Bern',
                            'ORCHIDEE', 'ORCHIDEE-CNP'],
              'GLDAS_Noah2.0': ['GLDAS_Noah2.0'],
              'ERA-Interim': ['ERA-Interim', 'ERA-Land'],
              'ERA5': ['ERA5'],
              'ERA20C': ['ERA20C'],
              'CERA20C': ['CERA20C']}
lsm_to_met = {}
for a,b in met_to_lsm.items():
    for bb in b:
        if not bb in list(lsm_to_met.keys()):
            lsm_to_met[bb] = a
        else:
            raise Exception('Too many met.')
clist_lsm = [cmap( (i+0.5)/len(lsm_all) ) for i in range(len(lsm_all))]
clist_cmip6 = [cmap( (i+0.5)/len(cmip6_list) ) for i in range(len(cmip6_list))]
season_list = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
               'Aug', 'Sep', 'Oct', 'Nov', 'Dec']


#
obs = {}
cmip6 = {}
for sign, dcm in it.product(['dry', 'wet'], depth_cm_new):
    for varname in ['ssi', 'evspsbl']:
        obs[(varname, sign, dcm)] = pd.read_csv(os.path.join(mg.path_out(),
            'obs_drivers_summary', 'sahara_lsm_avg_' + varname + '_' + \
            'collect_' + sign + '_' + dcm + '.csv'), index_col = 0,
                                                header = [0,1])
    obs_pr_temp = pd.read_csv(os.path.join(mg.path_out(),
                                           'obs_drivers_summary',
                                           'sahara_lsm_avg_pr_collect_' + \
                                           sign + '_' + dcm + '.csv'),
                              index_col = 0, header = [0,1])
    obs_pr = pd.DataFrame(np.nan, index = obs[('evspsbl', sign, dcm)].index,
                          columns = obs[('evspsbl', sign, dcm)].columns)
    for i in obs[('evspsbl', sign, dcm)].columns:
        obs_pr.loc[:, i] = obs_pr_temp.loc[:, (lsm_to_met[i[0]], i[1])]
        # ---- limit to the same years as ET
        obs_pr.loc[np.isnan(obs[('evspsbl', sign, dcm)].loc[:,i]), i] = np.nan
    obs[('pr', sign, dcm)] = obs_pr
    obs[('pr-evspsbl', sign, dcm)] = obs[('pr', sign, dcm)] - \
        obs[('evspsbl', sign, dcm)]

    for varname in ['ssi', 'pr', 'evspsbl']:
        cmip6[(varname, sign, dcm)] = pd.read_csv(os.path.join(mg.path_out(),
            'cmip6_drivers_summary', 'sahara_model_avg_' + varname + \
            '_collect_' + sign + '_' + dcm + '.csv'), index_col = 0,
                                                  header = [0,1])
    cmip6[('pr-evspsbl', sign, dcm)] = cmip6[('pr', sign, dcm)] - \
        cmip6[('evspsbl', sign, dcm)]


#
mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
for sign in ['dry', 'wet']:
    for dind, dcm in enumerate(depth_cm_new):
        bm_list = get_bm2(dcm, sign)
        # --- sort
        bm_list = [bm for _,bm in \
                   sorted(zip([int(bm2) for bm2 in bm_list], bm_list))]
        bm_name_list = [season_list[int(bm)] for bm in bm_list]

        fig = plt.figure(figsize = (6.5, 8))
        gs = GridSpec(2, 1, height_ratios = [3,1], hspace = 0.1)
        sub_gs1 = GridSpecFromSubplotSpec(6, len(bm_list),
                                          subplot_spec = gs[0],
                                          hspace = 0., wspace = 0.)
        sub_axes1 = np.empty([6,len(bm_list)], dtype = object)
        for i,j in it.product(range(6), range(len(bm_list))):
            if (i == 0) & (j == 0):
                sub_axes1[i,j] = fig.add_subplot(sub_gs1[i,j])
            else:
                sub_axes1[i,j] = fig.add_subplot(sub_gs1[i,j],
                                                 sharex = sub_axes1[0,0],
                                                 sharey = sub_axes1[0,0])
        sub_gs2 = GridSpecFromSubplotSpec(2, len(bm_list),
                                          subplot_spec = gs[1],
                                          hspace = 0., wspace = 0.)
        sub_axes2 = np.empty([2,len(bm_list)], dtype = object)
        for i,j in it.product(range(2), range(len(bm_list))):
            if (i == 0) & (j == 0):
                sub_axes2[i,j] = fig.add_subplot(sub_gs2[i,j])
            else:
                sub_axes2[i,j] = fig.add_subplot(sub_gs2[i,j],
                                                 sharex = sub_axes2[0,0],
                                                 sharey = sub_axes2[0,0])
        for sind, season in enumerate(bm_list):
            for vind, varname in enumerate(['pr', 'evspsbl', 'pr-evspsbl']):
                ax = sub_axes1[vind*2, sind]
                h_lsm = [None] * len(lsm_all)
                for ind, lsm in enumerate(lsm_all):
                    temp = obs[(varname, sign, dcm)].loc[:, (lsm, season)] - \
                        obs[(varname, sign, dcm)].loc[:, (lsm, season)].mean()
                    h_lsm[ind], = ax.plot(temp.index, temp,
                                          '-', color = clist_lsm[ind],
                                          lw = 0.5)
                fit_lr(obs[(varname, sign, dcm)].loc[:, (slice(None),
                                                         season)], ax) #
                if sind == 0:
                    ax.set_ylabel('ORS')
                else:
                    plt.setp(ax.get_yticklabels(), visible=False)
                    ax.tick_params('y', length = 0)
                ax.set_xlim([1971,2016])
                ax.tick_params('x', length = 0)
                plt.setp(ax.get_xticklabels(), visible=False)
                if vind == 0:
                    ax.set_title(bm_name_list[sind])
                if sind == 0:
                    if vind == 0:
                        ax.text(-0.5, 0., 'P', transform = ax.transAxes,
                                rotation = 90)
                    elif vind == 1:
                        ax.text(-0.5, 0., 'ET', transform = ax.transAxes,
                                rotation = 90)
                    else:
                        ax.text(-0.5, 0., 'P - ET', transform = ax.transAxes,
                                rotation = 90)
    
                ax = sub_axes1[vind*2+1, sind]
                h_cmip6 = [None] * len(cmip6_list)
                for ind, model in enumerate(cmip6_list):
                    temp = cmip6[(varname, sign, dcm)].loc[:, (model,
                                                               season)] - \
                        cmip6[(varname, sign, dcm)].loc[:, (model,
                                                            season)].mean()
                    h_cmip6[ind], = ax.plot(temp.index, temp,
                                            '-', color = [0.5, 0.5, 0.5],
                                            lw = 0.5)
                # weighted average
                temp = cmip6[(varname, sign, dcm)].loc[:, (slice(None),
                                                           season)] - \
                    cmip6[(varname, sign, dcm)].loc[:, (slice(None),
                                                        season)].mean(axis=0)
                temp.columns = temp.columns.droplevel(1)
                temp_model = [c.split('_')[0] for c in temp.columns]
                temp_mean = temp.groupby(temp_model,
                                         axis = 1).mean().mean(axis=1)
                ax.plot(temp_mean.index, temp_mean.values, '-k', lw = 1.)
                fit_lr(cmip6[(varname, sign, dcm)].loc[:, (slice(None),
                                                           season)], ax) #
                fit_lr(cmip6[(varname, sign, dcm)].loc[:, (slice(None),
                                                           season)], ax,
                       xlim = (1971,2016), posy = 0.7, color = '#2b8cbe')
                ax.set_xlim([1971,2016])
                if sind == 0:
                    ax.set_ylabel('CMIP6 ALL')
                else:
                    plt.setp(ax.get_yticklabels(), visible=False)
                    ax.tick_params('y', length = 0)
                ax.tick_params('x', length = 0)
                plt.setp(ax.get_xticklabels(), visible=False)
    
            #
            ax = sub_axes2[0, sind]
            prod = 'mean_noncmip'
            ax.plot(obs[('ssi',sign,dcm)].index,
                    obs[('ssi',sign,dcm)].loc[:, (prod, season)] - \
                    obs[('ssi',sign,dcm)].loc[:, (prod, season)].mean(),
                    '-', color = 'k', lw = 0.5)
            h1 = fit_lr(obs[('ssi',sign,dcm)].loc[:, ([prod], season)], ax)
            h2 = fit_lr(obs[('ssi',sign,dcm)].loc[:, ([prod], season)], ax,
                        xlim = (1971,2016), posy = 0.7, color = '#2b8cbe')
            ax.set_xlim([1971,2016])
            if sind == 0:
                ax.set_ylabel('Mean NonCMIP')
                ax.legend([h1,h2], ['1981-2010', '1971-2016'], ncol = 2,
                          loc = (0., -1.5))
            else:
                plt.setp(ax.get_yticklabels(), visible=False)
                ax.tick_params('y', length = 0)
            if sind == 0:
                ax.text(-0.5, 0., 'SSI', transform = ax.transAxes,
                        rotation = 90)
            ax.tick_params('x', length = 0)
            plt.setp(ax.get_xticklabels(), visible=False)
            if sind == (len(bm_list)-1):
                ax.legend(h_lsm, lsm_all, ncol = 5,
                          loc = (-len(bm_list) + 1, -2.3))
            ax = sub_axes2[1, sind]
            for ind, model in enumerate(cmip6_list):
                ax.plot(cmip6[('ssi',sign,dcm)].index,
                        cmip6[('ssi',sign,dcm)].loc[:, (model, season)] - \
                        cmip6[('ssi',sign,dcm)].loc[:, (model,
                                                        season)].mean(),
                        '-', color = [0.5, 0.5, 0.5], lw = 0.5)
            # weighted average
            temp = cmip6[('ssi',sign,dcm)].loc[:, (slice(None), season)] - \
                cmip6[('ssi',sign,dcm)].loc[:, (slice(None),
                                                season)].mean(axis = 0)
            temp.columns = temp.columns.droplevel(1)
            temp_model = [c.split('_')[0] for c in temp.columns]
            temp_mean = temp.groupby(temp_model, axis = 1).mean().mean(axis=1)
            fit_lr(cmip6[('ssi',sign,dcm)].loc[:, (slice(None), season)], ax)
            fit_lr(cmip6[('ssi',sign,dcm)].loc[:, (slice(None), season)], ax,
                   xlim = (1971,2016), posy = 0.7, color = '#2b8cbe')
            ax.set_xlim([1971,2016])
            if sind == 0:
                ax.set_ylabel('CMIP6 ALL')
            else:
                plt.setp(ax.get_yticklabels(), visible=False)
                ax.tick_params('y', length = 0)
            #if sind == (len(bm_list)-1):
                #ax.legend(h_cmip6, cmip6_list, ncol = 5,
                #          loc = (-len(bm_list), -5))
    
        fig.savefig(os.path.join(mg.path_out(), 'figures',
                                 'sahara_plot_drivers_' + sign + '_' + \
                                 dcm + '.png'),
                    dpi = 600., bbox_inches = 'tight')
        plt.close(fig)
