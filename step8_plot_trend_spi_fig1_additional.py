"""
20200901
ywang254@utk.edu

Plot the variation in products in the latitudinal trend & whether the 
 Mean NonCMIP trend is within the 95% interval of the ALL trend.

1951-1980, 1971-2000, 1987-2016

! hatch = 80% agreement
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import matplotlib as mpl
import utils_management as mg
import itertools as it
from matplotlib import gridspec
from misc.cmip6_utils import mrsol_availability
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5.
mpl.rcParams['hatch.linewidth'] = 0.5
mpl.rcParams['axes.titlepad'] = 1
lab = 'abcdefghijklmnopqrstuvwxyz'
cmap = cmap_div()
hs = '/////'
levels = np.arange(-2.5e-2, 2.51e-2, 1e-3)
clist = ['k', '#2c39b8', 'r', '#31a354', '#fd8d3c', '#41b6c4', '#dd1c77']
pct = 0.8
yrng = [-1.2, 0.8]
pct2 = 0.5

res = '0.5'
land_mask = 'vanilla'
lat_eof = [float(x) for x in lat_median]
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')

season_list = [str(i) for i in range(12)] # ensure order of x-axis
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']

folder = 'bylat'
suffix = ''
lag = 3

#year_range_list = [range(1951, 1981), range(1956, 1986), range(1961, 1991),
#                   range(1966, 1996), range(1971, 2001), range(1976, 2006),
#                   range(1981, 2011), range(1987, 2017)]
#year_range_list = [range(1951, 1976), range(1975, 1986), range(1985, 2017)]
year_range_list_list = [[range(1951, 1971), range(1971, 2017)],
                        [range(1951, 1976), range(1976, 2017)],
                        [range(1951, 1981), range(1981, 2017)],
                        [range(1951, 1986), range(1986, 2017)]]
year_list_list = [[str(yr[0]) + '-' + str(yr[-1]) for yr in year_list] \
                  for year_list in year_range_list_list]

expr = 'historical'
expr_name = 'ALL'

prod = 'mean_noncmip' # 'mean_noncmip'
prod_name = 'Mean NonCMIP' # 'Mean NonCMIP'


for yind, dist in it.product(range(len(year_range_list_list)),
                             ['gmm', 'weibull']):
    year_range_list = year_range_list_list[yind]
    year_list = year_list_list[yind]

    fig = plt.figure(figsize = (6.5, len(depth_cm_new) * 2.5))
    gs = gridspec.GridSpec(2, 2, hspace = 0.1, wspace = 0.05)

    for i, dcm in enumerate(depth_cm_new):
        #
        data_prod = pd.read_csv(os.path.join(mg.path_out(),
                                             'standard_diagnostics_spi',
                                             'products', dist,
                                             'bylat_trend_' + str(lag) + \
                                             '_' + res + '_' + dcm + \
                                             '_additional.csv'), index_col = 0,
                                header = [0,1,2])
        data_prod = data_prod.dropna(how = 'all')
        data_prod.index = ['%d' % x for x in data_prod.index.values]
        data_prod = data_prod.loc[lat_median, prod].reorder_levels([1,0],
                                                                   axis = 1)

        #
        data_cmip6 = pd.read_csv(os.path.join(mg.path_out(),
                                              'standard_diagnostics_spi',
                                              expr, dist, 'bylat_trend_' + \
                                              str(lag) + '_' + res + '_' + \
                                              dcm + '_additional.csv'),
                                 index_col = 0, header = [0,1,2])
        data_cmip6 = data_cmip6.dropna(how = 'all')
        data_cmip6.index = ['%d' % x for x in data_cmip6.index.values]
        data_cmip6 = data_cmip6.loc[lat_median, :]
        data_cmip6 = data_cmip6.reorder_levels([2,0,1], axis = 1)

        # ---- weight of the CMIP6 models
        temp_model = [x.split('_')[0] for x in \
                      data_cmip6.columns.get_level_values(1)]
        cmip6_vote = pd.DataFrame(np.nan, index = data_cmip6.index,
                                  columns = data_cmip6.columns)
        for ind, model in enumerate(temp_model):
            n_model = np.sum([x == model for x in temp_model])
            cmip6_vote.iloc[:, ind] = 1 / n_model
        cmip6_vote = cmip6_vote / len(np.unique(temp_model)) * \
            len(cmip6_vote.columns.levels[0]) * \
            len(cmip6_vote.columns.levels[2])

        # ---- Mean Products
        sub_gs1 = gridspec.GridSpecFromSubplotSpec(1, len(year_list),
                                                   subplot_spec = gs[2*i],
                                                   hspace = 0., wspace = 0.1)
        for j in range(len(year_list)):
            year_str = year_list[j]
            values = data_prod.loc[:, year_str].loc[:, season_list].values

            ax = fig.add_subplot(sub_gs1[j])
            h = ax.contourf(range(values.shape[1]), range(values.shape[0]),
                            values, cmap = cmap, levels = levels,
                            extend = 'both')
            ax.set_yticks(range(2, values.shape[0], 4))
            ax.set_xticks(np.linspace(0.35, 10.65, 12))
            ax.tick_params(axis = 'both', length = 1., pad = 1)
            if j == 0:
                ax.set_yticklabels(lat_median_name[2::4])
                ax.set_ylabel(dcm)
                #ax.text(-.9, 0.5, prod_name, rotation = 90,
                #        fontsize = 6, transform = ax.transAxes,
                #        verticalalignment = 'center')
            else:
                ax.set_yticklabels([])
            if i == 0:
                ax.text(0.5, 1.05, prod_name + '\n' + year_str,
                        fontsize = 6, transform = ax.transAxes,
                        horizontalalignment = 'center')
                ax.set_xticklabels([])
            else:
                ax.set_xticklabels(season_list_name)
            ax.set_title('(' + lab[i*6+j] + ')', loc = 'left')

            ## ---- weighted percentage of CMIP6 models that have consistent
            ##      trends with the product.
            #data2 = data_cmip6.loc[:, year_str]
            #vote2 = cmip6_vote.loc[:, year_str]
            #same_sign = pd.DataFrame(np.nan, index = data2.index,
            #                         columns = data2.columns)
            #for model in data2.columns.levels[0]:
            #    for sind, ss in enumerate(season_list):
            #        data3 = data2.loc[:, (model, ss)]
            #        same_sign.loc[:, (model, ss)] = \
            #            ((data3.values > 0.) & (values[:,sind] > 0.)) | \
            #            ((data3.values < 0.) & (values[:,sind] < 0.))
            #pct_same_sign = (same_sign * vote2 \
            #).groupby(level = 1, axis = 1).sum().loc[:, season_list]
            #hatch1 = pct_same_sign > pct2
            #stipple(ax, range(hatch1.shape[0]), range(hatch1.shape[1]),
            #        mask = hatch1, hatch = hs)

            # ---- whether the mean CMIP6 model has the same sign as the
            #      product.
            data2 = data_cmip6.loc[:, year_str]
            vote2 = cmip6_vote.loc[:, year_str]
            weighted_avg = (data2 * vote2).groupby(level = 1, axis = 1 \
            ).sum().loc[:, season_list]
            weighted_std = data2.groupby(level = 1,
                                         axis = 1).std().loc[:, season_list]
            hatch1 = ((values > 0) & (weighted_avg.values > 0)) | \
                ((values < 0) & (weighted_avg.values < 0))
            stipple(ax, range(hatch1.shape[0]), range(hatch1.shape[1]),
                    mask = hatch1, hatch = hs)
            # ---- whether the product is within 95% CI of the CMIP6
            #      models (True = Not)
            hatch2 = (~hatch1) & \
                ((values < (weighted_avg - 1.96*weighted_std).values) | \
                 (values > (weighted_avg + 1.96*weighted_std).values))
            stipple(ax, range(hatch2.shape[0]), range(hatch2.shape[1]),
                    mask = hatch2, hatch = '\\\\\\')

        # ---- CMIP6
        sub_gs2 = gridspec.GridSpecFromSubplotSpec(1, len(year_list),
                                                   subplot_spec = gs[2*i+1],
                                                   hspace = 0., wspace = 0.1)
        for j in range(len(year_list)):
            year_str = year_list[j]
            data2 = data_cmip6.loc[:, year_str]
            vote2 = cmip6_vote.loc[:, year_str]

            ax = fig.add_subplot(sub_gs2[j])
            # -------- weighted ensemble average of the trends
            weighted_avg = (data2 * vote2).groupby(level = 1, axis = 1 \
            ).sum().loc[:, season_list]
            # -------- std of the trends
            weighted_std = data2.groupby(level = 1,
                                         axis = 1).std().loc[:, season_list]
            #
            h = ax.contourf(range(weighted_avg.shape[1]),
                            range(weighted_avg.shape[0]),
                            weighted_avg.values, cmap = cmap,
                            levels = levels, extend = 'both')
            ax.set_yticks(range(2, weighted_avg.shape[0], 4))
            ax.set_xticks(np.linspace(0.35, 10.65, 12)) # range(0, 12, 1))
            if i == 0:
                ax.text(0.5, 1.05, expr_name + '\n' + year_str,
                        fontsize = 6, transform = ax.transAxes,
                        horizontalalignment = 'center')
                ax.set_xticklabels([])
            else:
                ax.set_xticklabels(season_list_name)
            #if j == 0:
                #ax.set_yticklabels(lat_median_name[2::4])
                #ax.set_ylabel(dcm)
                #ax.text(-.9, 0.5, expr_name, rotation = 90,
                #        fontsize = 6, transform = ax.transAxes,
                #        verticalalignment = 'center')
            #else:
            ax.set_yticklabels([])
            ax.set_title('(' + lab[i*6 + 3 + j] + ')', loc = 'left')
            ax.tick_params(axis = 'both', length = 0.5, pad = 1)
            # ---- weighted ensemble total of the consistency in trends
            pct_pos = ((data2 > 0.).astype(float) * vote2).groupby( \
                level = 1, axis = 1).sum().loc[:, season_list]
            pct_neg = ((data2 < 0.).astype(float) * vote2).groupby( \
                level = 1, axis = 1).sum().loc[:, season_list]
            hatch1 = ((weighted_avg.values > 0) & (pct_pos.values > pct)) | \
                ((weighted_avg.values < 0) & (pct_neg.values > pct))
            stipple(ax, range(hatch1.shape[0]), range(hatch1.shape[1]),
                    mask = hatch1, hatch = hs)
            #hatch2 = (~hatch1) & \
            #    (((weighted_avg.values > 0) & \
            #      (pct_pos.values > (pct-0.1))) | \
            #     ((weighted_avg.values < 0) & (pct_neg.values > (pct-0.1))))
            #stipple(ax, range(hatch2.shape[0]), range(hatch2.shape[1]),
            #        mask = hatch2, hatch = '\\\\\\')

    cax = fig.add_axes([0.1, 0.065, 0.8, 0.01])
    plt.colorbar(h, cax = cax, orientation = 'horizontal',
                 label = 'Trend of ' + str(lag) + '-month SSI (year$^{-1}$)')
    cax.text(.0, -1., 'Dry', fontsize = 5, transform = cax.transAxes,
             horizontalalignment = 'center') # , color = '#762a83')
    cax.text(1.,  -1., 'Wet', fontsize = 5, transform = cax.transAxes,
             horizontalalignment = 'center') # , color = '#3288bd')

    fig.savefig(os.path.join(mg.path_out(), 'figures',
                             'fig1_' + dist + '_trend_' + str(lag) + \
                             '_' + res + suffix + '_additional_' + str(yind) \
                             + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
