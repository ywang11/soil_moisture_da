"""
20190910
ywang254@utk.edu

Plot the EOF of the fingerprint.

The EOF is flipped already in the fingerprint calculation step to ensure the
  merged product projects positively unto them.

This version plots the EOF of all the experiments as a colormap. 
"""
import matplotlib.pyplot as plt
import pandas as pd
import xarray as xr
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import numpy as np
import cartopy.crs as ccrs
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil
from matplotlib import font_manager
for font in font_manager.findSystemFonts('/ccs/home/yaoping/.local/share/fonts'):
    font_manager.fontManager.addfont(font)

mpl.rcParams['font.size'] = 5.5
mpl.rcParams['axes.titleweight'] = 'normal'
mpl.rcParams['axes.titlesize'] = 5.5
mpl.rcParams['hatch.linewidth'] = 0.5
mpl.rcParams['font.family'] = 'arial'
mpl.rcParams['font.sans-serif'] = ['Arial']
cmap = cmap_div()
levels = np.linspace(-0.6, 0.6, 11) # np.arange(-0.84, 0.86, 0.04)
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list_list = {'bymonth': [str(i) for i in range(12)],
                    'byyear': ['annual_mean', 'annual_maximum', 'annual_minimum',
                               'growing_season_mean', 'growing_season_maximum', 'growing_season_minimum']}

# Setup
folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
season_name = 'bymonth'
lag = 3


season_list = season_list_list[season_name]


base_list = ['historical', 'historical-2', 'hist-GHG', 'hist-aer']
                    # 'NoNAT', 'GHGAER', 'hist-nat', 'OTH'
base_list2 = ['ALL', 'ALL-2', 'GHG', 'AER'] # 'ANT', 'GHGAER', 'NAT', 'OTH'

lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')

month_name = ['J','F','M','A','M','J','J','A','S','O','N','D']


###########################################################################
# EOF plot.
###########################################################################
for limit, dist in it.product([True, False], ['gmm','weibull']):
    if limit:
        prefix = 'limit_'
    else:
        prefix = ''
    
    flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                    prefix + 'flip_bylat' + suffix + '.csv'),
                       index_col = [0,1,2,3])

    fig, axes = plt.subplots(nrows = len(depth_cm_new), ncols = len(base_list),
                             figsize = (4.5, len(depth_cm_new) * 2.3),
                             sharex = True, sharey = True)
    fig.subplots_adjust(wspace = 0.01, hspace = 0.1)

    for bind, i in it.product(range(len(base_list)),
                              range(len(depth_cm_new))):
        base = base_list[bind]
        dcm = depth_cm_new[i]

        ax = axes[i, bind]

        eof1_collection = np.full([len(lat_median), len(season_list)], np.nan)
        #hatch_collection = np.full([len(lat_median), len(season_list)], bool)
        for sind, season in enumerate(season_list):
            eof = pd.read_csv(os.path.join(mg.path_out(),
                                           'cmip6_spi_fingerprint',
                                           base, 'bylat', prefix + \
                                           dist + '_eof_' + dcm + '_' + \
                                           str(lag) + '_' + season + \
                                           '_' + res + suffix + '.csv'),
                              index_col = 0)
            eof.index = eof.index.values.astype(str)
            eof = eof.loc[lat_median, :]

            eof1_collection[:,sind] = eof['Full'].values * \
                flip.loc[(dcm, int(season), dist, lag), base]
            #hatch_collection[:,sind] = \
            #    (np.sign(eof['Jackknife CI_lower'].values) == \
            #     np.sign(eof['Jackknife CI_upper'].values)) & \
            #    (np.sign(eof['Jackknife CI_lower'].values) == \
            #     np.sign(eof['Full'].values))
            #print(hatch_collection[:,sind])

        #hb = ax.imshow(eof_collection, cmap = cmap,
        #               vmin = -0.6, vmax = 0.6)
        hb = ax.contourf(range(eof1_collection.shape[1]),
                         range(eof1_collection.shape[0]),
                         eof1_collection, cmap = cmap,
                         levels = levels, extend = 'both')
        #stipple(ax, range(eof1_collection.shape[0]),
        #        range(eof1_collection.shape[1]), ~hatch_collection,
        #        hatch = '\\\\\\\\\\')
        ax.set_yticks(range(2, eof1_collection.shape[0], 4))
        ax.set_yticklabels(lat_median_name[2::4])
        ax.set_xticks(np.linspace(0.3,10.7,12))
        ax.tick_params(axis = 'both', length = 2., pad = 2.)
        if i == 1:
            ax.set_xticklabels(month_name)
        else:
            ax.set_xticklabels([])

        if i == 0:
            ax.set_title(base_list2[bind])
        if bind == 0:
            ax.set_ylabel(dcm)
        else:
            ax.tick_params(axis = 'y', length = 0.)
        ax.text(0.1, 1.03, lab[i*len(base_list)+bind],
                transform = ax.transAxes, weight = 'bold')

    cax = fig.add_axes([0.1, 0.04, 0.8, 0.015])
    plt.colorbar(hb, cax = cax, orientation = 'horizontal', ticks = levels,
                 label = 'Fingerprint of ' + str(lag) + '-month SSI')
    fig.savefig(os.path.join(mg.path_out(), 'figures', prefix + 'fig2_' + dist + '_eof_' + str(lag) + \
                             '_' + season_name + '_' + res + suffix + '.png'),
                dpi = 600., bbox_inches = 'tight')
    fig.savefig(os.path.join(mg.path_out(), 'figures', prefix + 'fig2_' + dist + '_eof_' + str(lag) + \
                             '_' + season_name + '_' + res + suffix + '.pdf'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
