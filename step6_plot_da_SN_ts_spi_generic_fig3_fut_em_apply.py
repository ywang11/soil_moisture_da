import matplotlib.pyplot as plt
import pandas as pd
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median # depth_cm
from misc.plot_utils import plot_ts_shade, cmap_div
import numpy as np
from misc.cmip6_utils import mrsol_availability
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
from misc.plot_utils import stipple
from matplotlib.cm import get_cmap
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from misc.em_utils import collect_SN_model, collect_eof_model, collect_noise, collect_SN_em
import scipy.stats as scistats


def read_bylat_trend():
    """ Read the modeled future trends of the CMIP6 models."""
    data2 = pd.read_csv(os.path.join(mg.path_out(),
                                     'standard_diagnostics_spi', expr,
                                     dist, prefix + 'bylat_trend_fut_' + \
                                     year_str + '_' + str(lag) + '_' + \
                                     res + '_' + dcm + suffix + '.csv'),
                        index_col = 0, header = [0,1])
    data2.index = data2.index.astype(str)
    return data2


def separate_bylat_trend(trend_total, signal, eof):
    """Separate the S/N accounted part of the trend from the remainder."""
    trend_resid = pd.DataFrame(np.nan, index = trend_total.index,
                               columns = trend_total.columns)
    trend_eof = pd.DataFrame(np.nan, index = trend_total.index,
                             columns = trend_total.columns)
    for season in season_list:
        for model in trend_total.columns.levels[0]:
            # subtract the S/N of the individual model.
            trend_eof.loc[eof['lat_median'], (model, season)] = \
                signal.loc[season, model] * \
                (eof.loc[:, season].values / wgts)
            trend_resid.loc[:, (model, season)] = \
                trend_total.loc[:, (model, season)] - \
                trend_eof.loc[eof['lat_median'], (model, season)]
    return trend_eof, trend_resid


def restore_bylat_trend(signal_em, trend_resid, eof):
    trend_total_em = pd.DataFrame(np.nan,
                                  index = trend_total.index,
                                  columns = trend_total.columns)
    trend_eof_em = pd.DataFrame(np.nan,
                                index = trend_total.index,
                                columns = pd.MultiIndex.from_product([season_list, 
                                                                      ['mean', 'CI90_low', 'CI90_up', 
                                                                       'CI80_low', 'CI80_up']]))
    for season in season_list:
        eof_mean = signal_em.loc[season, 'mean'] * (eof.loc[:, season].values / wgts)
        ##eof_std = np.sqrt(np.power(signal_em.loc[season, 'se'],2) + \
        ##                  np.power(signal_em.loc[season, 'std'],2)) \
        ##          * (eof.loc[:, season].values / wgts)
        ##eof_std = signal_em.loc[season, 'se'] * \
        ##    (eof.loc[:, season].values / wgts)
        trend_eof_em.loc[eof['lat_median'], (season, 'mean')] = eof_mean
        ##trend_eof_em.loc[eof['lat_median'], (season, 'CI95_low')] = \
        ##    eof_mean - scistats.t.ppf(0.975, 12) * eof_std
        ##trend_eof_em.loc[eof['lat_median'], (season, 'CI95_up')] = \
        ##    eof_mean + scistats.t.ppf(0.975, 12) * eof_std
        ##trend_eof_em.loc[eof['lat_median'], (season, 'CI90_low')] = \
        ##    eof_mean - scistats.t.ppf(0.95, 12) * eof_std
        ##trend_eof_em.loc[eof['lat_median'], (season, 'CI90_up')] = \
        ##    eof_mean + scistats.t.ppf(0.95, 12) * eof_std
        ##trend_eof_em.loc[eof['lat_median'], (season, 'CI80_low')] = \
        ##    eof_mean - scistats.t.ppf(0.90, 12) * eof_std
        ##trend_eof_em.loc[eof['lat_median'], (season, 'CI80_up')] = \
        ##    eof_mean + scistats.t.ppf(0.90, 12) * eof_std
        trend_eof_em.loc[eof['lat_median'], (season, 'CI90_low')] = \
            signal_em.loc[season, 'CI90_lower'] * (eof.loc[:, season].values / wgts)
        trend_eof_em.loc[eof['lat_median'], (season, 'CI90_up')] = \
            signal_em.loc[season, 'CI90_upper'] * (eof.loc[:, season].values / wgts)
        trend_eof_em.loc[eof['lat_median'], (season, 'CI80_low')] = \
            signal_em.loc[season, 'CI80_lower'] * (eof.loc[:, season].values / wgts)
        trend_eof_em.loc[eof['lat_median'], (season, 'CI80_up')] = \
            signal_em.loc[season, 'CI80_upper'] * (eof.loc[:, season].values / wgts)
        for model in trend_resid.columns.levels[0]:
            trend_total_em.loc[:, (model,season)] = trend_resid.loc[:, (model,season)] + \
                                                    signal_em.loc[season, 'mean'] * eof.loc[:, season].values / wgts
    return trend_total_em, trend_eof_em


def plot_bylat_trend(axes,
                     trend_total, trend_eof, trend_resid,
                     trend_eof_em, trend_total_em):
    """ Plot the total trend, two components of the trend,
        corrected, and post-correction."""
    def wavg(df_trend):
        # ---- weight of the individual ensemble members
        temp_model = [x.split('_')[0] for x in df_trend.columns.get_level_values(0)]
        model_vote = np.full(df_trend.shape, np.nan)
        for ind, model in enumerate(temp_model):
            n_model = np.sum([x == model for x in temp_model])
            model_vote[:, ind] = 1 / n_model
        model_vote = model_vote / \
            np.sum(model_vote, axis = 1).reshape(-1,1) * \
            len(df_trend.columns.levels[1])
    
        # ---- weighted ensemble average of the trends
        weighted_avg = (df_trend * model_vote).groupby(level = 1, axis = 1 \
        ).sum().loc[:, season_list]

        # ---- weighted ensemble total of the consistency in trends
        pct_pos = ((df_trend > 0.).astype(float) * model_vote).groupby( \
            level = 1, axis = 1).sum().loc[:, season_list]
        pct_neg = ((df_trend < 0.).astype(float) * model_vote).groupby( \
            level = 1, axis = 1).sum().loc[:, season_list]
        hatch1 = ((weighted_avg.values > 0) & (pct_pos.values > pct)) | \
            ((weighted_avg.values < 0) & (pct_neg.values > pct))
        hatch2 = (~hatch1) & \
            (((weighted_avg.values > 0) & (pct_pos.values > (pct-0.1))) | \
             ((weighted_avg.values < 0) & (pct_neg.values > (pct-0.1))))
        return weighted_avg, hatch1, hatch2

    def apanel(ax, values, hatch1, hatch2):
        h = ax.contourf(range(values.shape[1]),
                        range(values.shape[0]),
                        values.values, cmap = cmap, levels = levels,
                        extend = 'both')
        ax.set_yticks(range(2, values.shape[0], 4))
        ax.set_yticklabels(lat_median_name[2::4])
        ax.set_xticks(np.linspace(0.35, 10.65, 12)) # range(0, 12, 1))
        ax.set_xticklabels(season_list_name)
        ax.tick_params(axis = 'both', length = 2, pad = 2)
    
        stipple(ax, range(hatch1.shape[0]), range(hatch1.shape[1]),
                mask = hatch1, hatch = hs)
        stipple(ax, range(hatch2.shape[0]), range(hatch2.shape[1]),
                mask = hatch2, hatch = '---')
        return h

    #
    ax = axes[0]
    weighted_avg, hatch1, hatch2 = wavg(trend_total)
    h = apanel(ax, weighted_avg, hatch1, hatch2)

    #
    ax = axes[1]
    weighted_avg, hatch1, hatch2 = wavg(trend_eof)
    apanel(ax, weighted_avg, hatch1, hatch2)

    #
    ax = axes[2]
    weighted_avg, hatch1, hatch2 = wavg(trend_resid)
    apanel(ax, weighted_avg, hatch1, hatch2)

    #
    ax = axes[3]
    mean = trend_eof_em.loc[:, (slice(None), 'mean')]
    #cl_a = trend_eof_em.loc[:, (slice(None), 'CI90_low')]
    #cu_a = trend_eof_em.loc[:, (slice(None), 'CI90_up')]
    #cl_b = trend_eof_em.loc[:, (slice(None), 'CI80_low')]
    #cu_b = trend_eof_em.loc[:, (slice(None), 'CI80_up')]
    ax.contourf(range(mean.shape[1]), range(mean.shape[0]),
                mean.values, cmap = cmap, levels = levels,
                extend = 'both')
    ax.set_yticks(range(2, mean.shape[0], 4))
    ax.set_yticklabels(lat_median_name[2::4])
    ax.set_xticks(np.linspace(0.35, 10.65, 12)) # range(0, 12, 1))
    ax.set_xticklabels(season_list_name)
    ax.tick_params(axis = 'both', length = 2, pad = 2)

    # cu_95 is not necessarily the smaller of the two
    #hatch1 = np.sign(cu_a.values) == np.sign(cl_a.values)
    #hatch2 = (~hatch1) & (np.sign(cu_b.values) == np.sign(cl_b.values))
    #stipple(ax, range(hatch1.shape[0]), range(hatch1.shape[1]),
    #        mask = hatch1, hatch = hs)
    #stipple(ax, range(hatch2.shape[0]), range(hatch2.shape[1]),
    #        mask = hatch2, hatch = '---')
    hatch1 = (trend_eof_em.loc[:, (slice(None), 'CI90_low')].values > 0.) | \
        (trend_eof_em.loc[:, (slice(None), 'CI90_up')].values < 0.)
    ##print(hatch1)
    stipple(ax, range(hatch1.shape[0]), range(hatch1.shape[1]),
            mask = hatch1, hatch = hs)
    hatch2 = ((trend_eof_em.loc[:, (slice(None), 'CI80_low')].values > 0.) | \
              (trend_eof_em.loc[:, (slice(None), 'CI80_up')].values < 0.)) & ~hatch1
    ##print(hatch2)
    stipple(ax, range(hatch2.shape[0]), range(hatch2.shape[1]),
            mask = hatch2, hatch = '---')

    #
    ax = axes[4]
    weighted_avg, hatch1, hatch2 = wavg(trend_total_em)
    apanel(ax, weighted_avg, hatch1, hatch2)

    return h


#
mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titleweight'] = 'normal'
mpl.rcParams['hatch.linewidth'] = 0.5
lab = 'abcdefghijklmnopqrstuvwxyz'
cmap = cmap_div()
levels = np.arange(-1.6e-2, 1.61e-2, 1e-3)
hs = '|||'
pct = 0.9


#
res = '0.5'
base = 'historical'
expr = 'historical'
dist = 'gmm'
folder = 'bylat'
prefix = '' # ['limit_', '']
suffix = '_noSahel' # ['', '_noSahel']
lag = 3
L = 46 # 2016 - 1973 + 1
lat_eof = [float(x) for x in lat_median]
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')
season_list = [str(i) for i in range(12)] # ensure order of x-axis
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']
year_list = [range(i, i+L) for i in [1995, 2010, 2025, 2040, 2055]]
year_str_list = [str(i[0])+'-'+str(i[-1]) for i in year_list]


# MODIFY
constrained = False

#
##if constrained:
# Constrained signal
##else:
# Original S/N ratio
_, _, signal_original_collect = collect_SN_model(prefix, suffix, folder, base, expr, 
                                                 dist, lag, res, season_list, depth_cm_new, 
                                                 year_list[0][0], L, year_list[0][0] + 1)

# Constrained S/N ratio
signal_em_collect = collect_SN_em(dist, season_list, depth_cm_new)


# Noise
noise_collect = collect_noise(prefix, suffix, folder, base, dist,
                              lag, res, season_list, depth_cm_new, L)

# Fingerprint
eof_collect = collect_eof_model(prefix, suffix, base, expr,
                                dist, lag, res,
                                lat_median, season_list, depth_cm_new)

# Area weights
if suffix == '_noSahel':
    colname = 'noSahel'
else:
    colname = ''
lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(), 'bylat_area.csv'),
                        index_col = 0)[colname].loc[np.array(lat_median).astype(int)].values)
wgts = lat_area_sqrt / np.mean(lat_area_sqrt)



#
for dind, dcm in enumerate(depth_cm_new):
    fig, axes = plt.subplots(nrows = 5, ncols = len(year_list), figsize = (6.5, 9), sharex = True, sharey = True)
    fig.subplots_adjust(wspace = 0.05, hspace = 0.1)
    # Total trend
    for yind, year in enumerate(year_list):
        year_str = year_str_list[yind]
        trend_total = read_bylat_trend()
        
        # Convert S/N ratio to signal
        for sind, season in enumerate(season_list):
            if sind == 0:
                signal = pd.DataFrame(np.nan, index = season_list,
                                      columns = signal_original_collect[(season,dcm)].columns)
            signal.loc[season, :] = signal_original_collect[(season, dcm)].loc[year[0], :] * \
                noise_collect.loc[season, dcm]

        # Separate the total trend
        eof = eof_collect.loc[:, :, dcm]
        trend_eof, trend_resid = separate_bylat_trend(trend_total, signal, eof)

        # Get the constrained S/N ratio
        signal_em = pd.DataFrame(np.nan, index = season_list,
                                 columns = ['mean', 'CI90_lower', 'CI90_upper',
                                            'CI80_lower', 'CI80_upper']) # , 'std'])
        for season, which in it.product(season_list, signal_em.columns):
            if which == 'mean':
                signal_em.loc[season, which] = signal_em_collect[(season, dcm)].loc[year[0], 'mean']
            elif which == 'CI90_lower':
                signal_em.loc[season, which] = signal_em_collect[(season, dcm)].loc[year[0], 'mean'] - \
                    signal_em_collect[(season, dcm)].loc[year[0], 'se'] * scistats.t.ppf(0.90, 12) # 12 ESMs in historical
            elif which == 'CI90_upper':
                signal_em.loc[season, which] = signal_em_collect[(season, dcm)].loc[year[0], 'mean'] + \
                    signal_em_collect[(season, dcm)].loc[year[0], 'se'] * scistats.t.ppf(0.90, 12)
            elif which == 'CI80_lower':                
                signal_em.loc[season, which] = signal_em_collect[(season, dcm)].loc[year[0], 'mean'] - \
                    signal_em_collect[(season, dcm)].loc[year[0], 'se'] * scistats.t.ppf(0.80, 12)
            else:
                signal_em.loc[season, which] = signal_em_collect[(season, dcm)].loc[year[0], 'mean'] + \
                    signal_em_collect[(season, dcm)].loc[year[0], 'se'] * scistats.t.ppf(0.80, 12)
            # convert S/N ratio to signal per se
            signal_em.loc[season, which] = signal_em.loc[season, which] * noise_collect.loc[season, dcm]

        # Restore the total trend using constrained S/N
        trend_total_em, trend_eof_em = restore_bylat_trend(signal_em, trend_resid, eof)

        # Plot
        axes_col = axes[:, yind]
        h = plot_bylat_trend(axes_col,
                             trend_total, trend_eof, trend_resid,
                             trend_eof_em, trend_total_em)

        axes_col[0].set_title(year_str)
        if yind == 0:
            axes_col[0].set_ylabel('Trend (year$^{-1}$)')
            axes_col[1].set_ylabel('Trend S/N (year$^{-1}$)')
            axes_col[2].set_ylabel('Trend remainder (year$^{-1}$)')
            axes_col[3].set_ylabel('Trend S/N constrained (year$^{-1}$)')
            axes_col[4].set_ylabel('Trend S/N restored (year$^{-1}$)')
        for kk in range(5):
            axes_col[kk].text(0., 1.02, lab[yind + kk * len(year_list)],
                              fontdict = {'weight': 'bold'},
                              transform = axes_col[kk].transAxes)

    cax = fig.add_axes([0.1, 0.07, 0.8, 0.01])
    plt.colorbar(h, cax = cax, orientation = 'horizontal')
    fig.savefig(os.path.join(mg.path_out(), 'figures',
                             prefix + 'fig3_fut_' + dist + '_' + str(lag) + \
                             '_bymonth_L=' + str(L) + '_' + res + \
                             suffix + '_em_' + dcm + '.png'), dpi = 600.,
                bbox_inches = 'tight')
    plt.close(fig)
