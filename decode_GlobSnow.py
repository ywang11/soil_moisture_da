import struct
import utils_management as mg
import os
import numpy as np
import xarray as xr


lat = []
f = open(os.path.join(mg.path_data(), 'GlobSnow', 'NLLATLSB'), 'rb')
while True:
    try:
        lat.append(struct.unpack('<i', f.read(4))[0])
    except:
        break
f.close()

lat = np.reshape(np.array(lat), (721, 721)) / 100000
lat[np.abs(lat - 14316.55765) < 1e-6] = np.nan
lat = lat



lon = []
f = open(os.path.join(mg.path_data(), 'GlobSnow', 'NLLONLSB'), 'rb')
while True:
    try:
        lon.append(struct.unpack('<i', f.read(4))[0])
    except:
        break
f.close()

lon = np.reshape(np.array(lon), (721, 721)) / 100000
lon[np.abs(lon - 14316.55765) < 1e-6] = np.nan
lon = lon


#
xr.Dataset({'lat': (('Y','X'), lat), 'lon': (('Y','X'), lon)},
           coords = {'Y': range(721), 'X': range(721)} \
).to_netcdf(os.path.join(mg.path_data(), 'GlobSnow', 'coords.nc'))
