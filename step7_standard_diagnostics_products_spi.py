"""
Compute the following for SPI product.

1. Global map (averaged over time): NetCDF
2. Global average time series.
3. Latitude bands average time series.
4. Continental average time series.
5. SREX region average time series.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, target_lat, target_lon
import itertools as it
from misc.standard_diagnostics import standard_diagnostics
import os
import pandas as pd
import numpy as np
import multiprocessing as mp


year = range(1971, 2017)
time = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                     freq = 'MS')


dist_list = ['weibull', 'gmm']
lag_list = [1,3,6]
res = '0.5'


i = REPLACE1
dist = 'REPLACE2'
lag = REPLACE3


ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


#prod_list = [x + '_' + y for x in ['mean', 'dolce', 'em'] \
#             for y in ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']]
prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all', 'mean_noncmip', 'mean_products']

for prod in prod_list:
#for i,dist,lag,prod in it.product(range(2), dist_list, lag_list, prod_list):
#def calc(option):
#    i, dist, prod, lag = option
    #if (i != 1) | (dist != 'gmm') | (lag != 6):
    #    continue

    dcm = depth_cm_new[i]

    spi = np.full([len(year) * 12, len(target_lat[res]),
                   len(target_lon[res])], np.nan)

    for m in range(12):
        fi = os.path.join(mg.path_intrim_out(), 'SPI_Products',
                          'get_' + ref_period_str,
                          dist, prod + '_' + dcm + '_' + str(lag) + '_' + \
                          res + '_' + str(m) + '.nc')

        data = xr.open_dataset(fi, decode_times=False)
        data['time'] = pd.date_range('1951-01-01', '2016-12-31', freq = 'YS')
        tmp = data['spi'].values[(data['time'].to_index().year >= \
                                  year[0]) & \
                                 (data['time'].to_index().year <= \
                                  year[-1]), :, :].copy()
        spi[m::12, :, :] = tmp
        data.close()

    standard_diagnostics(spi, time, data.lat, data.lon, 
                         os.path.join(mg.path_out(),
                                      'standard_diagnostics_spi',
                                      'products', dist),
                         prod + '_' + str(lag) + '_' + res + '_' + dcm)

#p = mp.Pool(4)
#p.map_async(calc, list(it.product(range(2), dist_list, prod_list, lag_list)))
#p.close()
#p.join()
