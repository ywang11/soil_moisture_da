"""
20200526
ywang254@utk.edu

Plot the trend in seasonal SPI.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.cmip6_utils import mrsol_availability
from misc.plot_utils import plot_ts_shade, cmap_gen, stipple
import matplotlib as mpl
import utils_management as mg
import cartopy.crs as ccrs
import itertools as it
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 'medium'
mpl.rcParams['hatch.linewidth'] = 0.4


res = '0.5'
season_list = [str(i) for i in range(12)]
L = 2016 - 1973 + 1
year_list = [range(i, i+L) for i in [1951, 1971, 1991, 2011, 2031, 2051]]
year_str_list = [str(i[0])+'-'+str(i[-1]) for i in year_list]
season_list = ['Annual', 'DJF', 'MAM', 'JJA', 'SON']
expr = 'historical'
folder = 'historical_extra'
suffix = '_noSahel' # ['', '_noSahel']
lag = 3
pct = 0.9


cmap = cmap_gen('Wistia_r', 'cool')
levels = np.arange(-2e-2, 2.1e-2, 2e-3)


#
for dist, dcm in it.product(['gmm', 'weibull'], depth_cm_new):
    fig, axes = plt.subplots(nrows = len(year_list), ncols = len(season_list),
                             figsize = (2. * len(year_list),
                                        1 + len(season_list)),
                             sharex = True, sharey = True,
                             subplot_kw = {'projection': ccrs.Miller()})
    fig.subplots_adjust(wspace = 0.02, hspace = 0.02)

    for j, k in it.product(range(len(season_list)), range(len(year_list))):
        season = season_list[j]
        year = year_list[k]
        year_str = year_str_list[k]

        ax = axes[k, j]
        ax.coastlines(lw = 0.1)
        ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)  
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)  
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.1)
        ax.spines['bottom'].set_lw(0.1)
        ax.spines['left'].set_lw(0.1)
        ax.spines['right'].set_lw(0.1)

        # Trend of all the products
        if expr == 'historical':
            a, _ = mrsol_availability(dcm, 'vanilla', 'historical', res)
            b, _ = mrsol_availability(dcm, 'vanilla', 'ssp585', res)
            cmip6_list = sorted(list(set(a) & set(b)))
        else:
            cmip6_list, _ = mrsol_availability(dcm, 'vanilla', expr, res)

        for mind, model in enumerate(cmip6_list):
            data = xr.open_dataset(os.path.join(mg.path_out(),
                'standard_diagnostics_spi', folder, dist,
                model + '_' + year_str + '_' + str(lag) + '_' + \
                res + '_' + dcm + '_g_map_trend_' + season + '.nc'))
            if mind == 0:
                val = np.full([len(cmip6_list), len(data.lat), len(data.lon)],
                              np.nan)
            val[mind, :, :] = data['g_map_trend'].values.copy()
            data.close()

        # Calculate the weighted average
        temp_model = [x.split('_')[0] for x in cmip6_list]
        vote_model = np.full(len(cmip6_list), np.nan)
        for ind, model in enumerate(temp_model):
            n_model = np.sum([x == model for x in temp_model])
            vote_model[ind] = 1/n_model
        vote_model = vote_model / np.sum(vote_model)

        val_mean = np.sum(val * np.broadcast_to(vote_model.reshape(-1,1,1),
                                                val.shape), axis = 0)
        h = ax.contourf(data.lon, data.lat, val_mean, cmap = cmap,
                        levels = levels, extend = 'both',
                        transform = ccrs.PlateCarree())
        if j == 0:
            t = ax.text(-0.15, 0.5, year_str, rotation = 90,
                        transform = ax.transAxes, verticalalignment='center')
            t.set_multialignment('center')
        if k == 0:
            ax.set_title(season)

        pct_pos = np.sum((val > 0.).astype(float) * \
                         np.broadcast_to(vote_model.reshape(-1,1,1),
                                         val.shape), axis = 0)
        pct_neg = np.sum((val < 0.).astype(float) * \
                         np.broadcast_to(vote_model.reshape(-1,1,1),
                                         val.shape), axis = 0)
        hatch1 = ((val_mean > 0) & (pct_pos > pct)) | \
            ((val_mean < 0) & (pct_neg > pct))
        stipple(ax, data.lat, data.lon, mask = hatch1,
                transform = ccrs.PlateCarree(), hatch = '///////////')
        hatch2 = (~hatch1) & \
            (((val_mean > 0) & (pct_pos > (pct - 0.1))) | \
             ((val_mean < 0) & (pct_neg > (pct - 0.1))))
        stipple(ax, data.lat, data.lon, mask = hatch2,
                transform = ccrs.PlateCarree(), hatch = '\\\\\\\\\\\\')

    cax = fig.add_axes([0.1, 0.08, 0.8, 0.01])
    plt.colorbar(h, cax = cax, orientation = 'horizontal',
                 label = 'Trend of ' + str(lag) + '-month SSI (year$^{-1}$)')
    fig.savefig(os.path.join(mg.path_out(), 'figures',
                             'fig5S_' + dist + '_trend_' + dcm + '_' + \
                             str(lag) + '_' + res + suffix + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
