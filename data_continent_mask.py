"""
2019/07/03

Create 0.5 deg NetCDF file of global countries, and then continents. 
"""
import regionmask
import xarray as xr
import numpy as np
import sys
import os
import matplotlib.pyplot as plt
import time
import utils_management as mg
import pandas as pd


## define resolution in degree
lon_res = 0.5 ## 1.875
lat_res = 0.5 ## 1.875

## specify resolutions
lon = np.arange(-180 + lon_res/2, 180.1 - lon_res/2, lon_res)
lat = np.arange(-90 + lon_res/2, 90.1 - lon_res/2, lat_res)


###############################################################################
# Country level
###############################################################################
## create dataset
ds = xr.Dataset(coords={'lon': lon,
                        'lat': lat,})

## define the srex regions
region = regionmask.defined_regions.natural_earth.countries_110.mask(ds)

region[:,:] = np.where(np.isnan(region), -9999, region)

region = region.astype(int)
region.attrs = {'_FillValue': -9999}

ds = region.to_dataset()

###############################################################################
# Continent level
###############################################################################
mapper = pd.read_csv(os.path.join(mg.path_data(), 
                                  'natural_earth_continent.csv'))


region = -9999 * np.ones([len(lat), len(lon)], dtype=int)

for ind,p in mapper.iterrows():
    region[ds['region'].values == int(p['Number'])] = \
        int(p['Continent_Number'])

# Correct the French Guiana
lon2d, lat2d = np.meshgrid(lon, lat)
region[(region != -9999) & (lat2d <= 10) & (lat2d >= 0.) & \
       (lon2d >= -60.) & (lon2d <= -40.)] = 7

ds2 = xr.DataArray(data = region,
                   dims = ['lat', 'lon'],
                   coords={'lon': lon, 'lat': lat,},
                   attrs = {'_FillValue': -9999}).to_dataset(name='region')


description = 'Continent ID - Abbrevation; '
continents = ['AF', 'AN', 'AS', 'EU', 'NA', 'OC', 'SA']

for i in range(1, len(continents)+1):
    description += str(i) + ' - ' + str(continents[i-1]) + '; '

ds2.attrs['title'] = 'Natural Earth Countries Mask'
ds2.attrs['history'] = 'Created on ' + time.ctime()
ds2.attrs['creator_email'] = 'ywang254@utk.edu'

ds2.attrs['projection'] = 'lonlat'
ds2.attrs['geospatial_lat_resolution'] = str(lat_res)+' deg'
ds2.attrs['geospatial_lon_resolution'] = str(lon_res)+' deg'
ds2.attrs['geospatial_lat_min'] = -89.75
ds2.attrs['geospatial_lat_max'] = 89.75
ds2.attrs['geospatial_lon_min'] = -179.75
ds2.attrs['geospatial_lon_max'] = 179.75

ds2.attrs['comment'] = description


ds2.to_netcdf(os.path.join(mg.path_intrim_out(), 'da_masks',
                           'natural_earth_continent_mask_' + str(lon_res) + \
                           '.nc'))
