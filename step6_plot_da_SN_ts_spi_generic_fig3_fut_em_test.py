"""
20210210
ywang254@utk.edu

Use emergent constraint to modify the future signal time series.

Plot the times series of the signal, and threshold of 
   likely (S/N > 0.95, detectable at 66% confidence)
   very likely (S/N > 1.64, 90% confidence)
   virtually certain (S/N > 2.57, 99% confidence).

The thresholds follow Marvel et al. 2019 DOI: 10.1038/s41586-019-1149-8

The EOF is flipped already in the fingerprint calculation step to ensure most
  are negative values.

Historical + RCP85 only. Until 2100.

20210219
ywang254@utk.edu

Test the future evolution of the regression coefficients.
Found to be nonlinear.
"""
import matplotlib.pyplot as plt
import pandas as pd
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median # depth_cm
from misc.plot_utils import plot_ts_shade, cmap_gen
import numpy as np
import cartopy.crs as ccrs
from misc.cmip6_utils import mrsol_availability
##from cartopy.mpl.gridliner import Gridliner
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil
import statsmodels.api as stats
from statsmodels.stats.outliers_influence import summary_table
from matplotlib.cm import get_cmap
from misc.em_utils import collect_SN_model, collect_SN_obs, get_constrained


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list_list = {'bymonth': [str(i) for i in range(12)],
                    'byyear': ['annual_mean', 'annual_maximum',
                               'annual_minimum',
                               'growing_season_mean', 'growing_season_maximum',
                               'growing_season_minimum']}


lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


#Setup / MODIFY
folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
season_name = 'bymonth'
base = 'historical'
expr = 'historical'
expr_name = 'ALL'
season_list = season_list_list[season_name]
season_list_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug',
                    'Sep','Oct','Nov','Dec']
prod = 'mean_noncmip'
lag = 3
start = 1971
L = 2016 - start + 1
limit = False # Not enough ensemble members to estimate the
              # variation around each ESM's mean.
if limit:
    prefix = 'limit_'
else:
    prefix = ''


a, _ = mrsol_availability('0-10cm', 'vanilla', expr, res)
b, _ = mrsol_availability('0-10cm', 'vanilla', 'ssp585', res)
cmip6_list = list(set(a) & set(b))
cmip6_list_models = list(np.unique([t.split('_')[0] for t in cmip6_list]))
cmap = get_cmap('jet')
clist = {model: cmap((1+ind)/len(cmip6_list_models)) \
         for ind, model in enumerate(cmip6_list_models)}
# mlist = ['o', 's', '^', 'v'] # Too few


for dist in ['gmm','weibull']:
    signal_mean_collect, signal_std_collect, _ = \
        collect_SN_model(prefix, suffix, folder, base, expr, dist,
                         lag, res, season_list, depth_cm_new, start, L)
    signal_obs_collect = \
        collect_SN_obs(prefix, suffix, folder, base, prod, dist,
                       lag, res, season_list, depth_cm_new, start, L)

    signal_std_collect.to_csv(os.path.join(mg.path_out(), 'figures',
                                           'fig3_em_calc',
                                           prefix + dist + \
                                           '_signal_collect_std.csv'))

    signal_collect_beta = pd.DataFrame(np.nan,
                                       index = signal_std_collect.index,
                                       columns = signal_std_collect.columns)
    signal_collect_p = pd.DataFrame(np.nan,
                                    index = signal_std_collect.index,
                                    columns = signal_std_collect.columns)

    for season, dcm in it.product(season_list, depth_cm_new):
        for yind, yy in enumerate(range(start, 2056)):
            # Conduct the emergent constraint regression
            x = signal_mean_collect[(season, dcm)].loc[start, :]
            y = signal_mean_collect[(season, dcm)].loc[yy, :]
            ##reg = stats.OLS(y.values,
            ##                stats.add_constant(x.values)).fit()
            reg = stats.OLS(y.values,
                            stats.add_constant(x.values)).fit()

            # Fill the data frames
            signal_collect_beta.loc[yy, (season, dcm)] = reg.params[1]
            signal_collect_p.loc[yy, (season, dcm)] = reg.pvalues[1]

    signal_collect_beta.to_csv(os.path.join( \
        mg.path_out(), 'figures', 'fig3_em_calc',
        prefix + dist + '_signal_collect_beta.csv'))
    signal_collect_p.to_csv(os.path.join( \
        mg.path_out(), 'figures', 'fig3_em_calc',
        prefix + dist + '_signal_collect_p.csv'))
