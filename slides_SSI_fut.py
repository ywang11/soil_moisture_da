import matplotlib.pyplot as plt
import pandas as pd
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median # depth_cm
from misc.plot_utils import plot_ts_shade, cmap_div
import numpy as np
from misc.cmip6_utils import mrsol_availability
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
from misc.plot_utils import stipple
from matplotlib.cm import get_cmap
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from misc.em_utils import collect_SN_model, collect_eof_model, collect_noise, collect_SN_em
import scipy.stats as scistats


def read_bylat_trend():
    """ Read the modeled future trends of the CMIP6 models."""
    data2 = pd.read_csv(os.path.join(mg.path_out(),
                                     'standard_diagnostics_spi', expr,
                                     dist, prefix + 'bylat_trend_fut_' + \
                                     year_str + '_' + str(lag) + '_' + \
                                     res + '_' + dcm + suffix + '.csv'),
                        index_col = 0, header = [0,1])
    data2.index = data2.index.astype(str)

    data2_p = pd.read_csv(os.path.join(mg.path_out(),
                                       'standard_diagnostics_spi', expr,
                                       dist, prefix + 'bylat_trend_p_fut_' + \
                                       year_str + '_' + str(lag) + '_' + \
                                       res + '_' + dcm + suffix + '.csv'),
                         index_col = 0, header = [0,1])
    data2_p.index = data2_p.index.astype(str)

    return data2, data2_p


def plot_bylat_trend(ax, trend_total, trend_total_p):
    """ Plot the total trend, two components of the trend,
        corrected, and post-correction."""
    def wavg(df_trend, df_trend_p):
        # ---- weight of the individual ensemble members
        temp_model = [x.split('_')[0] for x in \
                      df_trend.columns.get_level_values(0)]
        model_vote = np.full(df_trend.shape, np.nan)
        for ind, model in enumerate(temp_model):
            n_model = np.sum([x == model for x in temp_model])
            model_vote[:, ind] = 1 / n_model
        model_vote = model_vote / \
            np.sum(model_vote, axis = 1).reshape(-1,1) * \
            len(df_trend.columns.levels[1])
    
        # ---- weighted ensemble average of the trends' significance
        weighted_avg = (df_trend * model_vote).groupby(level = 1, axis = 1 \
        ).sum().loc[:, season_list]

        # ------------ weighted_avg_p indicates at least 90%
        weighted_avg_p = ((df_trend_p <= 0.05) * model_vote).groupby(level = 1, axis = 1 \
            ).sum().loc[:, season_list]
        hatch = weighted_avg_p < 0.5

        return weighted_avg, hatch

    def apanel(ax, values, hatch):
        h = ax.contourf(range(values.shape[1]),
                        range(values.shape[0]),
                        values.values, cmap = cmap, levels = levels,
                        extend = 'both')
        ax.set_yticks(range(2, values.shape[0], 4))
        ax.set_yticklabels(lat_median_name[2::4])
        ax.set_xticks(np.linspace(0.35, 10.65, 12)) # range(0, 12, 1))
        ax.set_xticklabels(season_list_name)
        ax.tick_params(axis = 'both', length = 2, pad = 2)
    
        ax.contour(range(hatch.shape[1]), range(hatch.shape[0]),
                    hatch, levels = [0.5], colors = 'grey', linestyles = 'dashed',
                    linewidths = 0.5)
        ax.contourf(range(hatch.shape[1]), range(hatch.shape[0]),
                    hatch, levels = [0.5, 1.5], colors = 'w', alpha = 0.4)
        return h

    #
    weighted_avg, hatch = wavg(trend_total, trend_total_p)
    h = apanel(ax, weighted_avg, hatch)

    return h


#
mpl.rcParams['font.size'] = 16
mpl.rcParams['axes.titleweight'] = 'normal'
mpl.rcParams['hatch.linewidth'] = 0.5
lab = 'abcdefghijklmnopqrstuvwxyz'
cmap = cmap_div()
levels = np.arange(-1.6e-2, 1.61e-2, 1e-3)
hs = '|||'
pct = 0.9


#
res = '0.5'
base = 'historical'
expr = 'historical'
dist = 'gmm'
folder = 'bylat'
prefix = '' # ['limit_', '']
suffix = '_noSahel' # ['', '_noSahel']
lag = 3
L = 46 # 2016 - 1973 + 1
lat_eof = [float(x) for x in lat_median]
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')
season_list = [str(i) for i in range(12)] # ensure order of x-axis
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']
year_list = [range(i, i+L) for i in [1995, 2025, 2055]]
year_str_list = [str(i[0])+'-'+str(i[-1]) for i in year_list]


# MODIFY
constrained = False
method = 'gam'


# Area weights
if suffix == '_noSahel':
    colname = 'noSahel'
else:
    colname = ''
lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(), 'bylat_area.csv'),
                        index_col = 0)[colname].loc[np.array(lat_median).astype(int)].values)
wgts = lat_area_sqrt / np.mean(lat_area_sqrt)


#
fig, axes = plt.subplots(nrows = 2, ncols = len(year_list),
                         figsize = (8,8),
                         sharex = True, sharey = True)
fig.subplots_adjust(wspace = 0.05, hspace = 0.1)
for dind, dcm in enumerate(depth_cm_new):
    # Total trend
    for yind, year in enumerate(year_list):
        year_str = year_str_list[yind]
        trend_total, trend_total_p = read_bylat_trend()

        # Plot
        ax = axes[dind, yind]
        h = plot_bylat_trend(ax, trend_total, trend_total_p)

        if dind == 0:
            ax.set_title(year_str)
        if yind == 0:
            ax.set_ylabel(dcm)
        ax.text(0., 1.03, lab[dind*len(year_list) + yind], transform = ax.transAxes, weight = 'bold')

cax = fig.add_axes([0.95, 0.1, 0.01, 0.8])
plt.colorbar(h, cax = cax, orientation = 'vertical',
             label = 'Trend of ' + str(lag) + '-month SSI (year$^{-1}$)')
fig.savefig(os.path.join(mg.path_out(), 'figures',
                         prefix + 'slide_fut_' + dist + '_' + str(lag) + \
                         '_bymonth_L=' + str(L) + '_' + res + \
                         suffix + '_em.png'), dpi = 600.,
            bbox_inches = 'tight')
plt.close(fig)
