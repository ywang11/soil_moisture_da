"""
20200425
ywang254@utk.edu

Plot the trend in latitudinal mean SPI.
"""
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import matplotlib as mpl
import utils_management as mg
import itertools as it


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titleweight'] = 'normal'
mpl.rcParams['hatch.linewidth'] = 0.5
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list = [str(i) for i in range(12)]
#year_list = [range(i, i + 50) for i in range(1957, 2048, 10)]
L = 46 # 2016 - 1973 + 1
year_list = [range(i, i+L) for i in [1995, 2010, 2025, 2040, 2055]]
year_str_list = [str(i[0])+'-'+str(i[-1]) for i in year_list]
expr = 'historical'


folder = 'bylat'
prefix = '' # ['limit_', '']
suffix = '_noSahel' # ['', '_noSahel']
lag = 3


lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


season_list = [str(i) for i in range(12)] # ensure order of x-axis
#lat_median = lat_median[::-1] # ensure order of y-axis
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']


cmap = cmap_div()
levels = np.arange(-1.6e-2, 1.61e-2, 1e-3)
hs = '/////'
pct = 0.9


for dist in ['gmm', 'weibull']:
    fig, axes = plt.subplots(nrows = len(depth_cm_new), ncols = len(year_list),
                             figsize = (6.5, 6),
                             sharex = True, sharey = True)
    fig.subplots_adjust(wspace = 0.01, hspace = 0.1)
    for i,j in it.product(range(len(year_list)), range(len(depth_cm_new))):
        dcm = depth_cm_new[j]
        year = year_list[i]
        year_str = year_str_list[i]
    
        ax = axes.flat[j * len(year_list) + i]

        data2 = pd.read_csv(os.path.join(mg.path_out(),
                                         'standard_diagnostics_spi', expr,
                                         dist, prefix + 'bylat_trend_fut_' + \
                                         year_str + '_' + str(lag) + '_' + \
                                         res + '_' + dcm + suffix + '.csv'),
                            index_col = 0, header = [0,1])
        data2.index = data2.index.astype(str)

        # ---- weight of the individual ensemble members
        temp_model = [x.split('_')[0] for x in \
                      data2.columns.get_level_values(0)]
        data2_vote = np.full(data2.shape, np.nan)
        for ind, model in enumerate(temp_model):
            n_model = np.sum([x == model for x in temp_model])
            data2_vote[:, ind] = 1 / n_model
        data2_vote = data2_vote / \
            np.sum(data2_vote, axis = 1).reshape(-1,1) * \
            len(data2.columns.levels[1])
    
        # ---- weighted ensemble average of the trends
        weighted_avg = (data2 * data2_vote).groupby(level = 1, axis = 1 \
        ).sum().loc[:, season_list]
        h = ax.contourf(range(weighted_avg.shape[1]),
                        range(weighted_avg.shape[0]),
                        weighted_avg.values, cmap = cmap, levels = levels,
                        extend = 'both')
        ax.set_yticks(range(2, weighted_avg.shape[0], 4))
        ax.set_yticklabels(lat_median_name[2::4])
        ax.set_xticks(np.linspace(0.35, 10.65, 12)) # range(0, 12, 1))
        ax.set_xticklabels(season_list_name)
        ax.tick_params(axis = 'both', length = 2, pad = 2)
        if i == 0:
            ax.set_ylabel(dcm)
    
        # ---- weighted ensemble total of the consistency in trends
        pct_pos = ((data2 > 0.).astype(float) * data2_vote).groupby( \
            level = 1, axis = 1).sum().loc[:, season_list]
        pct_neg = ((data2 < 0.).astype(float) * data2_vote).groupby( \
            level = 1, axis = 1).sum().loc[:, season_list]
        hatch1 = ((weighted_avg.values > 0) & (pct_pos.values > pct)) | \
            ((weighted_avg.values < 0) & (pct_neg.values > pct))
        stipple(ax, range(hatch1.shape[0]), range(hatch1.shape[1]),
                mask = hatch1, hatch = hs)
        hatch2 = (~hatch1) & \
            (((weighted_avg.values > 0) & (pct_pos.values > (pct-0.1))) | \
             ((weighted_avg.values < 0) & (pct_neg.values > (pct-0.1))))
        stipple(ax, range(hatch2.shape[0]), range(hatch2.shape[1]),
                mask = hatch2, hatch = '\\\\\\')

        ax.text(0.05, 1.03, lab[j*len(year_list)+i],
                transform = ax.transAxes, weight = 'bold')
        ax.set_title(year_str, pad = 2)
    cax = fig.add_axes([0.1, 0.06, 0.8, 0.015])
    plt.colorbar(h, cax = cax, orientation = 'horizontal',
                 label = 'Trend of ' + str(lag) + '-month SSI (year$^{-1}$)')
    cax.text(.0, -1., 'Dry', fontsize = 6, transform = cax.transAxes,
             horizontalalignment = 'center') # , color = '#762a83')
    cax.text(1.,  -1., 'Wet', fontsize = 6, transform = cax.transAxes,
             horizontalalignment = 'center') # , color = '#3288bd')
    fig.savefig(os.path.join(mg.path_out(), 'figures', prefix + \
                             'fig5_' + dist + '_trend_' + str(lag) + \
                             '_' + res + suffix + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
