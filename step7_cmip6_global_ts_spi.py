import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, target_lat, target_lon
import itertools as it
from misc.cmip6_utils import mrsol_availability
from misc.analyze_utils import get_ndvi_mask
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
import sys
import time


res = '0.5'
land_mask = 'vanilla'


ref_expr = 'historical'
if ref_expr == 'piControl':
    ref_period = range(0, 201)
else:
    ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


#
expr = 'REPLACE1' # 'historical', 'hist-GHG', 'hist-aer', 'NoNAT',
                  # 'GHGAER', 'hist-nat'
noSahel = REPLACE2 # True, False
if noSahel:
    ndvi_mask = get_ndvi_mask(land_mask)
    suffix = '_noSahel'
else:
    suffix = ''
i = REPLACE3
lag = REPLACE4 # 1,3,6


# km^2 of land in each grid cell.
data = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                    'elm_half_degree_grid_negLon.nc'))
if noSahel:
    land_area = data.area.where(ndvi_mask).values * data.landfrac.values
else:
    land_area = data.area.values * data.landfrac.values
data.close()


for dist in ['gmm', 'weibull']:
    if expr == 'historical':
        year = range(1951, 2101)
    else:
        year = range(1951, 2021)

    dcm = depth_cm_new[i]
    period = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                           freq = 'MS')
    if expr == 'historical':
        a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
        b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
        cmip6_list = sorted(list(set(a) & set(b)))
    else:
        cmip6_list, _ = mrsol_availability(dcm, land_mask, expr, res)

    #for model in cmip6_list:
    def calc(model):
        start = time.time()

        spi = np.full([len(year), 12, len(target_lat[res]),
                       len(target_lon[res])], np.nan)
        for m in range(12):
            fi = os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                              'get_' + ref_expr + '_' + ref_period_str,
                              dist, expr, model + '_' + dcm + '_' + \
                              str(lag) + '_' + res + '_' + str(m) + '.nc')
            data = xr.open_dataset(fi)
            if noSahel:
                tmp = data['spi'].where(ndvi_mask).values.copy()
            else:
                tmp = data['spi'].values.copy()
            spi[:, m, :, :] = tmp
            data.close()
        spi = spi.reshape(len(year) * 12, len(target_lat[res]),
                          len(target_lon[res]))

        g_ts = np.nansum(spi * land_area.reshape(1, spi.shape[1],
                                                 spi.shape[2]),
                         axis=(1,2)) / np.nansum(land_area)
        pd.DataFrame(g_ts.reshape(-1,1), index = period,
                     columns = ['global_mean'] \
        ).to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_spi',
                              expr, dist, model + '_' + str(lag) + '_' + \
                              res + '_' + dcm + '_g_ts' + suffix + '.csv'))

        pos = np.where([x == model for x in cmip6_list])[0][0]

        end = time.time()
        print('Finished ' + dcm + ' ' + str(lag) + ' ' + dist + ' ' + \
              model + ' which is ' + str(pos) + '-th in the chunk in ' + \
              str(end - start) + ' seconds.')

    p = mp.Pool(2)
    p.map_async(calc, cmip6_list)
    p.close()
    p.join()
