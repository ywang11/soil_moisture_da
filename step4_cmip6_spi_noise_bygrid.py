"""
20210719

Trends in the piControl simulations of SSI. 
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, target_lat, target_lon
import itertools as it
from misc.cmip6_utils import one_layer_availability, mrsol_availability
from misc.analyze_utils import calc_reg, get_ndvi_mask
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
import sys
from map_cmip6_drivers_trend import get_model_list
import dask.array as dsa


land_mask = 'vanilla'
res = '0.5'
res_to = '5'
lag = 3
N = 200
L = 2016 - 1971 + 1
noSahel = True
dcm = depth_cm_new[REPLACE1] # '0-10cm'
season = 'REPLACE2' # 0..11


if noSahel:
    ndvi_mask = get_ndvi_mask(land_mask)
    suffix = '_noSahel'
else:
    suffix = ''
cmip6_list, _ = mrsol_availability(dcm, land_mask, 'piControl', res)
cmip6_list = sorted(cmip6_list)
model = cmip6_list[REPLACE3]


def read_piControl(dcm, model, season):
    ##data_time = pd.date_range(start = '2001-01-01', end = '2200-12-31', freq = 'YS')
    fn = os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                      'get_historical_1970-2014', 'gmm', 'piControl',
                      model + '_' + dcm + '_' + str(lag) + '_' + res + \
                      '_' + season + '.nc')
    hr = xr.open_dataset(fn, decode_times = False)
    if noSahel:
        spi = hr.spi.where(ndvi_mask).copy(deep = True)
    else:
        spi = hr.spi.copy(deep = True)
    hr.close()
    return spi


def nanmean(x):
    if sum(np.isnan(x.reshape(-1))) < (len(x.reshape(-1)) * 0.95):
        return np.array([[[np.nanmean(x)]]])
    else:
        return np.array([[[np.nan]]])


def interp(var):
    """ To 5 degrees. """
    da = dsa.from_array(var.values, chunks = (1, 10, 10))
    da2 = da.map_blocks(lambda x: nanmean(x), dtype = np.float64,
                        chunks = (1,1,1)).compute()
    var2 = xr.DataArray(da2, dims = ['time', 'lat', 'lon'],
                        coords = {'time': var['time'], 'lon': target_lon[res_to],
                                  'lat': target_lat[res_to]}).load()
    return var2


# calculate trend
g_map_trend = xr.DataArray(np.full([N-L+1, len(target_lat[res_to]),
                                    len(target_lon[res_to])], np.nan),
                           dims = ['time', 'lat', 'lon'],
                           coords = {'time': range(N-L+1),
                                     'lat': target_lat[res_to],
                                     'lon': target_lon[res_to]})
g_p_values = xr.DataArray(np.full([N-L+1, len(target_lat[res_to]),
                                   len(target_lon[res_to])], np.nan),
                          dims = ['time', 'lat', 'lon'],
                          coords = {'time': range(N-L+1),
                                    'lat': target_lat[res_to],
                                    'lon': target_lon[res_to]})
g_intercepts = xr.DataArray(np.full([N-L+1, len(target_lat[res_to]),
                                     len(target_lon[res_to])], np.nan),
                            dims = ['time', 'lat', 'lon'],
                            coords = {'time': range(N-L+1),
                                      'lat': target_lat[res_to],
                                      'lon': target_lon[res_to]})
spi = interp(read_piControl(dcm, model, season))

for yy in range(N - L + 1):
    temp = xr.apply_ufunc(calc_reg, spi[yy:(yy+L), :, :].chunk({'time':-1}),
                          input_core_dims = [['time']], vectorize = True,
                          dask = 'parallelized', output_core_dims = [['new']],
                          dask_gufunc_kwargs = {'output_sizes': {'new': 3}})
    temp.compute()
    g_map_trend[yy, :, :] = temp[:,:,1]
    g_p_values[yy, :, :] = temp[:,:,2]
    g_intercepts[yy, :, :] = temp[:,:,0]

    print(yy)
ds = xr.Dataset({'g_map_trend': g_map_trend,
                 'g_p_values': g_p_values,
                 'g_intercepts': g_intercepts})
ds['lon'].encoding['_FillValue'] = None
ds['lat'].encoding['_FillValue'] = None
ds['time'].encoding['_FillValue'] = None
ds.to_netcdf(os.path.join(mg.path_out(), 'cmip6_spi_noise', 'bygrid',
                          model + '_' + res_to + '_' + dcm + '_' + str(lag) + \
                          '_' + str(season) + suffix + '.nc'))
