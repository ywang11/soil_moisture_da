"""
20200425
ywang254@utk.edu

Check whether the seasonal trends in P, ET, Q and soil moisture matches.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median, \
    target_lat, target_lon
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from misc.plot_utils import plot_ts_shade, cmap_gen, stipple, cmap_div
import matplotlib as mpl
import utils_management as mg
import itertools as it
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


season_list = ['Annual'] # ['DJF', 'MAM', 'JJA', 'SON']

mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 'medium'
mpl.rcParams['hatch.linewidth'] = 0.4

res = '0.5'
land_mask = 'vanilla'
lat = target_lat[res]
lon = target_lon[res]
pct = 0.9 # consistent with "step8_plot_trend_spi_bylat.py"
hs = '\\\\\\\\'

cmap = cmap_div()
levels = np.arange(-2e-2, 2.1e-2, 2e-3)


model = 'CanESM5_r10i1p1f1'


for year, season in it.product([range(1951,2015)], # range(1981,2015)
                               season_list):
    year_str = str(year[0]) + '-' + str(year[-1])
    if year_str == '1951-2014':
        # 'mrro'
        var_list = ['pr', 'evspsbl', 'pr-evspsbl', 'pr-evspsbl-mrro', # 'mrro'
                    'sm_0-10cm', 'sm_0-100cm']
        var_list_name = ['P', 'ET', 'P-ET', 'P-ET-Q', # 'Q'
                         'θ 0-10cm', 'θ 0-100cm']
        var_list_units = ['mm day$^{-1}$ yr$^{-1}$',
                          'mm day$^{-1}$ yr$^{-1}$',
                          'mm day$^{-1}$ yr$^{-1}$',
                          'mm day$^{-1}$ yr$^{-1}$',
                          'mm mm$^{-1}$ yr$^{-1}$',
                          'mm mm$^{-1}$ yr$^{-1}$']
    else:
        var_list = ['pr', 'evspsbl', 'mrro', 'snw',
                    'pr-evspsbl-mrro', 'pr-evspsbl-mrro-dsnw',
                    'sm_0-10cm', 'sm_0-100cm']
        var_list_name = ['P', 'ET', 'Q', 'SWE', 'P-ET-Q', 'P-ET-Q-dSWE',
                         'θ 0-10cm', 'θ 0-100cm']
        var_list_units = ['mm day$^{-1}$ yr$^{-1}$',
                          'mm day$^{-1}$ yr$^{-1}$',
                          'mm day$^{-1}$ yr$^{-1}$',
                          'mm yr$^{-1}$',
                          'mm day$^{-1}$ yr$^{-1}$',
                          'mm day$^{-1}$ yr$^{-1}$',
                          'mm mm$^{-1}$ yr$^{-1}$',
                          'mm mm$^{-1}$ yr$^{-1}$']

    fig, axes = plt.subplots(nrows = int(np.ceil(len(var_list) // 2)),
                             ncols = 2,
                             figsize = (6.5, len(var_list) * 0.9),
                             sharex = True, sharey = True,
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    fig.subplots_adjust(wspace = 0.1, hspace = 0.1)
    for vind, vv in enumerate(var_list):
        hr = xr.open_dataset(os.path.join(mg.path_out(),
                                          'cmip6_drivers_summary',
                                          vv, model + '_' + res + \
                                          '_g_map_trend_' + season +\
                                          '_' + year_str + '.nc'))
        val = hr['g_map_trend'].values.copy()
        hr.close()

        ax = axes.flat[vind]
        ax.coastlines(lw = 0.1)
        gl = ax.gridlines(xlocs = [-180, 180],
                          ylocs = np.arange(-40, 61, 20),
                          linestyle = '--',
                          linewidth = 0.5, draw_labels = True)
        gl.xlabels_top = False
        gl.xlabels_bottom = False
        gl.ylabels_right = False
        gl.xlocator = mticker.FixedLocator([-180, 180])
        gl.ylocator = mticker.FixedLocator([-40, -20, 0, 20, 40, 60])
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        gl.xlabel_style = {'color': 'black', 'weight': 'normal'}
        ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.2)
        ax.spines['bottom'].set_lw(0.2)
        ax.spines['left'].set_lw(0.2)
        ax.spines['right'].set_lw(0.2)

        h = ax.contourf(lon, lat, val, cmap = cmap,
                        levels = levels, extend = 'both',
                        transform = ccrs.PlateCarree())
        ax.set_title(var_list_name[vind] + '\n' + var_list_units[vind])

    cax = fig.add_axes([0.1, 0.08, 0.8, 0.01])
    plt.colorbar(h, cax = cax, orientation = 'horizontal')
    fig.savefig(os.path.join(mg.path_out(), 'figures',
                             'check_' + model + '_drivers_trend_' + \
                             season + '_' + \
                             year_str + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
