"""
2020/03/13

ywang254@utk.edu

Calculate the linear trend in the annual time series, and in the per month
  time series by latitude band during the full period for each CMIP6 model.

Ensure same time period as "standard_diagnostics.py", i.e. 1967-2016
"""
import pandas as pd
from utils_management.constants import depth_cm_new, lat_median
import utils_management as mg
from misc.cmip6_utils import mrsol_availability
from misc.standard_diagnostics import get_bylat_trend
from misc.analyze_utils import get_ndvi_mask
import itertools as it
import os
import numpy as np
import multiprocessing as mp


res = '0.5'
land_mask = 'vanilla'

# Time period to calculate the trend on - MODIFY, which changes the 
# associated set of land surface models.
##year = range(1951, 2017)
year = range(1971,2017)
time_range = pd.date_range(start = str(year[0])+'-01-01',
                           end = str(year[-1])+'-12-31', freq = 'YS')


noSahel = True
if noSahel:
    suffix = '_noSahel'
else:
    suffix = ''

    
# 'OTH'
##for dist, expr, lag, i in it.product(['gmm', 'weibull'],
##                                     ['historical', 'hist-GHG', 'hist-nat',
##                                      'hist-aer', 'GHGAER', 'NoNAT'],
##                                     [1, 3, 6], range(2)):
def calc(option):
    limit, dist, expr, lag, i = option

    dcm = depth_cm_new[i]

    if expr == 'historical':
        a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
        b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
        cmip6_list = list(set(a) & set(b))
    else:
        cmip6_list, _ = mrsol_availability(dcm, land_mask, expr, res)

    if limit:
        prefix = 'limit_'
        c, _ = mrsol_availability(dcm, land_mask, 'piControl', res)
        cmip6_list = list(set(cmip6_list) & set(c))
    else:
        prefix = ''

    #
    trend_collection = pd.DataFrame(data = np.nan,
                                    index = lat_median,
        columns = pd.MultiIndex.from_product([cmip6_list, range(12)]))
    trend_p_collection = pd.DataFrame(data = np.nan,
                                      index = lat_median,
        columns = pd.MultiIndex.from_product([cmip6_list, range(12)]))

    for season, model in it.product(range(12), cmip6_list):
        fname = os.path.join(mg.path_out(), 'cmip6_spi_summary', expr, dist,
                             model + '_' + dcm + '_' + str(lag) + '_' + \
                             str(season) + '_' + res + '_g_lat_ts' + \
                             suffix + '.csv')
        trend, trend_p = get_bylat_trend(fname, time_range)

        trend_collection.loc[:, (model, season)] = trend
        trend_p_collection.loc[:, (model, season)] = trend_p

    trend_collection.to_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_spi', expr, dist, prefix + 'bylat_trend_' + \
        str(lag) + '_' + res + '_' + dcm + suffix + '.csv'))
    trend_p_collection.to_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_spi', expr, dist, prefix + 'bylat_trend_p_' + \
        str(lag) + '_' + res + '_' + dcm + suffix + '.csv'))


p = mp.Pool(6)
p.map_async(calc, list(it.product([True, False], ['gmm', 'weibull'],
                                  ['historical', 'hist-GHG', 'hist-nat',
                                   'hist-aer', 'GHGAER', 'NoNAT'],
                                  [1,3,6], range(2))))
p.close()
p.join()

