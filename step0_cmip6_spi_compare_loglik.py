"""
20200605

ywang254@utk.edu

Compare the percentage of situations when the log-likelihood of 
 Weibull was greater than that of GMM.

Plot for all CMIP6 ESMs, and for individual ESMs.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, depth_new, target_lat, \
    target_lon
from misc.da_utils import get_model_list
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import matplotlib as mpl
import multiprocessing as mp
import itertools as it
import time
from misc.analyze_utils import get_ndvi_mask
from cartopy.feature import LAND


land_mask = 'vanilla'
res = '0.5'
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'
lag_list = [1, 3, 6]
lat = target_lat[res]
lon = target_lon[res]
ref_expr = 'historical'
if ref_expr == 'historical':
    ref_period = range(1970, 2015) # range(1950, 2014)
else:
    ref_period = range(0, 200)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


mlist = ['BCC-CSM2-MR', 'BCC-ESM1', 'CESM2', 'CESM2-WACCM', 'CNRM-CM6-1',
         'CNRM-ESM2-1', 'CanESM5', 'EC-Earth3', 'EC-Earth3-Veg',
         'GISS-E2-1-G', 'GISS-E2-1-H', 'HadGEM3-GC31-LL', 'IPSL-CM6A-LR',
         'MIROC6', 'MPI-ESM1-2-HR', 'NorESM2-LM', 'SAM0-UNICON', 'UKESM1-0-LL']

# MODIFY
make_data = False
noSahel = True
if noSahel:
    suffix = '_noSahel'
    ndvi_mask = get_ndvi_mask(land_mask)


if make_data:
    pct_weibull_better = np.full([len(mlist), len(depth_cm_new), len(lag_list),
                                  len(lat), len(lon)], np.nan)
    for mind, lind, dind in it.product(range(1, len(mlist)),
                                       range(len(lag_list)),
                                       range(len(depth_cm_new))):
        model = mlist[mind]
        s = lag_list[lind]
        dcm = depth_cm_new[dind]
    
        pct_temp = np.full([12, len(lat), len(lon)], np.nan)
    
        # Skip if does not exist for this depth
        model_list = get_model_list(['historical'], dcm, land_mask, res)
        if not model in model_list.keys():
            continue
        for month in range(12):
            ## Temporary - not all models are complete
            ##if not os.path.exists(os.path.join(mg.path_intrim_out(),
            ##                                   'SPI_CMIP6',
            ##                                   'fit', 'weibull',
            ##                                   model + '_' + dcm + \
            ##                                   '_' + str(s) + '_' + res + \
            ##                                   '_' + str(month) + '.nc')):
            ##    continue

            hr = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                              'SPI_CMIP6',
                                              'fit_' + ref_expr + '_' + \
                                              ref_period_str, 'weibull',
                                              model + '_' + dcm + '_' + \
                                              str(s) + '_' + res + '_' + \
                                              str(month) + '.nc'))
            if noSahel:
                loglik_weibull = hr['loglik'].where(ndvi_mask).values.copy()
            else:
                loglik_weibull = hr['loglik'].values.copy()
            hr.close()
    
            hr = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                              'SPI_CMIP6',
                                              'fit_' + ref_expr + '_' + \
                                              ref_period_str, 'gmm',
                                              model + '_' + dcm + '_' + \
                                              str(s) + '_' + res + '_' + \
                                              str(month) + '.nc'))
            if noSahel:
                loglik_gmm = hr['loglik'].where(ndvi_mask).values.copy()
            else:
                loglik_gmm = hr['loglik'].values.copy()
            hr.close()
            
            temp = (loglik_weibull > loglik_gmm).astype(float)
            pct_temp[month, :, :] = np.where(~np.isnan(loglik_weibull),
                                             temp, np.nan)
        pct_weibull_better[mind, dind, lind, :, :] = pct_temp.mean(axis = 0)
    xr.DataArray(pct_weibull_better,
                 dims = ['model', 'depth', 'lag', 'lat', 'lon'],
                 coords = {'model': range(len(mlist)),
                           'depth': range(len(depth_cm_new)),
                           'lag': lag_list, 'lat': lat, 'lon': lon},
                 attrs = {'model_names': '; '.join([str(i) + ': ' + \
                                                    str(mlist[i]) for i in \
                                                    range(len(mlist))]),
                          'depth': '; '.join([str(i) + ': ' + \
                                              str(depth_cm_new[i]) for i in \
                                              range(len(depth_cm_new))])} \
    ).to_dataset(name = 'pct_weibull_better' \
    ).to_netcdf(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                             'fit_' + ref_expr + '_' + ref_period_str,
                             'compare.nc'))
else:
    hr = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                                      'fit_' + ref_expr + '_' + ref_period_str,
                                      'compare.nc'))
    pct_weibull_better = hr['pct_weibull_better'].values.copy()
    hr.close()


#All models plot
mpl.rcParams['font.size'] = 5.5
mpl.rcParams['axes.titlesize'] = 5.5
fig, axes = plt.subplots(len(lag_list), len(depth_cm_new),
                         figsize = (5.5, 4),
                         subplot_kw = {'projection': ccrs.Miller()})
fig.subplots_adjust(hspace = 0.01, wspace = 0.01)
for lind, dind in it.product(range(len(lag_list)),
                             range(len(depth_cm_new))):
    ax = axes[lind, dind]
    ax.coastlines(lw = 0.5, color = 'grey')
    if noSahel:
        ax.add_feature(LAND, facecolor = [0.9, 0.9, 0.9])
    ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())

    ## Temporary, not all models are complete
    #if np.sum(np.isfinite(np.nanmean(pct_weibull_better, axis = 0 \
    #)[dind, lind, :, :])) < 0.5:
    #    ax.axis('off')
    #    continue
    values = np.nanmean(pct_weibull_better, axis = 0)[dind, lind, :, :]
    if noSahel:
        values = np.where(ndvi_mask, values, np.nan)
    cf = ax.contourf(lon, lat, values,
                     cmap = 'RdYlBu', transform = ccrs.PlateCarree(),
                     levels = np.linspace(0, 1, 21))
    if lind == 0:
        ax.set_title(depth_cm_new[dind])
    if dind == 0:
        ax.text(-0.1, 0.5, 'Timescale = ' + str(lag_list[lind]) + ' months',
                verticalalignment = 'center', rotation = 90,
                transform = ax.transAxes)
    ax.text(0.02, 0.05, lab[lind * len(depth_cm_new) + dind],
            transform = ax.transAxes, weight = 'bold')
cax = fig.add_axes([0.92, 0.1, 0.02, 0.8])
plt.colorbar(cf, cax = cax, orientation = 'vertical',
             label = 'Fraction of Weibull better',
             ticks = np.linspace(0, 1, 11))
fig.savefig(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                         'fit_' + ref_expr + '_' + ref_period_str,
                         'compare_all' + suffix + '.png'), dpi = 600.,
            bbox_inches = 'tight')
plt.close(fig)


#Individual products plot
mpl.rcParams['font.size'] = 5.5
mpl.rcParams['axes.titlesize'] = 5.5
for mind, model in enumerate(mlist):
    fig, axes = plt.subplots(len(lag_list), len(depth_cm_new),
                             figsize = (5.5, 4),
                             subplot_kw = {'projection': ccrs.Miller()})
    fig.subplots_adjust(hspace = 0.01, wspace = 0.01)
    for lind, dind in it.product(range(len(lag_list)),
                                 range(len(depth_cm_new))):
        ax = axes[lind, dind]
        ax.coastlines(lw = 0.5, color = 'grey')
        if noSahel:
            ax.add_feature(LAND, facecolor = [0.9, 0.9, 0.9])
        ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())

        # Necessary, skip uncovered depths
        if np.sum(np.isfinite(pct_weibull_better[mind, dind, lind, :,
                                                 :])) < 0.5:
            ax.axis('off')
            continue

        values = pct_weibull_better[mind, dind, lind, :, :]
        if noSahel:
            values = np.where(ndvi_mask, values, np.nan)
        cf = ax.contourf(lon, lat, values,
                         cmap = 'RdYlBu', transform = ccrs.PlateCarree(),
                         levels = np.linspace(0, 1, 6))
        if lind == 0:
            ax.set_title(depth_cm_new[dind])
        if dind == 0:
            ax.text(-0.1, 0.5,
                    'Timescale = ' + str(lag_list[lind]) + ' months',
                    verticalalignment = 'center', rotation = 90,
                    transform = ax.transAxes)
        ax.text(0.02, 0.05, lab[lind * len(depth_cm_new) + dind],
                transform = ax.transAxes, weight = 'bold')

    cax = fig.add_axes([0.92, 0.1, 0.02, 0.8])
    plt.colorbar(cf, cax = cax, orientation = 'vertical',
                 label = 'Fraction of Weibull better',
                 ticks = np.linspace(0, 1, 11))
    fig.savefig(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                             'fit_' + ref_expr + '_' + ref_period_str,
                             'compare_' + model + suffix + '.png'), dpi = 600.,
                bbox_inches = 'tight')
    plt.close(fig)
