"""
2019/12/24
ywang254@utk.edu

Make sure that the calculated SPI is reasonable. Use 10 global points.
"""
from utils_management.constants import depth_cm, target_lat, target_lon
import utils_management as mg
import os
import xarray as xr
import numpy as np
import pandas as pd
import itertools as it
import matplotlib.pyplot as plt


res = '0.5'
land_mask = 'vanilla'


expr = 'historical'
m_v = 'BCC-CSM2-MR_r1i1p1f1'
dcm = '0-10cm'
year = range(1950, 2101)
period = pd.date_range('1950-01-01', '2100-12-31', freq = 'MS')
col_list = ['#1b9e77', '#d95f02', '#7570b3', '#e7298a']


#
ref_expr = 'historical'
if ref_expr == 'piControl':
    ref_period = range(0, 201)
else:
    ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


lat = target_lat[res]
lon = target_lon[res]


####
# Select 10 global points
####
data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                                    'merge_' + land_mask + '.nc'))
mask = data.mask.values.copy()
data.close()


lon2d, lat2d = np.meshgrid(data.lon.values, data.lat.values)
retain = np.where((mask > 0.5).reshape(-1))[0]
lon_array = lon2d.reshape(-1)[retain]
lat_array = lat2d.reshape(-1)[retain]


np.random.seed(100)
alist = [int(x) for x in (np.random.rand(10) * len(retain))]


####
# Extract the time series for these 10 global points from Interp_DA
####
pts_sm = pd.DataFrame(data = np.nan, index = period,
                      columns = ['point ' + str(i) for i in range(10)])
data = xr.open_mfdataset([os.path.join(mg.path_intrim_out(),
                                       'Interp_DA', land_mask, 'CMIP6', expr, 
                                       m_v, 'mrsol_' + res + '_' + str(y) + \
                                       '_' + dcm + '.nc') for y in \
                          range(1950, 2015)] + \
                         [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                       land_mask, 'CMIP6', 'ssp585', m_v,
                                       'mrsol_' + res + '_' + str(y) + \
                                       '_' + dcm + '.nc') for y in \
                          range(2015, 2101)], decode_times = False,
                         concat_dim = 'time')
data.sm.load()
for i in range(10):
    pts_sm.iloc[:, i] = data.sm.sel(lat = lat_array[alist[i]],
                                    lon = lon_array[alist[i]],
                                    method = 'nearest')
data.close()
pts_sm.to_csv(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                           'get_test_pts_sm.csv'))


####
# Extract the time series for these 10 global points from cmip6_to_spi
####
pts_spi = pd.DataFrame(data = np.nan, index = period,
                       columns = pd.MultiIndex.from_product([['point ' + \
                                                              str(i) for i in \
                                                              range(10)],
                                                             [3, 6, 12]]))
for month, lag in it.product(range(12), [3, 6, 12]):
    data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                                        'get_' + ref_expr + '_' + ref_period_str,
                                        expr, m_v + '_' + dcm + '_' + \
                                        str(lag) + '_' + res + '_' + \
                                        str(month) + '.nc'))
    data.spi.load()
    for i in range(10):
        pts_spi.loc[period.month == (1+month), ('point ' + str(i), lag)] = \
            data['spi'].sel(lat = lat_array[alist[i]],
                            lon = lon_array[alist[i]],
                            method = 'nearest')
    data.close()
pts_spi.to_csv(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                            'get_test_pts_spi.csv'))



####
# Plot
####
fig, axes = plt.subplots(nrows = 12, ncols = 10, figsize = (20, 20),
                         sharex = True, sharey = True)
fig.subplots_adjust(hspace = 0., wspace = 0.)
for month in range(12):
    for i in range(10):
        ax = axes[month, i]
        h = [None] * 4

        temp = pts_sm.loc[pts_sm.index.month == (month+1),
                          'point ' + str(i)]
        h[0], = ax.plot(year, (temp - np.mean(temp)) / np.std(temp),
                        col_list[0])
        for lag_ind, lag in enumerate([3, 6, 12], 1):
            h[lag_ind], = ax.plot(year,
                                  pts_spi.loc[pts_spi.index.month == (month+1),
                                              ('point ' + str(i), lag)],
                                  col_list[lag_ind])
        ax.set_xlim([1951, 2020])
ax.legend(h, ['SM', 'SPI-3', 'SPI-6', 'SPI-12'],
          loc = (-1, 0.1), ncol = 4)
fig.savefig(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6', 'get_test.png'),
            bbox_inches = 'tight', dpi = 600.)
