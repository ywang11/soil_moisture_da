"""
20200914
ywang254@utk.edu

Apply the common land mask of the interpolated CMIP6 models after their
interpolation to 0.5 degrees.
"""
import sys
import os
import xarray as xr
import numpy as np
import pandas as pd
from matplotlib import gridspec
from glob import glob
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import utils_management as mg
from utils_management.constants import year_cmip6, drivers_obs_time, var_to_name
from misc.cmip6_utils import one_layer_availability
import multiprocessing as mp
import gc
import itertools as it


res = '0.5'
target_lat = np.arange(-89.75, 89.76, 0.5)
target_lon = np.arange(-179.75, 179.76, 0.5)
land_mask = 'vanilla'


hr = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                                  'cmip6_da.nc'))
mask_check = hr['mask'].copy(deep = True)
hr.close()


#
def decode_month_since(time):
    ref = pd.Timestamp(time.attrs['units'].split(' ')[2])
    start = ref + pd.Timedelta(time.values[0], unit = 'M')
    start = start.replace(day = 1, hour = 0, minute = 0, second = 0)
    return pd.date_range(start, periods = len(time), freq = 'MS')


def plot_diag(var):
    fig = plt.figure(figsize = (6.5, 6.5))
    gs = gridspec.GridSpec(2, 1, figure = fig)

    if ('units' in var['time'].attrs) and \
       ('month' in var['time'].attrs['units']):
        tvec = decode_month_since(hr['time'])
    else:
        tvec = pd.to_datetime(var['time'].to_index().strftime('%Y-%m-%d'))

    ts = pd.Series(var.mean(dim = ['lat', 'lon']).values,
                      index = tvec)
    clim = var.mean(dim='time').copy(deep=True).load()
    unit = var.attrs['units']

    ax = fig.add_subplot(gs[0])
    ax.plot(ts.index, ts, '-')
    ax.set_title('Time Series ' + unit)

    ax = fig.add_subplot(gs[1], projection = ccrs.PlateCarree())
    ax.coastlines()
    ax.gridlines()
    h = ax.contourf(clim.lon.values, clim.lat.values, clim.values)
    plt.colorbar(h, ax = ax, shrink = 0.7)
    ax.set_title('Map ' + unit)
    return fig


#################################################################################
### Evapotranspiration
#################################################################################
##for model in ['CERA20C', 'CLASS-CTEM-N', 'CLM4', 'CLM4VIC',
##              'CLM5.0', 'ERA20C', 'ERA-Interim', 'ERA5', 'ERA-Land',
##              'GLDAS_Noah2.0', 'GLEAMv3.3a', 'ISAM', 'JSBACH', 'SiBCASA',
##              'ORCHIDEE-CNP']:
##    hr = xr.open_mfdataset(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
##                                        'None', model + '_evspsbl',
##                                        '*.nc'), decode_times = False)
##    # reverse latitude
##    var = hr['evspsbl']
##    if model == 'GLEAMv3.3a':
##        var['time'] = pd.date_range('1980-01-01', '2016-12-31', freq = 'MS')
##    elif model == 'GLDAS_Noah2.0':
##        var['time'] = xr.decode_cf(hr)['time']
##    else:
##        var['time'] = decode_month_since(hr['time'])
##    # time subset
##    var = var[(var['time'].to_index().year >= 1950) & \
##              (var['time'].to_index().year <= 2016), :, :]
##    var.attrs['units'] = 'mm/day'
##    # apply mask
##    var = var.where(mask_check)
##    hr.close()
##    # write to file
##    fout = os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask,
##                        model + '_evspsbl')
##    if not os.path.exists(fout):
##        os.mkdir(fout)
##    var.to_dataset(name = 'evspsbl').to_netcdf(os.path.join(fout,
##                                               'evspsbl_' + res + '.nc'))
##    fig = plot_diag(var)
##    fig.savefig(os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask,
##                             model + '_evspsbl', 'evspsbl_' + res + '.png'),
##                dpi = 600., bbox_inches = 'tight')
##    plt.close(fig)
##    hr.close()
##
##
#################################################################################
### GRUN runoff
#################################################################################
##hr = xr.open_dataset(os.path.join(mg.path_data(), 'GRUN',
##                                  'GRUN_v1_GSWP3_WGS84_05_1902_2014.nc'))
##var = hr['Runoff'][(hr['time'].to_index().year >= 1950) & \
##                   (hr['time'].to_index().year <= 2016), :, :]
##var = var.rename({'Y': 'lat', 'X': 'lon'})
##var = var.where(mask_check)
##var.to_dataset(name = 'mrro').to_netcdf(os.path.join(mg.path_intrim_out(),
##                                                     'Interp_DA', land_mask,
##                                                     'GRUN', \
##                                                     'mrro_' + res + '.nc'))
##fig = plot_diag(var)
##fig.savefig(os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
##                         'GRUN', 'mrro_' + res + '.png'),
##            dpi = 600., bbox_inches = 'tight')
##plt.close(fig)
##hr.close()
##
##
################################################################################
## GlobSnow
################################################################################
#hr = xr.open_mfdataset(os.path.join(mg.path_intrim_out(), 'Interp_DA',
#                                    'None', 'GlobSnow', 'snw_0.5_*.nc'),
#                       decode_times = False)
## reverse latitude
#var = hr['snw']
#var['time'] = drivers_obs_time['GlobSnow']
## time subset
#var = var[(var['time'].to_index().year >= 1950) & \
#          (var['time'].to_index().year <= 2016), :, :]
#var.attrs['units'] = 'mm'
## apply mask
#var = var.where(mask_check)
#hr.close()
## write to file
#var.to_dataset(name = 'snw').to_netcdf(os.path.join( \
#    mg.path_intrim_out(), 'Interp_DA', land_mask, 'GlobSnow',
#    'snw_' + res + '.nc'))
#fig = plot_diag(var)
#fig.savefig(os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
#                         'GlobSnow', 'snw_' + res + '.png'),
#            dpi = 600., bbox_inches = 'tight')
#plt.close(fig)
#hr.close()


###############################################################################
# Various
###############################################################################
for vv in sorted(var_to_name.keys()):
    for nn in var_to_name[vv]:
        if nn != 'CRU':
            hr = xr.open_mfdataset(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                                'None', nn, vv + '_0.5_*.nc'),
                                   decode_times = False)
            var = hr[vv]
        else:
            if vv == 'tas':
                hr = xr.open_mfdataset(os.path.join(mg.path_data(), 'Meteorological',
                                                    'CRU', 'CRU_v4.03',
                                                    'cru_ts4.03.*.tmn.dat.nc'),
                                       decode_times = False)
                var = hr['tmn']
            else:
                hr = xr.open_mfdataset(os.path.join(mg.path_data(), 'Meteorological',
                                                    'CRU', 'CRU_v4.03',
                                                    'cru_ts4.03.*.pre.dat.nc'),
                                       decode_times = False)
                var = hr['pre']
        var['time'] = drivers_obs_time[nn]

        if vv == 'lai':
            var.attrs['units'] = ''
        elif vv == 'pr':
            var.attrs['units'] = 'mm/day'
        else:
            var.attrs['units'] = 'degC'

        # apply mask
        var = var.where(mask_check)
        hr.close()

        # write to file
        var.to_dataset(name = vv).to_netcdf(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                                         land_mask, nn, vv + \
                                                         '_' + res + '.nc'))
        fig = plot_diag(var)
        fig.savefig(os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                                 nn, vv + '_' + res + '.png'),
                    dpi = 600., bbox_inches = 'tight')
        plt.close(fig)
        hr.close()
