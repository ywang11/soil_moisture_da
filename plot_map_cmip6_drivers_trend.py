"""
20200425
ywang254@utk.edu

Plot the trend in seasonal drivers of CMIP6 models.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from matplotlib.gridspec import GridSpec,GridSpecFromSubplotSpec
from utils_management.constants import depth_cm_new, lat_median, target_lat, target_lon, drivers
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from misc.plot_utils import plot_ts_shade, cmap_gen, stipple, cmap_div
import matplotlib as mpl
import utils_management as mg
import itertools as it
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
##from map_cmip6_drivers_trend import get_cmip6_list
from analyze_cmip6_drivers_map import get_model_list
from misc.analyze_utils import get_ndvi_mask


ndvi_mask = get_ndvi_mask('vanilla')
def read_trend(varname, model, season, year_str, expr):
    hr = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_drivers_summary',
                                      varname, expr, model + '_' + res + \
                                      '_g_map_trend_' + season + \
                                      '_' + year_str + '.nc'))
    var = hr['g_map_trend'].where(ndvi_mask).values.copy()
    hr.close()
    return var


def get_model_vote(model_id_list):
    temp_model = [x.split('_')[0] for x in model_id_list]
    vote_model = np.full(len(model_id_list), np.nan)
    for ind, model in enumerate(temp_model):
        n_model = np.sum([x == model for x in temp_model])
        vote_model[ind] = 1/n_model
    vote_model = vote_model / np.sum(vote_model)
    return vote_model


def get_hatch(val_per_model, vote_per_model, val_mean):
    pct_pos = np.sum((val_per_model > 0.).astype(float) * \
        np.broadcast_to(vote_per_model.reshape(-1,1,1),
                        val_per_model.shape), axis = 0)
    pct_neg = np.sum((val_per_model < 0.).astype(float) * \
        np.broadcast_to(vote_per_model.reshape(-1,1,1),
                        val_per_model.shape), axis = 0)
    hatch1 = ((val_mean > 0) & (pct_pos > pct)) | \
        ((val_mean < 0) & (pct_neg > pct))
    hatch2 = (~hatch1) & \
        (((val_mean > 0) & (pct_pos > (pct - 0.1))) | \
         ((val_mean < 0) & (pct_neg > (pct - 0.1))))
    return hatch1, hatch2


#
mpl.rcParams['font.size'] = 5.5
mpl.rcParams['axes.titlesize'] = 'medium'
mpl.rcParams['hatch.linewidth'] = 0.4
hs = '|||||' # '///////////'
cmap = [cmap_div(), cmap_div()] * 4 # ['RdBu', 'RdBu_r', 'RdBu', 'RdBu'] # cmap_div()
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx', 'yy', 'zz', 'aaa', 'bbb', 'ccc', 'ddd', 'eee']
pct = 0.9 # consistent with "step8_plot_trend_spi_bylat.py"
res = '0.5'
land_mask = 'vanilla'
lat = target_lat[res]
lon = target_lon[res]
season_list = ['DJF', 'MAM', 'JJA', 'SON']
var_list = ['pr', 'tas', 'lai', 'snw']
var_list_units = ['mm day$^{-1}$ yr$^{-1}$', '$^o$C yr$^{-1}$',
                  'yr$^{-1}$', 'mm yr$^{-1}$']
year = range(1971, 2017)
##L = 45 # 2016 - 1971 + 1
##year_range_list = [range(i, i+L) for i in [1975, 1995, 2015, 2035, 2045, 2050, 2055]]


for expr in ['historical']: # ['historical', 'hist-GHG', 'hist-aer']:
##for expr, year in it.product(['historical'], year_range_list):

    year_str = str(year[0]) + '-' + str(year[-1])
    
    fig, axes = plt.subplots(len(var_list) * len(season_list) // 2, 2,
                             figsize = (4.7, 8),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    fig.subplots_adjust(wspace = 0.02)
    for vind, sind in it.product(range(len(var_list)), range(len(season_list))):
        vv = var_list[vind]
        season = season_list[sind]
        cmip6_list = get_model_list(expr, drivers, '0-10cm', land_mask, res)

        ### Temp
        ###cmip6_list = ['ACCESS-CM2_r1i1p1f1']

        #
        val = np.full([len(cmip6_list), len(lat), len(lon)], np.nan)
        for mind, model in enumerate(cmip6_list):
            val[mind, :, :] = read_trend(vv, model, season, year_str, expr)
        # ---- calculate the weighted average
        vote_model = get_model_vote(cmip6_list)
        val_mean = np.sum(val * np.broadcast_to(vote_model.reshape(-1,1,1),
                                                val.shape), axis = 0)
        val_std = np.std(val, axis = 0)

        # 
        if not year_str == '1971-2016':
            if vv == 'pr':
                #levels = np.linspace(-.016, .016, 11)
                levels = np.linspace(-.008, .008, 11)
            elif vv == 'tas':
                levels = np.linspace(-0.08, 0.08, 11)
            elif vv == 'lai':
                levels = np.linspace(-0.012, 0.012, 11)
            elif vv == 'snw':
                levels = np.linspace(-.8, .8, 11)
        else:
            if vv == 'pr':
                levels = np.linspace(-.016, .016, 11)
            elif vv == 'tas':
                levels = np.linspace(-0.08, 0.08, 11)
            elif vv == 'lai':
                levels = np.linspace(-0.006, 0.006, 11)
            elif vv == 'snw':
                levels = np.linspace(-.4, .4, 11)

        ax = axes[sind + (vind//2)*len(season_list), np.mod(vind, 2)]
        ax.coastlines(lw = 0.1)
        gl = ax.gridlines(xlocs = [-180, 180],
                          ylocs = np.arange(-40, 61, 20),
                          linestyle = '--',
                          linewidth = 0.5, draw_labels = False) # = True)
        #gl.xlabels_top = False
        #gl.xlabels_bottom = False
        #gl.ylabels_right = False
        #if vind == 0:
        #    gl.ylabels_left = True
        #else:
        #    gl.ylabels_left = False
        #gl.xlocator = mticker.FixedLocator([-180, 180])
        #gl.ylocator = mticker.FixedLocator([-40, -20, 0, 20, 40, 60])
        #gl.xformatter = LONGITUDE_FORMATTER
        #gl.yformatter = LATITUDE_FORMATTER
        #gl.xlabel_style = {'color': 'black', 'weight': 'normal'}
        ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.2)
        ax.spines['bottom'].set_lw(0.2)
        ax.spines['left'].set_lw(0.2)
        ax.spines['right'].set_lw(0.2)

        h = ax.contourf(lon, lat, val_mean, cmap = cmap[vind],
                        levels = levels, extend = 'both',
                        transform = ccrs.PlateCarree())
        # add consistency hatch
        hatch1, hatch2 = get_hatch(val, vote_model, val_mean)
        stipple(ax, lat, lon, mask = hatch1,
                transform = ccrs.PlateCarree(), hatch = hs)
        stipple(ax, lat, lon, mask = hatch2,
                transform = ccrs.PlateCarree(), hatch = '-----') # '\\\\\\\\\\\\'
        # label the variable name
        ax.set_title(var_list[vind] + ' ' + \
                     var_list_units[vind], pad = 2)
        if np.mod(vind,2) == 0:
            ax.text(-0.15, 0.5, season, verticalalignment = 'center',
                    rotation = 90, transform = ax.transAxes)
        ax.text(0.01, 1.05, lab[sind*len(var_list) + vind],
                transform = ax.transAxes, weight = 'bold')

        if sind == (len(season_list)-1):
            cax = fig.add_axes([0.9 + 0.15 * np.mod(vind,2), 0.55 - (vind//2) * 0.4, 0.01, .3])
            cb = plt.colorbar(h, cax = cax, ticks = levels) # , orientation = 'horizontal')
            cb.set_label(vv + ' trends', labelpad = 0.1)

    fig.savefig(os.path.join(mg.path_out(), 'figures',
                             'plot_map_cmip6_drivers_trend_' + \
                             expr + '_' + year_str + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
