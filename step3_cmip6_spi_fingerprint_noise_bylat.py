"""
20190910
ywang254@utk.edu

Concatenate all the piControl runs, calculate the 1st EOF, 1st PC, and percent
 explained variance, with block jackknife sampling.

No flipping of the EOF here.
"""
import utils_management as mg
from utils_management.constants import lat_median, depth_cm_new # depth_cm
import os
import numpy as np
import pandas as pd
from misc.cmip6_utils import mrsol_availability
from glob import glob
import itertools as it
#from scipy.stats import linregress
import time
#import multiprocessing as mp
from misc.jackknife import *
import eofs


start = time.time()


#
land_mask = 'vanilla'
res = '0.5'
season_list = [str(i) for i in range(12)]
N = 200
blocksize = 50
base = 'piControl'
expr_list = ['piControl', 'historical', 'hist-GHG', 'hist-aer', 'hist-nat',
             'NoNAT', 'GHGAER']
noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['noSahel'].loc[np.array(lat_median).astype(int)].values)
else:
    suffix = ''
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['Normal'].loc[np.array(lat_median).astype(int)].values)


col_names = ['Full', 'Jackknife Estimate', 'Jackknife Bias',
             'Jackknife Std Err', 'Jackknife CI_lower', 'Jackknife CI_upper']

#
for expr, i, season, dist, lag in it.product(expr_list, [0,1],
                                             season_list, ['gmm','weibull'],
                                             [1, 3, 6]):
##    break

    #dcm = depth_cm[i]
    dcm = depth_cm_new[i]

    # The CMIP6 list of models.
    ##common = get_model_list(['piControl'], dcm, land_mask, res)
    model_version, _ = mrsol_availability(dcm, land_mask, 'piControl', res)
    mv2, _ = mrsol_availability(dcm, land_mask, expr, res)
    model_version = sorted(list(set(model_version) & set(mv2)))
    ## ---- manually remove NorESM2-LR
    #model_version.remove('NorESM2-LM_r1i1p1f1')

    # Last 200 years of the piControl of these models.
    noise = np.full([N * len(model_version), lat_area_sqrt.shape[0]], np.nan)

    #
    for mind, m_v in enumerate(model_version):
        # Read the by-latitude aggregated data.
        data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_summary',
                                        'piControl', dist, m_v + '_' + \
                                        dcm + '_' + str(lag) + '_' + \
                                        season + '_' + res + \
                                        '_g_lat_ts' + suffix + '.csv'),
                           index_col = 0, parse_dates = True)

        # Remove the mean at each latitude.
        noise[(mind*N):((mind+1)*N), :] = \
            data.loc[:, lat_median].values - \
            np.mean(data.loc[:, lat_median].values, axis = 0)

    ##noise = noise - noise.mean(axis = 0)

    # Calculate the noise fingeprint.
    wgts = lat_area_sqrt / np.mean(lat_area_sqrt)

    eof_0, pc1_0, pct_0 = calc_eof(noise, wgts)
    estimate, bias, std_err, conf_interval = jackknife_stats_eof(noise, wgts,
                                                                 blocksize)

    eof = pd.DataFrame(data = np.nan, index = lat_median,
                       columns = col_names)
    eof.iloc[:, 0] = eof_0
    eof.iloc[:, 1] = estimate[0]
    eof.iloc[:, 2] = bias[0]
    eof.iloc[:, 3] = std_err[0]
    eof.iloc[:, 4] = conf_interval[0][0,:]
    eof.iloc[:, 5] = conf_interval[0][1,:]

    pc1 = pd.DataFrame(data = np.nan, index = range(N * len(model_version)),
                       columns = col_names)
    pc1.iloc[:, 0] = pc1_0
    pc1.iloc[:, 1] = estimate[1]
    pc1.iloc[:, 2] = bias[1]
    pc1.iloc[:, 3] = std_err[1]
    pc1.iloc[:, 4] = conf_interval[1][0,:]
    pc1.iloc[:, 5] = conf_interval[1][1,:]

    pct = pd.DataFrame(data = np.nan, index = ['Pct'], columns = col_names)
    pct.iloc[:, 0] = pct_0
    pct.iloc[:, 1] = estimate[2]
    pct.iloc[:, 2] = bias[2]
    pct.iloc[:, 3] = std_err[2]
    pct.iloc[:, 4] = conf_interval[2][0,:]
    pct.iloc[:, 5] = conf_interval[2][1,:]

    eof.to_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                            'piControl', 'bylat', expr + '_' + dist + \
                            '_eof_' + dcm + '_' + str(lag) + '_' + \
                            season + '_' + res + suffix + '.csv'))
    pc1.to_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                            'piControl', 'bylat', expr + '_' + dist + \
                            '_pc_' + dcm + '_' + str(lag) + '_' + \
                            season + '_' + res + suffix + '.csv'))
    pct.to_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                            'piControl', 'bylat', expr + '_' + dist + \
                            '_pct_' + dcm + '_' + str(lag) + '_' + \
                            season + '_' + res + suffix + '.csv'))

end = time.time()
print('Script finished in ' + str((end - start) / 60) + ' minutes.')
