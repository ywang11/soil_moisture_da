"""
20190910

ywang254@utk.edu

Sum the products SPI to 0-100cm.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, depth, target_lat, \
    target_lon
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
from misc.spi_utils import *
from scipy.integrate import quad
import sys


land_mask = 'vanilla'
res = '0.5'
depth_weight = [0.1, 0.2, 0.2, 0.5]


#product_list = ['mean_lsm', 'mean_cmip5', 'mean_cmip6', 'mean_2cmip',
#                'mean_all',
#                'dolce_lsm', 'dolce_cmip5', 'dolce_cmip6', 'dolce_2cmip',
#                'dolce_all',
#                'em_lsm', 'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all']
product_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
                'em_2cmip', 'em_all', 'mean_products', 'mean_noncmip']
period = pd.date_range('1951-01-01', '2016-12-31', freq = 'MS')
year = range(1951, 2017)


#
ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


season_list = [str(m) for m in range(12)]


find_inf = pd.DataFrame(np.nan, index = period,
    columns = pd.MultiIndex.from_product( [['gmm', 'weibull'], [1,3,6],
                                           depth_cm_new, product_list] ))
for dist, prod, s, dind in it.product(['gmm', 'weibull'], product_list,
                                      [1, 3, 6], range(2)):
    dcm = depth_cm_new[dind]

    spi = np.full([len(year), 12, len(target_lat[res]),
                   len(target_lon[res])], np.nan)
    for sind, season in enumerate(season_list):
        h = xr.open_dataset(os.path.join(mg.path_intrim_out(),
            'SPI_Products', 'get_' + ref_period_str, dist,
            prod + '_' + dcm + '_' + str(s) + \
            '_' + res + '_' + season + '.nc'))
        spi[:, sind, :, :] = h['spi'].values.copy()
        h.close()
    spi = spi.reshape( len(year) * 12, len(target_lat[res]),
                       len(target_lon[res]) )

    find_inf.loc[:, (dist, s, dcm, prod)] = np.sum(np.sum(spi == np.inf,
                                                          axis = 2),
                                                   axis = 1)
find_inf.to_csv(os.path.join(mg.path_out(), 'products_spi_summary',
                             'checkvalidity.csv'))
