"""
2019/10/12
ywang254@utk.edu

Plot to check the look of the interpolated merge_vanilla_<res>.nc files
 created by "./interpolation/interp_land_mask.ncl"
"""
import matplotlib.pyplot as plt
import xarray as xr
import utils_management as mg
import os


for res in ['2.5', '5', '10']:
    fig, ax = plt.subplots()

    data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                        'land_mask_minimum', 
                                        'merge_vanilla_' + res + '.nc'))
    data.mask.plot()
    data.close()

    fig.savefig(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                             'merge_vanilla_' + res + '.png'), dpi = 600.)
