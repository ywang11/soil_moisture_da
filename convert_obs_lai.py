"""
2021/2/17
ywang254@utk.edu

Read binary LAI data, aggregate to NetCDF file.
"""
import xarray as xr
import numpy as np
import utils_management as mg
import os
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


year_list = range(1981, 2016)
month_list = ['jan','feb','mar','apr','may','jun','jul',
              'aug','sep','oct','nov','dec']


year = year_list[0]
mind = 8
month = month_list[mind]


#if ((year == 1981) & (mind <= 7)) | ((year == 2015) & (mind >= 10)):
#    continue


f_a = os.path.join(mg.path_data(), 'avhrrbulai_v02',
                   'avhrrbulai_v02', 'AVHRRBUVI02.' + \
                   str(year) + month + 'a.abl')
val_a = np.fromfile(f_a, dtype = '>u2').reshape(4320, 2160) * 0.001
#val_a = np.where(val_a > 1000, np.nan, val_a * 0.001) # scaling factor

f_b = os.path.join(mg.path_data(), 'avhrrbulai_v02',
                   'avhrrbulai_v02', 'AVHRRBUVI02.' + \
                   str(year) + month + 'b.abl')
val_b = np.fromfile(f_b, dtype = '>u2').reshape(4320, 2160) * 0.001
#val_b = np.where(val_b > 1000, np.nan, val_b * 0.001) # scaling factor

val = np.fmax(val_a, val_b)

#
fig, ax = plt.subplots()
ax.imshow(val)
fig.savefig(os.path.join(mg.path_out(), 'test.png'), dpi = 600.)
plt.close(fig)
