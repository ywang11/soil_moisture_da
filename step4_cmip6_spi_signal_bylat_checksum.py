"""
Ensure all files are generated in "step1_cmip6_spi_to_bylat_noSahel.py" & 
                                  "step1_cmip6_spi_to_bylat.py"
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new
import os
import numpy as np
from misc.da_utils import get_model_list
import pandas as pd
import itertools as it
from glob import glob


land_mask = 'vanilla'
res = '0.5'
season_list = [str(i) for i in range(12)]
expr_list = ['historical', 'NoNAT', 'hist-nat', 'hist-GHG', 'hist-aer',
             'GHGAER']
L_range = range(10,67)

noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
else:
    suffix = ''


for limit in [True, False]:
    if limit:
        prefix = 'limit_'
    else:
        prefix = ''
    
    f = pd.ExcelWriter(os.path.join(mg.path_out(), 'cmip6_spi_signal',
                                    prefix + 'bylat_checksum' + suffix + \
                                    '.xlsx'))

    for base in ['historical', 'hist-GHG', 'hist-aer']:
        ok = pd.DataFrame(data = ' ' * 100,
                          index = pd.MultiIndex.from_product([expr_list,
                                                              depth_cm_new]),
                          columns = pd.MultiIndex.from_product([['gmm',
                                                                 'weibull'],
                                                                [1,3,6]]))
        for expr, dcm, dist, lag in it.product(expr_list, depth_cm_new,
                                               ['gmm', 'weibull'],
                                               [1, 3, 6]):
            temp = glob(os.path.join(mg.path_out(), 'cmip6_spi_signal',
                                     'bylat', base + '_eofs', prefix + \
                                     dist + '_' + expr + '_signal_' + dcm + \
                                     '_' + str(lag) + '_*_0.5' + suffix + \
                                     '.csv'))
            if len(temp) != 12:
                ok.loc[(expr, dcm), (dist, lag)] = 'Bad'
            else:
                ok.loc[(expr, dcm), (dist, lag)] = 'Good'
        ok.to_excel(f, sheet_name = base)
    f.close()
