"""
2020/09/01
ywang254@utk.edu

Correlation between top and bottom level soil moisture, for each month & 
 latitude, for the mean products & historical + rcp585 simulations.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import matplotlib as mpl
import utils_management as mg
import itertools as it
from matplotlib import gridspec
from misc.cmip6_utils import mrsol_availability
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from scipy.stats import pearsonr


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5.
mpl.rcParams['hatch.linewidth'] = 0.5
mpl.rcParams['axes.titlepad'] = 1
lab = 'abcdefghijklmnopqrstuvwxyz'
cmap = cmap_div()
hs = '/////'
levels = np.arange(-1.6e-2, 1.61e-2, 1e-3)
clist = ['k', '#2c39b8', 'r', '#31a354', '#fd8d3c', '#41b6c4',
         '#dd1c77']
pct = 0.9
yrng = [-1.2, 0.8]


res = '0.5'
land_mask = 'vanilla'
lat_eof = [float(x) for x in lat_median]
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


season_list = [str(i) for i in range(12)] # ensure order of x-axis
season_list_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul',
                    'Aug','Sep','Oct','Nov','Dec']
year = range(1971, 2101)
L = 2016 - 1971 + 1


folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
lag = 3

expr = 'historical'
expr_name = 'ALL'


prod = 'mean_noncmip' # ['mean_products', 'mean_noncmip']
prod_name = 'Mean NonCMIP' # ['Mean NonCMIP', 'Mean Products']


def calc_corr(sf, rz):
    corr = pd.DataFrame(data = np.nan, index = sf.index.year[:(-L+1)],
                        columns = rz.columns)
    for ii in range(rz.shape[0] - L + 1):
        for jj in range(rz.shape[1]):
            r, p = pearsonr(sf.iloc[ii:(ii + L), jj],
                            rz.iloc[ii:(ii + L), jj])
            corr.iloc[ii, jj] = r
    return corr


def plot_corr(ax, corr, **kwargs):
    if min(corr.shape) < 2:
        cf, = ax.plot(corr.values.reshape(-1),
                      np.arange(corr.shape[1]), '-', **kwargs)
    else:
        cf = ax.contourf(range(corr.shape[0]), range(corr.shape[1]),
                         corr.values.T, cmap = 'Spectral', vmin = 0.,
                         vmax = 1, levels = np.linspace(0, 1, 21),
                         extend = 'both')
        ax.set_xticks(range(0, corr.shape[0], 10))
        ax.set_xticklabels(corr.index[::10])
    ax.set_ylim([0, corr.shape[1]])
    ax.set_yticks(range(2, corr.shape[1], 8))
    ax.set_yticklabels(lat_median_name[2::8])
    return cf


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
for dist in ['gmm', 'weibull']:
    fig = plt.figure(figsize = (6.5, 8))
    gs = gridspec.GridSpec(1, 2, figure = fig, width_ratios = [1, 2.6],
                           wspace = 0.2)
    sub_gs1 = gridspec.GridSpecFromSubplotSpec(12, 1, subplot_spec = gs[0],
                                               hspace = 0.25)
    sub_gs2 = gridspec.GridSpecFromSubplotSpec(12, 1, subplot_spec = gs[1],
                                               hspace = 0.25)
    for sind, season in enumerate(season_list):        
        # Mean Products
        dcm = depth_cm_new[0]
        prod_sf = pd.read_csv(os.path.join(mg.path_out(),
                                           'products_spi_summary', dist,
                                           prod + '_' + dcm + '_' + season + \
                                           '_' + str(lag) + '_' + res + \
                                           '_g_lat_ts' + suffix + '.csv'),
                              index_col = 0, parse_dates = True)
        prod_sf = prod_sf.loc[prod_sf.index.year >= year[0], :]
        ##print(prod_sf)
        dcm = depth_cm_new[1]
        prod_rz = pd.read_csv(os.path.join(mg.path_out(),
                                           'products_spi_summary', dist,
                                           prod + '_' + dcm + '_' + season + \
                                           '_' + str(lag) + '_' + res + \
                                           '_g_lat_ts' + suffix + '.csv'),
                              index_col = 0, parse_dates = True)
        prod_rz = prod_rz.loc[prod_rz.index.year >= year[0], :]
        prod_corr = calc_corr(prod_sf, prod_rz)
        ax = fig.add_subplot(sub_gs1[sind])
        h1 = plot_corr(ax, prod_corr, color = 'b', zorder = 3)
        ax.set_xlim([0., 1])
        if sind != (len(season_list)-1):
            ax.set_xticklabels([])
        else:
            ax.set_xlabel('Correlation')
        if sind == 0:
            ax.set_title('1971-2016', pad = 2) # prod_name
        ax.set_ylabel(season_list_name[sind])
        ax.tick_params('both', length = 2, pad = 2)
        ax.text(0.1, 1.05, lab[sind], transform = ax.transAxes,
                weight = 'bold')

        # CMIP6 ALL
        dcm = depth_cm_new[0]
        cmip6_sf = pd.read_csv(os.path.join(mg.path_out(),
                                            'cmip6_spi_ensemble_merge',
                                            dist + '_' + expr + '_' + \
                                            dcm + '_' + str(lag) + '_' + \
                                            str(season) + '_' + res + '.csv'),
                               index_col = 0, header = 0, parse_dates = True)
        cmip6_sf = cmip6_sf.loc[cmip6_sf.index.year >= year[0], :]
        dcm = depth_cm_new[1]
        cmip6_rz = pd.read_csv(os.path.join(mg.path_out(),
                                            'cmip6_spi_ensemble_merge',
                                            dist + '_' + expr + '_' + \
                                            dcm + '_' + str(lag) + '_' + \
                                            str(season) + '_' + res + '.csv'),
                               index_col = 0, header = 0, parse_dates = True)
        cmip6_rz = cmip6_rz.loc[cmip6_rz.index.year >= year[0], :]
        cmip6_corr = calc_corr(cmip6_sf, cmip6_rz)

        # Add to the obs. panel
        h2 = plot_corr(ax, cmip6_corr.iloc[[0], :],
                       color = 'r', lw = 0.7)

        ax = fig.add_subplot(sub_gs2[sind])
        cf = plot_corr(ax, cmip6_corr)
        if sind != (len(season_list)-1):
            ax.set_xticklabels([])
        if sind == 0:
            ax.set_title(expr_name + ' future', pad = 2)
        #ax.set_ylabel(season_list_name[sind])
        ax.text(0.028, 1.05, lab[12 + sind], transform = ax.transAxes,
                weight = 'bold')

    ax.legend([h1, h2], ['Mean NonCMIP', 'ALL'],
              loc = (-0.55, -1.2), ncol = 2)
    cax = fig.add_axes([0.38, 0.06, 0.52, 0.01])
    plt.colorbar(cf, cax = cax, orientation = 'horizontal',
                 extend = 'both', label = 'Correlation')
    fig.savefig(os.path.join(mg.path_out(), 'figures',
                             'plot_spi_corr_layer_' + dist + suffix + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
