"""
20210708
ywang254@utk.edu

Plot the trend and EOF of the NAT fingerprint.
"""
import matplotlib.pyplot as plt
import pandas as pd
import xarray as xr
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import numpy as np
import cartopy.crs as ccrs
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil
from matplotlib import gridspec


mpl.rcParams['font.size'] = 5.5
mpl.rcParams['axes.titleweight'] = 'normal'
mpl.rcParams['axes.titlesize'] = 5.5
mpl.rcParams['hatch.linewidth'] = 0.5
cmap = cmap_div()
levels = np.linspace(-0.016, 0.016, 41)
levels2 = np.arange(-0.84, 0.86, 0.04)
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list = [str(i) for i in range(12)]
month_name = ['J','F','M','A','M','J','J','A','S','O','N','D']
month_name2 = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
lag = 3
prefix = '' # 'limit_', ''
dist = 'gmm'
pct = 0.9
hs = '|||'


fig = plt.figure(figsize = (6.5, 8))


"""
gs = gridspec.GridSpec(len(depth_cm_new), 2, hspace = 0.12, width_ratios = [3, 1])
axes = {}
axes['fingerprint'] = np.empty(shape=(len(depth_cm_new)), dtype=object)
for ii, dcm in enumerate(depth_cm_new):
    axes['fingerprint'][ii] = fig.add_subplot(gs[ii,1])
axes['pc'] = np.empty(shape = (3,4,2), dtype = object)
for ii, dcm in enumerate(depth_cm_new):
    sub_gs = gridspec.GridSpecFromSubplotSpec(3, 4, subplot_spec = gs[ii,0],
                                              hspace = 0., wspace = 0.)
    for jj, kk in it.product(range(3), range(4)):
        axes['pc'][jj,kk,ii] = fig.add_subplot(sub_gs[jj,kk],
                                               sharex = axes['pc'][0,0,0],
                                               sharey = axes['pc'][0,0,0])
"""

flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                prefix + 'flip_bylat' + suffix + '.csv'),
                   index_col = [0,1,2,3])

"""
# PC
for i, dcm in enumerate(depth_cm_new):
    for sind, season in enumerate(season_list):
        pc = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint', expr, 'bylat',
                                      prefix + dist + '_pc_' + dcm + '_' + str(lag) + \
                                      '_' + season + '_' + res + suffix + '.csv'),
                        index_col = 0, parse_dates = True)
        ax = axes['pc'][sind//4, np.mod(sind,4), i]
        ax.plot(pc.index.year, pc['Full'] * flip.loc[(dcm, int(season), dist, lag), base])
        if sind == 4:
            ax.set_ylabel(dcm)
        ax.set_title(month_name2[sind])
        ax.text(0.05, 0.92, lab[i*len(depth_cm_new)] + str(sind),
                transform = ax.transAxes, weight = 'bold')

        ax.axvline(1982, lw = 0.5, color = '#2ca25f')
        ax.axvline(1991, lw = 0.5, color = '#2ca25f')

        ax.set_xticks(pc.index.year[::20])

        if sind < 8:
            plt.setp(ax.get_xticklabels(), visible=False)
        if np.mod(sind, 4) != 0:
            plt.setp(ax.get_yticklabels(), visible=False)
"""

# EOF
fig, axes = plt.subplots(nrows = len(depth_cm_new), ncols = 2, figsize = (4.5, 6.5))
fig.subplots_adjust(wspace = 0.1, hspace = 0.1)
for ii, jj in it.product(range(len(depth_cm_new)), range(2)):
    dcm = depth_cm_new[ii]
    if jj == 0:
        expr = 'hist-nat'
        base = 'hist-nat'
    else:
        expr = 'historical'
        base = 'historical'

    """ ax = axes['fingerprint'][i] """
    ax = axes[ii,jj]
    eof1_collection = np.full([len(lat_median), len(season_list)], np.nan)
    for sind, season in enumerate(season_list):
        eof = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                       base, 'bylat', prefix + dist + '_eof_' + dcm + '_' + \
                                       str(lag) + '_' + season + '_' + res + suffix + '.csv'),
                          index_col = 0)
        eof.index = eof.index.values.astype(str)
        eof = eof.loc[lat_median, :]

        eof1_collection[:,sind] = eof['Full'].values * \
            flip.loc[(dcm, int(season), dist, lag), base]
    hb = ax.contourf(range(eof1_collection.shape[1]),
                     range(eof1_collection.shape[0]),
                     eof1_collection, cmap = cmap,
                     levels = levels2, extend = 'both')
    ax.set_yticks(range(2, eof1_collection.shape[0], 4))
    ax.set_yticklabels([])
    ax.set_xticks(np.linspace(0.3,10.7,12))
    ax.tick_params(axis = 'both', length = 2., pad = 2.)
    if ii == 1:
        ax.set_xticklabels(month_name)
    else:
        ax.set_xticklabels([])
    if ii == 0:
        if jj == 0:
            ax.set_title('Fingerprint NAT')
        else:
            ax.set_title('Fingerprint ALL')
    ax.text(0.1, 1.03, lab[ii*len(depth_cm_new) + jj],
            transform = ax.transAxes, weight = 'bold')
cax = fig.add_axes([0.95, 0.1, 0.01, 0.8])
plt.colorbar(hb, cax = cax, orientation = 'vertical',
             label = 'Fingerprint of ' + str(lag) + '-month SSI')
fig.savefig(os.path.join(mg.path_out(), 'figures',
                         prefix + 'comment_' + dist + '_' + str(lag) + \
                         '_' + res + suffix + '.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)



"""
fig, axes = plt.subplots(nrows = len(depth_cm_new), ncols = 2, figsize = (5.5, 6.5))
fig.subplots_adjust(wspace = 0.1, hspace = 0.1)
# Trend
for i, dcm in enumerate(depth_cm_new):
    # ------------ trends of the individual model ensemble members
    data2 = pd.read_csv(os.path.join(mg.path_out(), 'standard_diagnostics_spi',
                                     expr, dist, prefix + 'bylat_trend_' + \
                                     str(lag) + '_' + res + '_' + \
                                     dcm + suffix + '.csv'),
                        index_col = 0, header = [0,1])
    data2.index = data2.index.astype(str)
    data2_p = pd.read_csv(os.path.join(mg.path_out(), 'standard_diagnostics_spi',
                                       expr, dist, prefix + 'bylat_trend_p_' + \
                                       str(lag) + '_' + res + '_' + \
                                       dcm + suffix + '.csv'),
                          index_col = 0, header = [0,1])
    data2_p.index = data2_p.index.astype(str)
    # ------------ weight of the individual ensemble members
    temp_model = [x.split('_')[0] for x in data2.columns.get_level_values(0)]
    data2_vote = np.full(data2.shape, np.nan)
    for ind, model in enumerate(temp_model):
        n_model = np.sum([x == model for x in temp_model])
        data2_vote[:, ind] = 1 / n_model
    data2_vote = data2_vote / \
        np.sum(data2_vote, axis = 1).reshape(-1,1) * \
        len(data2.columns.levels[1])
    # -------- weighted ensemble average of the trends
    weighted_avg = (data2 * data2_vote).groupby(level = 1, axis = 1 \
                                                ).sum().loc[:, season_list]
    # -------- std of the trends
    weighted_std = data2.groupby(level = 1,
                                 axis = 1).std().loc[:, season_list]
    # -------- weighted ensemble average of whether p <= 0.05
    weighted_avg_p = ((data2_p <= 0.05)* data2_vote).groupby(level = 1, axis = 1 \
                                                             ).sum().loc[:, season_list]
    #
    ax = axes[i,0]
    h = ax.contourf(range(weighted_avg.shape[1]),
                    range(weighted_avg.shape[0]),
                    weighted_avg.values, cmap = cmap, levels = levels,
                    extend = 'both')
    ax.set_yticks(range(2, weighted_avg.shape[0], 4))
    ax.set_yticklabels( [] )
    ax.set_xticks(np.linspace(0.35, 10.65, 12)) # range(0, 12, 1))
    if i == 0:
        ax.set_xticklabels([])
    else:
        ax.set_xticklabels(month_name)
    ax.set_ylabel(dcm)
    ax.tick_params(axis = 'both', length = 2, pad = 2)
    if i == 0:
        ax.text(0.5, 1.03, 'Trend NAT', fontsize = 5,
                transform = ax.transAxes, horizontalalignment = 'center')
    # ------------ weighted ensemble total of the consistency in trends
    pct_pos = ((data2 > 0.).astype(float) * data2_vote).groupby( \
        level = 1, axis = 1).sum().loc[:, season_list]
    pct_neg = ((data2 < 0.).astype(float) * data2_vote).groupby( \
        level = 1, axis = 1).sum().loc[:, season_list]
    hatch1 = ((weighted_avg.values > 0) & (pct_pos.values > pct)) | \
        ((weighted_avg.values < 0) & (pct_neg.values > pct))
    stipple(ax, range(hatch1.shape[0]), range(hatch1.shape[1]),
            mask = hatch1, hatch = hs)
    hatch2 = (~hatch1) & \
        (((weighted_avg.values > 0) & (pct_pos.values > (pct-0.1))) | \
         ((weighted_avg.values < 0) & (pct_neg.values > (pct-0.1))))
    stipple(ax, range(hatch2.shape[0]), range(hatch2.shape[1]),
            mask = hatch2, hatch = '---')
    # ------------ weighted_avg_p indicates at least 90%
    ax.contour(range(weighted_avg_p.shape[1]), range(weighted_avg_p.shape[0]),
               weighted_avg_p < 0.5, levels = [0.5], colors = 'grey', linestyles = 'dashed',
               linewidths = 0.5)
    ax.contourf(range(weighted_avg_p.shape[1]), range(weighted_avg_p.shape[0]),
                weighted_avg_p < 0.5, levels = [0.5, 1.5], colors = 'w', alpha = 0.36)
    ax.set_title(lab[i*len(depth_cm_new)], loc = 'left', weight = 'bold')
    ax.set_yticklabels(lat_median_name[2::4])
cax = fig.add_axes([0.1, 0.04, 0.4, 0.015])
plt.colorbar(h, cax = cax, orientation = 'horizontal',
             label = 'Trend of ' + str(lag) + '-month SSI')
"""
