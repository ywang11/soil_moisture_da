"""
20210210
ywang254@utk.edu

Use emergent constraint to modify the future signal time series.

Plot the times series of the signal, and threshold of 
   likely (S/N > 0.95, detectable at 66% confidence)
   very likely (S/N > 1.64, 90% confidence)
   virtually certain (S/N > 2.57, 99% confidence).

The thresholds follow Marvel et al. 2019 DOI: 10.1038/s41586-019-1149-8

The EOF is flipped already in the fingerprint calculation step to ensure most
  are negative values.

Historical + RCP85 only. Until 2100.

20210716

Allows more than one year to be used in the rolling regression.

20210803

Add alternative predictors to SSI.
"""
import matplotlib.pyplot as plt
import pandas as pd
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median, drivers # depth_cm
from misc.plot_utils import plot_ts_shade, cmap_gen
import numpy as np
import cartopy.crs as ccrs
from misc.cmip6_utils import mrsol_availability
import xarray as xr
import matplotlib as mpl
import itertools as it
import shutil
import statsmodels.api as stats
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from matplotlib.cm import get_cmap
from misc.em_utils import collect_SN_model, collect_drivers_model, collect_SN_obs, collect_drivers_obs, get_constrained
import scipy.stats as scistats
from analyze_cmip6_drivers_map import get_model_list


def plot_em(ax, reg, obs_value, predict_obs, predict_std, y_std, clist0):
    x = reg.model.exog[:, 1]
    y = reg.model.endog
    fittedvalues = reg.predict(reg.model.exog)
    # ---- change the weight to uniform in the prediction interval
    predict_se, predict_ci_low, predict_ci_upp = wls_prediction_std(reg, \
        exog = reg.model.exog, weights = np.ones(len(reg.model.exog)))

    ax.scatter(x, y, c = clist0, marker = 'o')

    x_ind = np.argsort(x)
    h2 = [None] * 2
    h2[0], = ax.plot(x[x_ind], fittedvalues[x_ind], '-g')
    h2[1] = ax.fill_between(x[x_ind], predict_ci_low[x_ind],
                            predict_ci_upp[x_ind],
                            interpolate = True, color = 'r', alpha = 0.2)

    h1 = [None] * 2
    h1[0] = ax.scatter(obs_value, predict_obs, c = 'k')
    h1[1] = ax.errorbar(obs_value, predict_obs,
                        predict_std * scistats.t.ppf(0.975, 12),
                        fmt = 'none', elinewidth = 2,
                        capsize = 1.5, ecolor = 'k')
    return ax, h2, h1


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list_list = {'bymonth': [str(i) for i in range(12)],
                    'byyear': ['annual_mean', 'annual_maximum',
                               'annual_minimum',
                               'growing_season_mean', 'growing_season_maximum',
                               'growing_season_minimum']}
#Setup / MODIFY
folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
season_name = 'bymonth'
base = 'historical'
expr = 'historical'
expr_name = 'ALL'
season_list = season_list_list[season_name]
season_list_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
prod = 'mean_noncmip'
lag = 3
start = 1971
L = 2016 - start + 1 # The length of observations
start2 = 1972
L2 = 60 # The length of future S/N ratios
limit = False # Not enough ensemble members to estimate the
              # variation around each ESM's mean.
if limit:
    prefix = 'limit_'
else:
    prefix = ''
half_window_list = [0] # [0, 1, 2, 4, 8, 16, 24]

cmip6_list = get_model_list(expr, drivers, '0-10cm', 'vanilla', res)
cmip6_list = sorted(cmip6_list)
cmip6_list_models = list(np.unique([t.split('_')[0] for t in cmip6_list]))

mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titleweight'] = 'normal'
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx', 'yy', 'zz', 'aaa', 'bbb', 'ccc', 'ddd', 'eee']
cmap = get_cmap('jet')
clist = {model: cmap((1+ind)/len(cmip6_list_models)) \
         for ind, model in enumerate(cmip6_list_models)}
drivers = ['pr', 'tas', 'lai'] # OVERRIDE; drivers + ['SSI2']


for dist in ['gmm','weibull']:
    signal_mean_collect, signal_weights_collect, _ = \
        collect_SN_model(prefix, suffix, folder, base, expr, dist, lag, res, season_list,
                         depth_cm_new, start, L, start2, L2, cmip6_list)
    signal_obs_collect = collect_SN_obs(prefix, suffix, folder, base, prod, dist,
                                        lag, res, season_list, depth_cm_new, start, L)
    predictor_mean_collect, predictor_weights_collect, _ = \
        collect_drivers_model(drivers, suffix, folder, expr, res, season_list, None, None,
                              cmip6_list)
    predictor_obs_collect = collect_drivers_obs(drivers, suffix, folder, expr, res, season_list)

    ind = signal_mean_collect[(season_list[0], depth_cm_new[0])].index
    col = pd.MultiIndex.from_product([half_window_list, drivers,
                                      depth_cm_new, season_list])
    signal_collect_constrained = pd.DataFrame(np.nan, index = ind[1:], columns = col)
    signal_collect_se = pd.DataFrame(np.nan, index = ind[1:], columns = col)
    signal_collect_p = pd.DataFrame(np.nan, index = ind[1:], columns = col)

    #
    signal_mean_sd = pd.DataFrame(np.nan, index = ind,
                                  columns = pd.MultiIndex.from_tuples(signal_mean_collect.keys()))
    for kk in signal_mean_collect.keys():
        signal_mean_sd.loc[:, kk] = signal_mean_collect[kk].std(axis = 1) / \
                                    signal_mean_collect[kk].iloc[0,:].std()
    signal_mean_sd.to_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc',
                                       'drivers', prefix + dist + \
                                       '_signal_mean_sd.csv'))

    for hw, season, ss, dcm in it.product(half_window_list, season_list, drivers, depth_cm_new):
        fig, axes = plt.subplots(3, 3, figsize = (6.5, 6.5), sharex = True, sharey = True)
        fig.subplots_adjust(hspace = 0.15, wspace = 0.05)
        count = 0

        for yind, yy in enumerate(range(start2, 2102-L2)):
            ##if yind == 0:
            ##    # Use original data
            ##    signal_collect_constrained.loc[yy, (hw, ss, dcm, season)] = \
            ##        signal_obs_collect.loc[season, dcm]
            ##    signal_collect_se.loc[yy, (hw, ss, dcm, season)] = 0
            ##    signal_collect_p.loc[yy, (hw, ss, dcm, season)] = 0
            ##    continue

            # Conduct the emergent constraint regredcmion
            if (yy - start2) < hw:
                y = signal_mean_collect[(season, dcm)].loc[:(yy + hw), :].values
                weights = signal_weights_collect[(season, dcm)].loc[:(yy + hw), :].values
                damper = np.exp(np.concatenate([np.arange(-(yy-start2), 0),
                                                np.arange(0, -hw-1, -1)]))
            elif (yy + L2 - 1 + hw) > 2100:
                y = signal_mean_collect[(season, dcm)].loc[(yy-hw):, :].values
                weights = signal_weights_collect[(season, dcm)].loc[(yy-hw):, :].values
                damper = np.exp(np.concatenate([np.arange(-hw, 0), np.arange(0, yy-2056, -1)]))
            else:
                y = signal_mean_collect[(season, dcm)].loc[(yy-hw):(yy + hw),:].values
                weights = signal_weights_collect[(season, dcm)].loc[(yy-hw):(yy + hw), :].values
                damper = np.exp(np.concatenate([np.arange(-hw, 0), np.arange(0, -hw-1, -1)]))
            if ss != 'SSI2':
                x = np.broadcast_to(predictor_mean_collect[(season, ss)].iloc[0,
                        :].values.reshape(1,-1), y.shape)
            else:
                if dcm == '0-10cm':
                    x = np.broadcast_to(signal_mean_collect[(season, '0-100cm')].loc[start,
                        :].values.reshape(1,-1), y.shape)
                else:
                    x = np.broadcast_to(signal_mean_collect[(season, '0-10cm')].loc[start,
                        :].values.reshape(1,-1), y.shape)

            weights = weights * np.broadcast_to(damper.reshape(-1,1), weights.shape)
            
            ##reg = stats.OLS(y.reshape(-1),
            ##                stats.add_constant(x.reshape(-1))).fit()
            reg = stats.WLS(y.reshape(-1), stats.add_constant(x.reshape(-1)),
                            weights.reshape(-1)).fit()

            # Fill the data frames
            if ss != 'SSI2':
                obs_value = predictor_obs_collect.loc[season, ss]
            else:
                if dcm == '0-10cm':
                    obs_value = signal_obs_collect.loc[season, '0-100cm']
                else:
                    obs_value = signal_obs_collect.loc[season, '0-10cm']
            predict_obs, predict_se, predict_p = get_constrained(reg, obs_value)
            signal_collect_constrained.loc[yy, (hw, ss, dcm, season)] = predict_obs
            signal_collect_se.loc[yy, (hw, ss, dcm, season)] = predict_se
            signal_collect_p.loc[yy, (hw, ss, dcm, season)] = predict_p

            # Plot the emergent constraint regredcmion
            if np.mod(yind, 10) == 4:
                ax = axes.flat[count]
                clist0 = [clist[m] \
                          for m in signal_mean_collect[(season, dcm)].columns] * x.shape[0]
                _, h2, h1 = plot_em(ax, reg, obs_value, predict_obs,
                                    predict_se, 0, clist0)
                ##dummy()

                intercept = ('%.2f' % reg.params[0])
                if reg.pvalues[0] <= 0.05:
                    intercept += '*'
                slope = ('%.2f' % reg.params[1])
                if reg.pvalues[1] <= 0.05:
                    slope += '*'
                if reg.params[1] > 0:
                    temp = 'Y = ' + intercept + \
                        ' + ' + slope + ' X'
                else:
                    temp = 'Y = ' + intercept + \
                        ' - ' + slope.replace('-', '') + ' X'
                if reg.params[1] > 0:
                    ax.text(0.05, 0.9, 'Y = ' + intercept + \
                            ' + ' + slope + ' X',
                            transform = ax.transAxes)
                else:
                    ax.text(0.05, 0.05, 'Y = ' + intercept + \
                            ' + ' + slope,
                            transform = ax.transAxes)
                if count == 3:
                    ax.set_ylabel('Future S/N ratio')
                if count == 7:
                    ax.set_xlabel('Historical S/N ratio')
                ax.set_title(str(yy) + '-' + str(yy+L-1), pad = 2)
                ax.tick_params('both', length = 2)
                ax.text(0., 1.01, lab[count],
                        fontdict = {'weight': 'bold'},
                        transform = ax.transAxes)
                count += 1

        h = [None] * len(cmip6_list_models)
        for ind, temp in enumerate(cmip6_list_models):
            h[ind], = ax.plot(np.nan, np.nan, 'o', color = clist[temp])
        ax.legend(h + h2 + h1,
                  cmip6_list_models + \
                  ['Emergent relationship', # '95% CI relationship', 
                   '95% CI predictand'] + \
                  ['Mean NonCMIP', '95% CI predictand'], #, '95% CI total'
                  ncol = 4, loc = [-2., -.65])
        fig.savefig(os.path.join(mg.path_out(),
                                 'figures', 'fig3_em_calc', 'drivers',
                                 prefix + dist + '_' + \
                                 season + '_' + ss + '_' + dcm + '_' + str(hw) + '.png'),
                    dpi = 600., bbox_inches = 'tight')
        plt.close(fig)

    signal_collect_constrained.to_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc',
                                                   'drivers', prefix + dist + \
                                                   '_signal_collect_constrained.csv'))
    signal_collect_se.to_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc',
                                          'drivers', prefix + dist + '_signal_collect_se.csv'))
    signal_collect_p.to_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc', 
                                         'drivers', prefix + dist + '_signal_collect_p.csv'))
