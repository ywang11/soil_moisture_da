"""
2019/07/05

ywang254@utk.edu

Map the 1950-2016 trend in soil moisture for the merged CMIP6 historical 
 models (with stipple for 90% model agreement, not averaging between 
 ensemble simulations, so that models with more ensemble
 members receive more weight).
"""
from utils_management.constants import depth_cm, depth
import utils_management as mg
from misc.plot_utils import plot_map_w_stipple
import numpy as np
import os
import itertools as it
from misc.plot_utils import cmap_gen
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from misc.cmip6_utils import mrsol_availability
import xarray as xr
import multiprocessing as mp


res = '0.5' # Just this will be enough.
season_list = ['annual', 'DJF', 'MAM', 'JJA', 'SON',
               'annual_maximum', 'annual_minimum']
# 'growing_season_mean', 'growing_season_minimum', 'growing_season_maximum'
expr_list = ['historical', 'hist-GHG', 'hist-aer', 'hist-nat']
year_str = '1950-2016'


levels = np.linspace(-0.00031, 0.00031, 21)
cmap = cmap_gen('autumn', 'winter_r')
land_mask = 'vanilla' # 'vanilla'

### MODIFY
##season = season_list[0]
##pct_hatch = 0.75 # hatch to indicate percent agreement between source models.


def plotter(option):
    season, pct_hatch = option
    fig, axes = plt.subplots(nrows = len(depth), ncols = len(expr_list),
                             figsize = (22, 12),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    for i, j in it.product(range(len(depth)), range(len(expr_list))):
        d = depth[i]
        dcm = depth_cm[i]
        expr = expr_list[j]
    
        ax = axes[i,j]
    
        data = xr.open_dataset(os.path.join(mg.path_out(),
                                            'calc_trend_map_cmip6',
                                            'ensemble_merge',
                                            expr + '_' + d + \
                                            '_' + season + '_' + \
                                            year_str + '.nc'))
        trend = data['trend'].values.copy()
        data.close()
    
        #######################################################################
        # Collect the relevant CMIP6 individual models and calculate percent
        # agreement.
        #######################################################################
        if expr == 'historical':
            list1, _ = mrsol_availability(dcm, land_mask, 'historical', res)
            list2, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
            cmip6_list = list(set(list1) & set(list2))
        else:
            cmip6_list, _ = mrsol_availability(dcm, land_mask, expr, res)
    
        trend_src = np.full([len(cmip6_list), trend.shape[0],
                             trend.shape[1]], np.nan)
        for j, m_v in enumerate(cmip6_list):
            data = xr.open_dataset(os.path.join(mg.path_out(),
                                                'calc_trend_map_cmip6', expr,
                                                m_v + '_' + d + '_' + \
                                                season + \
                                                '_' + year_str + '.nc'))
            trend_src[j, :, :] = data['trend'].values.copy()
            data.close()
    
        consistent_sign = ((trend_src > 0.) & (trend > 0.)) | \
                          ((trend_src < 0.) & (trend < 0.))
        pct_consistent_sign = np.mean(consistent_sign, axis = 0)
    
        ax, h = plot_map_w_stipple(ax, data.lat.values, data.lon.values,
                                   trend, hatch_mask = pct_consistent_sign > \
                                   pct_hatch, add_cyc = True,
                                   contour_style = {'levels': levels,
                                                    'cmap': cmap,
                                                    'extend': 'both'})
        ax.set_title(dcm + ' ' + expr)

    cax = fig.add_axes([0.2, 0.05, 0.6, 0.02])
    cb = fig.colorbar(h, cax=cax, boundaries = levels, shrink = 0.5,
                      orientation = 'horizontal')
    cb.set_ticks(np.arange(-0.0003, 0.00031, 0.00005))
    cb.set_ticklabels([('%.2f' % x) for x in np.arange(-3., 3.1, 0.5)])
    cb.ax.set_xlabel('1e-4 m$^3$/m$^3$/year')
    fig.savefig(os.path.join(mg.path_out(), 'plot_map_trend',
                             'ensemble_merge_' + season + '_' + \
                             str(pct_hatch) + '.png'), dpi = 600.,
                bbox_tight = True)
    plt.close(fig)


pool = mp.Pool(4)
pool.map_async(plotter, list(it.product(season_list, [0.9, 0.75, 0.5])))
pool.close()
pool.join()
