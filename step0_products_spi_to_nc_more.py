"""
20200603

ywang254@utk.edu

Average the products to mean_products
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, target_lat, target_lon
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
from misc.spi_utils import *
from scipy.integrate import quad
import sys


land_mask = 'vanilla'
res = '0.5'
lag_list = [1,3,6]
dist_list = ['weibull', 'gmm']


#prod_list = ['mean_lsm', 'mean_cmip5', 'mean_cmip6', 'mean_2cmip',
#             'mean_all',
#             'dolce_lsm', 'dolce_cmip5', 'dolce_cmip6', 'dolce_2cmip',
#             'dolce_all',
#             'em_lsm', 'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all']
prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
period = pd.date_range('1951-01-01', '2016-12-31', freq = 'YS')
year = range(1951, 2017)


#
ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


for dcm,dist,s,m in it.product(depth_cm_new, dist_list, lag_list, range(12)):

    for pind, prod in enumerate(prod_list):
        h = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'SPI_Products',
                                         'get_' + ref_period_str,
                                         dist, prod + '_' + dcm + \
                                         '_' + str(s) + '_' + res + '_' + \
                                         str(m) + '.nc'))
        if pind == 0:
            spi = h['spi'].values.copy() / len(prod_list)
        else:
            spi += (h['spi'].values.copy() / len(prod_list))
        h.close()

        #
        spi = xr.DataArray(spi, coords = {'time': period,
                                          'lat': target_lat[res],
                                          'lon': target_lon[res]},
                           dims = ['time', 'lat', 'lon'])            

        # Save to file.
        spi.to_dataset(name = 'spi').to_netcdf( os.path.join( \
            mg.path_intrim_out(), 'SPI_Products', 'get_' + ref_period_str,
            dist, 'mean_products_' + dcm + '_' + str(s) + '_' + res + \
            '_' + str(m) + '.nc') )
