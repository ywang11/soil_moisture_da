"""
2020/09/15
ywang254@utk.edu

Calculate the by-latitude land-area weighted mean drivers of the CMIP6 experiments.

2021/07/13

Mask using NDVI mask.
"""
from utils_management.constants import drivers, target_lat, target_lon
import utils_management as mg
from misc.analyze_utils import by_lat_avg, by_subcontinent_avg, get_ndvi_mask
from misc.cmip6_utils import one_layer_availability
from glob import glob
import os
import xarray as xr
import pandas as pd
import multiprocessing as mp
import itertools as it
import numpy as np


res = '0.5'
land_mask = 'vanilla'
season_list = [str(m) for m in range(12)]


#for varname in drivers:
varname = drivers[REPLACE1]
expr = 'REPLACE2'
noSahel = True # True, False


if expr == 'historical':
    tvec = pd.date_range(start = '1951-01-01', end = '2100-12-31', freq = 'YS')
    a, _ = one_layer_availability(varname, land_mask, 'historical', res)
    b, _ = one_layer_availability(varname, land_mask, 'ssp585', res)
    cmip6_list = list(set(a) & set(b))
else:
    tvec = pd.date_range(start = '1951-01-01', end = '2020-12-31', freq = 'YS')
    cmip6_list, _ = one_layer_availability(varname, land_mask, expr, res)
    cmip6_list = sorted(cmip6_list)


if noSahel:
    ndvi_mask = get_ndvi_mask(land_mask)
    suffix = '_noSahel'
else:
    suffix = ''


##for model in cmip6_list:
def calc(model):
##for model in ['BCC-CSM2-MR_r1i1p1f1']:
    print(model + ' start')

    if expr == 'historical':
        fhist = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                         land_mask, 'CMIP6',
                                         'historical', model,
                                         varname + '_' + res + '*.nc')))
        fssp585 = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                           land_mask, 'CMIP6',
                                           'ssp585', model,
                                           varname + '_' + res + '*.nc')))
        flist = fhist + fssp585
    else:
        flist = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                         land_mask, 'CMIP6', expr, 
                                         model, varname + '_' + res + '*.nc')))

    hr = xr.open_mfdataset(flist, decode_times = False)
    if noSahel:
        var0 = hr[varname].where(ndvi_mask)
    else:
        var0 = hr[varname]

    for season in season_list:
        # Average over the previous two months and the current month.
        # Debug!
        ##var = var0[(int(season)+12)::12, :, :].load() / 3 + \
        ##    var0[(int(season)+11):-1:12, :, :].values / 3 + \
        ##    var0[(int(season)+10):-2:12, :, :].values / 3
        var = var0[int(season)::12, :, :]

        var['time'] = tvec

        if ((model == 'EC-Earth3') | (model == 'EC-Earth3-Veg')) & \
           (varname == 'evspsbl'):
            # The values are probably reversed
            var = -var

        prefix = os.path.join(mg.path_out(), 'cmip6_drivers_summary', 
                              varname, expr, model + '_' + season)
        by_lat_avg(var, tvec, hr.lat.values, hr.lon.values,
                   prefix, res, suffix)

    hr.close()

    print(model + ' finish')

p = mp.Pool(4)
p.map_async(calc, cmip6_list)
p.close()
p.join()
