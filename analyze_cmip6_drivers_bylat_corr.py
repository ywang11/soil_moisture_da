import os
import numpy as np
import xarray as xr
import matplotlib as mpl
from utils_management.constants import *
import utils_management as mg
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from analyze_cmip6_drivers_map import get_model_list
from analyze_cmip6_drivers_bylat_plot_preliminary import get_sm_trend, get_driver_trend
from misc.plot_utils import cmap_div, stipple
from misc.standard_diagnostics import get_bylat_std
import multiprocessing as mp
import itertools as it
from matplotlib import gridspec


def get_model_vote(cmip6_list):
    temp_model = [x.split('_')[0] for x in cmip6_list]
    temp_vote = np.full(len(temp_model), np.nan)
    for ind, model in enumerate(temp_model):
        n_model = np.sum([x == model for x in temp_model])
        temp_vote[ind] = 1 / n_model
    temp_vote = temp_vote / np.sum(temp_vote)
    return temp_vote


land_mask = 'vanilla'
res = '0.5'
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']
lab = 'abcdefghijklmnopqrstuvwxyz'
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')
lat_median_name.reverse()
alpha_list = [0, 0.01, 0.1, 0.5, 0.9]
L = 45 # 2016 - 1971 + 1
year_range_list = [range(i, i+L) for i in  [1995, 2035, 2055]]
##year_range_list = [range(1971, 2017)]
expr = 'historical'


# MODIFY
alpha = 0.1


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
fig, axes = plt.subplots(2*len(year_range_list), len(drivers), figsize = (6.5, 8.),
                         sharex = True, sharey = True)
fig.subplots_adjust(hspace = 0.15, wspace = 0.1)
for dind, dcm in enumerate(depth_cm_new):
    for yind, vind in it.product(range(len(year_range_list)), range(len(drivers))):
        yrng = year_range_list[yind]
        year_str = str(yrng[0]) + '-' + str(yrng[-1])
        vv = drivers[vind]

        cmip6_list = get_model_list(expr, drivers, dcm, land_mask, res)
        temp_vote = get_model_vote(cmip6_list)

        ax = axes[yind + dind*len(year_range_list), vind]

        coef_collect = np.full([25, 12, len(cmip6_list)], np.nan)
        for jj, model in enumerate(cmip6_list):
            hr = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_drivers_corr', 
                                              'bylat', expr, 'alpha=%.2f' % alpha,
                                              model + '_' + dcm + '_' + year_str + '.nc'))
            coef_collect[:, :, jj] = hr['coef'].values[vind, :, :]
            hr.close()
        coef_collect_mean = np.average(coef_collect, axis = 2, weights = temp_vote)
        coef_collect_std = coef_collect.std(axis = 2)

        if vv == 'pr':
            levels = np.linspace(-1., 1., 41)
            levels2 = levels
        elif vv == 'tas':
            levels = np.linspace(-0.3, 0.3, 41)
            levels2 = levels
        elif vv == 'lai':
            levels = np.concatenate([-np.power(4, np.linspace(2., -2, 20)),
                                     np.array([0]),
                                     np.power(4, np.linspace(-2., 2., 20))])
            levels2 = np.linspace(-2., 2., 41)
        elif vv == 'snw':
            levels = np.concatenate([-np.power(10, np.linspace(1, -3., 20)),
                                     np.array([0]),
                                     np.power(10, np.linspace(-3., 1., 20))])
            levels2 = levels

            # mask where the average SNW values were small
            value_mask = np.full([25 ,12, len(cmip6_list)], np.nan)
            for jj, model in enumerate(cmip6_list):
                for season in range(12):
                    fname = os.path.join(mg.path_out(), 'cmip6_drivers_summary', vv,
                                         expr, model + '_' + str(season) + '_' + res + \
                                         '_g_lat_ts.csv')
                    _, clim = get_bylat_std(fname, pd.date_range(start = '1971-01-01',
                                                                 end = '2016-12-31', freq = 'YS'))
                    value_mask[:,season,jj] = clim.values
            value_mask = np.average(value_mask, axis = 2, weights = temp_vote)
            coef_collect_mean[value_mask < 0.01] = np.nan
            coef_collect_std[value_mask < 0.01] = np.nan

        cf = ax.contourf(hr['month'], hr['lat'], coef_collect_mean,
                         cmap = cmap_div(), levels = levels, extend = 'both',
                         norm = mpl.colors.BoundaryNorm(levels, 256))
        cf2 = ax.contour(hr['month'], hr['lat'], coef_collect_std,
                         cmap = 'gray_r', levels = levels2[5::10], linestyles = 'dashed',
                         linewidths = 0.5)
        ax.clabel(cf2, inline = True, fontsize = 5)

        if (dind==0) & (yind == 0):
            ax.set_title(vv)
        if vind == 0:
            ax.set_ylabel(year_str)
        ax.text(0., 1.02, lab[vind + yind*len(drivers) + dind*len(year_range_list)*len(drivers)],
                fontweight = 'bold', transform = ax.transAxes)
        ax.set_xticks(np.linspace(0.35, 10.65, 12))
        ax.set_xticklabels(season_list_name)
        ax.set_yticks([60, 40, 20, 0, -20, -40])
        ax.set_yticklabels(lat_median_name[2::4])
        ax.tick_params('both', length = 1.)
        if (yind == 1) & (vind == 0):
            ax.text(-0.5, 0.5, dcm, rotation = 90, verticalalignment = 'center',
                    transform = ax.transAxes)

        if (dind==1) & (yind == (len(year_range_list)-1)):
            cax = fig.add_axes([0.1 + 0.205 * vind, 0.05, 0.18, 0.01])
            plt.colorbar(cf, cax = cax, ticks = levels[::10], orientation = 'horizontal')

fig.savefig(os.path.join(mg.path_out(), 'cmip6_drivers_corr',
                         'bylat_plot', expr, 'alpha=%.2f' % alpha, 
                         'corr_summary.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
