"""
2019/10/09
ywang254@utk.edu

Convert the Aridity Index to several regions:
    <0.05 hyper-arid, 0.05-0.2 arid, 0.2-0.5 semiarid, 
    0.5-0.65 dry subhumid, >=0.65 humid,
following https://www.nature.com/articles/nclimate2837
"""
import os
import utils_management as mg
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt


res = '10' # 0.5, 2.5, 5, 10

data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'da_masks',
                                    'Aridity_Index_' + res + '.nc'))


# Re-fill the PET = 0 grids with -1
# ---- load a random grid
dummy = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'da_masks', 
                                     'natural_earth_continent_mask_' + \
                                     res + '.nc'))
data.ai.values = np.where(np.isnan(data.ai.values) & \
                          ~np.isnan(dummy.region.values),
                          -1, data.ai.values)
dummy.close()


ai_discrete = xr.DataArray(data = np.full(data.ai.shape, np.nan), 
                           coords = {'lat': data.lat, 'lon': data.lon},
                           dims = ['lat', 'lon'])
ai_discrete.values = np.where(data.ai.values < 0, -1,
    np.where(data.ai.values < 0.05, 0,
    np.where(data.ai.values < 0.2, 1, 
    np.where(data.ai.values < 0.5, 2, 
    np.where(data.ai.values <= 0.65, 3, 
    np.where(~np.isnan(data.ai.values), 4, np.nan))))))

ai_discrete.attrs['Key'] = '-1: Long-term PET < 1e-6; 0: hyper-arid; 1: arid; 2: semiarid; 3: dry subhumid; 4: humid; '

ai_discrete.to_dataset(name = 'ai').to_netcdf(os.path.join( \
    mg.path_intrim_out(), 'da_masks', 'Aridity_Index_discrete_' + res + '.nc'),
    encoding = {'ai': {'_FillValue': -999, 'dtype': int}})

fig, axes = plt.subplots(nrows = 2, ncols = 1)
c1 = axes[0].contourf(data.ai, cmap = 'Spectral', 
                      levels = [-1., 0., 0.05, 0.2, 0.5, 0.65, 1.],
                      extend = 'both')
plt.colorbar(c1, ax = axes[0])
c2 = axes[1].contourf(ai_discrete)
plt.colorbar(c2, ax = axes[1])
fig.savefig(os.path.join(mg.path_intrim_out(), 'da_masks', 
                         'Aridity_Index_discrete_' + res + '.png'))

data.close()
