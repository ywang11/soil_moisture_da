"""
20210715

Compare the residuals of the emergent constraint models over time.
"""
import pandas as pd
import os
from utils_management.constants import *
import utils_management as mg
import matplotlib.pyplot as plt
import matplotlib as mpl


mpl.rcParams['font.size'] = 6.
mpl.rcParams['font.size'] = 6.
clist = ['#7fc97f', '#beaed4', '#fdc086']


dist = 'gmm'
for dcm in depth_cm_new:
    fig, axes = plt.subplots(4, 3, figsize = (12., 6.5))

    for mon in range(12):
        ax = axes.flat[mon]

        h = []
        for count, method in enumerate(['lm2', 'gam', 'gls']):
            resid = pd.read_csv(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc',
                                             dist + '_' + method + '_signal_collect_resid.csv'),
                                index_col = 0, header = [0,1,2]).loc[:, (str(mon), dcm)]
            hb = ax.boxplot(resid.values.T, 
                            positions = resid.index.values + count * resid.shape[0],
                            whis = [2.5, 97.5], showfliers = False, 
                            patch_artist = True)
            for el in hb['boxes']:
                el.set_facecolor(clist[count])
            h.append(hb['boxes'][0])
    fig.savefig(os.path.join(mg.path_out(), 'figures', 'fig3_em_calc', 
                             dist + '_summary_' + dcm + '.png'),
                dpi = 600, bbox_inches = 'tight')
    plt.close(fig)


            
