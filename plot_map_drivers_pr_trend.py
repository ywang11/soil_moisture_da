"""
20200425
ywang254@utk.edu

Plot the trend in the CRU & CMIP6 precipitation.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, target_lat, target_lon
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import matplotlib as mpl
import matplotlib.patches as mpatches
import utils_management as mg
import itertools as it
from matplotlib import gridspec
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from cartopy.feature import LAND
from misc.analyze_utils import get_ndvi_mask


def get_cmip6_list(varname):
    if varname == 'pr-evspsbl':
        a1, _ = one_layer_availability('pr', land_mask, 'historical', res)
        a2, _ = one_layer_availability('pr', land_mask, 'ssp585', res)
        b1, _ = one_layer_availability('evspsbl', land_mask, 'historical',
                                       res)
        b2, _ = one_layer_availability('evspsbl', land_mask, 'ssp585', res)
        cmip6_list = sorted(list(set(a1) & set(a2) & set(b1) & set(b2)))
    elif varname == 'pr-evspsbl-mrro':
        a1, _ = one_layer_availability('pr', land_mask, 'historical', res)
        a2, _ = one_layer_availability('pr', land_mask, 'ssp585', res)
        b1, _ = one_layer_availability('evspsbl', land_mask, 'historical',
                                       res)
        b2, _ = one_layer_availability('evspsbl', land_mask, 'ssp585', res)
        c1, _ = one_layer_availability('mrro', land_mask, 'historical',
                                       res)
        c2, _ = one_layer_availability('mrro', land_mask, 'ssp585', res)
        cmip6_list = sorted(list(set(a1) & set(a2) & set(b1) & set(b2) & \
                                 set(c1) & set(c2)))
    elif varname == 'pr-evspsbl-mrro-dsnw':
        a1, _ = one_layer_availability('pr', land_mask, 'historical', res)
        a2, _ = one_layer_availability('pr', land_mask, 'ssp585', res)
        b1, _ = one_layer_availability('evspsbl', land_mask, 'historical',
                                       res)
        b2, _ = one_layer_availability('evspsbl', land_mask, 'ssp585', res)
        c1, _ = one_layer_availability('mrro', land_mask, 'historical',
                                       res)
        c2, _ = one_layer_availability('mrro', land_mask, 'ssp585', res)
        d1, _ = one_layer_availability('snw', land_mask, 'historical', res)
        d2, _ = one_layer_availability('snw', land_mask, 'ssp585', res)
        cmip6_list = sorted(list(set(a1) & set(a2) & set(b1) & set(b2) & \
                                 set(c1) & set(c2) & set(d1) & set(d2)))
    elif 'sm' in varname:
        dcm = varname.split('_')[1]
        a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
        b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
        cmip6_list = sorted(list(set(a) & set(b)))
    else:
        a, _ = one_layer_availability(varname, land_mask, 'historical',
                                      res)
        b, _ = one_layer_availability(varname, land_mask, 'ssp585', res)
        cmip6_list = sorted(list(set(a) & set(b)))
    return cmip6_list


def read_trend_model(varname, model, season, year_str, noSahel):
    hr = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_drivers_summary',
                                      varname, model + '_' + res + \
                                      '_g_map_trend_' + season + \
                                      '_' + year_str + '.nc'))
    if noSahel:
        var = hr['g_map_trend'].where(ndvi_mask).values.copy()
    else:
        var = hr['g_map_trend'].values.copy()
    hr.close()
    return var

def get_model_vote(model_id_list):
    temp_model = [x.split('_')[0] for x in model_id_list]
    vote_model = np.full(len(model_id_list), np.nan)
    for ind, model in enumerate(temp_model):
        n_model = np.sum([x == model for x in temp_model])
        vote_model[ind] = 1/n_model
    vote_model = vote_model / np.sum(vote_model)
    return vote_model


def get_hatch(val_per_model, vote_per_model, val_mean):
    pct_pos = np.sum((val_per_model > 0.).astype(float) * \
        np.broadcast_to(vote_per_model.reshape(-1,1,1),
                        val_per_model.shape), axis = 0)
    pct_neg = np.sum((val_per_model < 0.).astype(float) * \
        np.broadcast_to(vote_per_model.reshape(-1,1,1),
                        val_per_model.shape), axis = 0)
    hatch1 = ((val_mean > 0) & (pct_pos > pct)) | \
        ((val_mean < 0) & (pct_neg > pct))
    hatch2 = (~hatch1) & \
        (((val_mean > 0) & (pct_pos > (pct - 0.1))) | \
         ((val_mean < 0) & (pct_neg > (pct - 0.1))))
    return hatch1, hatch2


def read_trend_obs(varname, lsm, season, year_str, noSahel):
    hr = xr.open_dataset(os.path.join(mg.path_out(), 'obs_drivers_summary',
                                      lsm + '_' + varname + '_' + res + \
                                      '_g_map_trend_' + season + \
                                      '_' + year_str + '.nc'))
    if noSahel:
        var = hr['g_map_trend'].where(ndvi_mask).values.copy()
    else:
        var = hr['g_map_trend'].values.copy()
    hr.close()
    return var


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5.
mpl.rcParams['hatch.linewidth'] = 0.5
cmap = cmap_div()
levels = np.arange(-2.2e-2, 2.21e-2, 1e-3)
hs = '/////'
pct = 0.9


res = '0.5'
land_mask = 'vanilla'
lat = target_lat[res]
lon = target_lon[res]
lab = 'abcdefghijklmnopqrstuvwxyz'
prod_name = 'CRU v4.03'
expr = 'historical'
expr_name = 'ALL'
lsm_all = ['CERA20C', 'CLASS-CTEM-N', 'CLM4', 'CLM4VIC',
           'CLM5.0', 'ERA20C', 'ERA-Interim', 'ERA5', 'ERA-Land', 'ESA-CCI',
           'GLDAS_Noah2.0', 'GLEAMv3.3a', 'ISAM', 'JSBACH', 'JULES',
           'LPX-Bern', 'ORCHIDEE', 'ORCHIDEE-CNP', 'SiBCASA']
met_to_lsm = {'CRU_v4.03': ['ESA-CCI', 'GLEAMv3.3a'],
              'CRU_v3.20': ['CLASS-CTEM-N', 'CLM4', 'CLM4VIC', 'SiBCASA'],
              'CRU_v3.26': ['CLM5.0', 'ISAM', 'JSBACH', 'JULES', 'LPX-Bern',
                            'ORCHIDEE', 'ORCHIDEE-CNP'],
              'GLDAS_Noah2.0': ['GLDAS_Noah2.0'],
              'ERA-Interim': ['ERA-Interim', 'ERA-Land'],
              'ERA5': ['ERA5'],
              'ERA20C': ['ERA20C'],
              'CERA20C': ['CERA20C']}
lsm_to_met = {}
for a,b in met_to_lsm.items():
    for bb in b:
        if not bb in list(lsm_to_met.keys()):
            lsm_to_met[bb] = a
        else:
            raise Exception('Too many met.')
vv = 'pr'
year_str = '1971-2016'


noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
    land_mask = 'vanilla'
    ndvi_mask = get_ndvi_mask(land_mask)
else:
    ndvi_mask = ''


fig = plt.figure(figsize = (6., 4.))
gs = gridspec.GridSpec(1, 3, hspace = 0.02, wspace = 0.1)
sub_gs1 = gridspec.GridSpecFromSubplotSpec(4, 1,
                                           subplot_spec = gs[0],
                                           hspace = 0., wspace = 0.)
sub_gs2 = gridspec.GridSpecFromSubplotSpec(4, 1,
                                           subplot_spec = gs[1],
                                           hspace = 0., wspace = 0.)
sub_gs3 = gridspec.GridSpecFromSubplotSpec(4, 1,
                                           subplot_spec = gs[2],
                                           hspace = 0., wspace = 0.)

for sind, ss in enumerate(['DJF', 'MAM', 'JJA', 'SON']):
    #
    val0 = read_trend_obs(vv, 'CRU_v4.03', ss, year_str, noSahel)

    #
    cmip6_list = get_cmip6_list(vv)
    val = np.full([len(cmip6_list), len(lat), len(lon)], np.nan)
    for mind, model in enumerate(cmip6_list):
        val[mind, :, :] = read_trend_model(vv, model, ss, year_str, noSahel)
    # ---- calculate the weighted average
    vote_model = get_model_vote(cmip6_list)
    val_mean = np.sum(val * np.broadcast_to(vote_model.reshape(-1,1,1),
                                            val.shape), axis = 0)
    val_std = np.std(val, axis = 0)

    # Plot products
    ax = plt.subplot(sub_gs1[sind], projection = ccrs.PlateCarree())
    fig.add_subplot(ax)
    ax.coastlines(lw = 0.1)
    gl = ax.gridlines(xlocs = [-180, 180],
                      ylocs = np.arange(-40, 61, 5),
                      linestyle = '--',
                      linewidth = 0.5, draw_labels = True)
    gl.xlabels_top = False
    gl.xlabels_bottom = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-180, 180])
    gl.ylocator = mticker.FixedLocator([-40, -20, 0, 20, 40, 60])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'color': 'black', 'weight': 'normal'}
    ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
    ax.outline_patch.set_visible(False)
    ax.spines['top'].set_visible(True)
    ax.spines['bottom'].set_visible(True)
    ax.spines['left'].set_visible(True)
    ax.spines['right'].set_visible(True)
    ax.spines['top'].set_lw(0.2)
    ax.spines['bottom'].set_lw(0.2)
    ax.spines['left'].set_lw(0.2)
    ax.spines['right'].set_lw(0.2)
    ax.add_feature(LAND, facecolor = [0.9, 0.9, 0.9])
    h = ax.contourf(lon, lat, val0,
                    cmap = cmap, levels = levels, extend = 'both',
                    transform = ccrs.PlateCarree())
    ax.text(-0.3, 0.5, ss, fontsize = 5, rotation = 90.,
            verticalalignment = 'center', transform = ax.transAxes)
    ax.text(0.01, 0.05, lab[sind], weight = 'bold',
            transform = ax.transAxes)
    if sind == 0:
        ax.text(.5, 1.1, prod_name, fontsize = 5,
                horizontalalignment = 'center',
                transform = ax.transAxes)
    hatch1 = ((val_mean > 0) & (val0 > 0)) | \
        ((val_mean < 0) & (val0 < 0))
    stipple(ax, lat, lon, mask = hatch1,
            transform = ccrs.PlateCarree(), hatch = '///////////')
    hatch2 = (~hatch1) & \
        ((val0 < (val_mean - 1.96*val_std)) | \
         (val0 > (val_mean + 1.96*val_std)))
    stipple(ax, lat, lon, mask = hatch2,
            transform = ccrs.PlateCarree(), hatch = '\\\\\\\\\\\\')


    # Plot CMIP6
    ax = plt.subplot(sub_gs2[sind], projection = ccrs.PlateCarree())
    fig.add_subplot(ax)
    ax.coastlines(lw = 0.1)
    gl = ax.gridlines(xlocs = [-180, 180],
                      ylocs = np.arange(-40, 61, 20),
                      linestyle = '--', 
                      linewidth = 0.5, draw_labels = False) # True)
    #gl.xlabels_top = False
    #gl.xlabels_bottom = False
    #gl.ylabels_right = False
    #gl.xlocator = mticker.FixedLocator([-180, 180])
    #gl.ylocator = mticker.FixedLocator([-40, -20, 0, 20, 40, 60])
    #gl.xformatter = LONGITUDE_FORMATTER
    #gl.yformatter = LATITUDE_FORMATTER
    #gl.xlabel_style = {'color': 'black', 'weight': 'normal'}
    ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
    ax.outline_patch.set_visible(False)
    ax.spines['top'].set_visible(True)
    ax.spines['bottom'].set_visible(True)
    ax.spines['left'].set_visible(True)
    ax.spines['right'].set_visible(True)
    ax.spines['top'].set_lw(0.2)
    ax.spines['bottom'].set_lw(0.2)
    ax.spines['left'].set_lw(0.2)
    ax.spines['right'].set_lw(0.2)
    ax.add_feature(LAND, facecolor = [0.9, 0.9, 0.9])
    h = ax.contourf(lon, lat, val_mean, cmap = cmap,
                    levels = levels, extend = 'both',
                    transform = ccrs.PlateCarree())
    hatch1, hatch2 = get_hatch(val, vote_model, val_mean)
    stipple(ax, lat, lon, mask = hatch1,
            transform = ccrs.PlateCarree(), hatch = '///////////')
    stipple(ax, lat, lon, mask = hatch2,
            transform = ccrs.PlateCarree(), hatch = '\\\\\\\\\\\\')
    #ax.text(0.5, 1.05, ss, fontsize = 5,
    #        horizontalalignment = 'center',
    #        transform = ax.transAxes)
    ax.text(0.01, 0.05, lab[4 + sind], weight = 'bold',
            transform = ax.transAxes)
    if sind == 0:
        ax.text(.5, 1.17, expr_name, fontsize = 5,
                horizontalalignment = 'center', transform = ax.transAxes)

    # Plot product minus CMIP6
    ax = plt.subplot(sub_gs3[sind], projection = ccrs.PlateCarree())
    fig.add_subplot(ax)
    ax.coastlines(lw = 0.1)
    gl = ax.gridlines(xlocs = [-180, 180],
                      ylocs = np.arange(-40, 61, 20),
                      linestyle = '--', 
                      linewidth = 0.5, draw_labels = False) # True)
    #gl.xlabels_top = False
    #gl.xlabels_bottom = False
    #gl.ylabels_right = False
    #gl.xlocator = mticker.FixedLocator([-180, 180])
    #gl.ylocator = mticker.FixedLocator([-40, -20, 0, 20, 40, 60])
    #gl.xformatter = LONGITUDE_FORMATTER
    #gl.yformatter = LATITUDE_FORMATTER
    #gl.xlabel_style = {'color': 'black', 'weight': 'normal'}
    ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
    ax.outline_patch.set_visible(False)
    ax.spines['top'].set_visible(True)
    ax.spines['bottom'].set_visible(True)
    ax.spines['left'].set_visible(True)
    ax.spines['right'].set_visible(True)
    ax.spines['top'].set_lw(0.2)
    ax.spines['bottom'].set_lw(0.2)
    ax.spines['left'].set_lw(0.2)
    ax.spines['right'].set_lw(0.2)
    ax.add_feature(LAND, facecolor = [0.9, 0.9, 0.9])
    h = ax.contourf(lon, lat, val0 - val_mean, cmap = cmap,
                    levels = levels, extend = 'both',
                    transform = ccrs.PlateCarree())
    ax.text(0.01, 0.05, lab[8 + sind], weight = 'bold',
            transform = ax.transAxes)
    if sind == 0:
        ax.text(.5, 1.17, prod_name + ' - ' + expr_name, fontsize = 5,
                horizontalalignment = 'center', transform = ax.transAxes)

cax = fig.add_axes([0.1, 0.08, 0.8, 0.01])
plt.colorbar(h, cax = cax, orientation = 'horizontal',
             label = 'Trend of precipitation ' + \
             '(mm day$^{-1}$ year$^{-1}$)')
cax.text(.0, -1., 'Dry', fontsize = 5, transform = cax.transAxes,
         horizontalalignment = 'center') # , color = '#762a83')
cax.text(1.,  -1., 'Wet', fontsize = 5, transform = cax.transAxes,
         horizontalalignment = 'center') # , color = '#3288bd')
fig.savefig(os.path.join(mg.path_out(), 'figures',
                         'plot_map_drivers_' + 
                         'pr_trend_' + year_str + '.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
