"""
20200425
ywang254@utk.edu

Plot the trend in seasonal SPI.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.cmip6_utils import mrsol_availability
from misc.plot_utils import plot_ts_shade, cmap_gen, stipple
import matplotlib as mpl
import utils_management as mg
import itertools as it
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 'medium'
mpl.rcParams['hatch.linewidth'] = 0.4


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list = [str(i) for i in range(12)]


folder = 'bylat'
suffix = ''
lag = 3
pct = 0.9 # consistent with "step8_plot_trend_spi_bylat.py"

expr_list = ['historical', 'NoNAT', 'hist-nat', 'hist-GHG', 'hist-aer',
             'GHGAER'] # 'OTH'
expr_label = ['ALL', 'ANT', 'NAT', 'GHG', 'AER', 'GHGAER'] # 'OTH'

season_list = ['Annual', 'DJF', 'MAM', 'JJA', 'SON']
prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']

#cmap = get_cmap('Spectral')
cmap = cmap_gen('Wistia_r', 'cool')
levels = np.arange(-2e-2, 2.1e-2, 2e-3)


#
expr_list2 = expr_list
expr_label2 = expr_label
for dist, dcm in it.product(['gmm','weibull'], depth_cm_new):
    fig, axes = plt.subplots(nrows = len(expr_list2) + 1,
                             ncols = len(season_list),
                             figsize = (2. * len(season_list),
                                        (1+len(expr_list2)) * 1.),
                             sharex = True, sharey = True,
                             subplot_kw = {'projection': ccrs.Miller()})
    fig.subplots_adjust(wspace = 0.02, hspace = 0.02)

    ###########################################################################
    # Products
    ###########################################################################
    for j, season in enumerate(season_list):
        ax = axes[0, j]
        ax.coastlines(lw = 0.1)
        ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)  
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)  
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.1)
        ax.spines['bottom'].set_lw(0.1)
        ax.spines['left'].set_lw(0.1)
        ax.spines['right'].set_lw(0.1)

        # ---- mean_products
        data = xr.open_dataset(os.path.join(mg.path_out(),
                                            'standard_diagnostics_spi',
                                            'products', dist,
                                            'mean_products_' + \
                                            str(lag) + '_' + res + '_' + \
                                            dcm + '_g_map_trend_' + season + \
                                            '.nc'))
        val0 = data['g_map_trend'].values.copy()
        data.close()
        h = ax.contourf(data.lon, data.lat, val0,
                        cmap = cmap, levels = levels, extend = 'both',
                        transform = ccrs.PlateCarree())
        if j == 0:
            t = ax.text(-0.15, 0.5, 'Merged Product', rotation = 90,
                        transform = ax.transAxes, verticalalignment = 'center')
            t.set_multialignment('center')
        ax.set_title(season)

        # Gridliner not supported for Miller projection
        if False:
            gl = ax.gridlines(crs=ccrs.PlateCarree(), linewidth=0,
                              color='black', alpha=0.5, draw_labels=True)
            gl.xlabels_top = False
            gl.ylabels_left = True
            gl.ylabels_right = False
            gl.ylocator = mticker.FixedLocator(np.arange(-40, 61, 20))
            gl.xformatter = LONGITUDE_FORMATTER
            gl.yformatter = LATITUDE_FORMATTER
            #gl.xlabel_style = {'color': 'red', 'weight': 'bold'}
            if i == 1:
                gl.xlabels_bottom = True
                gl.xlocator = mticker.FixedLocator(np.arange(-160, 161, 20))
            else:
                gl.xlabels_bottom = False

        # ---- No stipple
        if False:
            val = np.full([len(prod_list), len(data.lat), len(data.lon)],
                          np.nan)
            for pind, prod in enumerate(prod_list):
                data = xr.open_dataset(os.path.join(mg.path_out(),
                   'standard_diagnostics_spi', 'products', dist,
                   prod + '_' +  str(lag) + '_' + res + '_' + dcm + \
                   '_g_map_trend_' + season + '.nc'))
                val[pind, :, :] = data['g_map_trend'].values.copy()
                data.close()
            ## ---- calculate 1.5 std of the product trends
            ##val = val.std(axis = 0) * 1.5
            ##stipple(ax, data.lat, data.lon, mask = val0 > val,
            ##        transform = ccrs.PlateCarree(), hatch = '.')
            # ---- calculate the consistency in sign.
            val = ((val0 > 0) & (np.mean(val > 0, axis = 0) > pct)) | \
                ((val0 < 0) & (np.mean(val < 0, axis = 0) > pct))
            stipple(ax, data.lat, data.lon, mask = val,
                    transform = ccrs.PlateCarree(), hatch = '//////////')

    ###########################################################################
    # CMIP6
    ###########################################################################
    for j, k in it.product(range(len(season_list)), range(len(expr_list2))):
        season = season_list[j]
        expr = expr_list2[k]

        ax = axes[k+1, j]
        ax.coastlines(lw = 0.1)
        ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)  
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)  
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.1)
        ax.spines['bottom'].set_lw(0.1)
        ax.spines['left'].set_lw(0.1)
        ax.spines['right'].set_lw(0.1)

#        # ---- ensemble merge
#        data = xr.open_dataset(os.path.join(mg.path_out(), 
#            'standard_diagnostics_spi', 'ensemble_merge',
#            expr + '_' + str(lag) + '_' + res + '_' + \
#            dcm + '_g_map_trend_' + season + '.nc'))
#        val0 = data['g_map_trend'].values.copy()
#        data.close()
#        h = ax.contourf(data.lon, data.lat, val0,
#                        cmap = cmap, levels = levels, extend = 'both',
#                        transform = ccrs.PlateCarree())
#        if j == 0:
#            t = ax.text(-0.15, 0.5, expr_label2[k], rotation = 90,
#                        transform = ax.transAxes, verticalalignment='center')
#            t.set_multialignment('center')
#
#        # ---- other

        # Trend of all the products
        if expr == 'historical':
            a, _ = mrsol_availability(dcm, 'vanilla', 'historical', res)
            b, _ = mrsol_availability(dcm, 'vanilla', 'ssp585', res)
            cmip6_list = sorted(list(set(a) & set(b)))
        else:
            cmip6_list, _ = mrsol_availability(dcm, 'vanilla', expr, res)

        #if (expr != 'historical') | (dcm != '0-100cm'):
        val = np.full([len(cmip6_list), len(data.lat), len(data.lon)],
                      np.nan)
        for mind, model in enumerate(cmip6_list):
            data = xr.open_dataset(os.path.join(mg.path_out(),
                'standard_diagnostics_spi', expr, dist, model + '_' + \
                str(lag) + '_' + res + '_' + dcm + '_g_map_trend_' + \
                                                season + '.nc'))
            val[mind, :, :] = data['g_map_trend'].values.copy()
            data.close()

        # Calculate the weighted average
        temp_model = [x.split('_')[0] for x in cmip6_list]
        vote_model = np.full(len(cmip6_list), np.nan)
        for ind, model in enumerate(temp_model):
            n_model = np.sum([x == model for x in temp_model])
            vote_model[ind] = 1/n_model
        vote_model = vote_model / np.sum(vote_model)

        val_mean = np.sum(val * np.broadcast_to(vote_model.reshape(-1,1,1),
                                                val.shape), axis = 0)
        h = ax.contourf(data.lon, data.lat, val_mean, cmap = cmap,
                        levels = levels, extend = 'both',
                        transform = ccrs.PlateCarree())
        if j == 0:
            t = ax.text(-0.15, 0.5, expr_label2[k], rotation = 90,
                        transform = ax.transAxes, verticalalignment='center')
            t.set_multialignment('center')


#        ## ---- calculate 1.5 std of the product trends
#        #val = val.std(axis = 0)  * 1.5
#        #stipple(ax, data.lat, data.lon, mask = val0 > val,
#        #        transform = ccrs.PlateCarree(), hatch = '.')
#        val = ((val0 > 0) & (np.mean(val > 0, axis = 0) > pct)) | \
#            ((val0 < 0) & (np.mean(val < 0, axis = 0) > pct))
#        stipple(ax, data.lat, data.lon, mask = val,
#                transform = ccrs.PlateCarree(), hatch = '//////////')
        pct_pos = np.sum((val > 0.).astype(float) * \
                         np.broadcast_to(vote_model.reshape(-1,1,1),
                                         val.shape), axis = 0)
        pct_neg = np.sum((val < 0.).astype(float) * \
                         np.broadcast_to(vote_model.reshape(-1,1,1),
                                         val.shape), axis = 0)
        hatch1 = ((val_mean > 0) & (pct_pos > pct)) | \
            ((val_mean < 0) & (pct_neg > pct))
        stipple(ax, data.lat, data.lon, mask = hatch1,
                transform = ccrs.PlateCarree(), hatch = '///////////')
        hatch2 = (~hatch1) & \
            (((val_mean > 0) & (pct_pos > (pct - 0.1))) | \
             ((val_mean < 0) & (pct_neg > (pct - 0.1))))
        stipple(ax, data.lat, data.lon, mask = hatch2,
                transform = ccrs.PlateCarree(), hatch = '\\\\\\\\\\\\')

    cax = fig.add_axes([0.1, 0.08, 0.8, 0.01])
    plt.colorbar(h, cax = cax, orientation = 'horizontal',
                 label = 'Trend of ' + str(lag) + '-month SSI (year$^{-1}$)')
    fig.savefig(os.path.join(mg.path_out(), 'figures', 'fig1S_' + dist + \
                             '_trend_' + dcm + '_' + str(lag) + '_' + \
                             res + suffix + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
