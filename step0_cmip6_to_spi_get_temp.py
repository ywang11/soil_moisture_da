"""
20190910

ywang254@utk.edu

Convert the CMIP6 historical soil moisture to 3-, 6-, and 12-month SSI, 
 following Hao & AghaKouchak 2013 Multivariate Standardized Drought Index:
                                  A parametric multi-index model.
 For the soil moisture pdf, use KDE implemented in sklearn and 5-fold
 cross-validation to find the optimal bandwidth.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm, year_cmip6, target_lat, \
    target_lon
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
from misc.spi_utils import *
from glob import glob
import sys


#
land_mask = 'vanilla'
res = '0.5'

#
ref_expr = 'historical'
if ref_expr == 'piControl':
    ref_period = range(0, 201)
else:
    ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])

#
i = 0
dcm = depth_cm[i]
expr = 'piControl'


print(dcm)
print(expr)


# All the necessary models for HIST85, piControl, hist-aer, hist-nat, hist-GHG
# model_list = get_model_list(['historical'], dcm, land_mask, res)
# sort(model_list.keys())
mlist = ['BCC-CSM2-MR', 'BCC-ESM1', 'CESM2', 'CESM2-WACCM', 'CNRM-CM6-1',
         'CNRM-ESM2-1', 'CanESM5', 'EC-Earth3', 'EC-Earth3-Veg',
         'GISS-E2-1-G', 'GISS-E2-1-H', 'HadGEM3-GC31-LL', 'IPSL-CM6A-LR',
         'MIROC6', 'MPI-ESM1-2-HR', 'NorESM2-LM', 'SAM0-UNICON', 'UKESM1-0-LL']
m_ind = 9 # {0..17}
m = mlist[m_ind]


# The CMIP6 list of models for individual experiments.
if expr == 'historical':
    model_list = get_model_list(['historical', 'ssp585'], dcm, 
                                land_mask, res)
else:
    model_list = get_model_list([expr], dcm, land_mask, res)

if not m in model_list.keys():
    sys.exit()

# Use 50 years for historical, and 200 years for piControl, for later
# omission of 1st year.
if expr == 'historical':
    period = pd.date_range(str(year_cmip6['historical'][1]) + '-01-01', 
                           str(year_cmip6['ssp585'][-1]) + '-12-31',
                           freq = 'YS')
elif expr == 'piControl':
    period = pd.date_range('2001-01-01', '2200-12-31', freq = 'YS')
elif (expr == 'hist-aer') | (expr == 'hist-nat') | (expr == 'hist-GHG'):
    period = pd.date_range('1951-01-01', '2020-12-31', freq = 'YS')


####################################
# Function to apply SPI in parallel.
####################################
def apply_spi(month, rank, vector):
    vector = vector.reshape(-1)
    coord_cdf = np.load(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                                     'fit', m + '_' + dcm + '_' + \
                                     str(s) + '_' + str(month), 
                                     'coord_cdf_' + str(rank) + '.npy'))
    return (get_zscore(coord_cdf[:,0], coord_cdf[:,1], vector), rank)


#
for r in model_list[m]:
    start = time.time() 

    print(m + '_' + r)

    ###########################################################################
    # Handle CMIP6 data reading
    ###########################################################################
    if expr == 'historical':
        fpath = [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                              land_mask, 'CMIP6', expr, m + '_' + r,
                              'mrsol_' + res + '_' + str(y) + \
                              '_' + dcm + '.nc') \
                 for y in year_cmip6['historical']] + \
                [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                              land_mask, 'CMIP6', 'ssp585', m + '_' + r,
                              'mrsol_' + res + '_' + str(y) + \
                              '_' + dcm + '.nc') \
                 for y in year_cmip6['ssp585']]
    elif expr == 'piControl':
        # Use N+1 year in the calculation, so that the first year
        # can be discarded.
        N = 201
        fpath = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                         land_mask, 'CMIP6', expr, m + '_' + r,
                                         'mrsol_' + res + '_*_' + dcm + \
                                         '.nc')),
                       key = lambda x: int(x.split('_')[-2]))
        fpath = fpath[-N:]
    else:
        fpath = [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                              land_mask, 'CMIP6', expr, m + '_' + r,
                              'mrsol_' + res + '_' + str(y) + \
                              '_' + dcm + '.nc') \
                 for y in range(1950, 2021)]

    data = xr.open_mfdataset(fpath, decode_times = False)
    data_array = data.sm.values.copy()
    data_array = data_array.reshape(data_array.shape[0], -1)
    # ---- subset to within the land mask.
    retain = np.where(~np.all(np.isnan(data_array), axis = 0))[0]
    data_array = data_array[:, retain]
    data.close()

    # 
    for s in [6]:
        print('lag ' + str(s))

        # ---- convert to moving average.
        data_array_apply = rolling_mean(data_array, s)
        # ---- remove the first year.
        data_array_apply = data_array_apply[12:, :]
        # ---- deal with small negative values
        data_array_apply[(data_array_apply > -1) & \
                         (data_array_apply <= 0.)] = 1e-6

        #######################################################################
        # Handle SSI calculation
        #######################################################################
        for month in [3]:
            print(month)
    
            data_array_apply2 = data_array_apply[month::12, :]
    
            spi = np.full([data_array_apply2.shape[0],
                           len(data.lat) * len(data.lon)], np.nan)
    
            ###################################################################
            # Read the pdf and calculate spi.
            ###################################################################
            data_array_apply2 = np.array_split(data_array_apply2,
                                               data_array_apply2.shape[1],
                                               axis = 1)
            p = mp.Pool(min(mp.cpu_count()-3, 20))
            results = [p.apply_async(apply_spi,
                                     args = (month, rank,
                                             data_array_apply2[rank])) \
                       for rank in range(len(data_array_apply2))]
            p.close()
            p.join()

            results = [result.get() for result in results]
    
            for a in range(len(data_array_apply2)):
                spi[:, retain[results[a][1]]] = results[a][0]
    
            spi = spi.reshape(-1, len(target_lat[res]), len(target_lon[res]))
    
            #
            spi = xr.DataArray(spi, coords = {'time': period,
                                              'lat': target_lat[res],
                                              'lon': target_lon[res]},
                               dims = ['time', 'lat', 'lon'])
    
            # Save to file.
            spi.to_dataset(name = 'spi').to_netcdf( os.path.join( \
                mg.path_intrim_out(), 'SPI_CMIP6',
                'get_' + ref_expr + '_' + ref_period_str, m + '_' + \
                r + '_' + dcm + '_' + str(s) + '_' + res + '_' + \
                str(month) + '.nc') )

    end = time.time()
    print(('Spent %.6f minutes reading ' + m + '_' + r) % ((end - start)/60))
