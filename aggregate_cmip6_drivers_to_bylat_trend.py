"""
20200916

ywang254@utk.edu

Calculate the linear trend in the per month time series by latitude 
  band during the full period for each CMIP6 model.
"""
import pandas as pd
from utils_management.constants import depth_cm_new, lat_median, drivers
import utils_management as mg
from misc.cmip6_utils import one_layer_availability
from misc.standard_diagnostics import get_bylat_trend
import itertools as it
import os
import numpy as np
import multiprocessing as mp


res = '0.5'
land_mask = 'vanilla'


# Time period to calculate the trend on - MODIFY, which changes the 
# associated set of land surface models.
year = range(1971, 2017)
year_str = str(year[0]) + '-' + str(year[-1])
time_range = pd.date_range(start = str(year[0])+'-01-01',
                           end = str(year[-1])+'-12-31', freq = 'YS')

#
for vv, expr in it.product(drivers, ['hist-GHG', 'hist-aer', 'historical']):
    if expr == 'historical':
        a, _ = one_layer_availability(vv, land_mask, 'historical', res)
        b, _ = one_layer_availability(vv, land_mask, 'ssp585', res)
        cmip6_list = list(set(a) & set(b))
    else:
        cmip6_list, _ = one_layer_availability(vv, land_mask, expr, res)

    #
    trend_collection = pd.DataFrame(data = np.nan, index = lat_median,
        columns = pd.MultiIndex.from_product([cmip6_list, range(12)]))
    trend_p_collection = pd.DataFrame(data = np.nan, index = lat_median,
        columns = pd.MultiIndex.from_product([cmip6_list, range(12)]))
    for model, season in it.product(cmip6_list, range(12)):
        fname = os.path.join(mg.path_out(), 'cmip6_drivers_summary', vv,
                             expr, model + '_' + str(season) + '_' + res + \
                             '_g_lat_ts.csv')
        trend, trend_p = get_bylat_trend(fname, time_range)
        trend_collection.loc[:, (model, season)] = trend
        trend_p_collection.loc[:, (model, season)] = trend_p

    trend_collection.to_csv(os.path.join(mg.path_out(),
                                         'cmip6_drivers_summary', vv, expr, 
                                         'bylat_trend_' + res + '.csv'))
    trend_p_collection.to_csv(os.path.join(mg.path_out(),
                                           'cmip6_drivers_summary', vv, expr,
                                           'bylat_p_trend_' + res + '.csv'))
