"""
20210713

ywang254@utk.edu

Plot the precipitation for Sahara & West Asia.
"""
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import matplotlib as mpl
import regionmask as rmk
import utils_management as mg
from utils_management.constants import depth, depth_cm, val_list, val_depth, val2_list, \
    met_to_lsm
from scipy import signal
from validate_gen_psd_plot_fewer import *
from scipy.stats import linregress


def df_trend(df):
    trends = pd.DataFrame(np.nan, index = df.columns, columns = ['trend', 'trend_p'])
    for col in df.columns:
        res = linregress(df.index, df[col])
        trends.loc[col, 'trend'] = res.slope
        trends.loc[col, 'trend_p'] = res.pvalue
    return trends


scaler = 1. # multiple the soil moisture values
sid_list = ['14', '19']
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'
yrng_list = [range(1970, 2011), range(1981, 2017), range(1970, 2017)]
year_str_list = [str(yrng[0]) + '-' + str(yrng[-1]) for yrng in yrng_list]
cmap = mpl.cm.get_cmap('jet')
clist = dict([(met, cmap((ii+0.5)/len(met_to_lsm.keys()))) \
              for ii, met in enumerate(sorted(met_to_lsm.keys()))])


fig, axes = plt.subplots(4, 1, figsize = (6.5, 6.5), sharex = True, sharey = False)
for ind0, vv, vlong in zip(range(2), ['pr', 'tas'], ['precip', 'temperature']):
    # Read dataset
    prod_set = pd.read_csv(os.path.join(mg.path_out(),'validate', 
                                        'met_srex_' + vlong + '_all.csv'),
                           index_col = 0, parse_dates = True, header = [0,1]) * scaler
    source_set = pd.read_csv(os.path.join(mg.path_out(), 'validate',
                                          'source_srex_' + vlong + '_all.csv'),
                             index_col = 0, parse_dates = True, header = [0,1]) * scaler

    #
    for ind, sid in enumerate(sid_list):
        prod = prod_set.loc[:, (slice(None), str(sid))]
        prod.columns = prod.columns.droplevel([1])
        prod = prod.groupby(prod.index.year).mean()
        prod_trends = {}
        for yrng in yrng_list:
            year_str = str(yrng[0]) + '-' + str(yrng[-1])
            prod_trends[year_str] = df_trend(prod.loc[yrng, :].dropna(axis = 1, how = 'any'))

        source = source_set.loc[:, (slice(None), str(sid))]
        source.columns = source.columns.droplevel([1])
        source = source.groupby(source.index.year).mean()
        source_trends = df_trend(source)
        source_trends = source_trends.loc[[ii for ii in source_trends.index \
                                           if not ii in prod.columns], :]

        ax = axes[ind0*2 + ind]
    
        ax.set_title(rmk.defined_regions.srex.names[int(sid)-1])
        ax.text(0.02, 0.85, '(' + lab[ind0*2 + ind] + ') ', transform = ax.transAxes)
    
        count = 0
        labels = []
        for year_str in year_str_list:
            ax.bar(count + np.arange(prod_trends[year_str].shape[0]), 
                   prod_trends[year_str]['trend'],
                   color = [clist[met] for met in prod_trends[year_str].index])
            labels.extend([met.replace('_',' ') for met in prod_trends[year_str].index])
            ax.axvline(count + prod_trends[year_str].shape[0] - 0.5, color = 'k')
            if vv == 'tas':
                ax.text(count, 0.056, year_str)
                ax.set_ylim([-0.002, 0.067])
            else:
                if ind == 0:
                    ax.text(count, 0.002, year_str)
                    ax.set_ylim([-0.003, 0.003])
                else:
                    ax.text(count, 0.002, year_str)
                    ax.set_ylim([-0.004, 0.004])
    
            count += prod_trends[year_str].shape[0]
    
        ax.boxplot(source_trends['trend'], positions = [count], whis = [0, 100],
                   widths = 0.8)
        labels.append('CMIP5+6\n1970-2016')
        ax.set_xticks(range(count + 1))
        ax.set_xticklabels(labels, rotation = 90)
    
        ##ax.plot(prod.index, prod)
        ##ax.fill_between(source.index, np.min(source, axis = 1),
        ##                np.max(source, axis = 1), interpolate = True, facecolor = 'grey', 
        ##                linewidth = 0.5, edgecolor = 'grey', alpha = 0.5)
    
    fig.savefig(os.path.join(mg.path_out(), 'validate', 'srex_met.png'), 
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
