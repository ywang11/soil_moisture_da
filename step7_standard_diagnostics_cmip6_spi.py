"""
Compute the following for SPI product.

1. Global map (averaged over time): NetCDF
2. Global average time series.
3. Latitude bands average time series.
4. Continental average time series.
5. SREX region average time series.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, target_lat, target_lon
import itertools as it
from misc.cmip6_utils import mrsol_availability
from misc.standard_diagnostics import standard_diagnostics
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
import sys


res = '0.5'
land_mask = 'vanilla'


ref_expr = 'historical'
if ref_expr == 'piControl':
    ref_period = range(0, 201)
else:
    ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


expr = 'REPLACE1'
i = REPLACE2
mod = REPLACE3 # too many models. Need indexer.
lag = REPLACE4


dcm = depth_cm_new[i]


if expr == 'historical':
    #year = range(1951, 2101)
    year = range(1971, 2021)
    a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
    b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
    cmip6_list = list(set(a) & set(b))
else:
    year = range(1971, 2021)
    cmip6_list, _ = mrsol_availability(dcm, land_mask, expr, res)
cmip6_list = sorted(cmip6_list)


time = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                     freq = 'MS')


stepsize = 15 # Number of models to calculate within each call
cmip6_list_chunks = [cmip6_list[cc:(cc+stepsize)] for cc in \
                     range(0, len(cmip6_list), stepsize)]

if mod >= len(cmip6_list_chunks):
    sys.exit('Not so many chunks')


cmip6_list = cmip6_list_chunks[mod]


def calc(option):
    dist, model = option

    spi = np.full([len(year), 12, len(target_lat[res]),
                   len(target_lon[res])], np.nan)
    for m in range(12):
        fi = os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                          'get_' + ref_expr + '_' + ref_period_str,
                          dist, expr, model + '_' + dcm + '_' + \
                          str(lag) + '_' + res + '_' + str(m) + '.nc')
        data = xr.open_dataset(fi)
        tmp = data['spi'].values[(data['time'].to_index().year >= \
                                  year[0]) & \
                                 (data['time'].to_index().year <= \
                                  year[-1]), :, :].copy()
        spi[:, m, :, :] = tmp
        data.close()
    spi = spi.reshape(len(year) * 12, len(target_lat[res]),
                      len(target_lon[res]))

    standard_diagnostics(spi, time, target_lat[res], target_lon[res], 
                         os.path.join(mg.path_out(),
                                      'standard_diagnostics_spi', expr, dist),
                         model + '_' + str(lag) + '_' + res + '_' + dcm)

    pos = np.where([x == model for x in cmip6_list])[0][0]
    print('Finished ' + model + ' which is ' + str(pos) + '-th in the chunck.')


##for dist, model in it.product(['gmm', 'weibull'], cmip6_list):
##    calc([dist, model])
p = mp.Pool(2)
p.map_async(calc, list(it.product(['gmm', 'weibull'], cmip6_list)))
p.close()
p.join()

##for dist in ['gmm', 'weibull']:
##    calc([dist, 'BCC-CSM2-MR_r1i1p1f1'])
