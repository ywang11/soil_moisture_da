"""
2019/06/12
ywang254@utk.edu

Plot the global maps of interpolated drivers.
"""
import xarray as xr
import sys
import os
import pandas as pd
import matplotlib.pyplot as plt
from utils_management.paths import path_intrim_out
from utils_management.constants import drivers
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import cartopy.crs as ccrs
from glob import glob
import multiprocessing as mp
from matplotlib import gridspec


land_mask = 'vanilla' # 'vanilla'
res = '0.5'
expr = 'ssp585' # 'historical', 'ssp585'


a, _ = mrsol_availability('0-10cm', land_mask, 'historical', res)
b, _ = mrsol_availability('0-10cm', land_mask, 'ssp585', res)
cmip6 = sorted(set(a) & set(b))
cmip6 = sorted(cmip6 + ['CESM2_r4i1p1f1', 'CESM2_r10i1p1f1',
                        'CESM2_r11i1p1f1'])


def decode_month_since(time):
    start = time.attrs['units'].split(' ')[2]
    return pd.date_range(start, periods = len(time), freq = '1M')


##for model in cmip6:
def plotter(model):    
    fig = plt.figure(figsize = (20,18))
    gs = gridspec.GridSpec(1, 2, figure = fig)
    sub_gs1 = gridspec.GridSpecFromSubplotSpec(4, 1, gs[0])
    sub_gs2 = gridspec.GridSpecFromSubplotSpec(4, 1, gs[1])

    for vind, vv in enumerate(drivers):
        filename = sorted(glob(os.path.join(path_intrim_out(), 'Interp_DA',
                                            land_mask, 'CMIP6', expr, model,
                                            vv + '_' + res + '_*.nc')))
        if len(filename) == 0:
            ax = fig.add_subplot(sub_gs1[vind])
            ax.set_xticklabels([])
            ax.set_yticklabels([])
            ax.set_title(vv + ' Time Series ()')
            ax = fig.add_subplot(sub_gs2[vind])
            ax.set_xticklabels([])
            ax.set_yticklabels([])
            ax.set_title(vv + ' Map ()')
            continue

        hr = xr.open_mfdataset(filename, decode_times = False,
                               concat_dim = 'time')
        #print(hr['time'])
        if 'month' in hr['time'].attrs['units']:
            tvec = decode_month_since(hr['time'])
        else:
            tvec = pd.to_datetime(xr.decode_cf(hr)['time' \
            ].to_index().strftime('%Y-%m-%d'))

        ts = pd.DataFrame(hr[vv].mean(dim = ['lat', 'lon']).values,
                          index = tvec)
        clim = hr[vv].mean(dim='time').copy(deep=True).load()
        unit = hr[vv].attrs['units']
        hr.close()

        ax = fig.add_subplot(sub_gs1[vind])
        ax.plot(ts.index, ts, '-')
        ax.set_title(vv + ' Time Series ' + unit)

        ax = fig.add_subplot(sub_gs2[vind], projection = ccrs.PlateCarree())
        ax.coastlines()
        ax.gridlines()
        h = ax.contourf(clim.lon.values, clim.lat.values, clim.values)
        plt.colorbar(h, ax = ax, shrink = 0.7)
        ax.set_title(vv + ' Map ' + unit)
    fig.savefig(os.path.join(path_intrim_out(), 'Interp_DA_plot', 
                             land_mask, expr, 
                             'global_drivers_map_' + model + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)


p = mp.Pool(4)
p.map_async(plotter, cmip6)
p.close()
p.join()

