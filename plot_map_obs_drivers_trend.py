"""
20200425
ywang254@utk.edu

Plot the trend in seasonal drivers of obs.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from matplotlib.gridspec import GridSpec,GridSpecFromSubplotSpec
from utils_management.constants import depth_cm_new, lat_median, \
    target_lat, target_lon
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from misc.plot_utils import plot_ts_shade, cmap_gen, stipple, cmap_div
import matplotlib as mpl
import utils_management as mg
import itertools as it
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


def read_trend(varname, lsm, season, year_str):
    hr = xr.open_dataset(os.path.join(mg.path_out(), 'obs_drivers_summary',
                                      lsm + '_' + varname + '_' + res + \
                                      '_g_map_trend_' + season + \
                                      '_' + year_str + '.nc'))
    var = hr['g_map_trend'].values.copy()
    hr.close()
    return var


def get_hatch(val_per_model, val_mean):
    pct_pos = np.sum((val_per_model > 0.).astype(float), axis = 0)
    pct_neg = np.sum((val_per_model < 0.).astype(float), axis = 0)
    hatch1 = ((val_mean > 0) & (pct_pos > pct)) | \
        ((val_mean < 0) & (pct_neg > pct))
    hatch2 = (~hatch1) & \
        (((val_mean > 0) & (pct_pos > (pct - 0.1))) | \
         ((val_mean < 0) & (pct_neg > (pct - 0.1))))
    return hatch1, hatch2


#
mpl.rcParams['font.size'] = 5.5
mpl.rcParams['axes.titlesize'] = 'medium'
mpl.rcParams['hatch.linewidth'] = 0.4
hs = '\\\\\\\\'
cmap = cmap_div()
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'

pct = 0.9 # consistent with "step8_plot_trend_spi_bylat.py"
res = '0.5'
land_mask = 'vanilla'
lat = target_lat[res]
lon = target_lon[res]
season_list = ['Annual', 'DJF', 'MAM', 'JJA', 'SON']
lsm_all = ['CERA20C', 'CLASS-CTEM-N', 'CLM4', 'CLM4VIC',
           'CLM5.0', 'ERA20C', 'ERA-Interim', 'ERA5', 'ERA-Land', 'ESA-CCI',
           'GLDAS_Noah2.0', 'GLEAMv3.3a', 'ISAM', 'JSBACH', 'JULES',
           'LPX-Bern', 'ORCHIDEE', 'ORCHIDEE-CNP', 'SiBCASA']
met_to_lsm = {'CRU_v4.03': ['ESA-CCI', 'GLEAMv3.3a'],
              'CRU_v3.20': ['CLASS-CTEM-N', 'CLM4', 'CLM4VIC', 'SiBCASA'],
              'CRU_v3.26': ['CLM5.0', 'ISAM', 'JSBACH', 'JULES', 'LPX-Bern',
                            'ORCHIDEE', 'ORCHIDEE-CNP'],
              'GLDAS_Noah2.0': ['GLDAS_Noah2.0'],
              'ERA-Interim': ['ERA-Interim', 'ERA-Land'],
              'ERA5': ['ERA5'],
              'ERA20C': ['ERA20C'],
              'CERA20C': ['CERA20C']}
lsm_to_met = {}
for a,b in met_to_lsm.items():
    for bb in b:
        if not bb in list(lsm_to_met.keys()):
            lsm_to_met[bb] = a
        else:
            raise Exception('Too many met.')
var_list = ['pr', 'evspsbl', 'pr-evspsbl', 'pr-evspsbl-mrro', 'snw']
var_list_name = ['P', 'ET', 'P-ET', 'P-ET-Q', 'SNW']
var_list_units = ['mm day$^{-1}$ yr$^{-1}$', 'mm day$^{-1}$ yr$^{-1}$',
                  'mm day$^{-1}$ yr$^{-1}$', 'mm day$^{-1}$ yr$^{-1}$',
                  'mm yr$^{-1}$']
use_lsm = False # use source models that merged the dataset


# 1981-2010 & 1981-2015: observed data (CRU, GLEAM, GRUN, GlobSnow)
for year in [range(1971, 2015)]: # [range(1971,2017)]: # range(1981,2011), range(1981, 2015)
    year_str = str(year[0]) + '-' + str(year[-1])

    fig, axes = plt.subplots(len(season_list), len(var_list),
                             figsize = (10.5, len(season_list)),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    fig.subplots_adjust(wspace = 0.02)
    for vind, sind in it.product(range(len(var_list)),
                                 range(len(season_list))):
        vv = var_list[vind]
        season = season_list[sind]

        if use_lsm:
            if (vv != 'pr-evspsbl-mrro') & (vv != 'snw'):
                # all the models
                val = np.full([len(lsm_all), len(lat), len(lon)], np.nan)
                for mind, lsm in enumerate(lsm_all):
                    if ('evspsbl' in vv) & (lsm == 'ESA-CCI'):
                        continue
                    if vv == 'pr':
                        val[mind, :, :] = read_trend(vv, lsm_to_met[lsm],
                                                     season, year_str)
                    else:
                        val[mind, :, :] = read_trend(vv, lsm, season, year_str)
                # ---- calculate the simple average
                val_mean = np.nanmean(val, axis = 0)
                val_std = np.nanstd(val, axis = 0)
            elif vv == 'pr-evspsbl-mrro':
                val_mean = read_trend(vv, 'CRU_v4.03', season, year_str)
            else:
                val_mean = read_trend(vv, 'GlobSnow', season, year_str)
        else:
            if vv == 'pr':
                val_mean = read_trend(vv, 'CRU_v4.03', season, year_str)
            elif vv == 'evspsbl':
                if (year_str == '1971-2016') | (year_str == '1971-2014'):
                    val_mean = read_trend(vv, 'GLDAS_Noah2.0', season,
                                          '1971-2014')
                else:
                    val_mean = read_trend(vv, 'GLEAMv3.3a', season,
                                          year_str)
                ## fix an errorneous location in Miller()
                #for nanlat, nanlon in [(-30.75,153.25), (55.25,-59.75),
                #                       (16.25,39.25), (64.75,-173.75)]:
                #    row = np.argmin(np.abs(lat - nanlat))
                #    col = np.argmin(np.abs(lon - nanlon))
                #    val_mean[row, col] = np.nan
            elif vv == 'pr-evspsbl':
                if year_str == '1971-2016':
                    val_mean = read_trend(vv, 'GRACE+GRUN', season,
                                          '1971-2014')
                elif year_str == '1981-2010':
                    val_mean = read_trend(vv, 'GLEAMv3.3a', season,
                                          year_str)
                else:
                    val_mean = read_trend(vv, 'CRU_v4.03', season, year_str)
            elif vv == 'pr-evspsbl-mrro':
                if year_str == '1971-2016':
                    val_mean = read_trend(vv, 'GRACE', season, '1971-2014')
                else:
                    val_mean = read_trend(vv, 'CRU_v4.03', season, year_str)
            elif vv == 'snw':
                if year_str == '1981-2010':
                    val_mean = read_trend(vv, 'GlobSnow', season, year_str)
                else:
                    val_mean = read_trend(vv, 'GlobSnow', season, '1980-2016')

        #
        if vv != 'snw':
            levels = np.linspace(-1.5e-2, 1.5e-2, 31)
        else:
            levels = np.linspace(-0.5, 0.5, 31)

        ax = axes[sind, vind]
        ax.coastlines(lw = 0.1)
        gl = ax.gridlines(xlocs = [-180, 180],
                          ylocs = np.arange(-40, 61, 20),
                          linestyle = '--',
                          linewidth = 0.5, draw_labels = False)
        #gl.xlabels_top = False
        #gl.xlabels_bottom = False
        #gl.ylabels_right = False
        #if vind == 0:
        #    gl.ylabels_left = True
        #else:
        #    gl.ylabels_left = False
        #gl.xlocator = mticker.FixedLocator([-180, 180])
        #gl.ylocator = mticker.FixedLocator([-40, -20, 0, 20, 40, 60])
        #gl.xformatter = LONGITUDE_FORMATTER
        #gl.yformatter = LATITUDE_FORMATTER
        #gl.xlabel_style = {'color': 'black', 'weight': 'normal'}
        ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.2)
        ax.spines['bottom'].set_lw(0.2)
        ax.spines['left'].set_lw(0.2)
        ax.spines['right'].set_lw(0.2)
    
        h = ax.contourf(lon, lat, val_mean, cmap = cmap,
                        levels = levels, extend = 'both',
                        transform = ccrs.PlateCarree())

        if use_lsm:
            if (vv != 'pr-evspsbl-mrro') & (vv != 'snw'):
                # add consistency hatch
                hatch1, hatch2 = get_hatch(val, val_mean)
                stipple(ax, lat, lon, mask = hatch1,
                        transform = ccrs.PlateCarree(), hatch = '///////////')
                stipple(ax, lat, lon, mask = hatch2,
                        transform = ccrs.PlateCarree(), hatch = '\\\\\\\\\\\\')

        if sind == 0:
            # label the variable name
            ax.set_title(var_list_name[vind] + '\n' + \
                         var_list_units[vind])
        if vind == 0:
            ax.text(-0.15, 0.5, season, verticalalignment = 'center',
                    rotation = 90, transform = ax.transAxes)
        ax.text(0.01, 1.15, '(' + lab[sind*len(var_list) + vind] + ')',
                transform = ax.transAxes)
    
        if sind == (len(season_list)-1):
            if vv == 'pr-evspsbl-mrro':
                cax = fig.add_axes([0.1, 0.08, 0.8, 0.01])
                cb = plt.colorbar(h, cax = cax, orientation = 'horizontal')
                cb.set_label('Non-SNW Trends', labelpad = 0.1)
            elif vv == 'snw':
                cax2 = fig.add_axes([0.1, 0., 0.8, 0.01])
                cb2 = plt.colorbar(h, cax = cax2, orientation = 'horizontal')
                cb2.set_label('SNW Trends', labelpad = 0.1)
    fig.savefig(os.path.join(mg.path_out(), 'figures',
                             'plot_map_obs_drivers_trend_' + \
                             year_str + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
