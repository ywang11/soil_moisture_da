"""
20190910
ywang254@utk.edu

Plot the PC of the fingerprint with uncertainty intervals.

Solid with shade - jackknife estimation mean & 95% CI
Dashed - full estimation for comparison
Title - average percentage explained variance and std of jackknife estimation
        (the full estimation already shown in main text)

Use flip of the EOF in the signal projection step.
"""
import matplotlib.pyplot as plt
import pandas as pd
import xarray as xr
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median
from misc.da_utils import calc_signal_bylat
from misc.plot_utils import plot_ts_shade
import numpy as np
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil
import matplotlib.gridspec as gridspec
from scipy.stats import linregress


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titleweight'] = 'normal'
mpl.rcParams['axes.titlesize'] = 5


res = '0.5'
season_list_list = {'bymonth': [str(i) for i in range(12)],
                    'byyear': ['annual_mean', 'annual_maximum',
                               'annual_minimum',
                               'growing_season_mean', 'growing_season_maximum',
                               'growing_season_minimum']}


folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
season_name = 'bymonth'
lag = 3
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


if suffix == '_noSahel':
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    ).loc[np.array(lat_median).astype(int), :].values[:, 1])
else:
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    ).loc[np.array(lat_median).astype(int), :].values[:, 0])


season_list = season_list_list[season_name]


# 'hist-nat', 'piControl', 'NoNAT', 'GHGAER', 'hist-nat'
expr_list = ['historical', 'historical-2', 'hist-GHG', 'hist-aer']
# 'NAT', 'piControl', 'ANT', 'GHGAER', 'NAT'
expr_list2 = ['ALL', 'ALL-2', 'GHG', 'AER']


month_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']


def plot_rep(ax, limit, dist, dcm, eind, sind):
    if limit:
        prefix = 'limit_'
    else:
        prefix = ''

    flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                    prefix + 'flip_bylat' + suffix + '.csv'),
                       index_col = [0,1,2,3])

    expr = expr_list[eind]
    season = season_list[sind]

    if limit & (expr == 'piControl'):
        expr = expr + '_historical' # Use this one

    pc = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                  expr, 'bylat', prefix + \
                                  dist + '_pc_' + dcm + '_' + \
                                  str(lag) + '_' + season + \
                                  '_' + res + suffix + '.csv'),
                     index_col = 0, parse_dates = True)
    if expr != 'piControl':
        pc.index = pc.index.year
    if flip.loc[(dcm, sind, dist, lag), expr] < 0:
        pc.loc[:, ['Full', 'Jackknife Estimate','Jackknife CI_lower',
                   'Jackknife CI_upper']] = \
        - pc.loc[:, ['Full', 'Jackknife Estimate','Jackknife CI_lower',
                     'Jackknife CI_upper']]
        temp = pc.loc[:, 'Jackknife CI_lower'].values.copy()
        pc.loc[:, 'Jackknife CI_lower'] = \
            pc.loc[:, 'Jackknife CI_upper'].values
        pc.loc[:, 'Jackknife CI_upper'] = temp

    h1, h12 = plot_ts_shade(ax, pc.index,
                            {'min': pc['Jackknife CI_lower'].values,
                             'mean': pc['Jackknife Estimate'].values,
                             'max': pc['Jackknife CI_upper'].values},
                            ts_col = 'r', shade_col = 'r')
    h2, = ax.plot(pc.index, pc['Full'].values, '--', color = 'k', lw = 0.5)

    if (expr == 'hist-nat') | (expr == 'historical-2'):
        ax.axvline(1963, color = '#006d2c', lw = 0.5)
        ax.axvline(1982, color = '#006d2c', lw = 0.5)
        ax.axvline(1991, color = '#006d2c', lw = 0.5)

    reg = linregress(np.arange(len(pc['Jackknife Estimate'].values)),
                     pc['Jackknife Estimate'].values)
    # '{:.2e}'.format(reg.slope).replace('e-0', 'x10$^{-') + '}$'
    slope = '%.3f' % reg.slope
    if reg.pvalue <= 0.05:
        fd = {'fontweight': 'bold'}
        color = '#08519c'
    else:
        fd = {'fontweight': 'bold'}
        color = '#6baed6'
    ax.text(0.25, 0.8, 'r = ' + slope, color = color,
            fontdict = fd, transform = ax.transAxes)

    if (eind == 0):
        ax.set_xticks([1991, 2030, 2070])
    elif ('piControl' in expr):
        ax.set_xticks([1000, 2000, 3000])
    else:
        ax.set_xticks([1975, 1995, 2010]) #  1982,
    if 'piControl' in expr:
        ax.set_xlim([pc.index.values[0], pc.index.values[-1]])
    else:
        ax.set_xlim([1971, pc.index.values[-1]])
    if sind != (len(season_list)-1):
        ax.tick_params('x', length = 0)
        ax.set_xticklabels([])
    if sind == 0:
        ax.set_title(expr_list2[eind] + ' ' + dcm)

    return h1, h12, h2


for limit, dist in it.product([False, True], ['gmm','weibull']):
    if limit:
        prefix = 'limit_'
    else:
        prefix = ''

    fig = plt.figure(figsize = (7.5, len(season_list) / 1.3))
    gs1 = fig.add_gridspec(nrows = 1, ncols = 2, width_ratios = [1, 3],
                           left = 0., right = 1., hspace = 0., wspace = 0.08)
    gs2 = gs1[0].subgridspec(nrows = len(season_list),
                             ncols = 2, hspace = 0., wspace = 0.)
    gs3 = gs1[1].subgridspec(nrows = len(season_list),
                             ncols = 2 * (len(expr_list)-1),
                             hspace = 0., wspace = 0.)

    ## Except piControl
    for eind, sind, dind in it.product(range(len(expr_list)),
                                       range(len(season_list)),
                                       range(len(depth_cm_new))):
        if eind == 0:
            ax = fig.add_subplot(gs2[sind, eind + dind])
        else:
            ax = fig.add_subplot(gs3[sind, eind-1 + dind*(len(expr_list)-1)])
        dcm = depth_cm_new[dind]
        h1, h12, h2 = plot_rep(ax, limit, dist, dcm, eind, sind)

        if (eind == 0) & (dind == 0):
            ax.set_ylabel(month_name[sind])
        elif not ((eind == 1) & (dind == 0)):
            plt.setp(ax.get_yticklabels(), visible = False)
        ax.set_yticks([-1.5, 0, 1.5, 3])

        if eind == 0:
            ax.set_ylim([-1.8, 4.2])
        else:
            ax.set_ylim([-1.8, 2.2])

        if eind == 0:
            ax.text(0.02, 0.86, lab[eind + dind] + str(sind+1),
                    transform = ax.transAxes, weight = 'bold')
        else:
            ax.text(0.02, 0.86, lab[eind + 1 + dind * (len(expr_list)-1)] + str(sind+1),
                    transform = ax.transAxes, weight = 'bold')

    ax.legend([h1, h12, h2], # , h3
              ['Jackknife Mean', 'Jackknife 95% CI', 'Main'], # 'Mean Products'
              loc = [-4, -0.8], ncol = 4)
    fig.savefig(os.path.join(mg.path_out(), 'figures', prefix + \
                             'fig2S_' + dist + '_pc_' + str(lag) + '_' + \
                             season_name + '_' + res + suffix + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
