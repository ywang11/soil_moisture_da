;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Interpolate the soil moisture products to 0.5, 2.5, 5, 10 degrees.
; The following products:
; (1) ERA-Land soil moisture interpolated to 0.5 degrees.
; (2) GLEAM v3.3a soil moisture interpolated to 0.5 degrees.
; (3) GLDAS Noah2.0 soil moisture interpolated to 0.5 degrees.
; (3) The individial dolce and emergent constraint products.
; (4) Median of the 6 different products.
; (5) Mean of all the source raw products.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "compute_horizontal_regrid_DA.ncl"


begin
  ; MODIFY ; 
  d_list = (/ "0.00-0.10", "0.10-0.30", "0.30-0.50", "0.50-1.00" /)
  dcm_list = (/ "0-10cm", "10-30cm", "30-50cm", "50-100cm" /)
  res_list = (/ "0.5" /) ; , "2.5", "5", "10"

  do d_ind = 0, dimsizes(d_list) - 1
    d = d_list(d_ind)
    dcm = dcm_list(d_ind)

    if d .eq. "0.00-0.10" then
      ;src = (/ "ERA-Land", "GLEAMv3.3a", \
      ;         "mean_lsm", "mean_cmip5", "mean_cmip6", "mean_2cmip", \
      ;         "mean_all", \
      ;         "dolce_lsm", "dolce_cmip5", "dolce_cmip6", "dolce_2cmip", \
      ;         "dolce_all", \
      ;         "em_lsm", "em_cmip5", "em_cmip6", "em_2cmip", "em_all" /)
      src = (/ "mean_lsm" /)
    else if d .eq. "0.10-0.30" then
      ;src = (/ "ERA-Land", \
      ;         "mean_lsm", "mean_cmip5", "mean_cmip6", "mean_2cmip", \
      ;         "mean_all", \
      ;         "dolce_lsm", "dolce_cmip5", "dolce_cmip6", "dolce_2cmip", \
      ;         "dolce_all", \
      ;         "em_lsm", "em_cmip5", "em_cmip6", "em_2cmip", "em_all" /)
      src = (/ "mean_lsm" /)
    else
      ;src = (/ "mean_lsm", "mean_cmip5", "mean_cmip6", "mean_2cmip", \
      ;         "mean_all", \
      ;         "dolce_lsm", "dolce_cmip5", "dolce_cmip6", "dolce_2cmip", \
      ;         "dolce_all", \
      ;         "em_lsm", "em_cmip5", "em_cmip6", "em_2cmip", "em_all" /)
      src = (/ "mean_lsm" /)
    end if
    end if

    do res_ind = 0, dimsizes(res_list) - 1
      res = res_list(res_ind)
    
      do i = 0, dimsizes(src) - 1
        print(src(i) + " " + d)

        if src(i) .eq. "ERA-Land" then
          sfile = addfiles(systemfunc("ls $PROJDIR/Soil_Moisture/" + \
            "intermediate/Interp_Merge/vanilla/ERA-Land/ERA-Land_*_" + \
            dcm + ".nc"), "r")

          ; read soil moisture data
          sm = sfile[:]->sm

        else if src(i) .eq. "GLEAMv3.3a" then
          sfile = addfiles(systemfunc("ls $PROJDIR/Soil_Moisture/" + \
            "intermediate/Interp_Merge/vanilla/GLEAMv3.3a/GLEAMv3.3a_*_" + \
            dcm + ".nc"), "r")
          ; read soil moisture data
          sm = sfile[:]->sm
    
        else if src(i) .eq. "GLDAS_Noah2.0" then
          sfile = addfiles(systemfunc("ls $PROJDIR/Soil_Moisture/" + \
            "intermediate/Interp_Merge/vanilla/GLDAS_Noah2.0/" + \
            "GLDAS_Noah2.0_*_" + dcm + ".nc"), "r")
          ; read soil moisture data
          sm = sfile[:]->sm

        else if (src(i) .eq. "mean_lsm") .or. \
          (src(i) .eq. "mean_cmip5") .or. (src(i) .eq. "mean_cmip6") .or. \
          (src(i) .eq. "mean_2cmip") .or. (src(i) .eq. "mean_all") then
          temp = str_split(src(i), "_")
          sfile = addfile("$PROJDIR/Soil_Moisture/output_product/" + \
                          "meanmedian_" + temp(1) + "/mean_" + d + \
                          "_1950-2016.nc", "r")
          sm = sfile->sm
          delete(temp)
        else if (src(i) .eq. "dolce_lsm") .or. \
                (src(i) .eq. "dolce_all") then
          sfile = addfile("$PROJDIR/Soil_Moisture/" + \
            "output_product/concat_" + src(i) + "/positive_average_" + d + \
            "_lu_weighted_ShrunkCovariance.nc", "r")

          ; read soil moisture data
          sm = sfile->sm
    
        else if (src(i) .eq. "dolce_cmip5") .or. \
                (src(i) .eq. "dolce_cmip6") .or. \
                (src(i) .eq. "dolce_2cmip") then
          sfile = addfile("$PROJDIR/Soil_Moisture/" + \
                          "output_product/" + src(i) + "_product/" + \
                          "weighted_average_1950-2016_" + \
                          d + "_lu_weighted_ShrunkCovariance.nc", "r")
          ; read soil moisture data
          sm = sfile->sm
    
        else if (src(i) .eq. "em_lsm") .or. \
                (src(i) .eq. "em_all") then
          sfile = addfile("$PROJDIR/Soil_Moisture/" + \
                          "output_product/concat_" + src(i) + "/" + \
                          "positive_CRU_v4.03_year_month_anomaly_9grid_" + \
                          dcm + "_predicted_1950-2016.nc", "r")
          ; read soil moisture data
          sm = sfile->predicted

        else if (src(i) .eq. "em_cmip5") .or. \
                (src(i) .eq. "em_cmip6") .or. \
                (src(i) .eq. "em_2cmip") then
          sfile = addfile("$PROJDIR/Soil_Moisture/" + \
            "output_product/" + src(i) + "_corr/" + \
            "CRU_v4.03_year_month_positive_9grid_" + dcm + "_1950-2016/" + \
            "predicted.nc", "r")
          ; read soil moisture data
          sm = sfile->predicted

        end if
        end if
        end if
        end if
        end if
        end if
        end if
        end if
  
        if .not. (res .eq. "0.5") then
          sm_regrid = compute_horizontal_regrid_DA(sm, \
              src(i) + "_" + d + "_" + res, res)
        else
          sm_regrid = sm
        end if
        delete(sm)
    
        ; write to file by year
        fout = "$PROJDIR/Soil_Moisture/intermediate/" + \
               "interp_merged_products/" + src(i) + "_" + d + "_" + \
               res + ".nc"
        system("rm -f " + fout)
        ncdf = addfile(fout, "c")
        filedimdef(ncdf, "time", -1, True)
    
        ;;printVarSummary(sm_regrid&time)
    
        if typeof(sm_regrid&time) .eq. "int64" then
          temp = tofloat(sm_regrid&time)
          copy_VarMeta(sm_regrid&time, temp)
          sm_regrid&time := temp
          delete(temp)
        end if
    
        ;;printVarSummary(sm_regrid&time)
    
        ncdf->sm = sm_regrid
        
        delete(fout)
        delete(ncdf)
        delete(sm_regrid)
        delete(sfile)
      end do
    
      delete(i)
      delete(res)
    end do
    delete(src)
    delete(dcm)
    delete(d)
  end do

end