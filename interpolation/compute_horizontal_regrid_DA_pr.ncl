load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"

function compute_horizontal_regrid_DA_pr(data, data_id, res)

begin
  printVarSummary(data)

  Opt = True

  Opt@SrcFileName = "$SCRATCHDIR/src_SCRIP_" + data_id + ".nc"
  Opt@DstFileName = "$SCRATCHDIR/dst_SCRIP_" + data_id + ".nc"
  Opt@WgtFileName = "$SCRATCHDIR/wgt_SCRIP_" + data_id + ".nc"
  Opt@ForceOverwrite = True
  Opt@InterpMethod = "conserve"

  if (min(data&lat) .ge. -85.) then
    Opt@SrcRegional = True
  else
    Opt@SrcRegional = False
  end if

  Opt@DstRegional = False

  print(typeof(data))
  print(dimsizes(dimsizes(data)))

;  if (dimsizes(dimsizes(data)) .eq. 3) then
;    Opt@SrcGridMask = where(.not.ismissing(dim_avg_n(data,0)), 1, 0)
;  else
;    Opt@SrcGridMask = where(.not.ismissing(data), 1, 0)
;  end if
  if res .eq. "0.5" then
    Opt@DstGridLat = fspan(-89.75, 89.75, 360)
    Opt@DstGridLon = fspan(-179.75, 179.75, 720)
  else if res .eq. "2.5" then
    Opt@DstGridLat = fspan(-88.75, 88.75, 72)
    Opt@DstGridLon = fspan(-178.75, 178.75, 144)
  else if res .eq. "5" then
    Opt@DstGridLat = fspan(-87.5, 87.5, 36)
    Opt@DstGridLon = fspan(-177.5, 177.5, 72)
  else if res .eq. "10" then
    Opt@DstGridLat = fspan(-85.00, 85., 18)
    Opt@DstGridLon = fspan(-175, 175, 36)
  end if
  end if
  end if
  end if

  data_regrid = ESMF_regrid(data, Opt)

  printVarSummary(data_regrid)

  system("rm " + Opt@SrcFileName)
  system("rm " + Opt@DstFileName)
  system("rm " + Opt@WgtFileName)
  system("rm PET0.RegridWeightGen.Log")

  return data_regrid
end