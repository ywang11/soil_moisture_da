; Interpolate soil moisture to 10x10 degrees.
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "compute_soil_wts_bnds.ncl"
load "compute_horizontal_regrid_DA.ncl"

begin
  model_member_list = (/ (/"ACCESS1-0", "r1i1p1"/), \
                         (/"ACCESS1-3", "r1i1p1"/), \
                         (/"CanESM2", "r1i1p1"/), \
                         (/"CanESM2", "r2i1p1"/), \
                         (/"CanESM2", "r3i1p1"/), \
                         (/"CanESM2", "r4i1p1"/), \
                         (/"CanESM2", "r5i1p1"/), \
                         (/"CNRM-CM5", "r1i1p1"/), \
                         (/"CNRM-CM5", "r2i1p1"/), \
                         (/"CNRM-CM5", "r4i1p1"/), \
                         (/"CNRM-CM5", "r6i1p1"/), \
                         (/"CNRM-CM5", "r10i1p1"/), \
                         (/"FGOALS-g2", "r1i1p1"/), \ 
                         (/"FGOALS-g2", "r2i1p1"/), \
                         (/"FGOALS-g2", "r3i1p1"/), \
                         (/"GFDL-CM3", "r1i1p1"/), \
                         (/"GFDL-ESM2G", "r1i1p1"/), \
                         (/"GFDL-ESM2M", "r1i1p1"/), \
                         (/"GISS-E2-H-CC", "r1i1p1"/), \
                         (/"GISS-E2-H", "r1i1p1"/), \
                         (/"GISS-E2-R-CC", "r1i1p1"/), \
                         (/"GISS-E2-R", "r1i1p1"/), \
                         (/"GISS-E2-R", "r1i1p2"/), \
                         (/"GISS-E2-R", "r1i1p3"/), \
                         (/"GISS-E2-R", "r2i1p1"/), \
                         (/"GISS-E2-R", "r2i1p3"/), \
                         (/"HadGEM2-CC", "r1i1p1"/), \ 
                         (/"HadGEM2-CC", "r2i1p1"/), \
                         (/"HadGEM2-CC", "r3i1p1"/), \
                         (/"HadGEM2-ES", "r1i1p1"/), \
                         (/"HadGEM2-ES", "r2i1p1"/), \
                         (/"HadGEM2-ES", "r3i1p1"/), \
                         (/"HadGEM2-ES", "r4i1p1"/), \
                         (/"inmcm4", "r1i1p1"/), \
                         (/"MIROC5", "r1i1p1"/), \
                         (/"MIROC5", "r2i1p1"/), \
                         (/"MIROC5", "r3i1p1"/), \
                         (/"MIROC5", "r4i1p1"/), \
                         (/"MIROC5", "r5i1p1"/), \
                         (/"MIROC-ESM-CHEM", "r1i1p1"/), \
                         (/"MIROC-ESM", "r1i1p1"/), \
                         (/"NorESM1-ME", "r1i1p1"/) /)

  index = REPLACE
  model = model_member_list(index, 0)
  member = model_member_list(index, 1)

  ;----------------------------------------------
  ; read soil moisture data
  ;----------------------------------------------
  ; focus on r1i1p1
  sfile = addfiles(systemfunc("ls $SCRATCHDIR/Soil_Moisture/" + \
                              "data/CMIP5/rcp85/mrlsl/" + \
                              "mrlsl_Lmon_" + model + "_rcp85_" + \
                              member + "_*.nc"),"r")
  ; soil layer bounds (unit: m)
  depth_bnds0 = sfile[0]->depth_bnds
  n_depth = dimsizes(depth_bnds0)
  depth_bnds = new( n_depth, float )
  ; ---- round to six decimal digits to handle situations like 
  ;      the first layer is until 0.100000001490116 meter (e.g. 
  ;      GISS-E2-R)
  opt = 1 ; return float
  do d=0, n_depth(0)-1
    depth_bnds(d,0) = round(depth_bnds0(d,0) * 1000000, opt) / 1000000
    depth_bnds(d,1) = round(depth_bnds0(d,1) * 1000000, opt) / 1000000
  end do
  delete(opt)
  delete(depth_bnds0)

  ; soil moisture (kg/m-2) and time vector
  msl = sfile[:]->mrlsl
  time = sfile[:]->time
  YYYYMM = cd_calendar(time, -1)

  iStart = 2006 ;; ind(YYYYMM .eq. 200601)
  iEnd = 2100 ;; ind(YYYYMM eq. 210012)

  delete(time)

  ; ---- convert kg/m^2 to m^3/m^3
  do d=0, n_depth(0) - 1
    msl(:,d,:,:) = msl(:,d,:,:) / (depth_bnds(d,1) - depth_bnds(d,0)) * 0.001
  end do

  ;----------------------------------------------
  ; 0-10cm, 10-30cm, 50-100cm, 1 deg -> 0.5 deg, monthly
  ;----------------------------------------------
  depth_to = (/ (/0., 0.1/), (/0.1, 0.3/), (/0.3, 0.5/), (/0.5, 1./) /)
  depth_to_names = (/ "0-10cm", "10-30cm", "30-50cm", "50-100cm" /)

  debug_depth_bnds = True

  do d=0,dimsizes(depth_to_names)-1
    print("----------------------------")
    print(d)
    print("")
    ; check if this depth can be calculated
    if depth_to(d,1) .gt. depth_bnds(n_depth(0)-1, 1) then
      if debug_depth_bnds then
        print("too deep")
        print("from " + to_string(depth_to(d,1)))
        print("/too deep")
      end if
      continue
    else
      ; bound within a single layer
      a0 = ind(depth_bnds(:,0) .lt. depth_to(d,0))
      b0 = ind(depth_bnds(:,1) .ge. depth_to(d,1))
      ax = a0(dimsizes(a0)-1)
      bx = b0(0)
      delete(a0)
      delete(b0)
      if ismissing(ax) then
        ax = -1
      end if

      c0 = ind(depth_bnds(:,0) .le. depth_to(d,0))
      d0 = ind(depth_bnds(:,1) .gt. depth_to(d,1))
      cx = c0(dimsizes(c0)-1)
      dx = d0(0)
      delete(c0)
      delete(d0)
      if model .eq. "inmcm4" then
        if ismissing(cx) then
          cx = -2
        end if
      end if
      if ismissing(dx) then
        dx = -1
      end if

      if debug_depth_bnds then
        print("check bounds")
        print(ax)
        print(bx)
        print(cx)
        print(dx)
        print("")
        print(depth_bnds(:,0) - depth_to(d,0))
        print("")
        print(depth_bnds(:,1) - depth_to(d,1))
        print("continue")
        print("/check bounds")
      end if

      if ((ax .eq. bx) .or. (cx .eq. dx)) then
        delete(ax)
        delete(bx)
        delete(cx)
        delete(dx)
        continue
      end if
    end if

    ; calculate the weighted average
    wgts = compute_soil_wts_bnds(depth_bnds, depth_to(d,0), depth_to(d,1))
    do w=0,dimsizes(wgts)-1
      if w .eq. 0 then
        sm_avg = msl(:, w, :, :) * wgts(w)
      else
        sm_avg = sm_avg + msl(:, w, :, :) * wgts(w)
      end if
    end do

    sm_avg!0 = "time"
    sm_avg&time = msl&time
    sm_avg!1 = "lat"
    sm_avg&lat = msl&lat
    sm_avg!2 = "lon"
    sm_avg&lon = msl&lon

    sm_avg@long_name = "Volumetric soil water"
    sm_avg@units = "m3/m3"
    sm_avg@depth_bnds = depth_to_names(d)

    delete(wgts)

    ; interpolate to 10 degrees
    sm = compute_horizontal_regrid_DA(sm_avg, model)
    delete(sm_avg)

    ; write to file by year
    do yy = iStart, iEnd
      fout = "$SCRATCHDIR/Soil_Moisture/intermediate/Interp_DA/" + \
             "None/CMIP5/" + model + "/sm_rcp85_" + member + "_" + \
             tostring(yy) + "_" + depth_to_names(d) + ".nc"
      system("rm -f " + fout)
      ncdf = addfile(fout, "c")
      filedimdef(ncdf, "time", -1, True)

      ncdf->sm = sm(ind((YYYYMM .ge. yy * 100) .and. \
                        (YYYYMM .lt. (yy * 100 + 100))), :, :)

      delete(fout)
      delete(ncdf)
    end do

    delete(sm)
    delete(yy)
  end do

  delete(msl)
  delete(model)
  delete(sfile)
  delete(n_depth)
  delete(depth_bnds)
  delete(YYYYMM)
end