;-------------------------------------------------------------------
; Interpolate the GlobSnow variables to 0.5 degrees
;-------------------------------------------------------------------
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"

begin
  ;----------------------------------------------
  ; read coordinates data
  ;----------------------------------------------
  sfile = addfile("$PROJDIR/Soil_Moisture/data/GlobSnow/" + \
                  "coords.nc", "r")
  lat = sfile->lat
  replace_ieeenan(lat, default_fillvalue("float"), 0)
  lat@_FillValue = default_fillvalue("float")
  lon = sfile->lon
  replace_ieeenan(lon, default_fillvalue("float"), 0)
  lon@_FillValue = default_fillvalue("float")
  delete(sfile)

  ;----------------------------------------------
  ; read snw data
  ;----------------------------------------------
  do yy = 1981, 2017 ; 1979, 2017
    snw_raw = new( (/12,721,721/), "float", default_fillvalue("float") )
    do mm = 1,12
      if (yy .eq. 1981) .and. (mm .eq. 12) then
        ; does not have data file.
        ;; Use previous month.
        ;; snw_raw(mm-1, :, :) = snw_raw(mm-2, :, :)
        continue
      end if

      if (mm .le. 5) .or. (mm .ge. 10) then
        sfile = addfile("$PROJDIR/Soil_Moisture/data/GlobSnow/" + \
                        "L3B_monthly_biascorrected_SWE/" + tostring(yy) + \
                        tostring_with_format(mm, "%02d") + \
                        "_northern_hemisphere_monthly_swe_0.25grid.nc", "r")
        snw_raw(mm-1, :, :) = sfile->swe
      else
        snw_raw(mm-1, :, :) = where(snw_raw(mm-2, :, :) .lt. 0, \
                                    snw_raw(mm-2, :, :), 0)
      end if
    end do
    snw_raw(:, :, :) = where(snw_raw .lt. 0, snw_raw@_FillValue, \
                             snw_raw)

    ;----------------------------------------------
    ; ESMF interpolate un-structured grids
    ;----------------------------------------------
    swe_regrid = new( (/12, 360, 720/), "float", snw_raw@_FillValue )
    do mm = 1,12
      if (yy .eq. 1981) .and. (mm .eq. 12) then
        ; does not have data file.
        continue
      end if

      lat1d       = ndtooned(lat)
      lon1d       = ndtooned(lon)
      swe1d       = ndtooned(snw_raw(mm-1, :, :))      
      ind_nomsg   = ind(.not.(ismissing(lat1d) .or. ismissing(swe1d)))
      lat1d_nomsg = lat1d(ind_nomsg)

      lon1d_nomsg = lon1d(ind_nomsg)
      swe1d_nomsg = swe1d(ind_nomsg)

      ;---Options for regridding
      Opt                   = True            ; Options for regridding
      Opt@SrcFileName       = "EASE_ESMF.nc"  ; Output files
      Opt@DstFileName       = "NH_SCRIP.nc"
      Opt@WgtFileName       = "EASE_2_NH_bilinear.nc"
      Opt@ForceOverwrite    = True
      Opt@SrcGridLat        = lat1d_nomsg  ; This is a 1D
      Opt@SrcGridLon        = lon1d_nomsg  ; unstructured grid.
      ;---Set these to True if you already have the weights file.
      Opt@SkipSrcGrid = False
      Opt@SkipDstGrid = False
      Opt@SkipWgtGen  = False
      Opt@DstGridLat = fspan(-89.75, 89.75, 360)
      Opt@DstGridLon = fspan(-179.75, 179.75, 720)
      Opt@InterpMethod      = "bilinear"
      Opt@Debug             = True

      swe_regrid(mm-1, :, :) = ESMF_regrid(swe1d_nomsg,Opt)

      delete(lat1d)
      delete(lon1d)
      delete(swe1d)
      delete(ind_nomsg)
      delete(lat1d_nomsg)
      delete(lon1d_nomsg)
      delete(swe1d_nomsg)
      delete(Opt)
    end do

    swe_regrid@name = "Snow Water Equivalent"
    swe_regrid@units = "mm"

    swe_regrid!0 = "time"
    swe_regrid&time = new( 12, "integer", "No_FillValue" )
    opt = 0
    opt@calendar = "standard"
    opt@return_type = "integer"
    do mm = 1,12
      swe_regrid&time(mm-1) = cd_inv_calendar(yy, mm, 1, 0, 0, 0, \
          "months since 1900-01-01 00:00:00", opt)
    end do
    swe_regrid&time@units = "months since 1900-01-01 00:00:00"
    swe_regrid&time@calendar = "standard"
    delete(opt)

    swe_regrid!1 = "lat"
    swe_regrid&lat = fspan(-89.75, 89.75, 360)
    swe_regrid&lat@units = "degrees north"
    swe_regrid!2 = "lon"
    swe_regrid&lon = fspan(-179.75, 179.75, 720)
    swe_regrid&lon@units = "degrees east"

    printVarSummary(swe_regrid)

    ;----------------------------------------------
    ; write to file
    ;----------------------------------------------
    fout = "$PROJDIR/Soil_Moisture/intermediate/Interp_DA/None/GlobSnow/" + \
           "snw_0.5_" + tostring(yy) + ".nc"
    system("rm -f " + fout)
    ncdf = addfile(fout, "c")
    filedimdef(ncdf, "time", -1, True)
    ncdf->snw = swe_regrid
    delete(fout)
    delete(ncdf)
  end do
  delete(yy)
end