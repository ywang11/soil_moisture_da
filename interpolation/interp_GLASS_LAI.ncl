load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "compute_horizontal_regrid_DA.ncl"


begin

  flist = systemfunc("ls $PROJDIR/DATA/Vegetation/LAI/GLASS/AVHRR/GLASS01G02.V50.*.hdf")

  lai = new( (/dimsizes(flist), 360, 720/), "float" )
  lai!0 = "time"
  lai!1 = "lat"
  lai!2 = "lon"
  lai&lat = fspan(89.75, -89.75, 360)
  lai&lon = fspan(-179.75, 179.75, 720)

  yyyymm = new( (/dimsizes(flist)/), "double", "No_FillValue" )
  yyyymm!0 = "time"
  yyyymm@calendar = "standard"
  yyyymm@units = "days since 1900-01-01"
  do ii = 0,dimsizes(flist) - 1
    sfile = addfile(flist(ii), "r")
    lai(ii, :, :) = (/ short2flt(sfile->LAI) /)

    ff = str_get_field(str_get_field( flist(ii), 10, "/" ), 3, ".")
    yy = str_get_cols(ff, 1, 4)
    doy = toint(str_get_cols(ff, 5, 7))
    doy@calendar = "standard"
    doy@units = "days since " + yy + "-01-01"
    yyyymm(ii) = cd_convert(doy, "days since 1900-01-01")

    delete(yy)
    delete(doy)
    delete(ff)
    delete(sfile)
  end do
  yyyymm&time = yyyymm
  lai&time = yyyymm
  printVarSummary(lai)

  ; scale by 0.1 to get the correct value
  lai = lai * 0.1

  lai_monthly = calculate_monthly_values(lai, "avg", 0, False)
  printVarSummary(lai_monthly)
  delete(lai)

  delete(yyyymm)
  delete(flist)

  ; reverse latitude
  lai_monthly = lai_monthly(:, ::-1, :)

  yyyy = cd_calendar(lai_monthly&time, -1) / 100
  do ii = yyyy(0), yyyy(dimsizes(yyyy)-1)
    fout = "$PROJDIR/Soil_Moisture/intermediate/Interp_DA/" + \
           "None/GLASS/lai_0.5_" + tostring(ii) + ".nc"
    system("rm -f " + fout)
    ncdf = addfile(fout, "c")
    filedimdef(ncdf, "time", -1, True)
    ncdf->lai = lai_monthly(ind(yyyy .eq. ii), :, :)

    delete(ncdf)
    delete(fout)
  end do

  delete(lai_monthly)
end