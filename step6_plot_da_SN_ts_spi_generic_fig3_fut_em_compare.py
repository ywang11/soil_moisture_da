"""
2021/07/16
ywang254@utk.edu

Plot the times series of the emergent constraint S/N, and threshold of 
   likely (S/N > 0.95, detectable at 66% confidence)
   very likely (S/N > 1.64, 90% confidence)
   virtually certain (S/N > 2.57, 99% confidence).

The thresholds follow Marvel et al. 2019 DOI: 10.1038/s41586-019-1149-8

The EOF is flipped already in the fingerprint calculation step to ensure most
  are negative values.

Historical + RCP85 only. Until 2100.
"""
import matplotlib.pyplot as plt
import pandas as pd
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median # depth_cm
from misc.plot_utils import plot_ts_shade, cmap_gen
import numpy as np
import cartopy.crs as ccrs
##from cartopy.mpl.gridliner import Gridliner
import xarray as xr
import matplotlib as mpl
import itertools as it
import multiprocessing as mp
import shutil


#Setup / MODIFY
folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
season_name = 'bymonth'
base = 'historical'
expr = 'historical'
expr_name = 'ALL'
lag = 3
start = 1971
L = 2016 - start + 1
half_window_list = [1,2,4,8,16,24]
lat_eof = [float(x) for x in lat_median]
season_list = [str(i) for i in range(12)]
season_list_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug',
                    'Sep','Oct','Nov','Dec']
dist = 'gmm'
limit = False


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titleweight'] = 'normal'
res = '0.5'
cmap = mpl.cm.get_cmap('jet')
clist = [cmap((ii+0.5)/(len(half_window_list)+1)) for ii in range(len(half_window_list)+1)]
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


if limit:
    prefix = 'limit_'
else:
    prefix = ''

for dind, dcm in enumerate(depth_cm_new):
    fig, axes = plt.subplots(nrows = 12, ncols = len(half_window_list),
                             figsize = (8, 8), sharex = False, sharey = True)
    fig.subplots_adjust(wspace = 0., hspace = 0.15)
    for sind, hind in it.product(range(len(season_list)), range(len(half_window_list))):
        season = season_list[sind]
        hw = half_window_list[hind]
        
        ax = axes[sind, hind]

        ###def plot_gls():
        ###    gls_constrained = pd.read_csv(os.path.join(mg.path_out(), 'figures',
        ###                                               'fig3_em_calc', dist + \
        ###                                    '_gls_signal_collect_constrained.csv'),
        ###                                  index_col = 0,
        ###                                  header = [0,1]).loc[:, (season,dcm)]
        ###    gls_pval = pd.read_csv(os.path.join(mg.path_out(), 'figures', 
        ###                                        'fig3_em_calc', dist + \
        ###                                        '_gls_signal_collect_pvals.csv'),
        ###                           index_col = 0, header = [0,1]).loc[:,(season,dcm)]
        ###    if gls_pval.loc['esm-1'] <= 0.05:
        ###        lw = 2
        ###    else:
        ###        lw = 1
        ###    ax.plot(range(gls_constrained.index[0],
        ###                  int(gls_pval.loc['Break'])),
        ###            gls_constrained.loc[:(int(gls_pval.loc['Break'])-1)],
        ###            color = clist[0], lw = lw)
        ###    if gls_pval.loc['esm-2'] <= 0.05:
        ###        lw = 2
        ###    else:
        ###        lw = 1
        ###    ax.plot(range(int(gls_pval.loc['Break']),
        ###                  gls_constrained.index[-1]+1),
        ###            gls_constrained.loc[int(gls_pval.loc['Break']):],
        ###            color = clist[0], lw = lw)
        ###plot_gls()

        # plot_moving window
        mw_constrained = pd.read_csv(os.path.join(mg.path_out(), 'figures',
                                          'fig3_em_calc',
                                          dist + '_signal_collect_constrained.csv'),
                             index_col = 0, header = [0,1,2]).loc[:,
                                                                  (str(hw), season, dcm)]
        mw_pval = pd.read_csv(os.path.join(mg.path_out(), 'figures',
                                           'fig3_em_calc',
                                           dist + '_signal_collect_p.csv'),
                              index_col = 0, header = [0,1,2]).loc[:,
                                                                   (str(hw),
                                                                    season, dcm)]
        ax.plot(mw_constrained.index, mw_constrained.values,
                color = clist[hind + 1], lw = 1)
        ax.plot(mw_constrained.index[mw_pval.values <= 0.05],
                mw_constrained.values[mw_pval.values <= 0.05],
                color = clist[hind + 1], lw = 2)
                                          
        ax.axhline(1.96, linestyle = '-', color = 'b',
                   linewidth = 0.5)
        ax.axhline(2.57, linestyle = '-', color = 'b',
                   linewidth = 1.)
        ax.axhline(0, linestyle = ':', color = 'b', linewidth = 0.5)

        if hind == 0:
            ax.set_ylabel(season_list_name[int(season)])
        if sind == 0:
            ax.set_title('half window size ' + str(hw), pad = 3.)
    if np.mod(sind, len(half_window_list)) == 0:
        #ax.set_ylabel(dcm)
        ax.tick_params(axis = 'y', length = 2., pad = 2.)
    else:
        ax.tick_params(axis = 'y', length = 0.)
    ax.set_xlim([mw_constrained.index[0],
                 mw_constrained.index[-1]])
    ax.set_xticks([1980, 2000, 2020, 2040])
    ax.tick_params(axis = 'x', length = 2., pad = 2.)
    if (sind <= 11):
        ax.set_xticklabels([])
    else:
        ax.set_xticklabels([1980, 2000, 2020, 2040])
    ax.text(0.01, 1.03, lab[sind], transform = ax.transAxes,
            weight = 'bold')
#ax.legend(h, ['ALL ' + x for x in depth_cm_new] + \
#          ['Gaussian 95% CI'] + \
#          ['95% Confidence level', '99% Confidence level'],
#          ncol = 3, loc = 'lower center', bbox_to_anchor = (-1.1, -0.4))
fig.savefig(os.path.join(mg.path_out(), 'figures', prefix + 'fig3_fut_' + \
                         dist + '_' + str(lag) + '_' + season_name + \
                         '_L=' + str(L) + '_' + res + suffix + '_compare.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
