"""
Check the completeness of the downloaded ensemble members of the drivers.
"""
import numpy as np
import os
from glob import glob
import utils_management as mg
from utils_management.constants import drivers
from misc.cmip6_utils import mrsol_availability
import pandas as pd
import itertools as it


expr = 'ssp585'


land_mask = 'vanilla'
res = '0.5'
a, _ = mrsol_availability('0-10cm', land_mask, 'historical', res)
b, _ = mrsol_availability('0-10cm', land_mask, 'ssp585', res)
cmip6_list = sorted(set(a) & set(b))


check_exist = pd.DataFrame(data = ' ' * 20, index = cmip6_list, 
                           columns = drivers)
for cc,vv in it.product(cmip6_list, drivers):
    model = cc.split('_')[0]
    eid = cc.split('_')[1]

    flist = glob(os.path.join(mg.path_data(), 'CMIP6', expr, vv,
                              model, '*_' + expr + '_' + eid + '_*.nc'))
    if len(flist) > 0:
        check_exist.loc[cc, vv] = 'T'
    else:
        check_exist.loc[cc, vv] = 'F'
check_exist.to_csv(os.path.join(mg.path_data(), 'CMIP6', expr,
                                'check_' + expr + '_data_drivers.csv'))
