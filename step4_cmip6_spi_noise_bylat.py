"""
20200514
ywang254@utk.edu

Obtain the collection of all L-length trends in piControl simulations.
Allow all available piControl models, which may be different from the number
of models used in the historical experiments.
"""
import utils_management as mg
from utils_management.constants import depth_cm_new, year_cmip6, \
    lat_median # depth_cm
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal_bylat, ls_trend
from misc.cmip6_utils import mrsol_availability
from glob import glob
import itertools as it
import time
import multiprocessing as mp


start = time.time()

noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['noSahel'].loc[np.array(lat_median).astype(int)].values)
else:
    suffix = ''
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['Normal'].loc[np.array(lat_median).astype(int)].values)


land_mask = 'vanilla'
res = '0.5'
season_list = [str(i) for i in range(12)]
N = 200
L_range = range(10, 67)


#for base, limit, i, season, dist, lag in it.product(['historical',
#                                              'hist-GHG', 'hist-aer'],
#                                              [True, False], [0,1],
#                                              season_list,
#                                              ['gmm','weibull'],
#                                              [1, 3, 6]):
def calc(option):
    base, limit, i, season, dist, lag = option
    if limit:
        prefix = 'limit_'
    else:
        prefix = ''

    #dcm = depth_cm[i]
    dcm = depth_cm_new[i]

    ##################################################
    # Last 200 years of the piControl of these models.
    ##################################################
    model_version, _ = mrsol_availability(dcm, land_mask, 'piControl', res)
    if limit:
        mv2, _ = mrsol_availability(dcm, land_mask, base, res)
        model_version = sorted(list(set(model_version) & set(mv2)))
    else:
        model_version = sorted(model_version)

    noise = np.full([N * len(model_version), lat_area_sqrt.shape[0]], np.nan)

    for mind, m_v in enumerate(model_version):
        # Read the by-latitude aggregated data.
        data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_summary',
                                        'piControl', dist,
                                        m_v + '_' + dcm + '_' + str(lag) + \
                                        '_' + season + '_' + res + \
                                        '_g_lat_ts' + suffix + '.csv'),
                           index_col = 0, parse_dates = True)

        # Remove the mean at each latitude.
        noise[(mind*N):((mind+1)*N), :] = \
            data.loc[:, lat_median].values - \
            np.mean(data.loc[:, lat_median].values, axis = 0)

    ##noise = noise - noise.mean(axis = 0)

    #####################
    # Load the first EOF
    #####################
    data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                    base, 'bylat', prefix + \
                                    dist + '_eof_' + dcm + \
                                    '_' + str(lag) + '_' + season + '_' + \
                                    res + suffix + '.csv'),
                       index_col = 0)
    data.index = data.index.astype(str)
    eof1 = data.loc[lat_median, 'Full'].values

    #################################
    # Calculate the noise fingeprint.
    #################################
    wgts = lat_area_sqrt / np.mean(lat_area_sqrt)

    trend = pd.DataFrame(data = np.nan, index = range(noise.shape[0]),
                         columns = list(L_range))
    pc = pd.DataFrame(data = np.nan, index = range(noise.shape[0]),
                      columns = ['pc'])
    for L in L_range:
        if noSahel:
            tt, pp = calc_signal_bylat(eof1[:-1], noise[:, :-1],
                                       wgts[:-1], L)
        else:
            tt, pp = calc_signal_bylat(eof1, noise, wgts, L)
        trend.loc[:, L] = tt
    pc.loc[:, 'pc'] = pp

    trend.to_csv(os.path.join(mg.path_out(), 'cmip6_spi_noise', 'bylat',
                              base + '_eofs', prefix + dist + '_trend_' + \
                              dcm + '_' + str(lag) + '_' + \
                              season + '_' + res + suffix + '.csv'))
    pc.to_csv(os.path.join(mg.path_out(), 'cmip6_spi_noise', 'bylat',
                           base + '_eofs', prefix + dist + '_pc_' + dcm + \
                           '_' + str(lag) + '_' + season + '_' + res + \
                           suffix + '.csv'))


p = mp.Pool(6)
p.map_async(calc, list(it.product(['historical', 'hist-GHG', 'hist-aer'],
                                  [True, False], [0,1], season_list,
                                  ['gmm','weibull'], [1, 3, 6])))
p.close()
p.join()

end = time.time()
# Script finished in 107.16491745710373 minutes. (4 nodes)
# Script finished in 49.90838576157888 minutes. (6 nodes)
print('Script finished in ' + str((end - start) / 60) + ' minutes.')
