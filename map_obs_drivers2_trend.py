"""
20201027

ywang254@utk.edu

Calculate the global trend in dTWS + Q in the unit of mm/day/year
"""
import os
import utils_management as mg
from utils_management.constants import target_lat, target_lon
import xarray as xr
import numpy as np
import pandas as pd
import itertools as it
from glob import glob
import multiprocessing as mp
from time import time
from misc.analyze_utils import calc_seasonal_trend0, get_land_mask


res = '0.5'
land_mask = 'vanilla'
yrng = range(1971, 2015) # last year 2014
ymin,ymax = 1971, 2014
year_str = str(ymin) + '-' + str(ymax)


mask = get_land_mask(land_mask)


def read_dtws():
    # dTWS (mm/month)
    for gind, gr in enumerate(['JPL_GSWP3', 'GSFC_GSWP3']):
        hr = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'dTWS+Q',
                                          'GRACE_REC_v03_' + gr + \
                                          '_TWS_via_daily.nc'))
        # --- read, limit to year range
        var = hr['dTWS'].loc[(hr['time'].to_index().year >= (ymin-1)) & \
                             (hr['time'].to_index().year <= ymax), :,
                             :].copy(deep = True).where(mask)
        hr.close()
        # --- convert unit to mm/day by dividing the days in month
        var = var / np.broadcast_to(var['time'].to_index().to_period( \
        ).days_in_month.values.reshape(-1, 1, 1), var.shape)

        if gind == 0:
            var_mean = var * 0.5
        else:
            var_mean = var * 0.5 + var_mean
    return var_mean


def read_grun():
    # Q (mm/month)
    hr = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'dTWS+Q',
                                      'GRUN_v1_GSWP3_WGS84_' \
                                      + '05_1902_2014_runoff.nc'))
    var = hr['Runoff'].loc[(hr['time'].to_index().year >= (ymin-1)) & \
                           (hr['time'].to_index().year <= ymax), :,
                           :].copy(deep = True).where(mask)
    # --- convert unit to mm/day by dividing the days in month
    var = var / np.broadcast_to(var['time'].to_index().to_period( \
    ).days_in_month.values.reshape(-1, 1, 1), var.shape)
    hr.close()
    return var


# calculate trends
x = read_dtws()
for s in ['Annual', 'DJF', 'MAM', 'JJA', 'SON']:
    g_map_trend, g_p_values, g_intercepts = calc_seasonal_trend0(x, s)
    xr.Dataset({'g_map_trend': (['lat','lon'], g_map_trend),
                'g_p_values': (['lat','lon'], g_p_values),
                'g_intercepts': (['lat','lon'], g_intercepts)},
               coords = {'lon':target_lon[res], 'lat':target_lat[res]} \
    ).to_netcdf(os.path.join(mg.path_out(), 'obs_drivers_summary',
                             'GRACE_pr-evspsbl-mrro_' + res + \
                             '_g_map_trend_' + s + '_' + year_str + '.nc'))

#y = read_grun()
#var = x + y
#for s in ['Annual', 'DJF', 'MAM', 'JJA', 'SON']:
#    g_map_trend, g_p_values, g_intercepts = calc_seasonal_trend0(var, s)
#    xr.Dataset({'g_map_trend': (['lat','lon'], g_map_trend),
#                'g_p_values': (['lat','lon'], g_p_values),
#                'g_intercepts': (['lat','lon'], g_intercepts)},
#               coords = {'lon':target_lon[res], 'lat':target_lat[res]} \
#    ).to_netcdf(os.path.join(mg.path_out(), 'obs_drivers_summary',
#                             'GRACE+GRUN_pr-evspsbl_' + res + \
#                             '_g_map_trend_' + s + '_' + year_str + '.nc'))
