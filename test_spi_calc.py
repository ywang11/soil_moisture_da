"""
20200114

Test the SSI calculation using a random soil moisture file, for 3-, 6-, 
 and 12-month SSI,
 following Hao & AghaKouchak 2013 Multivariate Standardized Drought Index:
                                  A parametric multi-index model.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm, depth, target_lat, \
    target_lon, year_cmip6
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
from misc.spi_utils import *
from scipy.integrate import quad
import sys
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib import cm


land_mask = 'vanilla'
res = '0.5'


#
i = 0 # REPLACE1 # 0, 1, 2, 3
d = depth[i]
dcm = depth_cm[i]


# The CMIP6 list of models for individual experiments.
expr = 'historical'
if expr == 'historical':
    model_list = get_model_list(['historical', 'ssp585'], dcm,
                                land_mask, res)
else:
    model_list = get_model_list([expr], dcm, land_mask, res)
m = 'CanESM5'
r = model_list[m][0]

period = pd.date_range(str(year_cmip6['historical'][1]) + '-01-01',
                       str(year_cmip6['ssp585'][-1]) + '-12-31',
                       freq = 'YS')


print(i)
print(m)


####################################
# Function to apply SPI in parallel.
####################################
def apply_spi(rr0, vector):
    vector = vector.reshape(-1)
    return (get_zscore(rr0[0][:,0], rr0[0][:,1], vector), rr0[1])


#######################################################################
# Read merged products data.
#######################################################################
fpath = [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                      land_mask, 'CMIP6', expr, m + '_' + r,
                      'mrsol_' + res + '_' + str(y) + \
                      '_' + dcm + '.nc') \
         for y in year_cmip6['historical']] + \
        [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                      land_mask, 'CMIP6', 'ssp585', m + '_' + r,
                      'mrsol_' + res + '_' + str(y) + \
                      '_' + dcm + '.nc') \
         for y in year_cmip6['ssp585']]
data = xr.open_mfdataset(fpath, decode_times = False)
data_array = data.sm.values.copy()
data.close()

data_array = data_array.reshape(data_array.shape[0], -1)

# Subset to within the land mask.
retain = np.where(~np.all(np.isnan(data_array), axis = 0))[0]
data_array = data_array[:, retain]


#######################################################################
# SSI calculation.
#######################################################################
start = time.time()
for s, month in it.product([3, 6, 12], range(12)):

    # Convert to moving average
    data_array_apply = rolling_mean(data_array, s, month)

    # Remove small negative values
    data_array_apply[(data_array_apply > -1) & \
                     (data_array_apply <= 0.)] = 1e-6

    #######################################################################
    # Estimate the empirical pdf using 1951-2014 as the reference period.
    #######################################################################
    fit_period = (period.year >= 1951) & (period.year <= 2014)

    data_new = data_array_apply[fit_period, :]
    data_new = data_new[np.isfinite(data_new[:, 0]), :]

    start_in = time.time()
    results0 = fit_pdf(data_new)
    end_in = time.time()
    print(('Spent %.6f minutes fitting pdf s = ' + str(s)) % \
          ((end_in - start_in)/60))

    #######################################################################
    # Apply the empirical pdf.
    #######################################################################
    data_array_apply2 = data_array_apply

    spi = np.full([data_array_apply.shape[0],
                   len(data.lat) * len(data.lon)], np.nan)

    data_array_apply2 = np.array_split(data_array_apply2,
                                       data_array_apply2.shape[1],
                                       axis = 1)

    p = mp.Pool(mp.cpu_count() - 3)
    results = [p.apply_async(apply_spi, args = (results0[rank], 
                                                data_array_apply2[rank])) \
               for rank in range(len(data_array_apply2))]
    p.close()
    p.join()

    results = [result.get() for result in results]

    for a in range(len(data_array_apply2)):
        spi[:, retain[results[a][1]]] = results[a][0]

    spi = spi.reshape(-1, len(target_lat[res]), len(target_lon[res]))

    #
    spi = xr.DataArray(spi, coords = {'time': period,
                                      'lat': target_lat[res],
                                      'lon': target_lon[res]},
                       dims = ['time', 'lat', 'lon'])

    # Save to file.
    spi.to_dataset(name = 'spi').to_netcdf( os.path.join( \
        mg.path_out(), 'test_spi_calc_' + str(s) + '_' + str(month) + '.nc') )


    ###########################################################################
    # Test the valid values
    ###########################################################################
    cmap = cm.get_cmap('Spectral')
    map_extent = [-180, 180, -60, 90]
    eps = 1e-6
    alpha = 0.5 # Faintness of the contour color
    
    fig, ax = plt.subplots(figsize = (12, 8),
                           subplot_kw = {'projection': ccrs.PlateCarree()})
    # Plot.
    ax.coastlines()
    ax.set_extent(map_extent)
    spi_cyc, lon_cyc = add_cyclic_point(np.mean(spi.values, axis = 0),
                                        coord=spi.lon)
    cf = ax.contourf(lon_cyc, spi.lat, spi_cyc, cmap = cmap)
    cb = plt.colorbar(cf, ax = ax, shrink = 0.7)
    
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                      linewidth=1, color='gray', alpha=0.5, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator(np.arange(-180, 181, 20.))
    gl.ylocator = mticker.FixedLocator(np.arange(-90., 91., 10.))
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'color': 'black', 'weight': 'bold', 'size': 10}
    gl.ylabel_style = {'color': 'black', 'weight': 'bold', 'size': 10}
    ax.text(-0.07, 0.55, 'latitude', va='bottom', ha='center',
            rotation='vertical', rotation_mode='anchor',
            transform=ax.transAxes)
    ax.text(0.5, -0.2, 'longitude', va='bottom', ha='center',
            rotation='horizontal', rotation_mode='anchor',
            transform=ax.transAxes)
    fig.savefig(os.path.join(mg.path_out(), 'test_spi_calc_' + \
                             str(s) + '_' + str(month) + '.png'), dpi = 600.)
    plt.close(fig)

end = time.time()
print('Spent %.6f minutes ' % ((end_in - start_in)/60))
