"""
20200805
ywang254@utk.edu

Plot the merged products for global srex regions.

20200526
ywang254@utk.edu

Plot only the main regions.
"""
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import matplotlib as mpl
import regionmask as rmk
import utils_management as mg
from utils_management.constants import depth_cm_new
from scipy import signal


def small_panel(a_gs, prod_by_depth, source_by_depth):
    sub_gs = gridspec.GridSpecFromSubplotSpec(2, 1, a_gs, hspace = 0.)
    axes = np.empty(2, dtype = object)
    for i in range(2):
        if i == 0:
            axes[i] = plt.subplot(sub_gs[i])
        else:
            axes[i] = plt.subplot(sub_gs[i], sharex = axes[0],
                                  sharey = axes[0])
        ax = axes[i]

        h = [None] * prod_by_depth[i].shape[1]
        for j in range(prod_by_depth[i].shape[1]):
            freqs, psd = signal.welch(prod_by_depth[i].values[:,j],
                                      fs = 12) # sampling frequency = 12

            h[j], = ax.plot(1/freqs, psd, '-', color = clist[j], lw = 0.3, zorder = 3)
        if i == 0:
            h0 = h # has all the products

        # Calculate the envelop of the PSD of the source datasets.
        source_psd = np.full([len(freqs), source_by_depth[i].shape[1]], np.nan)
        for j in range(source_by_depth[i].shape[1]):
            temp = source_by_depth[i].values[:,j]
            temp = temp[~np.isnan(temp)]
            if len(temp) == 0:
                continue
            freqs, source_psd[:, j] = signal.welch(temp,
                                                   fs = 12) # sampling frequency = 12
        ## ---- weighted average by standard deviation
        #source_psd = pd.DataFrame(source_psd, index = range(len(freqs)),
        #                          columns = source_by_depth[i].columns)
        #temp_model = [x.split('_')[0] for x in source_psd.columns]
        #temp_mean = source_psd.groupby(temp_model, axis = 1).mean().mean(axis = 1)
        #temp_std = source_psd.std(axis = 1)

        hf = ax.fill_between(1/freqs, np.nanmin(source_psd, axis = 1),
                             np.nanmax(source_psd, axis = 1), zorder = 2,
                             edgecolor = '#636363', facecolor = '#f7f7f7',
                             lw = 1)

        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        #ax.ticklabel_format(axis = 'y', scilimits = (-2,2))
        ax.tick_params('both', length = 1.5)
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_ylim([1e-9, 1])
        ax.set_yticks([1e-6, 1e-4, 1e-2])
        ax.set_xlim([0.166667, 21.33333])
        #ax.set_xlim([1/4, 10])
        ax.set_xticks([1/4, 0.5, 1, 10])
        ax.get_xaxis().set_major_formatter(mpl.ticker.ScalarFormatter())
        ax.set_xticklabels(['1/4','1/2','1','10'])
        #ylim = ax.get_ylim()
        #ax.set_ylim([ylim[0], ylim[1]*1.2])
    return h0, hf, axes, sub_gs


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_names = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
              'EC CMIP5+6', 'EC ALL']


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5
mpl.rcParams['axes.titleweight'] = 'bold'
mpl.rcParams['axes.titlepad'] = 1.5
mpl.rcParams['axes.linewidth'] = 0.5
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx']
cmap = mpl.cm.get_cmap('jet')
clist = [cmap( (i+0.5)/len(prod_list) ) for i in range(len(prod_list))]
suffix = '_sm' # '_sm', '_ssi'


scaler = 1. # multiple the soil moisture values
# Read dataset
prod_set = pd.read_csv(os.path.join(mg.path_out(), 'prod_srex_all.csv'),
                       index_col = 0, parse_dates = True,
                       header = [0,1,2]) * scaler
source_set = pd.read_csv(os.path.join(mg.path_out(), 
                                      'validate_gen_ts_cmip6only' + suffix + '.csv'),
                         index_col = 0, parse_dates = True,
                         header = [0,1,2]) * scaler


sid_list = [str(ii) for ii in range(1,27)]
fig = plt.figure(figsize = (6.5, 8.5))
gs = gridspec.GridSpec(6, 5, hspace = 0.16, wspace = 0.08)
for ind, sid in enumerate(sid_list):
    prod_by_depth = {}
    for dind, dcm in enumerate(depth_cm_new):
        prod_by_depth[dind] = prod_set.loc[:, (slice(None), dcm, str(sid))]
        prod_by_depth[dind].columns = prod_by_depth[dind].columns.droplevel([1,2])
        
    source_by_depth = {}
    for dind, dcm in enumerate(depth_cm_new):
        source_by_depth[dind] = source_set.loc[:, (slice(None), dcm, str(sid))]
        source_by_depth[dind].columns = source_by_depth[dind].columns.droplevel([1,2])

    gs_panel = gs[ind//5, np.mod(ind, 5)]
    h, hf, axes, sub_gs = small_panel(gs_panel, prod_by_depth, source_by_depth)
    axes[0].set_title(rmk.defined_regions.srex.names[int(sid)-1])

    for i in range(2):
        axes[i].text(0.02, 0.85, '(' + lab[ind] + str(i) + ') ' + \
                     depth_cm_new[i].replace('-', u'\u2212').replace('cm', ' cm'),
                     transform = axes[i].transAxes)
    if ind >= 21:
        plt.setp(axes[0].get_xticklabels(), visible=False)
        plt.setp(axes[1].get_xticklabels(), visible=True)
        axes[1].set_xlabel('Period (year)')
    else:
        for i in range(2):
            plt.setp(axes[i].get_xticklabels(), visible=False)

    if np.mod(ind,5) == 0:
        axes[0].text(-0.3, 0., 'Power', horizontalalignment = 'center', rotation = 90,
                     transform = axes[0].transAxes)
    else:
        for i in range(2):
            plt.setp(axes[i].get_yticklabels(), visible=False)
axes[1].legend(h + [hf], prod_names + ['Source datasets'],
               loc = (1.3, 0.5), ncol = 5)
fig.savefig(os.path.join(mg.path_out(), 'srex_psd_rest' + suffix + '.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
