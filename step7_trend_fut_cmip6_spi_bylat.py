"""
2020/03/13

ywang254@utk.edu

Calculate the linear trend in the annual time series, and in the per month
  time series by latitude band during the full period for each CMIP6 model.

Calculate the trend using a moving window of every 20 years.

Ensure the length of the periods is consistent with D&A.
"""
import pandas as pd
from utils_management.constants import depth_cm_new, lat_median
import utils_management as mg
from misc.cmip6_utils import mrsol_availability
from misc.standard_diagnostics import get_bylat_trend
import itertools as it
import os
import numpy as np
import multiprocessing as mp


res = '0.5'
land_mask = 'vanilla'
expr = 'historical'
suffix = '_noSahel' # ['', '_noSahel']


L = 46 # 2016 - 1971 + 1
year_range_list = [range(i, i+L) for i in [1995, 2010, 2025, 2040, 2055]]


##for dist, lag, year, i in it.product(['gmm','weibull'], [1,3,6],
##                                     year_range_list, range(2)):
def calc(option):
    dist, lag, year, i = option
    
    # Time period to calculate the trend on - MODIFY, which changes the 
    # associated set of land surface models.
    time_range = pd.date_range(start = str(year[0])+'-01-01',
                               end = str(year[-1])+'-12-31', freq = 'YS')
    year_str = str(year[0]) + '-' + str(year[-1])

    dcm = depth_cm_new[i]
    a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
    b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
    cmip6_list = list(set(a) & set(b))

    #
    trend_collection = pd.DataFrame(data = np.nan, index = lat_median,
        columns = pd.MultiIndex.from_product([cmip6_list, range(12)]))
    trend_p_collection = pd.DataFrame(data = np.nan, index = lat_median,
        columns = pd.MultiIndex.from_product([cmip6_list, range(12)]))

    for season, model in it.product(range(12), cmip6_list):
        fname = os.path.join(mg.path_out(), 'cmip6_spi_summary', expr, dist,
                             model + '_' + dcm + '_' + str(lag) + '_' + \
                             str(season) + '_' + res + '_g_lat_ts' + \
                             suffix + '.csv')
        trend, trend_p = get_bylat_trend(fname, time_range)

        trend_collection.loc[:, (model, season)] = trend
        trend_p_collection.loc[:, (model, season)] = trend_p

    trend_collection.to_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_spi', expr, dist, 'bylat_trend_fut_' + \
        year_str + '_' + str(lag) + '_' + res + '_' + dcm + suffix + '.csv'))
    trend_p_collection.to_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_spi', expr, dist, 'bylat_trend_p_fut_' + \
        year_str + '_' + str(lag) + '_' + res + '_' + dcm + suffix + '.csv'))


p = mp.Pool(4)
p.map_async(calc, list(it.product(['gmm','weibull'], [1,3,6],
                                  year_range_list, range(2))))
p.close()
p.join()
