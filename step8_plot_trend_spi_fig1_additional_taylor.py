"""
20200901
ywang254@utk.edu

Plot Taylor diagram between the product's trend and each CMIP6 model's trend.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import matplotlib as mpl
import utils_management as mg
import itertools as it
from matplotlib import gridspec
from misc.cmip6_utils import mrsol_availability
import matplotlib.pyplot as plt
from misc.plot_utils import plot_convenient_taylor


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5.
mpl.rcParams['hatch.linewidth'] = 0.5
mpl.rcParams['axes.titlepad'] = 1
lab = 'abcdefghijklmnopqrstuvwxyz'
levels = np.arange(-1.6e-2, 1.61e-2, 1e-3)
cmap = get_cmap('Spectral')


res = '0.5'
land_mask = 'vanilla'
lat_eof = [float(x) for x in lat_median]
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


season_list = [str(i) for i in range(12)] # ensure order of x-axis
season_list_name = ['Jan','Feb','Mar','Apr','May','Jun','Jul',
                    'Aug','Sep','Oct','Nov','Dec']

folder = 'bylat'
suffix = ''
lag = 3

year_range_list = [range(1951, 1981), range(1956, 1986), range(1961, 1991),
                   range(1966, 1996), range(1971, 2001), range(1976, 2006),
                   range(1981, 2011), range(1987, 2017)]
year_list = [str(yr[0]) + '-' + str(yr[-1]) for yr in year_range_list]

expr = 'historical'
expr_name = 'ALL'

prod = 'mean_noncmip'
prod_name = 'Mean NonCMIP'


for dist in ['gmm', 'weibull']:
    data_prod = {}
    data_cmip6 = {}
    for dcm in depth_cm_new:
        data_prod[dcm] = pd.read_csv(os.path.join(mg.path_out(),
                                                  'standard_diagnostics_spi',
                                                  'products', dist,
                                                  'bylat_trend_' + str(lag) + \
                                                  '_' + res + '_' + dcm + \
                                                  '_additional.csv'),
                                     index_col = 0, header = [0,1,2])
        data_prod[dcm].index = data_prod[dcm].index.values.astype(str)
        data_prod[dcm] = data_prod[dcm].loc[lat_median, prod]
        data_prod[dcm] = data_prod[dcm]

        data_cmip6[dcm] = pd.read_csv(os.path.join(mg.path_out(),
                                                   'standard_diagnostics_spi',
                                                   expr, dist,
                                                   'bylat_trend_' + str(lag) +\
                                                   '_' + res + '_' + dcm +\
                                                   '_additional.csv'),
                                      index_col = 0, header = [0,1,2])
        data_cmip6[dcm].index = ['%d' % x for x in data_cmip6[dcm].index]
        data_cmip6[dcm] = data_cmip6[dcm].loc[lat_median, :]
        data_cmip6[dcm] = data_cmip6[dcm].reorder_levels([1,2,0], axis = 1)
    model_list = sorted(data_cmip6['0-10cm'].columns.levels[2])

    clist = ['k'] + [cmap(i) for i in np.arange(0.05/len(model_list), 1.,
                                                1./len(model_list))]
    for i, dcm in enumerate(depth_cm_new):
        fig = plt.figure(figsize = (len(year_list) * 3,
                                    36))
        gs = gridspec.GridSpec(12, len(year_list), hspace = 0.1, wspace = 0.05)
        for yind in range(len(year_list)):
            year_str = year_list[yind]
            for sind, ss in enumerate(season_list):
                ax = fig.add_subplot(gs[sind, yind])

                pdata = data_prod[dcm].loc[:, (ss, year_str)]
                cdata = data_cmip6[dcm].loc[:, (ss,
                                                year_str)].loc[:, model_list]
                plot_convenient_taylor(ax, pdata.values, cdata.values,
                                       clist, ['' for m in \
                                               range(len(model_list)+1)])

                if sind == 0:
                    ax.text(0.5, 1.03, year_str, fontsize = 6,
                            transform = ax.transAxes,
                            horizontalalignment = 'center')
                if yind == 0:
                    ax.set_ylabel(season_list_name[sind])
                else:
                    ax.set_yticklabels([])
                ax.set_title('(' + lab[sind] + str(yind) + ')',
                             loc = 'left')
        fig.savefig(os.path.join(mg.path_out(), 'figures',
                                 'fig1_trend_' + str(lag) + '_' + res + \
                                 '_bymodel', 'taylor_' + dist + '_' + \
                                 dcm + suffix + '.png'),
                    dpi = 600., bbox_inches = 'tight')
        plt.close(fig)
        ##break
    ##break
