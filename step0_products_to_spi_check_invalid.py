"""
2020/01/16
Check for weird values in products SPI.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm, depth, target_lat, \
    target_lon, year_cmip6
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib import cm


res = '0.5'


period = pd.date_range('1951-01-01', '2016-12-31', freq = 'YS')


#for s, month in it.product([3, 6, 12], range(12)):
def calc(option):
    i, m, s, month = option
    d = depth[i]
    dcm = depth_cm[i]

    print(i)
    print(m)

    data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'SPI_Products',
                                        m + '_' + dcm + '_' + str(s) + \
                                        '_' + res + '_' + str(month) + '.nc'))

    spi = data['spi'].values.copy()
    data.close()

    # Check invalid values.
    invalid = np.isnan(spi)

    ts_invalid = np.sum(np.sum(invalid, axis = 2), axis = 1)
    map_invalid = np.sum(invalid, axis = 0)

    fig = plt.figure(figsize = (12, 12))
    ax = fig.add_subplot(211)
    ax.plot(period.year.values + (period.month.values-0.5)/12, ts_invalid)
    ax.set_xticks(period.year[::60])
    ax.set_title('# invalids per time step')

    ax = fig.add_subplot(212, projection = ccrs.PlateCarree())
    ax.coastlines()
    ax.set_extent([-180, 180, -60, 90])
    h = ax.contourf(data.lon.values, data.lat.values, map_invalid,
                    corner_mask = False)
    plt.colorbar(h, ax = ax, shrink = 0.7)

    fig.savefig(os.path.join(mg.path_intrim_out(), 'SPI_Products', 'check',
                             m + '_' + dcm + '_' + str(s) + '_' + res + \
                             '_' + str(month) + '.png'), dpi = 600.)
    plt.close(fig)


p = mp.Pool(4)
p.map_async(calc, list(it.product(range(4), product_list, [3,6,12],
                                  range(12))))
p.close()
p.join()
