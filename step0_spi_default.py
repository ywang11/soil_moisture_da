"""
Show the default normal distribution used when the historical variability
  in soil moisture is too low to fit meaningful SPI.
"""
from scipy.stats import norm
import matplotlib.pyplot as plt
import numpy as np
import os
import utils_management as mg


loc = 0
scale = 1 # 0.10551038946821095


fig, axes = plt.subplots(1, 2)

x = np.linspace(norm.ppf(0.0001, loc, scale),
                norm.ppf(0.9999, loc, scale), 100)

axes[0].plot(x, norm.pdf(x, loc, scale), 'r-', lw=5, alpha=0.6)
axes[0].set_title('norm pdf')
axes[0].set_xlim([0,1])
axes[1].plot(x, norm.cdf(x, loc, scale),'r-', lw=5, alpha=0.6)
axes[1].set_title('norm cdf')
axes[1].set_xlim([0,1])

fig.savefig(os.path.join(mg.path_intrim_out(), 'spi_default.png'))
plt.close(fig)
