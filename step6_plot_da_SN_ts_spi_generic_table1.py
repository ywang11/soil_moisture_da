"""
20200515
ywang254@utk.edu

Determine whether the product passes the threshold of virtually certain 
 (S/N > 2.57, 99% confidence) (time of emergence of signal)
(  likely (S/N > 0.95, detectable at 66% confidence)
   very likely (S/N > 1.64, 90% confidence)  )

The thresholds follow Marvel et al. 2019 DOI: 10.1038/s41586-019-1149-8

The EOF is flipped already in the fingerprint calculation step to ensure that 
 the merged product tends toward positive values.

Record the range of significant time periods surronding the highest S/N ratio.
"""
import matplotlib.pyplot as plt
import pandas as pd
import os
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median # depth_cm
from misc.plot_utils import plot_ts_shade, cmap_gen
import numpy as np
import cartopy.crs as ccrs
import xarray as xr
import matplotlib as mpl
from matplotlib import patches
import itertools as it
import multiprocessing as mp
import shutil


#cmap = cmap_gen('Reds_r', 'Blues')
cmap = mpl.cm.get_cmap('nipy_spectral')
mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titleweight'] = 'normal'


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list_list = {'bymonth': [str(i) for i in range(12)],
                    'byyear': ['annual_mean', 'annual_maximum',
                               'annual_minimum',
                               'growing_season_mean', 'growing_season_maximum',
                               'growing_season_minimum']}


# MODIFY:
folder = 'bylat' # 'global', 'hemisphere', 'basins', 'bylat'


#Setup / MODIFY
prod = 'mean_noncmip' # 'mean_products'
prod_name = 'Mean NonCMIP' # 'Mean Products'
folder = 'bylat'
suffix = '_noSahel' # '', '_noSahel'
season_name = 'bymonth'
base_list = ['historical', 'hist-GHG', 'hist-aer']
expr = 'historical'
first_year = 1971
L_range = range(10, 2016 - first_year + 2)
L_range_str = [str(ll) for ll in L_range]


season_list = season_list_list[season_name]


for limit in [True, False]:
    if limit:
        prefix = 'limit_'
    else:
        prefix = ''

    flip = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                    prefix + 'flip_bylat.csv'),
                       index_col = [0,1,2,3])

    # ToM - time of emergence
    summary = pd.DataFrame(data = '                          ',
                           index = pd.MultiIndex.from_product([['gmm',
                                                                'weibull'],
                                                               depth_cm_new,
                                                               season_list]),
                           columns = pd.MultiIndex.from_product( \
                              [[1, 3, 6], base_list]))
    for lag, base, dist, season, dcm in it.product([1,3,6], base_list,
                                                   ['gmm','weibull'],
                                                   season_list, depth_cm_new):
        noise_std = pd.read_csv(os.path.join(mg.path_out(),
                                             'cmip6_spi_noise',
                                             folder, base + '_eofs',
                                             prefix + dist + \
                                             '_trend_' + dcm + \
                                             '_' + str(lag) + '_' + \
                                             season + '_' + res + \
                                             suffix + '.csv'),
                                index_col = 0).loc[:, L_range_str]
        noise_std = np.nanstd(noise_std.values, axis = 0)

## Debug
##        if limit | (lag != 3) | (base != 'historical') | \
##           (dist != 'gmm') | (season != '11') | (dcm != '0-10cm'):
##            continue

        # CMIP6 signals
        signal_hist = pd.read_csv(os.path.join(mg.path_out(),
            'cmip6_spi_signal', folder, base + '_eofs', prefix + \
            dist + '_' + expr + '_signal_' + dcm + '_' + \
            str(lag) + '_' + season + '_' + res + suffix + '.csv'),
                                  header = [0,1] \
        ).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
        signal_hist.index = pd.to_datetime(signal_hist.index).year
        signal_hist.index.name = 'year'
        signal_hist = signal_hist.reorder_levels([1, 0], axis = 1)
        signal_hist = signal_hist.loc[first_year,
                                      :].unstack().loc[:, L_range_str]
        signal_hist = signal_hist * \
            flip.loc[(dcm, int(season), dist, lag), base] \
            / noise_std.reshape(1, -1)
        # ---- need to weight the different models
        temp_model = [x.split('_')[0] for x in signal_hist.index]
        signal_mean = signal_hist.groupby(temp_model,
                                          axis = 0).mean().mean(axis = 0)
        signal_std = signal_hist.std(axis = 0)

        # Products signals
        signal2 = pd.read_csv(os.path.join(mg.path_out(),
            'products_spi_signal', folder, base + '_eofs', prefix + \
            dist + '_signal_' + dcm + '_' + str(lag) + '_' + \
            season + '_' + res + suffix + '.csv'), header = [0,1] \
        ).set_index(('Unnamed: 0_level_0', 'Unnamed: 0_level_1'))
        signal2.index = pd.to_datetime(signal2.index).year
        signal2.index.name = 'year'
        signal2 = signal2.stack().loc[:, L_range_str]
        signal2 = signal2.loc[first_year, :].loc[prod, :]

        #print(signal2)
        signal2 = signal2 * flip.loc[(dcm, int(season), dist, lag),
                                     base] / noise_std.reshape(-1)

        # 1.96 = 95% level, 2.57 = 99% level
        tom = np.array([' ' * 10, ' ' * 10])
        for tt,ff,nn in zip([0,1], [1.96, 2.57], ['95% level', '99% level']):
            ind = (signal2.values >= ff) & \
                (signal2.values <= (signal_mean.values + \
                                    1.96*signal_std.values)) & \
                (signal2.values >= (signal_mean.values - \
                                    1.96*signal_std.values))
            tom[tt] = ''
            if sum(ind) != 0:
                # where first crosses the significance & continue to be so
                # afterwards
                ind2 = np.cumsum(ind[::-1].astype(int))
                for yy in range(len(ind2)):
                    if ind2[yy] != (yy + 1):
                        break
                    else:
                        tom[tt] = str(first_year + L_range[-1] - 1 - yy)
        summary.loc[(dist, dcm, season),
                    (lag, base)] = tom[0] + '(' + tom[1] + ')'

    summary.to_csv(os.path.join(mg.path_out(), 'figures', prefix + \
                                'table1_' + res + suffix + '.csv'))
