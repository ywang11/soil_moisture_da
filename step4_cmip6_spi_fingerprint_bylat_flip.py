"""
20200513
ywang254@utk.edu

For historical, historial-2, hist-GHG, NoNAT, GHGAER, flip if the PC has
  negative trend during 1951-2016. For hist-nat, hist-aer, flip if the
  PC is mostly negative.
"""
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median # depth_cm
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal_bylat, ls_trend
from misc.cmip6_utils import mrsol_availability
from glob import glob
import itertools as it
import time
import multiprocessing as mp
from scipy.stats import linregress, spearmanr


start = time.time()


#
land_mask = 'vanilla'
res = '0.5'
season_list = [str(i) for i in range(12)]
noSahel = True # , False
if noSahel:
    suffix = '_noSahel'
else:
    suffix = ''


for limit in [True, False]:
    if limit:
        prefix = 'limit_'
        expr_list = ['historical', 'historical-2', 'hist-GHG', 'hist-nat',
                     'hist-aer', 'NoNAT', 'GHGAER',
                     'piControl_historical', 'piControl_hist-GHG',
                     'piControl_hist-nat', 'piControl_hist-aer',
                     'piControl_NoNAT', 'piControl_GHGAER']
    else:
        prefix = ''
        expr_list = ['historical', 'historical-2', 'hist-GHG', 'hist-nat',
                     'hist-aer', 'NoNAT', 'GHGAER', 'piControl']

    flip = pd.DataFrame(data = 1, columns = expr_list,
                        index = pd.MultiIndex.from_product( \
        [depth_cm_new, season_list, ['gmm','weibull'], [1,3,6]],
        names = ['depth','season','dist','lag']))
    for base, i, season, dist, lag in it.product(expr_list, [0,1], season_list,
                                                 ['gmm','weibull'], [1, 3, 6]):
        #dcm = depth_cm[i]
        dcm = depth_cm_new[i]

        # Load the principal component
        if limit:
            if 'piControl' in base:
                base0 = base.split('_')[0]
                prefix0 = base.split('_')[1] + '_'
            else:
                base0 = base
                prefix0 = prefix
        else:
            base0 = base
            if 'piControl' in base:
                prefix0 = 'piControl_'
            else:
                prefix0 = prefix
        data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                                        base0, 'bylat', prefix0 + dist + \
                                        '_pc_' + dcm + '_' + str(lag) + \
                                        '_' + season + '_' + res + \
                                        suffix + '.csv'),
                           index_col = 0, parse_dates = True)
        pc = data.loc[:, 'Full']

        #
        #if base0 in ['historical', 'historical-2', 'hist-GHG', 'NoNAT',
        #             'GHGAER']:
        #    slope, _, _, _, _ = linregress(pc.index.year, pc.values)
        #    if slope < 0:
        #        flip.loc[(dcm, season, dist, lag), base] = -1
        #elif base0 == 'hist-aer':
        #    if ((int(season) >= 8) & (dcm == '0-10cm')) | (dcm == '0-100cm'):
        #        slope, _, _, _, _ = linregress(pc.index.year, pc.values)
        #        if slope < 0:
        #            flip.loc[(dcm, season, dist, lag), base] = -1
        #    else:
        #        slopes = np.full(len(pc) - 10, np.nan)
        #        for L in range(10, len(pc)):
        #            slopes[L-10], _, _, _, _ = linregress(pc.index.year[:L],
        #                                                  pc.values[:L])
        #        if np.sum(slopes > 0) < np.sum(slopes < 0):
        #            flip.loc[(dcm, season, dist, lag), base] = -1
        #elif base0 == 'hist-nat':
        #    slopes = np.full(len(pc) - 10, np.nan)
        #    for L in range(10, len(pc)):
        #        slopes[L-10], _, _, _, _ = linregress(pc.index.year[:L],
        #                                              pc.values[:L])
        #    if np.sum(slopes > 0) < np.sum(slopes < 0):
        #        flip.loc[(dcm, season, dist, lag), base] = -1
        if base0 == 'piControl':
            x = pc.index
        else:
            x = pc.index.year
        slope, pvalue = spearmanr(x, pc.values) # linregress(x, pc.values)
        ##if pvalue <= 0.05:
        if slope < 0:
            flip.loc[(dcm, season, dist, lag), base] = -1
        #else:
        #    slopes = np.full(len(pc) - 10, np.nan)
        #    for L in range(10, len(pc)):
        #        slopes[L-10], _ = spearmanr(x[:L], pc.values[:L])
        #    if np.sum(slopes > 0) < np.sum(slopes < 0):
        #        flip.loc[(dcm, season, dist, lag), base] = -1

    flip.to_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                             prefix + 'flip_bylat' + suffix + '.csv'))

    end = time.time()
    print('Script finished in ' + str((end - start) / 60) + ' minutes.')
