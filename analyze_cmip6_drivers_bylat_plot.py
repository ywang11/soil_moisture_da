import os
import numpy as np
import xarray as xr
import matplotlib as mpl
from utils_management.constants import *
import utils_management as mg
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from analyze_cmip6_drivers_map import get_model_list
from analyze_cmip6_drivers_bylat_plot_preliminary import get_sm_trend, get_driver_trend
from misc.plot_utils import cmap_div, stipple
from misc.standard_diagnostics import get_bylat_std
import multiprocessing as mp
import itertools as it
from matplotlib import gridspec


def get_model_vote(cmip6_list):
    temp_model = [x.split('_')[0] for x in cmip6_list]
    temp_vote = np.full(len(temp_model), np.nan)
    for ind, model in enumerate(temp_model):
        n_model = np.sum([x == model for x in temp_model])
        temp_vote[ind] = 1 / n_model
    temp_vote = temp_vote / np.sum(temp_vote)
    return temp_vote


land_mask = 'vanilla'
res = '0.5'
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']
lab = 'abcdefghijklmnopqrstuvwxyz'
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')
lat_median_name.reverse()
alpha_list = [0, 0.01, 0.1, 0.5, 0.9]
L = 45 # 2016 - 1971 + 1
##year_range_list = [range(i, i+L) for i in  [1975, 1995, 2035, 2045, 2050, 2055]] # 2015, 
##expr = 'historical'
year_range_list = [range(1971, 2017)]
expr = 'historical' # MODIFY; 'historical', 'hist-GHG', 'hist-aer'


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6


# for alpha in alpha_list:
alpha = 0.1
for yrng in year_range_list:    
    year_str = str(yrng[0]) + '-' + str(yrng[-1])

    # Average errors
    fig, axes = plt.subplots(2, 3, figsize = (6.5, 6.5), sharex = True, sharey = True)
    fig.subplots_adjust(hspace = 0.05, wspace = 0.1)
    for ii, vind in it.product(range(2), range(3)):
        dcm = depth_cm_new[ii]
        
        cmip6_list = get_model_list(expr, drivers, dcm, land_mask, res)
        temp_vote = get_model_vote(cmip6_list)

        if vind == 0:
            vv = 'r2'
        elif vind == 1:
            vv = 'mse'
        else:
            vv = 'rpd'

        ax = axes[ii, vind]

        metric_collect = np.full([25, 12, len(cmip6_list)], np.nan)
        for jj, model in enumerate(cmip6_list):
            hr = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_drivers_corr', 
                                              'bylat', expr, 'alpha=%.2f' % alpha,
                                              model + '_' + dcm + '_' + year_str + '.nc'))
            metric_collect[:, :, jj] = hr[vv].values
            hr.close()
        metric_collect_mean = np.average(metric_collect, axis = 2, weights = temp_vote)
        metric_collect_std = metric_collect.std(axis = 2)

        if vv == 'r2':
            levels = np.linspace(0., .8, 11)
            levels2 = levels
        elif vv == 'mse':
            levels = np.linspace(0.05, 1.05, 11)
            levels2 = levels
        else:
            levels = np.linspace(0.8, 1.8, 11)
            levels2 = levels * 0.2
        cf = ax.contourf(hr['month'], hr['lat'], metric_collect_mean,
                         cmap = 'Spectral', levels = levels, extend = 'both')
        cf2 = ax.contour(hr['month'], hr['lat'], metric_collect_std,
                         cmap = 'gray', levels = levels2[3::10], linestyles = 'dashed', 
                         linewidths = 0.5)
        ax.clabel(cf2, inline = True, fontsize = 5)

        ax.set_title(vv)
        plt.colorbar(cf, ax = ax, ticks = levels, orientation = 'horizontal')
        ax.text(0., 1.05, lab[vind + ii*3], fontweight = 'bold', transform = ax.transAxes)
        ax.set_xticks(np.linspace(0.35, 10.65, 12))
        ax.set_xticklabels(season_list_name)
        ax.set_yticks([60, 40, 20, 0, -20, -40])
        ax.set_yticklabels(lat_median_name[2::4])
        ax.tick_params('both', length = 1.)
    fig.savefig(os.path.join(mg.path_out(), 'cmip6_drivers_corr',
                             'bylat_plot', expr, 'alpha=%.2f' % alpha,
                             'metrics_' + year_str + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)


    # Average coefficients
    fig, axes = plt.subplots(4, 2, figsize = (6.5, 8.), sharex = True, sharey = True)
    fig.subplots_adjust(hspace = 0.15, wspace = 0.1)
    for ii, vind in it.product(range(2), range(len(drivers))):
        dcm = depth_cm_new[ii]
        cmip6_list = get_model_list(expr, drivers, dcm, land_mask, res)
        temp_vote = get_model_vote(cmip6_list)
        vv = drivers[vind]
        ax = axes[vind, ii]

        coef_collect = np.full([25, 12, len(cmip6_list)], np.nan)
        for jj, model in enumerate(cmip6_list):
            hr = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_drivers_corr', 
                                              'bylat', expr, 'alpha=%.2f' % alpha,
                                              model + '_' + dcm + '_' + year_str + '.nc'))
            coef_collect[:, :, jj] = hr['coef'].values[vind, :, :]
            hr.close()
        coef_collect_mean = np.average(coef_collect, axis = 2, weights = temp_vote)
        coef_collect_std = coef_collect.std(axis = 2)

        if vv == 'pr':
            levels = np.linspace(-1., 1., 11)
            levels2 = levels
        elif vv == 'tas':
            levels = np.linspace(-0.3, 0.3, 11)
            levels2 = levels
        elif vv == 'lai':
            levels = np.concatenate([-np.power(10, np.linspace(1., -2, 20)),
                                     np.array([0]),
                                     np.power(10, np.linspace(-2., 1., 20))])
            levels2 = np.linspace(-2., 2., 11)

            ## mask where the average LAI values were small (does not exist)
            #value_mask = np.full([25, 12, len(cmip6_list)], np.nan)
            #for jj, model in enumerate(cmip6_list):
            #    for season in range(12):
            #        fname = os.path.join(mg.path_out(), 'cmip6_drivers_summary', vv, 
            #                             model + '_' + str(season) + '_' + res + \
            #                             '_g_lat_ts.csv')
            #        _, clim = get_bylat_std(fname, 
            #                                pd.date_range(start = '1971-01-01',
            #                                              end = '2016-12-31', freq = 'YS'))
            #        value_mask[:,season,jj] = clim.values
            #value_mask = value_mask.mean(axis = 2)
            #coef_collect_mean[value_mask < 0.01] = np.nan
            #coef_collect_stid[value_mask < 0.01] = np.nan
        elif vv == 'snw':
            levels = np.linspace(-0.04, 0.04, 11)
            levels2 = levels

            # mask where the average SNW values were small
            value_mask = np.full([25 ,12, len(cmip6_list)], np.nan)
            for jj, model in enumerate(cmip6_list):
                for season in range(12):
                    fname = os.path.join(mg.path_out(), 'cmip6_drivers_summary', vv,
                                         expr, model + '_' + str(season) + '_' + res + \
                                         '_g_lat_ts.csv')
                    _, clim = get_bylat_std(fname,
                                            pd.date_range(start = '1971-01-01',
                                                          end = '2016-12-31', freq = 'YS'))
                    value_mask[:,season,jj] = clim.values
            value_mask = np.average(value_mask, axis = 2, weights = temp_vote)
            coef_collect_mean[value_mask < 0.01] = np.nan
            coef_collect_std[value_mask < 0.01] = np.nan

        cf = ax.contourf(hr['month'], hr['lat'], coef_collect_mean,
                         cmap = cmap_div(), levels = levels, extend = 'both',
                         norm = mpl.colors.BoundaryNorm(levels, 256))
        cf2 = ax.contour(hr['month'], hr['lat'], coef_collect_std,
                         cmap = 'gray_r', levels = levels2[3::10], linestyles = 'dashed',
                         linewidths = 0.5)
        ax.clabel(cf2, inline = True, fontsize = 5)

        if ii == 0:
            ax.set_ylabel(vv)
        if vind == 0:
            ax.set_title(dcm)
        plt.colorbar(cf, ax = ax, ticks = levels)
        ax.text(0., 1.02, lab[vind*2 + ii], fontweight = 'bold', transform = ax.transAxes)
        ax.set_xticks(np.linspace(0.35, 10.65, 12))
        ax.set_xticklabels(season_list_name)
        ax.set_yticks([60, 40, 20, 0, -20, -40])
        ax.set_yticklabels(lat_median_name[2::4])
    fig.savefig(os.path.join(mg.path_out(), 'cmip6_drivers_corr',
                             'bylat_plot', expr, 'alpha=%.2f' % alpha, 
                             'corr_' + year_str + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)

    # Accounted-for trend in soil moisture by trend
    fig = plt.figure(figsize = (6.5, 8.))
    gs = gridspec.GridSpec(1, 2, hspace = 0.2, width_ratios = [0.5, 1])
    gs_1 = gridspec.GridSpecFromSubplotSpec(4, 1, subplot_spec = gs[0],
                                            hspace = 0.1, wspace = 0.)
    gs_2 = gridspec.GridSpecFromSubplotSpec(4, 2, subplot_spec = gs[1],
                                            hspace = 0.1, wspace = 0.02)
    axes = np.empty([4,3], dtype = object)
    for ii in range(4):
        axes[ii,0] = fig.add_subplot(gs_1[ii])
        for jj in range(2):
            axes[ii,jj+1] = fig.add_subplot(gs_2[ii,jj])
    for vind, vv in enumerate(drivers):
        cmip6_list = get_model_list(expr, drivers, '0-10cm', land_mask, res)
        temp_vote = get_model_vote(cmip6_list)

        # (1) Trend in the driver variables
        # The same for '0-10cm' and '0-100cm' anyway.
        trend_collect = np.full([25, 12, len(cmip6_list)], np.nan)
        trend_p_collect = np.full([25, 12, len(cmip6_list)], np.nan)
        for jj, model in enumerate(cmip6_list):
            trend, trend_p = get_driver_trend(expr, vv, model, year_str)
            trend_collect[:, :, jj] = trend
            trend_p_collect[:, :, jj] = trend_p
        trend_collect_mean = np.average(trend_collect, axis = 2, weights = temp_vote)
        trend_collect_pct_same = np.average((trend_collect > 0) == \
                                         (np.broadcast_to(trend_collect_mean[...,np.newaxis],
                                                          trend_collect.shape) > 0), 
                                            axis = 2, weights = temp_vote)
        trend_collect_p_pct = np.average(trend_p_collect <= 0.05, axis = 2,
                                         weights = temp_vote)
        ##if vv == 'tas':
        ##    dummy()

        ax = axes[vind, 0]
        if not year_str == '1971-2016':
            if vv == 'pr':
                levels = np.linspace(-.015, .015, 11)
            elif vv == 'tas':
                levels = np.linspace(-0.15, 0.15, 11)
            elif vv == 'lai':
                levels = np.linspace(-0.012, 0.012, 11)
            elif vv == 'snw':
                levels = np.linspace(-.8, .8, 11)
        else:
            if vv == 'pr':
                levels = np.linspace(-.015, .015, 11)
            elif vv == 'tas':
                levels = np.linspace(-0.08, 0.08, 11)
            elif vv == 'lai':
                levels = np.linspace(-0.006, 0.006, 11)
            elif vv == 'snw':
                levels = np.linspace(-.4, .4, 11)

        if vv == 'snw':
            # mask where the average SNW values were small
            value_mask = np.full([25 ,12, len(cmip6_list)], np.nan)
            for jj, model in enumerate(cmip6_list):
                for season in range(12):
                    fname = os.path.join(mg.path_out(), 'cmip6_drivers_summary', vv,
                                         expr, model + '_' + str(season) + '_' + res + \
                                         '_g_lat_ts.csv')
                    _, clim = get_bylat_std(fname,
                                            pd.date_range(start = '1971-01-01',
                                                          end = '2016-12-31', freq = 'YS'))
                    value_mask[:,season,jj] = clim.values
            value_mask = np.average(value_mask, axis = 2, weights = temp_vote)
            trend_collect_mean[value_mask < 0.01] = np.nan
            trend_collect_pct_same[value_mask < 0.01] = np.nan
            trend_collect_p_pct[value_mask < 0.01] = np.nan

        cf = ax.contourf(trend.columns.astype(int), trend.index.astype(int),
                         trend_collect_mean,
                         cmap = cmap_div(), levels = levels,
                         extend = 'both')

        print(vv)
        ##print(trend_collect_pct_same >= 0.8)
        
        stipple(ax, trend.index.astype(int), trend.columns.astype(int),
                trend_collect_pct_same >= 0.9, 
                hatch = '|||')
        ax.contour(trend.columns.astype(int), trend.index.astype(int),
                   trend_collect_p_pct >= 0.5, 
                   levels = [0.5], colors = 'grey', linestyles = 'dashed', 
                   linewidths = 0.5)
        ax.contourf(trend.columns.astype(int), trend.index.astype(int),
                    trend_collect_p_pct >= 0.5, 
                    levels = [0.5, 1.5], colors = 'grey', alpha = 0.36)
        cbar = plt.colorbar(cf, ax = ax, ticks = levels)
        cbar.set_label(' (' + drivers_units[vv] + ' year$^{-1}$)')
        ax.set_ylabel(vv)
        if vind == 0:
            ax.set_title('Drivers trend', pad = 10)
        ax.text(0., 1.02, lab[vind * 3], fontweight = 'bold', transform = ax.transAxes)
        ax.set_xticks(np.linspace(0.35, 10.65, 12))
        if vind == (len(drivers)-1):
            ax.set_xticklabels(season_list_name)
        else:
            ax.set_xticklabels([])
        ax.set_yticks([60, 40, 20, 0, -20, -40])
        ax.set_yticklabels(lat_median_name[2::4])
        ax.tick_params('both', length = 1.)

        # (2) Accounted-for trend in soil moisture given the regression coefficients
        for ii, dcm in enumerate(depth_cm_new):
            cmip6_list = get_model_list(expr, drivers, dcm, land_mask, res)
            temp_vote = get_model_vote(cmip6_list)

            coef_collect = np.full([25, 12, len(cmip6_list)], np.nan)
            for jj, model in enumerate(cmip6_list):
                hr = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_drivers_corr', 
                                                  'bylat', expr, 'alpha=%.2f' % alpha,
                                                  model + '_' + dcm + '_' + year_str + '.nc'))
                coef_collect[:, :, jj] = hr['coef'].values[vind, :, :]
                hr.close()
            trend_coef_mean = np.average(trend_collect * coef_collect, axis = 2,
                                         weights = temp_vote)
            trend_coef_std = np.std(trend_collect * coef_collect, axis = 2)
    
            #
            ax = axes[vind, ii + 1]
            levels = np.linspace(-0.005, 0.005, 11)
            levels2 = np.linspace(-0.016, 0.016, 11)
            cf = ax.contourf(hr['month'], hr['lat'], trend_coef_mean,
                             cmap = cmap_div(), levels = levels,
                             extend = 'both')
            cf2 = ax.contour(hr['month'], hr['lat'], trend_coef_std,
                             cmap = 'gray_r', levels = levels2, linestyles = 'dashed',
                             linewidths = 0.5)
            ax.clabel(cf2, inline = True, fontsize = 5)
    
            if vind == 0:
                ax.set_title('Accounted SSI trend ' + dcm, pad = 10)
            ax.text(0., 1.02, lab[vind * 3 + ii + 1],
                    fontweight = 'bold', transform = ax.transAxes)
            ax.set_xticks(np.linspace(0.35, 10.65, 12))
            if vind == (len(drivers)-1):
                ax.set_xticklabels(season_list_name)
            else:
                ax.set_xticklabels([])
            ax.set_yticks([60, 40, 20, 0, -20, -40])
            ax.set_yticklabels([])
            ax.tick_params('both', length = 1.)
    cax = fig.add_axes([0.94, 0.1, 0.01, 0.8])
    cbar = plt.colorbar(cf, cax = cax, ticks = levels)
    cbar.set_label('(year$^{-1}$)')
    fig.savefig(os.path.join(mg.path_out(), 'cmip6_drivers_corr', 'bylat_plot',
                             expr, 'alpha=%.2f' % alpha,
                             'accounted_trend_' + year_str + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
