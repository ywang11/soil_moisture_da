"""
2019/07/05

ywang254@utk.edu

Map the 1950-2016 trend in soil moisture for the merged products, 
 and merged CMIP6 historical models (with stipple for 90% model agreement,
 not averaging between ensemble simulations, so that models with more ensemble
 members receive more weight.
"""
from utils_management.constants import depth_cm, depth
import utils_management as mg
from misc.plot_utils import plot_map_w_stipple
import numpy as np
import os
import itertools as it
from misc.plot_utils import cmap_gen
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from misc.cmip6_utils import mrsol_availability
import xarray as xr
import multiprocessing as mp


res = '0.5'
season_list = ['annual', 'DJF', 'MAM', 'JJA', 'SON',
               'annual_maximum', 'annual_minimum']
# 'growing_season_mean', 'growing_season_minimum', 'growing_season_maximum'
product_list = ['dolce_lsm', 'dolce_cmip5', 'dolce_cmip6',
                'em_lsm', 'em_cmip5', 'em_cmip6', 'dolce_products']
year_str = '1950-2016'


levels = np.linspace(-0.00031, 0.00031, 21)
cmap = cmap_gen('autumn', 'winter_r')


def plotter(option):
    season, d = option

    fig, axes = plt.subplots(nrows = 4, ncols = 4, figsize = (22, 12),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    axes = axes.T
    ###########################################################################
    # (1) Products
    ###########################################################################
    for i, prod in enumerate(product_list):
        if i < 3:
            ax = axes.flat[i]
        else:
            ax = axes.flat[i+1]
    
        data = xr.open_dataset(os.path.join(mg.path_out(),
                                            'calc_trend_map_products', prod + \
                                            '_' + d + '_' + season + '_' + \
                                            year_str + '.nc'))
        ax, h = plot_map_w_stipple(ax, data.lat.values, data.lon.values,
                                   data['trend'].values,
                                   hatch_mask = (data['p_value'].values <= \
                                                 0.05),
                                   add_cyc = True,
                                   contour_style = {'levels': levels,
                                                    'cmap': cmap,
                                                    'extend': 'both'})
        ax.set_title(prod)
    cax = fig.add_axes([0.15, 0.05, 0.3, 0.02])
    cb = fig.colorbar(h, cax=cax, boundaries = levels, shrink = 0.5,
                      orientation = 'horizontal')
    cb.set_ticks(np.arange(-0.0003, 0.00031, 0.00005))
    cb.set_ticklabels([('%.2f' % x) for x in np.arange(-3., 3.1, 0.5)])
    cb.ax.set_xlabel('1e-4 m$^3$/m$^3$/year')
    ###########################################################################
    # (2) Products - CMIP6 historical ensemble mean
    ###########################################################################
    for i, prod in enumerate(product_list):
        if i < 3:
            ax = axes.flat[len(product_list) + 1 + i]
        else:
            ax = axes.flat[len(product_list) + 2 + i]
    
        data = xr.open_dataset(os.path.join(mg.path_out(),
                                            'calc_trend_map_cmip6',
                                            'ensemble_merge', 'historical_' + \
                                            d + '_' + season + \
                                            '_' + year_str + '.nc'))
        trend0 = data['trend'].values.copy()
        data.close()
    
    
        data = xr.open_dataset(os.path.join(mg.path_out(),
                                            'calc_trend_map_products', prod + \
                                            '_' + d + '_' + season + '_' + \
                                            year_str + '.nc'))
        trend = data['trend'].values.copy()
        data.close()
        
        same_trend = ((trend > 0.) & (trend0 > 0.)) | \
                     ((trend < 0.) & (trend0 < 0.))
    
        ax, h = plot_map_w_stipple(ax, data.lat.values, data.lon.values,
                                   trend - trend0,
                                   hatch_mask = same_trend,
                                   add_cyc = True,
                                   contour_style = {'levels': levels,
                                                    'cmap': 'coolwarm_r',
                                                    'extend': 'both'})
        ax.set_title(prod + ' - CMIP6$_{mean}$')
    cax = fig.add_axes([0.55, 0.05, 0.3, 0.02])
    cb = fig.colorbar(h, cax=cax, boundaries = levels, shrink = 0.5,
                      orientation = 'horizontal')
    cb.set_ticks(np.arange(-0.0003, 0.00031, 0.00005))
    cb.set_ticklabels([('%.2f' % x) for x in np.arange(-3., 3.1, 0.5)])
    cb.ax.set_xlabel('1e-4 m$^3$/m$^3$/year')
    fig.savefig(os.path.join(mg.path_out(), 'plot_map_trend',
                             'compare_products_cmip6_' + season + '_' + \
                             d + '.png'), dpi = 600.,
                bbox_tight = True)
    plt.close(fig)


p = mp.Pool(4)
p.map_async(plotter, list(it.product(season_list, depth)))
p.close()
p.join()
