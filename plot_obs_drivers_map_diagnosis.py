"""
2019/06/12
ywang254@utk.edu

Plot the global maps of interpolated drivers.
"""
import xarray as xr
import sys
import os
import pandas as pd
import matplotlib.pyplot as plt
from utils_management.paths import path_intrim_out
from utils_management.constants import drivers, drivers_obs_set, \
    drivers_obs_time
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import cartopy.crs as ccrs
from glob import glob
import multiprocessing as mp
from matplotlib import gridspec


land_mask = 'vanilla'
res = '0.5'


fig = plt.figure(figsize = (20,18))
gs = gridspec.GridSpec(1, 2, figure = fig)
sub_gs1 = gridspec.GridSpecFromSubplotSpec(5, 1, gs[0])
sub_gs2 = gridspec.GridSpecFromSubplotSpec(5, 1, gs[1])

count = 0
for vv in drivers:
    for obs in drivers_obs_set[vv]:
        filename = sorted(glob(os.path.join(path_intrim_out(), 'Interp_DA',
                                            land_mask, obs,
                                            vv + '_' + res + '*.nc')))
        if land_mask == 'None':
            dc = False
        else:
            dc = True
        # temporary fix
        if vv == 'snw':
            filename = sorted(glob(os.path.join(path_intrim_out(), 'Interp_DA',
                                                'None', obs,
                                                vv + '_' + res + '*.nc')))
            dc = False
        hr = xr.open_mfdataset(filename, decode_times = dc,
                               concat_dim = 'time')
        if land_mask == 'None':
            hr['time'] = drivers_obs_time[obs]
        hr.close()
        ts = pd.DataFrame(hr[vv].mean(dim = ['lat', 'lon']).values,
                          index = hr['time'].to_index())
        clim = hr[vv].mean(dim='time').copy(deep=True).load()
        unit = hr[vv].attrs['units']
        hr.close()

        ax = fig.add_subplot(sub_gs1[count])
        ax.plot(ts.index, ts, '-')
        ax.set_title(vv + ' Time Series ' + unit)

        ax = fig.add_subplot(sub_gs2[count], projection = ccrs.PlateCarree())
        ax.coastlines()
        ax.gridlines()
        h = ax.contourf(clim.lon.values, clim.lat.values, clim.values)
        plt.colorbar(h, ax = ax, shrink = 0.7)
        ax.set_title(vv + ' Map ' + unit)

        count += 1
fig.savefig(os.path.join(path_intrim_out(), 'Interp_DA_plot', 
                         land_mask,
                         'global_drivers_map_obs.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
