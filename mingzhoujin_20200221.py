"""
Some plots for mingzhou's group meeting.
"""
import matplotlib.pyplot as plt
import pandas as pd
import os
import utils_management as mg
from utils_management.constants import depth_cm, lat_median
from misc.plot_utils import plot_ts_shade, cmap_gen
import numpy as np


seasons = ['DJF', 'MAM', 'JJA', 'SON']


########
# Noise
########
fig, axes = plt.subplots(ncols = 2, nrows = 2, figsize = (10, 6))
for ss_ind, ss in enumerate(seasons):
    df = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_noise', 'global',
                                  'historical_eofs',
                                  'L=50_0-10cm_' + ss + '_0.5.csv'),
                     index_col = 0).dropna(axis = 1, how = 'all')
    ax = axes.flat[ss_ind]
    #plot_ts_shade(ax, time = range(df.shape[0]),
    #              ts = {'min': np.min(df, axis = 1),
    #                    'mean': np.mean(df, axis = 1),
    #                    'max': np.max(df, axis = 1)}, ts_col = 'blue')
    ax.plot(df.index.values, df.values, color = 'grey')
    ax.set_title(ss)
fig.savefig(os.path.join(mg.path_out(), 'mingzhoujin_global_noise.png'),
            dpi = 600.)


########
# Signal
########
fig, axes = plt.subplots(ncols = 2, nrows = 2, figsize = (10, 6))
for ss_ind, ss in enumerate(seasons):
    df = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_signal', 'global',
                                  'historical_eofs',
                                  'L=50_0-10cm_' + ss + '_0.5.csv'),
                     index_col = 0, parse_dates = True).dropna(axis = 0)
    ax = axes.flat[ss_ind]
    #plot_ts_shade(ax, time = df.index.year.values,
    #              ts = {'min': np.min(df, axis = 1),
    #                    'mean': np.mean(df, axis = 1),
    #                    'max': np.max(df, axis = 1)}, ts_col = 'blue')
    ax.plot(df.index.year.values, df.values)
    ax.set_title(ss)
    ax.set_xlim([2000, 2020])
    ax.set_xticklabels([str(int(x)) for x in df.index.year.values])
fig.savefig(os.path.join(mg.path_out(), 'mingzhoujin_global_signal.png'),
            dpi = 600.)


########
# Obs
########
fig, axes = plt.subplots(ncols = 2, nrows = 2, figsize = (10, 6))
for ss_ind, ss in enumerate(seasons):
    df = pd.read_csv(os.path.join(mg.path_out(), 'products_signal', 'global',
                                  'historical_eofs',
                                  'L=50_0-10cm_' + ss + '_0.5.csv'),
                     index_col = 0, parse_dates = True).dropna(axis = 0)
    ax = axes.flat[ss_ind]
    #plot_ts_shade(ax, time = df.index.year.values,
    #              ts = {'min': np.min(df, axis = 1),
    #                    'mean': np.mean(df, axis = 1),
    #                    'max': np.max(df, axis = 1)}, ts_col = 'blue')
    ax.plot(df.index.year.values.astype(int), df.values)
    ax.set_title(ss)
    ax.set_xlim([2000, 2020])
    ax.set_xticklabels([str(int(x)) for x in df.index.year.values])
fig.savefig(os.path.join(mg.path_out(), 'mingzhoujin_global_obs.png'),
            dpi = 600.)
