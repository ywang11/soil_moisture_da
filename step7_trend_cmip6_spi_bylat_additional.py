"""
20200901

ywang254@utk.edu

Calculate the linear trend in the annual time series, and in the per month
  time series by latitude band during the full period for each CMIP6 model.

1951-1980, 1971-2000, 1987-2016
"""
import pandas as pd
from utils_management.constants import depth_cm_new, lat_median
import utils_management as mg
from misc.cmip6_utils import mrsol_availability
from misc.standard_diagnostics import get_bylat_trend
import itertools as it
import os
import numpy as np
import multiprocessing as mp


year_str0 = '1950-2016'
res = '0.5'
land_mask = 'vanilla'


# Time period to calculate the trend on - MODIFY, which changes the 
# associated set of land surface models.
##year_range_list = [range(1951, 1981), range(1956, 1986), range(1961, 1991),
##                   range(1966, 1996), range(1971, 2001), range(1976, 2006),
##                   range(1981, 2011), range(1987, 2017)]
##year_range_list = [range(1951, 1976), range(1975, 1986), range(1985, 2017)]
##year_range_list = [range(1951, 1971), range(1971, 1981), range(1981,2017)]
year_range_list = [range(1951, 1971), range(1971, 2017),
                   range(1951, 1976), range(1976, 2017),
                   range(1951, 1981), range(1981, 2017),
                   range(1951, 1986), range(1986, 2017)]
year_list = [str(yr[0]) + '-' + str(yr[-1]) for yr in year_range_list]


#
#for dist, expr, lag, i in it.product(['gmm', 'weibull'],
#                                     ['historical', 'hist-GHG', 'hist-nat',
#                                      'hist-aer', 'GHGAER', 'NoNAT'],
#                                     [1, 3, 6], range(2)):
def calc(option):
    dist, expr, lag, i = option

    dcm = depth_cm_new[i]

    if expr == 'historical':
        a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
        b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
        cmip6_list = list(set(a) & set(b))
    else:
        cmip6_list, _ = mrsol_availability(dcm, land_mask, expr, res)

    #
    trend_collection = pd.DataFrame(data = np.nan, index = lat_median,
        columns = pd.MultiIndex.from_product([cmip6_list, range(12),
                                              year_list]))
    trend_p_collection = pd.DataFrame(data = np.nan, index = lat_median,
        columns = pd.MultiIndex.from_product([cmip6_list, range(12),
                                              year_list]))
    for model, season, yind in it.product(cmip6_list, range(12),
                                          range(len(year_list))):
        yearstr = year_list[yind]
        year = year_range_list[yind]
        time_range = pd.date_range(start = str(year[0])+'-01-01',
                                   end = str(year[-1])+'-12-31', freq = 'YS')

        fname = os.path.join(mg.path_out(), 'cmip6_spi_summary', expr, dist,
                             model + '_' + dcm + '_' + str(lag) + '_' + \
                             str(season) + '_' + res + '_g_lat_ts.csv')
        trend, trend_p = get_bylat_trend(fname, time_range)
        trend_collection.loc[:, (model, season, yearstr)] = trend
        trend_p_collection.loc[:, (model, season, yearstr)] = trend_p

    trend_collection.to_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_spi', expr, dist, 'bylat_trend_' + \
        str(lag) + '_' + res + '_' + dcm + '_additional.csv'))
    trend_p_collection.to_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_spi', expr, dist, 'bylat_trend_p_' + \
        str(lag) + '_' + res + '_' + dcm + '_additional.csv'))


p = mp.Pool(6)
p.map_async(calc, list(it.product(['gmm', 'weibull'],
                                  ['historical', 'hist-GHG', 'hist-nat',
                                   'hist-aer', 'GHGAER', 'NoNAT'],
                                  [1,3,6], range(2))))
p.close()
p.join()
