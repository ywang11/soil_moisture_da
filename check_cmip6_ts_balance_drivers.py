import os
import xarray as xr
import utils_management as mg
import matplotlib.pyplot as plt
from glob import glob
import pandas as pd
import numpy as np
import itertools as it
from matplotlib.cm import get_cmap
from scipy.stats import linregress


model = 'CanESM5_r10i1p1f1'
m = model.split('_')[0]
c = model.split('_')[-1]


box = [(-110, 42.5), (-58.7, -8.7), (3.2, 19.4)] # (lat, lon)


var_list = ['pr', 'evspsbl', 'mrro', 'snw',
            'mrsol 0-0.1', 'mrsol 0.1-0.35', 'mrsol 0.35-4.1']
var_list2 = ['pr-evspsbl-mrro', 'pr-evspsbl-mrro-dsnw']


ts = pd.DataFrame(np.nan,
                  index = pd.date_range('1850-01-01', '2014-12-31',
                                                freq = 'MS'),
                  columns = pd.MultiIndex.from_product( \
                    [range(len(box)), var_list] ))
for bind, varname in it.product(range(3), var_list):
    if 'mrsol' in varname:
        hr = xr.open_mfdataset(glob(os.path.join(mg.path_data(), 'CMIP6',
                                                 'historical', 'mrsol', m,
                                                 'mrsol_Emon_' + m + \
                                                 '_historical_' + c + \
                                                 '_*.nc')))
        hr['lon'] = np.where(hr['lon'] > 180, hr['lon'] - 360.,
                             hr['lon'])
        var = hr['mrsol'].sortby('lon')
        temp = var.sel(depth = float(varname.split(' ')[-1].split('-')[-1]),
                       lon = box[bind][0], lat = box[bind][1],
                       method = 'nearest')
        ts.loc[:, (bind, varname)] = temp.values.copy()
        hr.close()
    else:
        hr = xr.open_mfdataset(glob(os.path.join(mg.path_data(), 'CMIP6',
                                                 'historical', varname, m,
                                                 varname + '_*_' + m + \
                                                 '_historical_' + c + \
                                                 '_*.nc')))
        hr['lon'] = np.where(hr['lon'] > 180, hr['lon'] - 360.,
                             hr['lon'])
        var = hr[varname].sortby('lon')

        if varname in ['pr', 'evspsbl', 'mrro']:
            var = var * 86400
        
        temp = var.sel(lon = box[bind][0], lat = box[bind][1],
                       method = 'nearest')
        ts.loc[:, (bind, varname)] = temp.values.copy()
        hr.close()


# Plot
dom = ts.index.to_period('M').days_in_month
for bind, vv in it.product(range(len(box)), ['pr', 'evspsbl', 'mrro']):
    ts.loc[:, (bind,vv)] = ts.loc[:, (bind,vv)] * dom * 3
ts2 = ts.groupby(ts.index.to_period('Q')).mean()
ts2 = ts2.loc[ts2.index >= 1950, :]


cmap = get_cmap('Spectral')
clist = [cmap(i/5 + 0.1) for i in range(5)]
h = [None] * 5
season_list = ['DJF', 'MAM', 'JJA', 'SON']


fig, axes = plt.subplots(4, len(box), figsize = (10, 10))
for bind in range(len(box)):
    for aind in range(4):
        ax = axes[aind, bind]

        ts3 = ts2.loc[ts2.index.quarter == (aind + 1), bind]

        h[0], = ax.plot(ts3.index.year, ts3['pr'] - ts3['evspsbl'] - \
                       ts3['mrro'], color = clist[0])
        h[1], = ax.plot(ts3.index.year[1:], ts3['pr'].iloc[1:] - \
                       ts3['evspsbl'].iloc[1:] - \
                       ts3['mrro'].iloc[1:] - \
                       (ts3['snw'].iloc[1:] - ts3['snw'].iloc[:(-1)].values),
                       color = clist[1])
        h[2], = ax.plot(ts3.index.year, ts3['mrsol 0-0.1'],
                       color = clist[2])
        h[3], = ax.plot(ts3.index.year, ts3['mrsol 0.1-0.35'],
                       color = clist[3])
        h[4], = ax.plot(ts3.index.year, ts3['mrsol 0.35-4.1'],
                       color = clist[4])
        ax.set_ylabel(season_list[aind])
        if aind == 0:
            ax.set_title(bind)
ax.legend(h, ['pr-evspsbl-mrro', 'pr-evspsbl-mrro-dsnw',
              'mrsol 0-0.1', 'mrsol 0.1-0.35', 'mrsol 0.35-4.1'],
          loc = (-2, -0.5), ncol = 3)
fig.savefig(os.path.join(mg.path_out(), 'figures',
                         'check_cmip6_ts_balance_drivers.png'), dpi = 600.,
            bbox_inches = 'tight')
plt.close(fig)


# Calculate trend
fig, axes = plt.subplots(5, len(box), figsize = (10, 10)) #,
                         #sharex = True, sharey = True)
for bind in range(len(box)):
    for aind in range(5):
        ax = axes[aind, bind]

        if aind == 0:
            ts3 = ts2.loc[:, bind].groupby(ts2.index.year).mean()
        else:
            ts3 = ts2.loc[ts2.index.quarter == aind, bind]
            ts3.index = ts3.index.year

        trend = [np.nan] * 5
        std_trend = [np.nan] * 5

        slope, _, _, _, std = linregress(ts3.index[1:],
                                         ts3['pr'][1:] - ts3['evspsbl'][1:] - \
                                         ts3['mrro'][1:])
        trend[0] = slope
        std_trend[0] = std

        slope, _, _, _, std = linregress(ts3.index[1:],
                                         ts3['pr'].iloc[1:] - \
                                         ts3['evspsbl'][1:] - \
                                         ts3['mrro'][1:] - \
                                         (ts3['snw'].iloc[1:] - \
                                          ts3['snw'].iloc[:(-1)].values))
        trend[1] = slope
        std_trend[1] = std

        slope, _, _, _, std = linregress(ts3.index[1:],
                                         ts3['mrsol 0-0.1'][1:])
        trend[2] = slope
        std_trend[2] = std

        slope, _, _, _, std = linregress(ts3.index[1:],
                                         ts3['mrsol 0.1-0.35'][1:])
        trend[3] = slope
        std_trend[3] = std

        slope, _, _, _, std = linregress(ts3.index[1:],
                                         ts3['mrsol 0.35-4.1'][1:])
        trend[4] = slope
        std_trend[4] = std

        ax.bar(range(5), trend, color = clist)
        ax.errorbar(range(5), trend, yerr = 1.96 * np.array(std_trend),
                    ecolor = 'k')

        if bind == 0:
            ax.set_ylabel('Annual')
        else:
            ax.set_ylabel(season_list[aind - 1])
        if aind == 0:
            ax.set_title(bind)
        ax.set_xticks(range(5))
        if aind < 3:
            ax.set_xticklabels([])
        else:
            ax.set_xticklabels(['pr-evspsbl-mrro', 'pr-evspsbl-mrro-dsnw',
                                'mrsol 0-0.1', 'mrsol 0.1-0.35',
                                'mrsol 0.35-4.1'], rotation = 90)
fig.savefig(os.path.join(mg.path_out(), 'figures',
                         'check_cmip6_ts_balance_drivers2.png'), dpi = 600.,
            bbox_inches = 'tight')
plt.close(fig)

