"""
20210717

SREX time series of precipitation in the Sahara region.
"""
import regionmask as rmk
import matplotlib.pyplot as plt
import os
import pandas as pd
import xarray as xr
import numpy as np
import utils_management as mg
from utils_management.constants import *
from misc.analyze_utils import decode_month_since
from misc.cmip6_utils import *
from time import time


def avg_box(sm, box):
    sm = sm[:, (sm['lat'] >= box[0]) & (sm['lat'] <= box[1]),
            :][:, :, (sm['lon'] >= box[2]) & (sm['lon'] <= box[3])]
    area_temp = area[(area['lat'] >= box[0]) & (area['lat'] <= box[1]),
                     :][:, (area['lon'] >= box[2]) & (area['lon'] <= box[3])]
    sm = (sm * area_temp).sum(dim = ['lat','lon']) / np.sum(area_temp)
    sm = pd.Series(data = sm.values, index = sm['time'].to_index())
    return sm


# Grid area
hr = xr.open_dataset(os.path.join(mg.path_data(),  'Global_Masks',
                                    'elm_half_degree_grid_negLon.nc'))
area = hr.area.copy(deep = True)
hr.close()


#
land_mask = 'vanilla'
res = '0.5'
bounding_box = [0, 20, -5, 40]
met_list = ['CRU_v4.03', 'CRU_v3.20', 'CRU_v3.26', 'GLDAS_Noah2.0', 'ERA-Interim',
            'ERA5', 'ERA20C', 'CERA20C']


#
a, _ = mrsol_availability('0-10cm', land_mask, 'historical', res)
b, _ = mrsol_availability('0-10cm', land_mask, 'ssp585', res)
c, _ = one_layer_availability('pr', land_mask, 'historical', res)
cmip6_list = sorted(set(a) & set(b))


met_source_all = pd.DataFrame(data = np.nan,
                              index = pd.date_range('1970-01-01', '2016-12-31', freq = 'MS'),
                              columns = cmip6_list)
for nn in cmip6_list:
    flist = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                     'vanilla', 'CMIP6', 'historical', nn, 'pr_*.nc'))) + \
        sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                 'vanilla', 'CMIP6', 'ssp585', nn, 'pr_*.nc')))
    ylist = [int(yy.split('/')[-1].split('_')[-1][:4]) for yy in flist]

    hr = xr.open_mfdataset(flist, decode_times = False)
    var = hr['pr'].copy(deep = True)
    var['time'] = pd.date_range(str(ylist[0]) + '-01-01',
                                str(ylist[-1]) + '-12-31', freq = 'MS')
    var = var[(var['time'].to_index().year >= 1970) & \
              (var['time'].to_index().year <= 2016), :, :]
    hr.close()
    met_source_all.loc[:, nn] = avg_box(var, bounding_box)
met_source_all.to_csv(os.path.join(mg.path_out(),
                                   'source_srex_precip_all.csv'))

