"""
20190909
ywang254@utk.edu

Calculate the first EOF of the model ensemble average of by-latitude average
 soil moisture.

The EOF was 1950-2100 and 1950-2020 for HIST85, and 1950-2020 for the others.

Save the 1st EOF, the 1st PC, and the percent explained variance, with block
 jackknife sampling.

No flipping of the EOF here.
"""
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median, lat_bnds
import os
import xarray as xr
import numpy as np
import pandas as pd
import itertools as it
from misc.jackknife import *
import time


start = time.time()


res = '0.5'
season_list = [str(i) for i in range(12)]
expr_list = ['historical', 'historical-2', 'NoNAT', 'hist-nat',
             'GHGAER', 'hist-aer', 'hist-GHG']
blocksize = 1 # Using 1 leads to strong Jackknife bias
noSahel = True # True, False


if noSahel:
    colname = 'noSahel'
    suffix = '_noSahel'
else:
    colname = 'Normal'
    suffix = ''


col_names = ['Full', 'Jackknife Estimate', 'Jackknife Bias',
             'Jackknife Std Err', 'Jackknife CI_lower', 'Jackknife CI_upper']
#
for limit, expr, i, season, dist, lag in it.product([True, False], \
    expr_list, range(2), season_list, ['gmm','weibull'], [1, 3, 6]):
    #dcm = depth_cm[i]
    dcm = depth_cm_new[i]

    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )[colname].loc[np.array(lat_median).astype(int)].values)

    if limit:
        prefix = 'limit_'
    else:
        prefix = ''

    # read the by-latitude time series
    if expr == 'historical-2':
        expr2 = 'historical'
    else:
        expr2 = expr

    data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_spi_ensemble_merge',
                                    prefix + dist + '_' + expr2 + '_' + \
                                    dcm + '_' + str(lag) + '_' + \
                                    season + '_' + res + suffix + '.csv'),
                       index_col = 0, parse_dates = True)
    data = data.loc[data.index.year >= 1971, :] # 1971, 1951
    if expr == 'historical-2':
        # Reducing the HIST85's length to 2020
        data = data.loc[data.index.year <= 2020, :]

    # Convert to anomalies relative to the merged product climatological
    # period.
    data = data - data.loc[(data.index.year >= 1971) & \
                           (data.index.year <= 2016), :].mean(axis=0).values

    # Ensure the order of the latitudes
    values = data.loc[:, lat_median].values

    # Calculate the fingerprint.
    wgts = lat_area_sqrt / np.mean(lat_area_sqrt)

    eof_0, pc1_0, pct_0 = calc_eof(values, wgts)
    ## check that this has the same effect as above;
    ## also check that pc1_0 = np.matmul(np.matmul(values, np.diag(wgts)),
    ##                                   eof_0)
    ##eof_1, pc1_1, pct_1 = calc_eof(values * wgts, np.ones(len(wgts)))

    estimate, bias, std_err, conf_interval = jackknife_stats_eof(values, wgts,
                                                                 blocksize)

    eof = pd.DataFrame(data = np.nan, index = lat_median,
                       columns = col_names)
    eof.iloc[:, 0] = eof_0
    eof.iloc[:, 1] = estimate[0]
    eof.iloc[:, 2] = bias[0]
    eof.iloc[:, 3] = std_err[0]
    eof.iloc[:, 4] = conf_interval[0][0,:]
    eof.iloc[:, 5] = conf_interval[0][1,:]

    pc1 = pd.DataFrame(data = np.nan, index = data.index, columns = col_names)
    pc1.iloc[:, 0] = pc1_0
    pc1.iloc[:, 1] = estimate[1]
    pc1.iloc[:, 2] = bias[1]
    pc1.iloc[:, 3] = std_err[1]
    pc1.iloc[:, 4] = conf_interval[1][0,:]
    pc1.iloc[:, 5] = conf_interval[1][1,:]

    pct = pd.DataFrame(data = np.nan, index = ['Pct'], columns = col_names)
    pct.iloc[:, 0] = pct_0
    pct.iloc[:, 1] = estimate[2]
    pct.iloc[:, 2] = bias[2]
    pct.iloc[:, 3] = std_err[2]
    pct.iloc[:, 4] = conf_interval[2][0,:]
    pct.iloc[:, 5] = conf_interval[2][1,:]

    eof.to_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                            expr, 'bylat', prefix + dist + '_eof_' + \
                            dcm + '_' + str(lag) + '_' + season + '_' + \
                            res + suffix + '.csv'))
    pc1.to_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                            expr, 'bylat', prefix + dist + '_pc_' + \
                            dcm + '_' + str(lag) + '_' + season + '_' + \
                            res + suffix + '.csv'))
    pct.to_csv(os.path.join(mg.path_out(), 'cmip6_spi_fingerprint',
                            expr, 'bylat', prefix + dist + '_pct_' + dcm + \
                            '_' + str(lag) + '_' + season + '_' + \
                            res + suffix + '.csv'))

end = time.time()
print('Script finished in ' + str((end - start) / 60) + ' minutes.')
