import os
import numpy as np
import xarray as xr
import matplotlib as mpl
from utils_management.constants import *
import utils_management as mg
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from analyze_cmip6_drivers_map import get_model_list
from analyze_cmip6_drivers_bylat_plot_preliminary import get_sm_trend, get_driver_trend
from misc.plot_utils import cmap_div, stipple
from misc.standard_diagnostics import get_bylat_std
import multiprocessing as mp
import itertools as it
from matplotlib import gridspec


def get_model_vote(cmip6_list):
    temp_model = [x.split('_')[0] for x in cmip6_list]
    temp_vote = np.full(len(temp_model), np.nan)
    for ind, model in enumerate(temp_model):
        n_model = np.sum([x == model for x in temp_model])
        temp_vote[ind] = 1 / n_model
    temp_vote = temp_vote / np.sum(temp_vote)
    return temp_vote


land_mask = 'vanilla'
res = '0.5'
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']
lab = 'abcdefghijklmnopqrstuvwxyz'
lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')
lat_median_name.reverse()
alpha_list = [0, 0.01, 0.1, 0.5, 0.9]
L = 45 # 2016 - 1971 + 1
##year_range_list = [range(i, i+L) for i in  [1975, 1995, 2035, 2045, 2050, 2055]] # 2015, 
##expr = 'historical'
year_range_list = [range(1971, 2017)]
expr = 'historical' # MODIFY; 'historical', 'hist-GHG', 'hist-aer'


mpl.rcParams['font.size'] = 16
mpl.rcParams['axes.titlesize'] = 16


# for alpha in alpha_list:
alpha = 0.1
for yrng in [year_range_list[0]]:    
    year_str = str(yrng[0]) + '-' + str(yrng[-1])

    # Accounted-for trend in soil moisture by trend
    fig = plt.figure(figsize = (10, 10))
    gs = gridspec.GridSpec(2, 1, hspace = 0.2, height_ratios = [0.6, 1])
    gs_1 = gridspec.GridSpecFromSubplotSpec(1, 4, subplot_spec = gs[0],
                                            hspace = 0.12, wspace = 0.02)
    gs_2 = gridspec.GridSpecFromSubplotSpec(2, 4, subplot_spec = gs[1],
                                            hspace = 0.12, wspace = 0.02)
    axes = np.empty([3,4], dtype = object)
    for ii in range(4):
        axes[0,ii] = fig.add_subplot(gs_1[ii])
        for jj in range(2):
            axes[jj+1,ii] = fig.add_subplot(gs_2[jj,ii])
    for vind, vv in enumerate(drivers):
        cmip6_list = get_model_list(expr, drivers, '0-10cm', land_mask, res)
        temp_vote = get_model_vote(cmip6_list)

        # (1) Trend in the driver variables
        # The same for '0-10cm' and '0-100cm' anyway.
        trend_collect = np.full([25, 12, len(cmip6_list)], np.nan)
        trend_p_collect = np.full([25, 12, len(cmip6_list)], np.nan)
        for jj, model in enumerate(cmip6_list):
            trend, trend_p = get_driver_trend(expr, vv, model, year_str)
            trend_collect[:, :, jj] = trend
            trend_p_collect[:, :, jj] = trend_p
        trend_collect_mean = np.average(trend_collect, axis = 2, weights = temp_vote)
        trend_collect_pct_same = np.average((trend_collect > 0) == \
                                         (np.broadcast_to(trend_collect_mean[...,np.newaxis],
                                                          trend_collect.shape) > 0), 
                                            axis = 2, weights = temp_vote)
        trend_collect_p_pct = np.average(trend_p_collect <= 0.05, axis = 2,
                                         weights = temp_vote)
        ##if vv == 'tas':
        ##    dummy()

        ax = axes[0, vind]
        if not year_str == '1971-2016':
            if vv == 'pr':
                levels = np.linspace(-.016, .016, 41)
            elif vv == 'tas':
                levels = np.linspace(-0.16, 0.16, 41)
            elif vv == 'lai':
                levels = np.linspace(-0.012, 0.012, 41)
            elif vv == 'snw':
                levels = np.linspace(-.8, .8, 41)
        else:
            if vv == 'pr':
                levels = np.linspace(-.016, .016, 41)
            elif vv == 'tas':
                levels = np.linspace(-0.08, 0.08, 41)
            elif vv == 'lai':
                levels = np.linspace(-0.006, 0.006, 41)
            elif vv == 'snw':
                levels = np.linspace(-.4, .4, 41)

        if vv == 'snw':
            # mask where the average SNW values were small
            value_mask = np.full([25 ,12, len(cmip6_list)], np.nan)
            for jj, model in enumerate(cmip6_list):
                for season in range(12):
                    fname = os.path.join(mg.path_out(), 'cmip6_drivers_summary', vv,
                                         expr, model + '_' + str(season) + '_' + res + \
                                         '_g_lat_ts.csv')
                    _, clim = get_bylat_std(fname,
                                            pd.date_range(start = '1971-01-01',
                                                          end = '2016-12-31', freq = 'YS'))
                    value_mask[:,season,jj] = clim.values
            value_mask = np.average(value_mask, axis = 2, weights = temp_vote)
            trend_collect_mean[value_mask < 0.01] = np.nan
            trend_collect_pct_same[value_mask < 0.01] = np.nan
            trend_collect_p_pct[value_mask < 0.01] = np.nan

        cf = ax.contourf(trend.columns.astype(int), trend.index.astype(int),
                         trend_collect_mean,
                         cmap = cmap_div(), levels = levels,
                         extend = 'both')

        print(vv)
        ##print(trend_collect_pct_same >= 0.8)

        #stipple(ax, trend.index.astype(int), trend.columns.astype(int),
        #        trend_collect_pct_same >= 0.8, 
        #        hatch = '|||')
        ax.contour(trend.columns.astype(int), trend.index.astype(int),
                   trend_collect_p_pct < 0.5, 
                   levels = [0.5], colors = 'grey', linestyles = 'dashed', 
                   linewidths = 0.5)
        ax.contourf(trend.columns.astype(int), trend.index.astype(int),
                    trend_collect_p_pct < 0.5, 
                    levels = [0.5, 1.5], colors = 'w', alpha = 0.36)
        cbar = plt.colorbar(cf, ax = ax, ticks = levels[::20], orientation = 'horizontal',
                            pad = 0.05)
        cbar.ax.tick_params(rotation=50)
        if vv == 'pr':
            ax.set_title(vv + ' trend (mm \nday$^{-1}$ year$^{-1}$)')
        elif vv == 'lai':
            ax.set_title(vv + ' trend\n(year$^{-1}$)')
        else:
            ax.set_title(vv + ' trend\n(' + drivers_units[vv] + ' year$^{-1}$)')
        ax.text(0., 1.03, lab[vind * 3], fontweight = 'bold', transform = ax.transAxes)
        ax.set_xticks(np.linspace(0.35, 10.65, 12))
        ax.set_xticklabels([])
        ax.set_yticks([60, 40, 20, 0, -20, -40])
        if vind == 0:
            ax.set_yticklabels(lat_median_name[2::4])
        else:
            ax.set_yticklabels([])
        ax.tick_params('both', length = 1.)

        # (2) Accounted-for trend in soil moisture given the regression coefficients
        for ii, dcm in enumerate(depth_cm_new):
            cmip6_list = get_model_list(expr, drivers, dcm, land_mask, res)
            temp_vote = get_model_vote(cmip6_list)

            coef_collect = np.full([25, 12, len(cmip6_list)], np.nan)
            for jj, model in enumerate(cmip6_list):
                hr = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_drivers_corr', 
                                                  'bylat', expr, 'alpha=%.2f' % alpha,
                                                  model + '_' + dcm + '_' + year_str + '.nc'))
                coef_collect[:, :, jj] = hr['coef'].values[vind, :, :]
                hr.close()
            trend_coef_mean = np.average(trend_collect * coef_collect, axis = 2,
                                         weights = temp_vote)
            trend_coef_std = np.std(trend_collect * coef_collect, axis = 2)

            #
            ax = axes[ii + 1, vind]
            levels = np.linspace(-0.016, 0.016, 41)
            cf = ax.contourf(hr['month'], hr['lat'], trend_coef_mean,
                             cmap = cmap_div(), levels = levels,
                             extend = 'both')
            cf2 = ax.contour(hr['month'], hr['lat'], trend_coef_std,
                             cmap = 'gray_r', levels = levels[3::10], linestyles = 'dashed',
                             linewidths = 0.5)
            ax.clabel(cf2, inline = True, fontsize = 5)

            if vind == 0:
                ax.set_ylabel(dcm)
            ax.text(0., 1.03, lab[vind * 3 + ii + 1],
                    fontweight = 'bold', transform = ax.transAxes)
            ax.set_xticks(np.linspace(0.35, 10.65, 12))
            if ii == 1:
                ax.set_xticklabels(season_list_name)
            else:
                ax.set_xticklabels([])
            ax.set_yticks([60, 40, 20, 0, -20, -40])
            if vind == 0:
                ax.set_yticklabels(lat_median_name[2::4])
            else:
                ax.set_yticklabels([])
            ax.tick_params('both', length = 1.)
    cax = fig.add_axes([0.93, 0.1, 0.01, 0.4])
    cbar = plt.colorbar(cf, cax = cax, label = 'Accounted SSI trend (year$^{-1}$)')
    fig.savefig(os.path.join(mg.path_out(), 'figures', 
                             'slides_accounted_trend_' + year_str + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
