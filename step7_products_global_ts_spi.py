import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, target_lat, target_lon
import itertools as it
from misc.cmip6_utils import mrsol_availability
from misc.analyze_utils import get_ndvi_mask
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
import sys


res = '0.5'
land_mask = 'vanilla'
ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])
season_list = [str(m) for m in range(12)]


flist = []
for dcm in depth_cm_new:
    flist.append('mean_lsm_' + dcm)
    flist.append('dolce_lsm_' + dcm)
    flist.append('em_lsm_' + dcm)
    flist.append('em_cmip5_' + dcm)
    flist.append('em_cmip6_' + dcm)
    flist.append('em_2cmip_' + dcm)
    flist.append('em_all_' + dcm)
    flist.append('mean_noncmip_' + dcm)
    flist.append('mean_products_' + dcm)


noSahel = True # True, False
if noSahel:
    ndvi_mask = get_ndvi_mask(land_mask)
    suffix = '_noSahel'
else:
    suffix = ''


# km^2 of land in each grid cell.
data = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                    'elm_half_degree_grid_negLon.nc'))
if noSahel:
    land_area = data.area.where(ndvi_mask).values * data.landfrac.values
else:
    land_area = data.area.values * data.landfrac.values
data.close()


#
for lag, dist in it.product([1,3,6], ['gmm', 'weibull']):
    year = range(1951, 2017)
    time = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                         freq = 'MS')
    #for prod in flist:
    def calc(prod):
        spi = np.full([len(year) * 12, len(target_lat[res]),
                       len(target_lon[res])], np.nan)
        for sind, season in enumerate(season_list):
            fn = os.path.join(mg.path_intrim_out(), 'SPI_Products',
                              'get_' + ref_period_str, dist,
                              prod + '_' + str(lag) + '_' + \
                              res + '_' + season + '.nc')
            data = xr.open_dataset(fn, decode_times = False)
            if noSahel:
                spi[sind::12, :, :] = data.spi.where(ndvi_mask).values.copy()
            else:
                spi[sind::12, :, :] = data.spi.values.copy()
            data.close()

        g_ts = np.nansum(spi * land_area.reshape(1, spi.shape[1],
                                                 spi.shape[2]),
                         axis=(1,2)) / np.nansum(land_area)
        pd.DataFrame(g_ts.reshape(-1,1), index = time,
                     columns = ['global_mean'] \
        ).to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_spi',
                              'products', dist, prod.split('_')[0] + \
                              '_' + prod.split('_')[1] + '_' + \
                              str(lag) + '_' + \
                              res + '_' + prod.split('_')[2] + \
                              '_g_ts' + suffix + '.csv'))

    p = mp.Pool(4)
    p.map_async(calc, flist)
    p.close()
    p.join()
