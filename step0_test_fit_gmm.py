import numpy as np
import itertools as it
from scipy.stats import norm, gamma, exponweib
from sklearn.mixture import GaussianMixture
import matplotlib.pyplot as plt
import utils_management as mg
import os


# Number of samples per component
n_samples = 500

# Generate random sample, two components
np.random.seed(0)
C1 = norm.rvs(loc = -1, scale = 3, size = n_samples)
C2 = norm.rvs(loc = 8, scale = 1.7, size = n_samples)
X = np.concatenate([C1, C2]).reshape(-1,1)

# Fit a Gaussian mixture with EM using five components
gmm = GaussianMixture(n_components=2, random_state = 999).fit(X)
params = {'weights': gmm.weights_, 'means': gmm.means_,
          'cov': gmm.covariances_}

#
fig, ax = plt.subplots()
y, x = np.histogram(X, bins=50, normed=True)
x = x[1:]
hb = ax.bar(x, y, width = np.mean(np.diff(x)))

#
pdf = params['weights'][0] * norm.pdf(x, loc = params['means'][0],
                                      scale = np.sqrt(params['cov'][0])) + \
      params['weights'][1] * norm.pdf(x, loc = params['means'][1],
                                      scale = np.sqrt(params['cov'][1]))
pdf = pdf.reshape(-1,1)

#
h, = ax.plot(x, pdf, '-r')

fig.savefig(os.path.join(mg.path_intrim_out(), 'temp.png'), dpi = 600.)
plt.close(fig)
