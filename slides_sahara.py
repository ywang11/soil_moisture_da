"""
20210713

ywang254@utk.edu

Plot the precipitation for Sahara.
"""
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import matplotlib as mpl
import regionmask as rmk
import utils_management as mg
from utils_management.constants import *
from scipy import signal
from scipy.stats import linregress
from matplotlib.patches import Rectangle
import cartopy.crs as ccrs


def df_trend(df):
    trends = pd.DataFrame(np.nan, index = df.columns, columns = ['trend', 'trend_p'])
    for col in df.columns:
        res = linregress(df.index, df[col])
        trends.loc[col, 'trend'] = res.slope
        trends.loc[col, 'trend_p'] = res.pvalue
    return trends


scaler = 1. # multiple the soil moisture values
met_to_lsm = ['CRU_v4.03', 'CRU_v3.20', 'CRU_v3.26', 'GLDAS_Noah2.0', 'ERA-Interim',
              'ERA5', 'ERA20C', 'CERA20C']
bounding_box = [0, 20, -5, 40]
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'
yrng_list = [range(1970, 2011), range(1981, 2017), range(1970, 2017)]
year_str_list = [str(yrng[0]) + '-' + str(yrng[-1]) for yrng in yrng_list]
cmap = mpl.cm.get_cmap('jet')
clist = dict([(met, cmap((ii+0.5)/len(met_to_lsm))) \
              for ii, met in enumerate(sorted(met_to_lsm))])


# Read dataset
prod_set = pd.read_csv(os.path.join(mg.path_out(), 'met_srex_precip_all.csv'),
                       index_col = 0, parse_dates = True) * scaler
source_set = pd.read_csv(os.path.join(mg.path_out(), 'source_srex_precip_all.csv'),
                         index_col = 0, parse_dates = True) * scaler


mpl.rcParams['font.size'] = 16
fig = plt.figure(figsize = (8, 2))
axes = np.empty(2, dtype = object)
axes[0] = fig.add_axes([0., 0., 0.3, 1.], projection = ccrs.PlateCarree())
axes[1] = fig.add_axes([0.4, 0., 0.65, 1.])

ax = axes[0]
ax.coastlines()
ax.set_extent([-30, 100, -30, 40])
rect = Rectangle(xy = (bounding_box[2], bounding_box[0]),
                 width = bounding_box[3], height = bounding_box[1])
ax.add_patch(rect)
ax.text(0.02, 0.15, lab[0], transform = ax.transAxes, fontweight = 'bold')


ax = axes[1]

prod = prod_set.groupby(prod_set.index.year).mean()
prod_trends = {}
for yrng in yrng_list:
    year_str = str(yrng[0]) + '-' + str(yrng[-1])
    prod_trends[year_str] = df_trend(prod.loc[yrng, :].dropna(axis = 1, how = 'any'))

source = source_set.groupby(source_set.index.year).mean()
source_trends = df_trend(source)
source_trends = source_trends.loc[[ii for ii in source_trends.index \
                                   if not ii in prod.columns], :]

count = 0
labels = []
for year_str in year_str_list:
    ax.bar(count + np.arange(prod_trends[year_str].shape[0]), 
           prod_trends[year_str]['trend'],
           color = [clist[met] for met in prod_trends[year_str].index])
    labels.extend([met.replace('_',' ') for met in prod_trends[year_str].index])
    ax.axvline(count + prod_trends[year_str].shape[0] - 0.5, color = 'k')
    ax.text((count + 1) / 13, 1.05, year_str,
             transform = ax.transAxes)
    if count == 0:
        ax.text(0.2, 1.2, 'Precipitation trend (mm year$^{-1}$)', transform = ax.transAxes)
    count += prod_trends[year_str].shape[0]

ax.boxplot(source_trends['trend'], positions = [count], whis = [0, 100],
           widths = 0.8)
labels.append('ALL\n1970-2016')
ax.set_xticks(range(count + 1))
ax.set_xticklabels(labels, rotation = 90)
ax.text(0.02, 0.88, lab[1], transform = ax.transAxes, fontweight = 'bold')
    
fig.savefig(os.path.join(mg.path_out(), 'figures', 'slide_srex_met.png'), 
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
