###############################################################################
# Calculate and plot the global mean time series.
###############################################################################
cd '/lustre/haven/user/ywang254/Soil_Moisture/output_product/concat_em_lsm'


import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import xarray as xr
import numpy as np
import pandas as pd
import os

period = {'1950-2010': pd.date_range('1950-01-01', '2010-12-31', freq = 'MS'),
          '1981-2010': pd.date_range('1981-01-01', '2010-12-31', freq = 'MS'),
          '1981-2016': pd.date_range('1981-01-01', '2016-12-31', freq = 'MS')}

# Anomaly
for year in ['1950-2010', '1981-2010', '1981-2016']:
    fname = 'anomaly_CRU_v4.03_year_month_anomaly_9grid_10-30cm_predicted_' + year + '.nc'
    data = xr.open_dataset(fname, decode_times = False)
    var = data['predicted'].mean(dim = ['lat', 'lon'])
    data.close()

    # Create the figure, get the panel (ax)
    fig, ax = plt.subplots()
    ax.plot(period[year], var.values)

    fig.savefig('anomaly_' + year + '.png')

# Scaled
for year in ['1950-2010', '1981-2010']:
    fname = 'scaled_CRU_v4.03_year_month_anomaly_9grid_10-30cm_predicted_' + year + '.nc'
    data = xr.open_dataset(fname, decode_times = False)
    var = data['predicted'].mean(dim = ['lat', 'lon'])
    data.close()

    # Create the figure, get the panel (ax)
    fig, ax = plt.subplots()
    ax.plot(period[year], var.values)

    fig.savefig('scaled_' + year + '.png')

# Restored
fig, ax = plt.subplots()

for year in ['1950-2010', '1981-2010']:
    fname = 'restored_CRU_v4.03_year_month_anomaly_9grid_10-30cm_predicted_' + year + '.nc'
    data = xr.open_dataset(fname, decode_times = False)
    var = data['predicted'].mean(dim = ['lat', 'lon'])
    data.close()

    # Create the figure, get the panel (ax)
    ax.plot(period[year], var.values, label = 'restored_' + year)

data = xr.open_mfdataset([os.path.join('/lustre/haven/user/ywang254/Soil_Moisture/output_product/',
           'em_lsm_corr', 'CRU_v4.03_year_month_restored_9grid_' + \
           '10-30cm_1981-2016', 'predicted_' + str(yr) + \
           '.nc') for yr in range(1981,2017)],
         decode_times = False, concat_dim = 'time')
var = data['predicted'].mean(dim = ['lat', 'lon'])
ax.plot(period['1981-2016'], var.values, label = 'original_1981-2016')
data.close()

ax.legend()
fig.savefig('Compare_concat.png')
