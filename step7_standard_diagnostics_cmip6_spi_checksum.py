"""
Check whether the standard diagnostics have been run for all the CMIP6
 models.

Output: gap between the number of produced model ensemble members and 
        numbers needed.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new
from misc.cmip6_utils import mrsol_availability
import itertools as it
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
from glob import glob


lag_list = [1,3,6]
res = '0.5'
land_mask = 'vanilla'


# ---= Number of files produced by standard_diagnostics
nfile_per_diag = 13


expr_list = ['historical', 'hist-GHG', 'hist-aer', 'hist-nat', 'GHGAER',
             'NoNAT']


gap = pd.DataFrame(data = np.nan, index = expr_list,
                   columns = pd.MultiIndex.from_product([lag_list,
                                                         ['gmm','weibull'],
                                                         depth_cm_new]))
for expr, lag, dist, i in it.product(expr_list, lag_list,
                                     ['gmm','weibull'], range(2)):
    dcm = depth_cm_new[i]

    if expr == 'historical':
        a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
        b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
        cmip6_list = list(set(a) & set(b))
    else:
        cmip6_list, _ = mrsol_availability(dcm, land_mask, expr, res)
    
    cmip6_list = sorted(cmip6_list)

    nexpected = len(cmip6_list)

    nfiles = len(glob(os.path.join(mg.path_out(), 'standard_diagnostics_spi',
                                   expr, dist,
                                   '*_' + str(lag) + '_' + res + '_' + \
                                   dcm + '_*'))) // nfile_per_diag

    gap.loc[expr, (lag, dist, dcm)] = nexpected - nfiles

gap.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_spi',
                        'checksum.csv'))


# Fragment of code to examine which model is missing
if False:
    which = []
    for m_v in cmip6_list:
        flist = glob(os.path.join(mg.path_out(), 'standard_diagnostics_spi',
                                  expr, dist,
                                  m_v + '_' + str(lag) + '_' + res + \
                                  '_' + dcm + '_*'))
        if len(flist) != nfile_per_diag:
            which.append(m_v)
