"""
20210707

Calculate the EOF of the drivers of soil moisture.
There is enough models to add these drivers into emergent constraint.
"""
import utils_management as mg
from utils_management.constants import *
import os
import xarray as xr
import numpy as np
import pandas as pd
import itertools as it
from misc.jackknife import *
import time
from misc.cmip6_utils import one_layer_availability, mrsol_availability


start = time.time()


res = '0.5'
season_list = [str(i) for i in range(12)]
expr = 'hist-aer' # 'historical', 'hist-GHG', 'hist-aer'
noSahel = True # True, False
col_names = ['Full', 'Jackknife Estimate', 'Jackknife Bias',
             'Jackknife Std Err', 'Jackknife CI_lower', 'Jackknife CI_upper']
land_mask = 'vanilla'
blocksize = 1


if noSahel:
    suffix = '_noSahel'
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['noSahel'].loc[np.array(lat_median).astype(int)].values)
else:
    suffix = ''
    lat_area_sqrt = np.sqrt(pd.read_csv(os.path.join(mg.path_out(),
                                                     'bylat_area.csv'),
                                        index_col = 0 \
    )['Normal'].loc[np.array(lat_median).astype(int)].values)


for vv in drivers:
    if expr == 'historical':
        tvec = pd.date_range(start = '1951-01-01', end = '2100-12-31', freq = 'YS')
        a, _ = one_layer_availability(vv, land_mask, 'historical', res)
        b, _ = one_layer_availability(vv, land_mask, 'ssp585', res)
        c, _ = mrsol_availability('0-10cm', land_mask, 'historical', res)
        d, _ = mrsol_availability('0-10cm', land_mask, 'historical', res)
        cmip6_list = list(set(a) & set(b) & set(c) & set(d))
    else:
        tvec = pd.date_range(start = '1951-01-01', end = '2020-12-31', freq = 'YS')
        a, _ = one_layer_availability(vv, land_mask, expr, res)
        b, _ = mrsol_availability('0-10cm', land_mask, expr, res)
        cmip6_list = list(set(a) & set(b))

    # Calculate the ensemble average
    (unique, counts) = np.unique([model.split('_')[0] for model in cmip6_list], 
                                 return_counts = True)
    model_counts = dict(zip(unique, counts))
    for season in range(12):
        for ii, model in enumerate(cmip6_list):
            nn = model_counts[model.split('_')[0]]
            data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_drivers_summary', vv,
                                            expr, model + '_' + str(season) + '_' + res + \
                                            '_g_lat_ts' + suffix + '.csv'),
                               index_col = 0, parse_dates = True)
            data = data.loc[data.index.year >= 1971, :] # 1971, 1951
            if ii == 0:
                data_weighted = data / nn
            else:
                data_weighted = data_weighted + data / nn
        data_weighted = data_weighted / len(unique)

        # Convert to anomalies relative to the merged product climatological
        # period.
        data_weighted = data_weighted - data_weighted.loc[(data_weighted.index.year >= 1971) & \
                               (data_weighted.index.year <= 2016), :].mean(axis=0).values

        # Ensure the order of the latitudes
        values = data_weighted.loc[:, lat_median].values

        # Calculate the fingerprint.
        wgts = lat_area_sqrt / np.mean(lat_area_sqrt)

        eof_0, pc1_0, pct_0 = calc_eof(values, wgts)
        ## check that this has the same effect as above;
        ## also check that pc1_0 = np.matmul(np.matmul(values, np.diag(wgts)),
        ##                                   eof_0)
        ##eof_1, pc1_1, pct_1 = calc_eof(values * wgts, np.ones(len(wgts)))

        estimate, bias, std_err, conf_interval = jackknife_stats_eof(values, wgts,
                                                                     blocksize)

        eof = pd.DataFrame(data = np.nan, index = lat_median,
                           columns = col_names)
        eof.iloc[:, 0] = eof_0
        eof.iloc[:, 1] = estimate[0]
        eof.iloc[:, 2] = bias[0]
        eof.iloc[:, 3] = std_err[0]
        eof.iloc[:, 4] = conf_interval[0][0,:]
        eof.iloc[:, 5] = conf_interval[0][1,:]

        pc1 = pd.DataFrame(data = np.nan, index = data_weighted.index, columns = col_names)
        pc1.iloc[:, 0] = pc1_0
        pc1.iloc[:, 1] = estimate[1]
        pc1.iloc[:, 2] = bias[1]
        pc1.iloc[:, 3] = std_err[1]
        pc1.iloc[:, 4] = conf_interval[1][0,:]
        pc1.iloc[:, 5] = conf_interval[1][1,:]

        pct = pd.DataFrame(data = np.nan, index = ['Pct'], columns = col_names)
        pct.iloc[:, 0] = pct_0
        pct.iloc[:, 1] = estimate[2]
        pct.iloc[:, 2] = bias[2]
        pct.iloc[:, 3] = std_err[2]
        pct.iloc[:, 4] = conf_interval[2][0,:]
        pct.iloc[:, 5] = conf_interval[2][1,:]
    
        eof.to_csv(os.path.join(mg.path_out(), 'cmip6_drivers_fingerprint',
                                expr, 'bylat', vv + '_eof_' + str(season) + suffix + '.csv'))
        pc1.to_csv(os.path.join(mg.path_out(), 'cmip6_drivers_fingerprint',
                                expr, 'bylat', vv + '_pc_' + str(season) + suffix + '.csv'))
        pct.to_csv(os.path.join(mg.path_out(), 'cmip6_drivers_fingerprint',
                                expr, 'bylat', vv + '_pct_' + str(season) + suffix + '.csv'))

end = time.time()
print('Script finished in ' + str((end - start) / 60) + ' minutes.')
