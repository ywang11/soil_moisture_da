"""
20210708
ywang254@utk.edu

Flip if the PC has negative trend during 1951-2016.
"""
import utils_management as mg
from utils_management.constants import depth_cm_new, lat_median, drivers
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal_bylat, ls_trend
from misc.cmip6_utils import mrsol_availability
from glob import glob
import itertools as it
import time
import multiprocessing as mp
from scipy.stats import linregress, spearmanr


start = time.time()


#
land_mask = 'vanilla'
res = '0.5'
season_list = [str(i) for i in range(12)]
expr_list = ['historical', 'hist-GHG', 'hist-aer']
flip = pd.DataFrame(data = 1, columns = expr_list,
                    index = pd.MultiIndex.from_product([season_list, drivers]))
suffix = '_noSahel'
for expr, season, vv in it.product(expr_list, season_list, drivers):
    data = pd.read_csv(os.path.join(mg.path_out(), 'cmip6_drivers_fingerprint',
                                    expr, 'bylat', vv + '_pc_' + str(season) + suffix + '.csv'),
                       index_col = 0, parse_dates = True)
    pc = data.loc[:, 'Full']
    x = pc.index.year
    slope, pvalue = spearmanr(x, pc.values) # linregress(x, pc.values)
    if slope < 0:
        flip.loc[(season, vv), expr] = -1

flip.to_csv(os.path.join(mg.path_out(), 'cmip6_drivers_fingerprint',
                         'flip_bylat' + suffix + '.csv'))

end = time.time()
print('Script finished in ' + str((end - start) / 60) + ' minutes.')
