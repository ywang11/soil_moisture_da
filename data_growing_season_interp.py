"""
2019/12/25
ywang254@utk.edu

Interp the growing season mask.
"""
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
from rasterio.fill import fillnodata
import cartopy.crs as ccrs
import utils_management as mg
import os


data = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                    'Zhu_globalMonthlyGS.nc'))
temp = data['growing_season'].values.copy()
data.close()

#
mask = np.sum(temp > 0.5,  axis = 0) > 0

# No season.
temp = np.where(mask, temp, np.nan)


############################
# Applying mask after interpolation rather excludes more points,
# because the NDVI < 0.11 regions have many high latitude grids.
# ---- excluding ocean points
data0 = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                                     'merge_vanilla.nc'))
#mask = np.where(data0['mask'].values < 0.5, np.nan, mask)
mask[data0['mask'].values < 0.5] = np.nan
data0.close()
# ---- exclude NDVI < 0.11 points
data0 = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                                     'ndvi_mask_0.5_0.11.nc'))
#mask = np.where(data0['mask'].values < 0.5, np.nan, mask)
mask[data0['mask'].values < 0.5] = np.nan
data0.close()
############################


# Interpolate to no season. Increasing the pts beyond 10 seems to have no 
# effect.
temp2 = temp.copy()
for month in range(12):
    temp2[month, :, :] = fillnodata(temp2[month, :, :], mask = mask,
                                    max_search_distance = 10,
                                    smoothing_iterations = 0)

temp2[temp2 > 1e-3] = 1.
temp2[temp2 <= 1e-3] = 0.

xr.DataArray(temp2, coords = {'month': data['month'],
                              'lat': data['lat'], 'lon': data['lon']},
             dims = ['month', 'lat', 'lon'] \
             ).to_dataset(name = 'growing_season' \
             ).to_netcdf(os.path.join(mg.path_data(), 'Global_Masks',
                                      'Zhu_globalMonthlyGS_interp.nc'))
