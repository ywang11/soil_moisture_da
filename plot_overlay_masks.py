"""
20190108

Plot the overlaid continents, riverbasins, and CONUS areas.
"""
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib import cm
import os
import utils_management as mg
import numpy as np


def plotter(mask, ax, colorbar = False, cm_label = False, alpha = 1,
            cmap = 'Spectral'):
    eps = 1e-6

    mask_levels = np.sort(np.unique(mask[~np.isnan(mask)]))

    if len(mask_levels) == 1:
        mask_levels_bounds = np.array([mask_levels[0]-0.5, mask_levels[0],
                                       mask_levels[0]+0.5])
    else:
        mask_levels_bounds = np.append(mask_levels - \
                                       (mask_levels[1]-mask_levels[0])/2,
                                       [mask_levels[-1] + \
                                        (mask_levels[-1]-mask_levels[-2])/2])

    mask_cyc, lon_cyc = add_cyclic_point(mask, coord=data0.lon)
    cf = ax.contourf(lon_cyc, data0.lat, mask_cyc, cmap = cmap,
                     levels = mask_levels_bounds, alpha = alpha,
                     antialiased = True)

    if colorbar:
        cb = plt.colorbar(cf, ax = ax, boundaries = mask_levels_bounds,
                          shrink = 1.1, orientation = 'horizontal')
        cb.set_ticks(mask_levels)

        return ax, cf, cb

    if cm_label:
        lon2d, lat2d = np.meshgrid(data0.lon.values, data0.lat.values)
        for mk_ind, mk in enumerate(mask_levels):
            mask_subset = (np.abs(mask - mk) < eps).astype(int)
            x = np.average(lon2d, weights = mask_subset)
            y = np.average(lat2d, weights = mask_subset)
            ax.text(x, y, '%d' % mk, fontdict = {'fontsize': 16,
                                                 'fontweight': 'bold'})
        return ax, cf

    return ax, cf


fig, ax = plt.subplots(figsize = (12, 8),
                       subplot_kw = {'projection': ccrs.PlateCarree()})
map_extent = [-180, 180, -60, 90]
ax.coastlines()
ax.set_extent(map_extent)
# (1)
data0 = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'da_masks',
                                     'natural_earth_continent_mask_0.5.nc'))
mask = data0['region'].values.copy()
mask[np.abs(mask - 2) < 1e-6] = np.nan
data0.close()
ax, cf = plotter(mask, ax, False, True, alpha = 1., cmap = 'YlGnBu')
# (2)
data0 = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'da_masks',
                                     'US_0.5.nc'))
mask = np.where(~np.isnan(data0['region'].values), 1, np.nan)
mask = np.where(data0.lon > -140., mask, np.nan)
data0.close()
ax, cf = plotter(mask, ax, False, False, alpha = 1, cmap = 'Greys_r')
# (3)
data0 = xr.open_dataset(os.path.join(mg.path_data(), 'Mao_basins',
                                     'basins_half.nc'))
mask = data0['basin'].values
data0.close()
ax, cf, cb = plotter(mask, ax, True, False, alpha = 0.6, cmap = 'plasma')
cb.ax.set_ylabel('Basin')


#
gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                  linewidth=1, color='gray', alpha=0.5, linestyle='--')
gl.xlabels_top = False
gl.ylabels_right = False
gl.xlocator = mticker.FixedLocator(np.arange(-180, 181, 40.))
gl.ylocator = mticker.FixedLocator(np.arange(-90., 91., 10.))
gl.xformatter = LONGITUDE_FORMATTER
gl.yformatter = LATITUDE_FORMATTER
gl.xlabel_style = {'color': 'black', 'weight': 'bold', 'size': 10}
gl.ylabel_style = {'color': 'black', 'weight': 'bold', 'size': 10}
ax.text(-0.07, 0.55, 'Latitude', va='bottom', ha='center',
        rotation='vertical', rotation_mode='anchor',
        transform=ax.transAxes)
ax.text(0.5, -0.2, 'Longitude', va='bottom', ha='center',
        rotation='horizontal', rotation_mode='anchor',
        transform=ax.transAxes)


fig.savefig(os.path.join(mg.path_intrim_out(), 'plot_overlay_masks.png'),
            dpi = 600.)
plt.close(fig)
