#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1:ppn=16,walltime=2:00:00


#####!/bin/bash -l
#####SBATCH --qos=regular
#####SBATCH --time=1
#####SBATCH --nodes=1
#####SBATCH --constraint=haswell


###module load ncl
###source /global/project/projectdirs/cmip6/software/anaconda_envs/load_latest_e3sm_unified.sh

cd ~/Git/soil_moisture_da/interpolation/

module unload anaconda3
conda deactivate
#module load ncl
