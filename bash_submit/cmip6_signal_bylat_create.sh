create=0
if [ ${create} -eq 1 ]
then
    for i in historical hist-GHG hist-aer
    do
	for j in True False
	do
	    for k in 0 1
	    do
		cat cmip6_signal_bylat_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > cmip6_signal_bylat_${i}_${j}_${k}.sh
		cat ../step4_cmip6_spi_signal_bylat.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > ../cmip6_signal_bylat_${i}_${j}_${k}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in historical hist-GHG hist-aer
    do
        for j in True False
        do
            for k in 0 1
            do
		rm cmip6_signal_bylat_${i}_${j}_${k}.sh
		rm ../cmip6_signal_bylat_${i}_${j}_${k}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in historical hist-GHG hist-aer
    do
        for j in False True 
        do
            for k in 0 1
            do
		qsub cmip6_signal_bylat_${i}_${j}_${k}.sh
	    done
	done
    done
fi
