create=0
if [ ${create} -eq 1 ]
then
    for i in {0..1}
    do
        for j in {0..11}
	do
            for k in {0..18}
	    do
	        cat step4_cmip6_spi_noise_bygrid_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > step4_cmip6_spi_noise_bygrid_${i}_${j}_${k}.sh
		cat ../step4_cmip6_spi_noise_bygrid.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > ../step4_cmip6_spi_noise_bygrid_${i}_${j}_${k}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..1}
    do
        for j in {0..11}
	do
            for k in {0..18}
	    do
		rm step4_cmip6_spi_noise_bygrid_${i}_${j}_${k}.sh
		rm ../step4_cmip6_spi_noise_bygrid_${i}_${j}_${k}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..1}
    do
        for j in {0..11}
	do
            for k in {0..18}
	    do
		qsub step4_cmip6_spi_noise_bygrid_${i}_${j}_${k}.sh
	    done
	done
    done
fi
