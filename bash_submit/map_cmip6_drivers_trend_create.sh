create=0
if [ ${create} -eq 1 ]
then
    for i in {0..3}
    do
        for j in {0..12}
	do
            for k in 0 # {0..6}
	    do
		for m in historical # historical hist-GHG hist-aer
		do
	            cat map_cmip6_drivers_trend_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ | sed s/REPLACE4/${m}/ > map_cmip6_drivers_trend_${i}_${j}_${k}_${m}.sh
		    cat ../map_cmip6_drivers_trend.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ | sed s/REPLACE4/${m}/ > ../map_cmip6_drivers_trend_${i}_${j}_${k}_${m}.py
		done
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..3}
    do
        for j in {0..12}
	do
            for k in {0..6}
	    do
		for m in historical hist-GHG hist-aer
		do
		    rm map_cmip6_drivers_trend_${i}_${j}_${k}_${m}.sh
		    rm ../map_cmip6_drivers_trend_${i}_${j}_${k}_${m}.py
		done
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..3}
    do
        for j in {0..12}
	do
            for k in 0 # {0..6}
	    do
		for m in historical # historical hist-GHG hist-aer
		do
		    qsub map_cmip6_drivers_trend_${i}_${j}_${k}_${m}.sh
		done
	    done
	done
    done
fi
