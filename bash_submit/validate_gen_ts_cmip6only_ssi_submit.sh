#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1:ppn=16,walltime=10:00:00

cd ~/Git/soil_moisture_da
conda activate myenv

ipython -i validate_gen_ts_cmip6only_ssi.py
