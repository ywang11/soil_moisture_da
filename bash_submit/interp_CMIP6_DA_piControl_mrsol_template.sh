#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1:ppn=24,walltime=2:00:00

# Note the 24ppn memory is necessary

#####!/bin/bash -l
####SBATCH --qos=regular
####SBATCH --time=1
####SBATCH --nodes=1
####SBATCH --constraint=haswell

#source /global/project/projectdirs/cmip6/software/anaconda_envs/load_latest_e3sm_unified.sh

cd ~/Git/soil_moisture_da/interpolation/

module unload anaconda3
conda deactivate
