create=0
if [ ${create} -eq 1 ]
then
    for i in {0..72} # {0..72} - historical, {0..16} - hist-GHG, {0..24} hist-aer
    do
	for j in historical # historical, hist-GHG, hist-aer
	do
	    for k in {0..7}
	    do
		cat analyze_cmip6_drivers_bylat_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > analyze_cmip6_drivers_bylat_${i}_${j}_${k}.sh
		cat ../analyze_cmip6_drivers_bylat.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > ../analyze_cmip6_drivers_bylat_${i}_${j}_${k}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..72} # {0..72} - historical, {0..16} - hist-GHG, {0..24} hist-aer
    do
	for j in historical hist-GHG hist-aer
	do
	    for k in {0..7}
	    do
		rm analyze_cmip6_drivers_bylat_${i}_${j}_${k}.sh
		rm ../analyze_cmip6_drivers_bylat_${i}_${j}_${k}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..72} # {0..72} - historical, {0..16} - hist-GHG, {0..24} hist-aer
    do
	for j in historical # historical, hist-GHG, hist-aer
	do
	    for k in {0..7} # {4..7}
	    do
		qsub analyze_cmip6_drivers_bylat_${i}_${j}_${k}.sh
	    done
	done
    done
fi
