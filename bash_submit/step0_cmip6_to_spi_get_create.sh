create=0
if [ ${create} -eq 1 ]
then
    for i in {0..1}
    do
	for expr in historical hist-aer hist-nat hist-GHG piControl
	do
	    for s in 1 3 6
	    do
		for m in {0..17}
		do
		    cat step0_cmip6_to_spi_get_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${expr}/ | sed s/REPLACE3/${s}/ | sed s/REPLACE4/${m}/ > step0_cmip6_to_spi_get_${i}_${expr}_${s}_${m}.sh
		    cat ../step0_cmip6_to_spi_get.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${expr}/ | sed s/REPLACE3/${s}/ | sed s/REPLACE4/${m}/ > ../step0_cmip6_to_spi_get_${i}_${expr}_${s}_${m}.py
		done
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..1}
    do
	for expr in historical piControl hist-aer hist-nat hist-GHG
	do
	    for s in 1 3 6
	    do
		for m in {0..17}
		do
		    rm step0_cmip6_to_spi_get_${i}_${expr}_${s}_${m}.sh
		    rm ../step0_cmip6_to_spi_get_${i}_${expr}_${s}_${m}.py
		done
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..1}
    do
	for expr in historical hist-aer hist-nat hist-GHG piControl
	do
	    for s in 1 3 6
	    do
		for m in {0..17}
		do
		    qsub step0_cmip6_to_spi_get_${i}_${expr}_${s}_${m}.sh
		done
	    done
	done
    done
fi
