create=0
if [ ${create} -eq 1 ]
then
    for i in 0 1
    do
	for j in {0..4}
	do
	    for k in {0..5}
	    do
		for m in gmm weibull
		do
		    cat step7_standard_diagnostics_fut_cmip6_spi_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ | sed s/REPLACE4/${m}/ > step7_standard_diagnostics_fut_cmip6_spi_${i}_${j}_${k}_${m}.sh
		    cat ../step7_standard_diagnostics_fut_cmip6_spi.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ | sed s/REPLACE4/${m}/ > ../step7_standard_diagnostics_fut_cmip6_spi_${i}_${j}_${k}_${m}.py
		done
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in 0 1
    do
        for j in {0..4}
        do
            for k in {0..5}
            do
		for m in gmm weibull
		do
		    rm step7_standard_diagnostics_fut_cmip6_spi_${i}_${j}_${k}_${m}.sh
		    rm ../step7_standard_diagnostics_fut_cmip6_spi_${i}_${j}_${k}_${m}.py
		done
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in 0 1
    do
        for j in {0..4}
        do
            for k in {0..5}
            do
		for m in gmm weibull
		do
		    qsub step7_standard_diagnostics_fut_cmip6_spi_${i}_${j}_${k}_${m}.sh
		done
	    done
	done
    done
fi
