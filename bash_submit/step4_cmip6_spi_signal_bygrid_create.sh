create=0
if [ ${create} -eq 1 ]
then
    for i in {0..1}
    do
        for j in hist-aer hist-GHG historical
	do
            for k in {0..84}
	    do
	        cat step4_cmip6_spi_signal_bygrid_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > step4_cmip6_spi_signal_bygrid_${i}_${j}_${k}.sh
		cat ../step4_cmip6_spi_signal_bygrid.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > ../step4_cmip6_spi_signal_bygrid_${i}_${j}_${k}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..1}
    do
        for j in hist-aer hist-GHG historical
	do
            for k in {0..84}
	    do
		rm step4_cmip6_spi_signal_bygrid_${i}_${j}_${k}.sh
		rm ../step4_cmip6_spi_signal_bygrid_${i}_${j}_${k}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..1}
    do
        for j in hist-aer hist-GHG historical
	do
            for k in {0..84}
	    do
		qsub step4_cmip6_spi_signal_bygrid_${i}_${j}_${k}.sh
	    done
	done
    done
fi
