#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1:ppn=16,walltime=2:00:00

#####!/bin/bash -l
####SBATCH --qos=regular
####SBATCH --time=1
####SBATCH --nodes=1
####SBATCH --constraint=haswell

cd ~/Git/soil_moisture_da/interpolation/

module unload anaconda3
conda deactivate
#module load ncl

# ncl interp_CMIP6_DA_hist_mrsol_REPLACE1_REPLACE2_REPLACE3_REPLACE4.ncl
