#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1:ppn=16,walltime=1:00:00

conda deactivate
activate myenv

cd ~/Git/soil_moisture_da

ipython -i analyze_cmip6_drivers_bylat_plot.py
