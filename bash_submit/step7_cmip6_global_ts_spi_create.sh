create=0
if [ ${create} -eq 1 ]
then
    for i in historical hist-GHG hist-aer hist-nat GHGAER NoNAT
    do
	for j in True False
	do
	    for k in 0 1
	    do
		for m in 1 3 6
		do
		    cat step7_cmip6_global_ts_spi_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ | sed s/REPLACE4/${m}/ > step7_cmip6_global_ts_spi_${i}_${j}_${k}_${m}.sh
		    cat ../step7_cmip6_global_ts_spi.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ | sed s/REPLACE4/${m}/ > ../step7_cmip6_global_ts_spi_${i}_${j}_${k}_${m}.py
		done
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in historical hist-GHG hist-aer hist-nat GHGAER NoNAT
    do
        for j in True False
        do
            for k in 0 1
            do
                for m in 1 3 6
                do
		    rm step7_cmip6_global_ts_spi_${i}_${j}_${k}_${m}.sh
		    rm ../step7_cmip6_global_ts_spi_${i}_${j}_${k}_${m}.py
		done
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in historical hist-GHG hist-aer hist-nat GHGAER NoNAT
    do
        for j in True False
        do
            for k in 0 1
            do
                for m in 1 3 6
                do
		    qsub step7_cmip6_global_ts_spi_${i}_${j}_${k}_${m}.sh
		done
	    done
	done
    done
fi
