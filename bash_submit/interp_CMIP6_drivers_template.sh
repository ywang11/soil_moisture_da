#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1:ppn=16,walltime=2:00:00

cd ~/Git/soil_moisture_da/interpolation/

conda activate base

ncl interp_CMIP6_drivers_REPLACE1_REPLACE2_REPLACE3.ncl
