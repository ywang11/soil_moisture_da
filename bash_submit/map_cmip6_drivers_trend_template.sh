#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1:ppn=16,walltime=6:00:00

cd ~/Git/soil_moisture_da

conda activate myenv

ipython -i map_cmip6_drivers_trend_REPLACE1_REPLACE2_REPLACE3_REPLACE4.py
