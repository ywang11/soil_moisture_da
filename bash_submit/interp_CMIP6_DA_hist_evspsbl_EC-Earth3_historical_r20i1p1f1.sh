###!/bin/bash -l
###SBATCH --qos=regular
###SBATCH --time=1
###SBATCH --nodes=1
###SBATCH --constraint=haswell

###source /global/project/projectdirs/cmip6/software/anaconda_envs/load_latest_e3sm_unified.sh


#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1,walltime=10:00:00

module load ncl

cd ~/Git/soil_moisture_DA/interpolation/

# ncl interp_CMIP6_DA_hist_mrsos_REPLACE1_REPLACE2_REPLACE3.ncl

ncl interp_CMIP6_DA_hist_evspsbl_EC-Earth3_historical_r20i1p1f1.ncl
