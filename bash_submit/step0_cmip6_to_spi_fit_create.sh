create=0
if [ ${create} -eq 1 ]
then
    for i in {0..1}
    do
	for m_ind in {0..17}
	do
	    for s in 1 3 6
	    do
		for month in {0..11}
		do
		    cat step0_cmip6_to_spi_fit_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${m_ind}/ | sed s/REPLACE3/${s}/ | sed s/REPLACE4/${month}/ > step0_cmip6_to_spi_fit_${i}_${m_ind}_${s}_${month}.sh
		    cat ../step0_cmip6_to_spi_fit.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${m_ind}/ | sed s/REPLACE3/${s}/ | sed s/REPLACE4/${month}/ > ../step0_cmip6_to_spi_fit_${i}_${m_ind}_${s}_${month}.py
		done
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..1}
    do
        for m_ind in {0..17}
        do
            for s in 1 3 6
            do
                for month in {0..11}
                do
		    rm step0_cmip6_to_spi_fit_${i}_${m_ind}_${s}_${month}.sh
		    rm ../step0_cmip6_to_spi_fit_${i}_${m_ind}_${s}_${month}.py
		done
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..1}
    do
        for m_ind in {0..17}
        do
            for s in 1 3 6
            do
                for month in {0..11}
                do
		    qsub step0_cmip6_to_spi_fit_${i}_${m_ind}_${s}_${month}.sh
		done
	    done
	done
    done
fi
