create=0
if [ ${create} -eq 1 ]
then
    for i in {0..3}
    do
	for j in 1 2 3
	do
	    for k in 0 # {0..72}
	    do
		cat analyze_cmip6_drivers_map_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > analyze_cmip6_drivers_map_${i}_${j}_${k}.sh
		cat ../analyze_cmip6_drivers_map.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > ../analyze_cmip6_drivers_map_${i}_${j}_${k}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..3}
    do
	for j in 1 2 3
	do
	    for k in {0..72}
	    do
		rm analyze_cmip6_drivers_map_${i}_${j}_${k}.sh
		rm ../analyze_cmip6_drivers_map_${i}_${j}_${k}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..3}
    do
	for j in 1 2 3
	do
	    for k in {0..72}
	    do
		qsub analyze_cmip6_drivers_map_${i}_${j}_${k}.sh
	    done
	done
    done
fi
