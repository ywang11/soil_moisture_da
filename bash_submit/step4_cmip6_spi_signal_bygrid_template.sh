#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1:ppn=16,walltime=8:00:00

cd ~/Git/soil_moisture_da

conda activate myenv

ipython -i step4_cmip6_spi_signal_bygrid_REPLACE1_REPLACE2_REPLACE3.py
