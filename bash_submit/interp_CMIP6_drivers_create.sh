create=0
if [ ${create} -eq 1 ]
then
    for i in pr evspsbl mrro snw
    do
      for j in historical ssp585
      do
        for k in {0..84}
	do
	    cat interp_CMIP6_drivers_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > interp_CMIP6_drivers_${i}_${j}_${k}.sh
	    cat ../interpolation/interp_CMIP6_drivers_template.ncl | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > ../interpolation/interp_CMIP6_drivers_${i}_${j}_${k}.ncl
	done
      done
    done
fi

clean=1
if [ ${clean} -eq 1 ]
then
    for i in pr evspsbl mrro snw
    do
      for j in historical ssp585
      do
        for k in {0..84}
	do
	    rm interp_CMIP6_drivers_${i}_${j}_${k}.sh
	    rm ../interpolation/interp_CMIP6_drivers_${i}_${j}_${k}.ncl
	done
      done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in pr evspsbl mrro snw 
    do
      for j in historical ssp585
      do
        for k in {0..84}
	do
            qsub interp_CMIP6_drivers_${i}_${j}_${k}.sh
	done
      done
    done
fi
