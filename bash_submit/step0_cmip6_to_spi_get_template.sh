#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1:ppn=16,walltime=8:00:00


# 8 hours is necessary for CanESM5, which has 50 ensemble members.


cd ~/Git/soil_moisture_da

ipython -i step0_cmip6_to_spi_get_REPLACE1_REPLACE2_REPLACE3_REPLACE4.py
