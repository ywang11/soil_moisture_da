###!/bin/bash -l
###SBATCH --qos=regular
###SBATCH --time=1
###SBATCH --nodes=1
###SBATCH --constraint=haswell

###source /global/project/projectdirs/cmip6/software/anaconda_envs/load_latest_e3sm_unified.sh


#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1:ppn=16,walltime=10:00:00

conda activate base
module load ncl
cd ~/Git/soil_moisture_da/interpolation/

# ncl interp_CMIP6_DA_mrsos_REPLACE1_REPLACE2_REPLACE3.ncl
