#!/bin/bash
#SBATCH -A ccsi
#SBATCH -p batch
#SBATCH --mem=64g
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -n 16
#SBATCH -t 08:00:00
#SBATCH -o step0_cmip6_to_spi_get_slurm_REPLACE1_REPLACE2_REPLACE3_REPLACE4.o%j
#SBATCH -e step0_cmip6_to_spi_get_slurm_REPLACE1_REPLACE2_REPLACE3_REPLACE4.e%j


# 8 hours is necessary for CanESM5, which has 50 ensemble members.


cd ~/Git/soil_moisture_da

ipython -i step0_cmip6_to_spi_get_REPLACE1_REPLACE2_REPLACE3_REPLACE4.py
