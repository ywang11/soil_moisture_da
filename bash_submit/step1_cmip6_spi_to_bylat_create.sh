create=0
if [ ${create} -eq 1 ]
then
    for i in historical hist-GHG hist-aer hist-nat piControl GHGAER NoNAT
    do
	for j in {0..1} # {0..3}
	do
	    for k in 1 3 6
	    do
		for m in gmm weibull
		do
		    cat step1_cmip6_spi_to_bylat_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ | sed s/REPLACE4/${m}/ > step1_cmip6_spi_to_bylat_${i}_${j}_${k}_${m}.sh
		    cat ../step1_cmip6_spi_to_bylat.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ | sed s/REPLACE4/${m}/ > ../step1_cmip6_spi_to_bylat_${i}_${j}_${k}_${m}.py
		done
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in historical hist-GHG hist-aer hist-nat piControl GHGAER NoNAT
    do
        for j in {0..1} # {0..3}
        do
            for k in 1 3 6
            do
                for m in gmm weibull
                do
		    rm step1_cmip6_spi_to_bylat_${i}_${j}_${k}_${m}.sh
		    rm ../step1_cmip6_spi_to_bylat_${i}_${j}_${k}_${m}.py
		done
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in historical hist-GHG hist-aer hist-nat piControl GHGAER NoNAT
    do
	for j in {0..1}
	do
	    for k in 1 3 6
	    do
		for m in gmm weibull
		do
		    qsub step1_cmip6_spi_to_bylat_${i}_${j}_${k}_${m}.sh
		done
	    done
	done
    done
fi
