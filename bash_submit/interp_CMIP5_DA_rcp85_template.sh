#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1,walltime=06:00:00
#PBS -o interp_CMIP5_DA_rcp85_REPLACE.o$PBS_JOBID

cd $PBS_O_WORKDIR

cd ../interpolation

ncl interp_CMIP5_DA_rcp85_REPLACE.ncl
