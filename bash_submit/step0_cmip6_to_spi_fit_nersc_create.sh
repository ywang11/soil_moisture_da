create=1
if [ ${create} -eq 1 ]
then
    for i in {0..3}
    do
	for m_ind in {0..17}
	do
	    for s in 3 6 12
	    do
		cat step0_cmip6_to_spi_fit_nersc_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${m_ind}/ | sed s/REPLACE3/${s}/ | sed s/REPLACE4/${month}/ > step0_cmip6_to_spi_fit_${i}_${m_ind}_${s}.sh
		cat ../step0_cmip6_to_spi_fit.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${m_ind}/ | sed s/REPLACE3/${s}/ | sed s/REPLACE4/${month}/ > ../step0_cmip6_to_spi_fit_${i}_${m_ind}_${s}.py
	    done
	done
    done
fi


clean=0
if [ ${clean} -eq 1 ]
then
    for i in {0..3}
    do
	for m_ind in {0..17}
	do
	    for s in 3 6 12
	    do
		rm step0_cmip6_to_spi_fit_${i}_${m_ind}_${s}.sh
		rm ../step0_cmip6_to_spi_fit_${i}_${m_ind}_${s}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..3}
    do
	for m_ind in {0..17}
	do
	    for s in 3 6 12
	    do
		sbatch step0_cmip6_to_spi_fit_${i}_${m_ind}_${s}.sh
	    done
	done
    done
fi
