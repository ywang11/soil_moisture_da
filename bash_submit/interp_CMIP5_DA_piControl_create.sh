create=0
if [ ${create} -eq 1 ]
then
    for ind in {0..24}
    do
	cat interp_CMIP5_DA_piControl_template.sh | sed s/REPLACE/${ind}/ > interp_CMIP5_DA_piControl_${ind}.sh
	cat ../interpolation/interp_CMIP5_DA_piControl_template.ncl | sed s/REPLACE/${ind}/ > ../interpolation/interp_CMIP5_DA_piControl_${ind}.ncl
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for ind in {0..24}
    do
	rm interp_CMIP5_DA_piControl_${ind}.sh
	rm ../interpolation/interp_CMIP5_DA_piControl_${ind}.ncl
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for ind in {0..24}
    do
        qsub interp_CMIP5_DA_piControl_${ind}.sh
    done
fi
