create=0
if [ ${create} -eq 1 ]
then
    for i in {0..3}
    do
	for j in hist-GHG hist-aer historical
	do
	    cat aggregate_cmip6_drivers_to_bylat_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > aggregate_cmip6_drivers_to_bylat_${i}_${j}.sh
	    cat ../aggregate_cmip6_drivers_to_bylat.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > ../aggregate_cmip6_drivers_to_bylat_${i}_${j}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..3}
    do
	for j in hist-GHG hist-aer historical
	do
	    rm aggregate_cmip6_drivers_to_bylat_${i}_${j}.sh
	    rm ../aggregate_cmip6_drivers_to_bylat_${i}_${j}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..3}
    do
	for j in hist-GHG hist-aer historical
	do
	    qsub aggregate_cmip6_drivers_to_bylat_${i}_${j}.sh
	done
    done
fi
