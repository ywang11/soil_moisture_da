create=0
if [ ${create} -eq 1 ]
then
    for i in {0..1}
    do
	for m in {0..6}
	do
	    for month in {0..11}
	    do
		cat step0_products_to_spi_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${m}/ | sed s/REPLACE3/${month}/ > step0_products_to_spi_${i}_${m}_${month}.sh
		cat ../step0_products_to_spi.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${m}/ | sed s/REPLACE3/${month}/ > ../step0_products_to_spi_${i}_${m}_${month}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..1}
    do
	for m in {0..6}
	do
	    for month in {0..11}
	    do
		rm step0_products_to_spi_${i}_${m}_${month}.sh
		rm ../step0_products_to_spi_${i}_${m}_${month}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..1}
    do
	for m in {0..6}
	do
	    for month in {0..11}
	    do
		qsub step0_products_to_spi_${i}_${m}_${month}.sh
	    done
	done
    done
fi
