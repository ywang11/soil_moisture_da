#!/bin/bash
#SBATCH -A ccsi
#SBATCH -p batch
#SBATCH --mem=64g
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -n 16
#SBATCH -t 08:00:00
#SBATCH -o step0_products_to_spi_slurm_REPLACE1_REPLACE2_REPLACE3.o%j
#SBATCH -e step0_products_to_spi_slurm_REPLACE1_REPLACE2_REPLACE3.e%j

cd ~/Git/soil_moisture_da

ipython -i step0_products_to_spi_REPLACE1_REPLACE2_REPLACE3.py
