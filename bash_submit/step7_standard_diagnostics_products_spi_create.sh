create=0
if [ ${create} -eq 1 ]
then
    for i in 0 1
    do
	for j in weibull gmm
	do
	    for k in 1 3 6
	    do
		cat step7_standard_diagnostics_products_spi_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > step7_standard_diagnostics_products_spi_${i}_${j}_${k}.sh
		cat ../step7_standard_diagnostics_products_spi.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > ../step7_standard_diagnostics_products_spi_${i}_${j}_${k}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in 0 1
    do
	for j in weibull gmm
	do
	    for k in 1 3 6
	    do
		rm step7_standard_diagnostics_products_spi_${i}_${j}_${k}.sh
		rm ../step7_standard_diagnostics_products_spi_${i}_${j}_${k}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in 0 1
    do
	for j in gmm weibull
	do
	    for k in 1 3 6
	    do
		qsub step7_standard_diagnostics_products_spi_${i}_${j}_${k}.sh
	    done
	done
    done
fi
