create=0
if [ ${create} -eq 1 ]
then
    for ind in {0..95}
    do
	cat interp_CMIP5_DA_historical_template.sh | sed s/REPLACE/${ind}/ > interp_CMIP5_DA_historical_${ind}.sh
	cat ../interpolation/interp_CMIP5_DA_historical_template.ncl | sed s/REPLACE/${ind}/ > ../interpolation/interp_CMIP5_DA_historical_${ind}.ncl
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for ind in {0..95}
    do
	rm interp_CMIP5_DA_historical_${ind}.sh
	rm ../interpolation/interp_CMIP5_DA_historical_${ind}.ncl
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for ind in {0..95}
    do
        qsub interp_CMIP5_DA_historical_${ind}.sh
    done
fi
