"""
2021/02/16
ywang254@utk.edu

Plot the mean and trend in the GLEAM/GLDAS & CMIP6 P-ET.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, target_lat, target_lon
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
import matplotlib as mpl
import matplotlib.patches as mpatches
import utils_management as mg
import itertools as it
from matplotlib import gridspec
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from cartopy.feature import LAND
from misc.analyze_utils import get_ndvi_mask


def get_cmip6_list(varname):
    if varname == 'pr-evspsbl':
        a1, _ = one_layer_availability('pr', land_mask, 'historical', res)
        a2, _ = one_layer_availability('pr', land_mask, 'ssp585', res)
        b1, _ = one_layer_availability('evspsbl', land_mask, 'historical',
                                       res)
        b2, _ = one_layer_availability('evspsbl', land_mask, 'ssp585', res)
        cmip6_list = sorted(list(set(a1) & set(a2) & set(b1) & set(b2)))
    elif varname == 'pr-evspsbl-mrro':
        a1, _ = one_layer_availability('pr', land_mask, 'historical', res)
        a2, _ = one_layer_availability('pr', land_mask, 'ssp585', res)
        b1, _ = one_layer_availability('evspsbl', land_mask, 'historical',
                                       res)
        b2, _ = one_layer_availability('evspsbl', land_mask, 'ssp585', res)
        c1, _ = one_layer_availability('mrro', land_mask, 'historical',
                                       res)
        c2, _ = one_layer_availability('mrro', land_mask, 'ssp585', res)
        cmip6_list = sorted(list(set(a1) & set(a2) & set(b1) & set(b2) & \
                                 set(c1) & set(c2)))
    elif varname == 'pr-evspsbl-mrro-dsnw':
        a1, _ = one_layer_availability('pr', land_mask, 'historical', res)
        a2, _ = one_layer_availability('pr', land_mask, 'ssp585', res)
        b1, _ = one_layer_availability('evspsbl', land_mask, 'historical',
                                       res)
        b2, _ = one_layer_availability('evspsbl', land_mask, 'ssp585', res)
        c1, _ = one_layer_availability('mrro', land_mask, 'historical',
                                       res)
        c2, _ = one_layer_availability('mrro', land_mask, 'ssp585', res)
        d1, _ = one_layer_availability('snw', land_mask, 'historical', res)
        d2, _ = one_layer_availability('snw', land_mask, 'ssp585', res)
        cmip6_list = sorted(list(set(a1) & set(a2) & set(b1) & set(b2) & \
                                 set(c1) & set(c2) & set(d1) & set(d2)))
    elif 'sm' in varname:
        dcm = varname.split('_')[1]
        a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
        b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
        cmip6_list = sorted(list(set(a) & set(b)))
    else:
        a, _ = one_layer_availability(varname, land_mask, 'historical',
                                      res)
        b, _ = one_layer_availability(varname, land_mask, 'ssp585', res)
        cmip6_list = sorted(list(set(a) & set(b)))
    return cmip6_list


def read_model(varname, which, model, season, year_str, noSahel):
    hr = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_drivers_summary',
                                      varname, model + '_' + res + \
                                      '_g_map_' + which + '_' + season + \
                                      '_' + year_str + '.nc'))
    if noSahel:
        var = hr['g_map_' + which].where(ndvi_mask).values.copy()
    else:
        var = hr['g_map_' + which].values.copy()
    hr.close()
    return var


def get_model_vote(model_id_list):
    temp_model = [x.split('_')[0] for x in model_id_list]
    vote_model = np.full(len(model_id_list), np.nan)
    for ind, model in enumerate(temp_model):
        n_model = np.sum([x == model for x in temp_model])
        vote_model[ind] = 1/n_model
    vote_model = vote_model / np.sum(vote_model)
    return vote_model


def get_hatch(val_per_model, vote_per_model, val_mean):
    pct_pos = np.sum((val_per_model > 0.).astype(float) * \
        np.broadcast_to(vote_per_model.reshape(-1,1,1),
                        val_per_model.shape), axis = 0)
    pct_neg = np.sum((val_per_model < 0.).astype(float) * \
        np.broadcast_to(vote_per_model.reshape(-1,1,1),
                        val_per_model.shape), axis = 0)
    hatch1 = ((val_mean > 0) & (pct_pos > pct)) | \
        ((val_mean < 0) & (pct_neg > pct))
    hatch2 = (~hatch1) & \
        (((val_mean > 0) & (pct_pos > (pct - 0.1))) | \
         ((val_mean < 0) & (pct_neg > (pct - 0.1))))
    return hatch1, hatch2


def read_obs(varname, which, lsm, season, year_str, noSahel):
    hr = xr.open_dataset(os.path.join(mg.path_out(), 'obs_drivers_summary',
                                      lsm + '_' + varname + '_' + res + \
                                      '_g_map_' + which + '_' + season + \
                                      '_' + year_str + '.nc'))
    if noSahel:
        var = hr['g_map_' + which].where(ndvi_mask).values.copy()
    else:
        var = hr['g_map_' + which].values.copy()
    hr.close()
    return var


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5.
mpl.rcParams['hatch.linewidth'] = 0.5
cmap = cmap_div() #.reversed()
hs = '/////'
pct = 0.9


res = '0.5'
land_mask = 'vanilla'
lat = target_lat[res]
lon = target_lon[res]
lab = 'abcdefghijklmnopqrstuvwxyz'
expr = 'historical'
expr_name = 'ALL'
prod = 'CDR_LAI'
year_str = '1982-2016'
vv = 'lai'


noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
    land_mask = 'vanilla'
    ndvi_mask = get_ndvi_mask(land_mask)
else:
    ndvi_mask = ''


#
fig = plt.figure(figsize = (6.5, 8.5))
gs = gridspec.GridSpec(2, 3, hspace = 0.2, wspace = 0.02)
for i, which in zip(range(2), ['mean', 'trend']):
    if which == 'mean':
        levels = np.arange(-6., 6.1, 0.03)
    else:
        levels = np.arange(-2.2e-2, 2.21e-2, 1e-3)

    sub_gs1 = gridspec.GridSpecFromSubplotSpec(4, 1,
                                               subplot_spec = gs[i,0],
                                               hspace = 0., wspace = 0.)
    sub_gs2 = gridspec.GridSpecFromSubplotSpec(4, 1,
                                               subplot_spec = gs[i,1],
                                               hspace = 0., wspace = 0.)
    sub_gs3 = gridspec.GridSpecFromSubplotSpec(4, 1,
                                               subplot_spec = gs[i,2],
                                               hspace = 0., wspace = 0.)

    for sind, ss in enumerate(['DJF', 'MAM', 'JJA', 'SON']):
        val0 = read_obs(vv, which, prod, ss, year_str, noSahel)

        cmip6_list = get_cmip6_list(vv)
        val = np.full([len(cmip6_list), len(lat), len(lon)], np.nan)
        for mind, model in enumerate(cmip6_list):
            val[mind, :, :] = read_model(vv, which, model, ss, year_str,
                                         noSahel)
        # ---- calculate the weighted average
        vote_model = get_model_vote(cmip6_list)
        val_mean = np.sum(val * np.broadcast_to(vote_model.reshape(-1,1,1),
                                                val.shape), axis = 0)
        val_std = np.std(val, axis = 0)

        # Plot products
        ax = plt.subplot(sub_gs1[sind],
                         projection = ccrs.PlateCarree())
        fig.add_subplot(ax)
        ax.coastlines(lw = 0.1)
        gl = ax.gridlines(xlocs = [-180, 180],
                          ylocs = np.arange(-40, 61, 5),
                          linestyle = '--',
                          linewidth = 0.5, draw_labels = True)
        gl.xlabels_top = False
        gl.xlabels_bottom = False
        gl.ylabels_right = False
        gl.xlocator = mticker.FixedLocator([-180, 180])
        gl.ylocator = mticker.FixedLocator([-40, -20, 0, 20, 40, 60])
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        gl.xlabel_style = {'color': 'black', 'weight': 'normal'}
        ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.2)
        ax.spines['bottom'].set_lw(0.2)
        ax.spines['left'].set_lw(0.2)
        ax.spines['right'].set_lw(0.2)
        ax.add_feature(LAND, facecolor = [0.9, 0.9, 0.9])
        h = ax.contourf(lon, lat, val0,
                        cmap = cmap, levels = levels, extend = 'both',
                        transform = ccrs.PlateCarree())
        if which == 'trend':
            hatch1 = ((val_mean > 0) & (val0 > 0)) | \
                ((val_mean < 0) & (val0 < 0))
            stipple(ax, lat, lon, mask = hatch1,
                    transform = ccrs.PlateCarree(), hatch = '///////////')
            hatch2 = (~hatch1) & \
                ((val0 < (val_mean - 1.96*val_std)) | \
                 (val0 > (val_mean + 1.96*val_std)))
            stipple(ax, lat, lon, mask = hatch2,
                    transform = ccrs.PlateCarree(), hatch = '\\\\\\\\\\\\')
        ax.text(-0.25, 0.5, ss, fontsize = 5, rotation = 90.,
                verticalalignment = 'center', transform = ax.transAxes)
        ax.text(0.01, 0.05, lab[i*8 + sind],
                transform = ax.transAxes, weight = 'bold')
        if sind == 0:
            ax.text(.5, 1.1, prod.replace('_', ' '), fontsize = 5,
                    horizontalalignment = 'center',
                    transform = ax.transAxes)
            ax.text(-.35, 0., which.capitalize(), rotation = 90, fontsize = 5,
                    verticalalignment = 'center',
                    transform = ax.transAxes)

        # Plot CMIP6
        ax = plt.subplot(sub_gs2[sind], # sind//2, int(np.mod(sind,2))],
                         projection = ccrs.PlateCarree())
        fig.add_subplot(ax)
        ax.coastlines(lw = 0.1)
        gl = ax.gridlines(xlocs = [-180, 180],
                          ylocs = np.arange(-40, 61, 20),
                          linestyle = '--', 
                          linewidth = 0.5, draw_labels = False) # True)
        #gl.xlabels_top = False
        #gl.xlabels_bottom = False
        #gl.ylabels_right = False
        #gl.xlocator = mticker.FixedLocator([-180, 180])
        #gl.ylocator = mticker.FixedLocator([-40, -20, 0, 20, 40, 60])
        #gl.xformatter = LONGITUDE_FORMATTER
        #gl.yformatter = LATITUDE_FORMATTER
        #gl.xlabel_style = {'color': 'black', 'weight': 'normal'}
        ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.2)
        ax.spines['bottom'].set_lw(0.2)
        ax.spines['left'].set_lw(0.2)
        ax.spines['right'].set_lw(0.2)
        ax.add_feature(LAND, facecolor = [0.9, 0.9, 0.9])
        h = ax.contourf(lon, lat, val_mean, cmap = cmap,
                        levels = levels, extend = 'both',
                        transform = ccrs.PlateCarree())
        if which == 'trend':
            hatch1, hatch2 = get_hatch(val, vote_model, val_mean)
            stipple(ax, lat, lon, mask = hatch1,
                    transform = ccrs.PlateCarree(), hatch = '///////////')
            stipple(ax, lat, lon, mask = hatch2,
                    transform = ccrs.PlateCarree(), hatch = '\\\\\\\\\\\\')
        #ax.text(0.5, 1.05, ss, fontsize = 5,
        #        horizontalalignment = 'center',
        #        transform = ax.transAxes)
        ax.text(0.01, 0.05, lab[i*8 + 4 + sind], weight = 'bold',
                transform = ax.transAxes)
        if sind == 0:
            ax.text(.5, 1.1, expr_name, fontsize = 5,
                    horizontalalignment = 'center',
                    transform = ax.transAxes)

        # Plot the difference, no hatch
        ax = plt.subplot(sub_gs3[sind], projection = ccrs.PlateCarree())
        fig.add_subplot(ax)
        ax.coastlines(lw = 0.1)
        gl = ax.gridlines(xlocs = [-180, 180],
                          ylocs = np.arange(-40, 61, 20),
                          linestyle = '--', 
                          linewidth = 0.5, draw_labels = False) # True)
        #gl.xlabels_top = False
        #gl.xlabels_bottom = False
        #gl.ylabels_right = False
        #gl.xlocator = mticker.FixedLocator([-180, 180])
        #gl.ylocator = mticker.FixedLocator([-40, -20, 0, 20, 40, 60])
        #gl.xformatter = LONGITUDE_FORMATTER
        #gl.yformatter = LATITUDE_FORMATTER
        #gl.xlabel_style = {'color': 'black', 'weight': 'normal'}
        ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.2)
        ax.spines['bottom'].set_lw(0.2)
        ax.spines['left'].set_lw(0.2)
        ax.spines['right'].set_lw(0.2)
        ax.add_feature(LAND, facecolor = [0.9, 0.9, 0.9])
        h = ax.contourf(lon, lat, val0 - val_mean, cmap = cmap,
                        levels = levels, extend = 'both',
                        transform = ccrs.PlateCarree())
        ax.text(0.01, 0.05, lab[i*8 + 8 + sind], transform = ax.transAxes,
                weight = 'bold')
        if sind == 0:
            ax.text(.5, 1.1, prod.replace('_', ' ') + \
                    ' - ' + expr_name, fontsize = 5,
                    horizontalalignment = 'center',
                    transform = ax.transAxes)

        if i == 0:
            if sind == 3:
                cax = fig.add_axes([0.1, 0.51, 0.8, 0.01])
                plt.colorbar(h, cax = cax, orientation = 'horizontal',
                             label = 'Mean LAI')
        else:
            if sind == 3:
                cax = fig.add_axes([0.1, 0.08, 0.8, 0.01])
                plt.colorbar(h, cax = cax, orientation = 'horizontal',
                             label = 'Trend in LAI (year$^{-1}$)')

fig.savefig(os.path.join(mg.path_out(), 'figures',
                         'plot_map_drivers_' + vv +
                         '_trend_' + year_str + '.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
