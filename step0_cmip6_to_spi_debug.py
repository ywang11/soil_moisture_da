"""
20200529

ywang254@utk.edu

Convert the merged products historical soil moisture to 1-, 3-, and 6-
 month SSI, following Hao & AghaKouchak 2013 Multivariate Standardized
                      Drought Index: A parametric multi-index model.

Check if there were infinity & too many NaN's.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, depth_new, target_lat, \
    target_lon
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
from misc.spi_utils import *
from scipy.integrate import quad
import sys
from glob import glob


land_mask = 'vanilla'
res = '0.5'

#
ref_expr = 'historical'
if ref_expr == 'piControl':
    ref_period = range(0, 201)
else:
    ref_period = range(1970, 2015) # range(1950, 2014)
ref_period_str = str(ref_period[0]) + '-' + str(ref_period[-1])


for expr,i,dist,s in it.product(['piControl', 'historical', 'hist-nat',
                                 'hist-aer', 'hist-GHG'], range(2),
                                ['weibull', 'gmm'], [1,3,6]):

    d = depth_new[i]
    dcm = depth_cm_new[i]

    # The CMIP6 list of models for individual experiments.
    if expr == 'historical':
        model_list_list = get_model_list(['historical', 'ssp585'], dcm,
                                    land_mask, res)
    else:
        model_list_list = get_model_list([expr], dcm, land_mask, res)

    model_list = []
    for m in model_list_list.keys():
        model_list.extend([m + '_' + r for r in model_list_list[m]])

    for m_v in model_list:
        if expr == 'historical':
            fpath = [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                  land_mask, 'CMIP6', expr, m_v,
                                  'mrsol_' + res + '_' + str(y) + \
                                  '_' + dcm + '.nc') \
                     for y in year_cmip6['historical']] + \
                    [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                  land_mask, 'CMIP6', 'ssp585', m_v,
                                  'mrsol_' + res + '_' + str(y) + \
                                  '_' + dcm + '.nc') \
                     for y in year_cmip6['ssp585']]
        elif expr == 'piControl':
            # Use N+1 year in the calculation, so that the first year
            # can be discarded.
            N = 201
            fpath = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                             land_mask, 'CMIP6', expr, m_v,
                                             'mrsol_' + res + '_*_' + dcm + \
                                             '.nc')),
                           key = lambda x: int(x.split('_')[-2]))
            fpath = fpath[-N:]
        else:
            fpath = [os.path.join(mg.path_intrim_out(), 'Interp_DA',
                                  land_mask, 'CMIP6', expr, m_v,
                                  'mrsol_' + res + '_' + str(y) + \
                                  '_' + dcm + '.nc') \
                     for y in range(1950, 2021)]

        hr = xr.open_mfdataset(fpath, decode_times = False)
        sm = hr['sm'].values.copy()
        hr.close()

        for month in range(12):
            hr = xr.open_dataset( os.path.join( \
                 mg.path_intrim_out(), 'SPI_CMIP6',
                 'get_' + ref_expr + \
                 '_' + ref_period_str, dist, expr,
                 m_v + '_' + dcm + '_' + str(s) + '_' + res + '_' + \
                                                str(month) + '.nc') )
            spi = hr['spi'].values.copy()
            spi_NotNaN = np.sum(~np.isnan(spi)) / spi.shape[0]
            spi_inf = (hr['spi'].values == np.inf) | \
                (hr['spi'].values == -np.inf)
            hr.close()

            if ( np.sum(spi_inf) > 0 ) | (np.abs(spi_NotNaN - 56407) > 1e-3):
                sm_temp = rolling_mean(sm, s, month)[1:, :, :]
    
                f = open(os.path.join(mg.path_intrim_out(), 'SPI_CMIP6',
                                      'debug', expr + '_' + dist + '_' + \
                                      m_v + '_' + dcm + '_' + \
                                      str(s) + '_' + str(month) + '.csv'), 'w')
                f.write('Year_Ind,Lat_Ind,Lon_Ind,Year,Lat,Lon,SPI,SM,' + \
                        'Avg_NotNaN\n')
                a,b,c = np.where(spi_inf)
                for aa,bb,cc in zip(a,b,c):
                    f.write(str(aa) + ',' + str(bb) + ',' + str(cc) + ',' + \
                            str(hr['time'].to_index().year[aa]) + ',' + \
                            ('%.2f' % hr['lat'].values[bb]) + ',' + \
                            ('%.2f' % hr['lon'].values[cc]) + ',' + \
                            str(spi[aa,bb,cc]) + ',' + \
                            ('%.3f' % sm_temp[aa,bb,cc]) + ',' + \
                            spi_NotNaN + '\n')
                f.close()
