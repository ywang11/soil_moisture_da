"""
20191126

Compare the trend between the latitudinal mean soil moisture of CMIP6
 models of HIST85, hist-GHG, and (hist-GHG + hist-aer) for the overlapping
 models.
"""
import matplotlib.pyplot as plt
from misc.da_utils import get_model_list
from scipy.stats import pearsonr
import utils_management as mg
from utils_management.constants import depth_cm
import os
import pandas as pd
import numpy as np
import multiprocessing as mp
import itertools as it


lat_median = [str(x) for x in range(-45, 66, 10)]
season_list = ['annual', 'DJF', 'MAM', 'JJA', 'SON']
land_mask = 'vanilla'
res = '0.5'


#
for dcm, ndvi in it.product(depth_cm, [True, False]):
    if ndvi:
        suffix = '_ndvi_mask'
    else:
        suffix = ''

    cmip6_list = get_model_list(['historical', 'ssp585', 'hist-GHG', 
                                 'hist-aer'], dcm, land_mask, res)

    model_list = list(cmip6_list.keys())

    fig, axes = plt.subplots(nrows = len(model_list), ncols = len(season_list),
                             sharex = True, sharey = True, figsize = (10,10))
    for i, j in it.product(range(len(model_list)), range(len(season_list))):
        model = model_list[i]
        season = season_list[j]

        # (1) historical
        data = pd.read_csv(os.path.join(mg.path_out(), 'sanity_check_trend',
                                        'historical_' + model + '_' + res + \
                                        '_' + season + '_' + dcm + \
                                        suffix + '.csv'), index_col = 0)
        # (2) hist-GHG
        data2 = pd.read_csv(os.path.join(mg.path_out(), 'sanity_check_trend',
                                         'hist-GHG_' + model + '_' + res + \
                                         '_' + season + '_' + dcm + \
                                         suffix + '.csv'), index_col = 0)
        # (3) hist-aer
        data3 = pd.read_csv(os.path.join(mg.path_out(), 'sanity_check_trend',
                                         'hist-aer_' + model + '_' + res + \
                                         '_' + season + '_' + dcm + \
                                         suffix + '.csv'), index_col = 0)

#        h1 = axes[i,j].scatter(data.values, data2.values, c = 'b', s = 1)
#        h2 = axes[i,j].scatter(data.values, data2.values + data3.values, 
#                               c = 'r', s = 1)
#        axes[i,j].plot([-0.5, 0.5], [-0.5, 0.5], '-k')
        h1 = axes[i,j].plot(data.index, data.values.mean(axis = 1), '-k')
        h2 = axes[i,j].plot(data2.index, data2.values.mean(axis = 1), '-r')
        h3 = axes[i,j].plot(data3.index, (data2 + data3).values.mean(axis = 1),
                            '-b')

        if i == 0:
            axes[i,j].set_title(season)
        if i == (len(model_list) - 1):
            axes[i,j].set_xlabel('historical')
        if j == 0:
            axes[i,j].set_ylabel(model)

    axes[i,j].legend([h1[0], h2[0], h3[0]], 
                     ['historical', 'hist-GHG', '(hist-GHG + hist-aer)/2'],
                     ncol = 3, loc = [-3, -0.5])

    fig.savefig(os.path.join(mg.path_out(), 'sanity_check_trend', 
                             'compare_expr_' + dcm + '_' + res + suffix + \
                             '.png'), dpi = 600., bbox_tight = True)
    plt.close(fig)
