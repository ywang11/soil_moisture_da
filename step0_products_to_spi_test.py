"""
20200529

ywang254@utk.edu

Trial code on one product, one grid, one lag, one month.
 
Convert the merged products historical soil moisture to 1-, 3-, 6-, and 
 12-month SSI, following Hao & AghaKouchak 2013 Multivariate Standardized
                         Drought Index: A parametric multi-index model.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth_cm_new, depth_new, target_lat, \
    target_lon
import os
import numpy as np
import pandas as pd
from misc.da_utils import calc_signal, get_model_list
import multiprocessing as mp
import itertools as it
import time
from misc.spi_utils import *
import matplotlib.pyplot as plt
import sys
from scipy.stats import norm, gamma, exponweib


land_mask = 'vanilla'
res = '0.5'



period = pd.date_range('1951-01-01', '2016-12-31', freq = 'YS')


for i, lag, month, prod in it.product([1,0], [3,6,1], range(12),
                                      ['em_all','mean_lsm', 'dolce_lsm', ]):
    d = depth_new[i]
    dcm = depth_cm_new[i]

    # Read merged products data.
    data = xr.open_mfdataset(os.path.join(mg.path_intrim_out(),
                                          'interp_merged_products', prod + \
                                          '_' + d + '_' + res + '.nc'),
                             decode_times = False)
    data_array = data.sm.values.copy()
    data.close()
    
    pos_list = []
    for a,b in it.product(range(data_array.shape[1]),
                          range(data_array.shape[2])):
        if ~np.isnan(data_array[0,a,b]):
            pos_list.append([a,b])
    
    # Take 100 samples
    np.random.seed(i*4 + lag)
    position = [pos_list[int(i)] for i in \
                np.random.randint(len(pos_list), size = 50)]

    for pind, pos in enumerate(position):
        a,b = pos
    
        vector = data_array[:,a,b]
        
        # Convert to moving average
        vector = rolling_mean(vector.reshape(-1,1), lag, month)
        
        # Remove small negative values
        vector[(vector > -1) & (vector <= 0.)] = 1e-8
        
        # Remove too large positive values
        vector[(vector >= (1. - 1e-8))] = 1. - 1e-8
        
        # Estimate the empirical pdf using 1950-2014 as the reference period.
        vector = vector[:-2, :]
        vector = vector[np.isfinite(vector[:, 0]), :]
        
        #
        fig, axes = plt.subplots(3, 2, figsize = (10, 10))
        for ind, dist in enumerate(['gamma', 'weibull', 'gmm']):
            start = time.time()
            params, _ = fit_samples(vector, dist, 0)
            zs = get_zscore(params, dist, vector)
            end = time.time()
            print( str(end - start)  + ' seconds.' )
        
            ax = axes[ind, 0]
            y, x = np.histogram(vector, bins=10, normed=True)
            x = x[1:]
            hb = ax.bar(x, y, width = np.mean(np.diff(x)))
            if dist == 'gamma':
                pdf = gamma.pdf(x, a = params['shape'],
                                loc = params['loc'], scale = params['scale'])
            elif dist == 'weibull':
                pdf = exponweib.pdf(x, a = params['a'], c = params['shape'],
                                    loc = params['loc'],
                                    scale = params['scale'])
            elif dist == 'gmm':
                pdf = params['weights'][0] * norm.pdf(x,
                      loc = params['means'][0], scale = params['sd'][0]) + \
                      params['weights'][1] * norm.pdf(x,
                      loc = params['means'][1], scale = params['sd'][1])
                pdf = pdf.reshape(-1,1)
            h, = ax.plot(x, pdf, '-r')
            ax.legend([hb, h], ['Histogram', 'Fitted distribution'])
        
            ax = axes[ind, 1]
            ax.plot(vector, zs, 'o')
            ax.set_xlabel('Data')
            ax.set_ylabel('Z-score')
            ax.text(0.01, 0.85, ('log-lik = %.3f' % params['ll']),
                    fontsize = 6, transform = ax.transAxes)
        
        fig.savefig(os.path.join(mg.path_intrim_out(), 'SPI_Products', 'test',
                                 prod + '_' + str(lag), 
                                 'test_' + str(month) + '_' + d + '_' + \
                                 str(pind) + '.png'),
                    dpi = 600., bbox_inches = 'tight')
        plt.close(fig)
