"""
Check the completeness of the downloaded ensemble members of the drivers.
"""
import numpy as np
import os
from glob import glob
import utils_management as mg
from utils_management.constants import drivers
from misc.cmip6_utils import mrsol_availability
import pandas as pd
import itertools as it


land_mask = 'vanilla'
res = '0.5'
a, _ = mrsol_availability('0-10cm', land_mask, 'historical', res)
b, _ = mrsol_availability('0-10cm', land_mask, 'ssp585', res)
cmip6_list = sorted(set(a) & set(b))
cmip6_list = sorted(cmip6_list + ['CESM2_r4i1p1f1', 'CESM2_r10i1p1f1',
                                  'CESM2_r11i1p1f1'])
var_list = ['pr', 'evspsbl', 'mrro', 'snw',
            'pr-evspsbl', 'pr-evspsbl-mrro', 'pr-evspsbl-mrro-dsnw',
            'sm_0-10cm', 'sm_0-100cm']


check_exist = pd.DataFrame(data = ' ' * 20, index = cmip6_list, 
                           columns = pd.MultiIndex.from_product([var_list,
                                                                 ['1951-2014',
                                                                  '1971-2014',
                                                                  '1981-2014']\
                           ]))
for year_str,cc,vv in it.product(['1951-2014', '1971-2014', '1981-2014'],
                                 cmip6_list, var_list):
    flist = glob(os.path.join(mg.path_out(), 'cmip6_drivers_summary',
                              vv, cc + '_' + res + '_g_map_trend_*_' + \
                              year_str + '.nc'))
    if len(flist) == 5:
        check_exist.loc[cc, (vv, year_str)] = 'T'
    else:
        check_exist.loc[cc, (vv, year_str)] = 'F'

with pd.ExcelWriter(os.path.join(mg.path_out(), 'cmip6_drivers_summary',
                                 'check_data_drivers_interp.xlsx')) as writer:
    for vv in var_list:
        tt = check_exist.loc[:, vv]
        tt.to_excel(writer, sheet_name = vv)
