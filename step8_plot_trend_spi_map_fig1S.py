"""
20200425
ywang254@utk.edu

Plot the trend in the Mean Products and ALL SPI.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from utils_management.constants import depth_cm_new, lat_median
from misc.plot_utils import plot_ts_shade, cmap_div, stipple
from misc.analyze_utils import get_ndvi_mask
import matplotlib as mpl
import matplotlib.patches as mpatches
import utils_management as mg
import itertools as it
from matplotlib import gridspec
from misc.cmip6_utils import mrsol_availability
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from cartopy.feature import LAND
import seaborn as sns


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5.
mpl.rcParams['hatch.linewidth'] = 0.5


res = '0.5'
lat_eof = [float(x) for x in lat_median]
season_list = [str(i) for i in range(12)]
lab = 'abcdefghijklmnopqrstuvwxyz'


folder = 'bylat'
lag = 3
noSahel = True # True, False
if noSahel:
    suffix = '_noSahel'
    land_mask = 'vanilla'
    ndvi_mask = get_ndvi_mask(land_mask)
else:
    ndvi_mask = ''


expr = 'historical' # 'hist-GHG' # 'historical', 'hist-aer'
expr_name = 'ALL' # 'GHG' # 'ALL'
prod = 'mean_noncmip' # 'mean_products'
prod_name = 'Mean NonCMIP' # 'Mean Products'


if prod == 'mean_noncmip':
    limit = False
    prefix = ''
else:
    limit = True
    prefix = 'limit_'


lat_median_name = []
for i in lat_median:
    if '-' in i:
        lat_median_name.append(i.replace('-','') + '$^o$S')
    else:
        lat_median_name.append(i + '$^o$N')


season_list = [str(i) for i in range(12)] # ensure order of x-axis
season_list_name = ['J','F','M','A','M','J','J','A','S','O','N','D']


cmap = cmap_div()
#cmap = sns.diverging_palette(240, 10, as_cmap = True)
#cmap = mpl.colors.ListedColormap(sns.color_palette('colorblind'))
levels = [-0.04, -0.032, -0.024, -0.016, -0.008, 0., 0.008, 0.016, 0.024, 0.032, 0.04] #np.arange(-4.2e-2, 4.21e-2, 0.009) # 1e-3)
hs = '/////'
pct = 0.9


for dist in ['gmm', 'weibull']:
    fig = plt.figure(figsize = (6., 8.))
    gs = gridspec.GridSpec(2, 2, hspace = 0.05, wspace = 0.02)

    for i, dcm in enumerate(depth_cm_new):
        sub_gs1 = gridspec.GridSpecFromSubplotSpec(4, 1,
                                                   subplot_spec = gs[i,0],
                                                   hspace = 0., wspace = 0.)
        sub_gs2 = gridspec.GridSpecFromSubplotSpec(4, 1,
                                                   subplot_spec = gs[i,1],
                                                   hspace = 0., wspace = 0.)

        for sind, ss in enumerate(['DJF', 'MAM', 'JJA', 'SON']):
            #
            data = xr.open_dataset(os.path.join(mg.path_out(),
                                                'standard_diagnostics_spi',
                                                'products', dist,
                                                prod + '_' + str(lag) + '_' + \
                                                res + '_' + dcm + \
                                                '_g_map_trend_' + ss + '.nc'))
            if noSahel:
                val0 = data['g_map_trend'].where(ndvi_mask).values.copy()
            else:
                val0 = data['g_map_trend'].values.copy()
            data.close()

            #
            if expr == 'historical':
                a, _ = mrsol_availability(dcm, 'vanilla', 'historical', res)
                b, _ = mrsol_availability(dcm, 'vanilla', 'ssp585', res)
                cmip6_list = sorted(list(set(a) & set(b)))
            else:
                cmip6_list, _ = mrsol_availability(dcm, 'vanilla', expr, res)
            if limit:
                c, _ = mrsol_availability(dcm, 'vanilla', 'piControl', res)
                cmip6_list = list(set(c) & set(cmip6_list))
            val = np.full([len(cmip6_list), len(data.lat), len(data.lon)],
                          np.nan)
            for mind, model in enumerate(cmip6_list):
                data = xr.open_dataset(os.path.join(mg.path_out(),
                    'standard_diagnostics_spi', expr, dist, model + '_' + \
                    str(lag) + '_' + res + '_' + dcm + '_g_map_trend_' + \
                                                    ss + '.nc'))
                if noSahel:
                    val[mind, :, :] = data['g_map_trend' \
                    ].where(ndvi_mask).values.copy()
                else:
                    val[mind, :, :] = data['g_map_trend'].values.copy()
                data.close()

            temp_model = [x.split('_')[0] for x in cmip6_list]
            vote_model = np.full(len(cmip6_list), np.nan)
            for ind, model in enumerate(temp_model):
                n_model = np.sum([x == model for x in temp_model])
                vote_model[ind] = 1/n_model
            vote_model = vote_model / np.sum(vote_model)
    
            val_mean = np.sum(val * np.broadcast_to(vote_model.reshape(-1,1,1),
                                                    val.shape), axis = 0)
            val_std = np.std(val, axis = 0)

            ###################################################################
            # Plot products
            ###################################################################
            ax = plt.subplot(sub_gs1[sind], # sind//2, int(np.mod(sind,2))],
                             projection = ccrs.PlateCarree())
            fig.add_subplot(ax)
            #if sind == 0:
                #if dcm == '0-10cm':
                #    ax.add_patch(mpatches.Rectangle(xy = [0, 5],
                #                                    width = 55, height = 25,
                #                                    facecolor = [0,0,0,0],
                #                                    edgecolor = 'k',
                #                                    lw = 1.,
                #                                    transform = \
                #                                    ccrs.PlateCarree()))
                #else:
                #    ax.add_patch(mpatches.Rectangle(xy = [0, 15],
                #                                    width = 55, height = 15,
                #                                    facecolor = [0,0,0,0],
                #                                    edgecolor = 'k',
                #                                    lw = 1.,
                #                                    transform = \
                #                                    ccrs.PlateCarree()))
            ax.coastlines(lw = 0.1)
            gl = ax.gridlines(xlocs = np.arange(-180, 181, 360),
                              ylocs = np.arange(-40, 61, 5),
                              linestyle = '--',
                              linewidth = 0.5, draw_labels = True)
            gl.xlabels_top = False
            gl.xlabels_bottom = False
            gl.ylabels_right = False
            gl.xlocator = mticker.FixedLocator(np.arange(-180, 181, 360))
            gl.ylocator = mticker.FixedLocator(np.arange(-40, 61, 20))
            gl.xformatter = LONGITUDE_FORMATTER
            gl.yformatter = LATITUDE_FORMATTER
            gl.xlabel_style = {'color': 'black', 'weight': 'normal'}
            ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
            ax.outline_patch.set_visible(False)
            ax.spines['top'].set_visible(True)
            ax.spines['bottom'].set_visible(True)
            ax.spines['left'].set_visible(True)
            ax.spines['right'].set_visible(True)
            ax.spines['top'].set_lw(0.2)
            ax.spines['bottom'].set_lw(0.2)
            ax.spines['left'].set_lw(0.2)
            ax.spines['right'].set_lw(0.2)
            ax.add_feature(LAND, facecolor = [0.9, 0.9, 0.9])
            h = ax.contourf(data.lon, data.lat, val0,
                            cmap = cmap, levels = levels, extend = 'both',
                            transform = ccrs.PlateCarree())
            ax.text(-0.2, 0.5, ss, fontsize = 5, rotation = 90.,
                    verticalalignment = 'center', transform = ax.transAxes)
            ax.text(0.01, 0.05, lab[i*8 + sind],
                    transform = ax.transAxes, weight = 'bold')
            if sind == 0:
                if i == 0:
                    ax.text(.5, 1.17, prod_name, fontsize = 5,
                            horizontalalignment = 'center',
                            transform = ax.transAxes)
                ax.text(-.3, 0., dcm, rotation = 90, fontsize = 5,
                        verticalalignment = 'center',
                        transform = ax.transAxes)
            hatch1 = ((val_mean > 0) & (val0 > 0)) | \
                ((val_mean < 0) & (val0 < 0))
            stipple(ax, data.lat, data.lon, mask = hatch1,
                    transform = ccrs.PlateCarree(), hatch = '||||||')
            hatch2 = (~hatch1) & \
                ((val0 < (val_mean - 1.96*val_std)) | \
                 (val0 > (val_mean + 1.96*val_std)))
            stipple(ax, data.lat, data.lon, mask = hatch2,
                    transform = ccrs.PlateCarree(), hatch = '------')

            ###################################################################
            # CMIP6
            ###################################################################
            ax = plt.subplot(sub_gs2[sind], # sind//2, int(np.mod(sind,2))],
                             projection = ccrs.PlateCarree())
            fig.add_subplot(ax)
            ax.coastlines(lw = 0.1)
            gl = ax.gridlines(xlocs = np.arange(-180, 181, 360),
                              ylocs = np.arange(-40, 61, 20),
                              linestyle = '--', 
                              linewidth = 0.5, draw_labels = True)
            gl.xlabels_top = False
            gl.xlabels_bottom = False
            gl.ylabels_right = False
            gl.xlocator = mticker.FixedLocator(np.arange(-180, 181, 360))
            gl.ylocator = mticker.FixedLocator([-40, -20, 0, 20, 40, 60])
            gl.xformatter = LONGITUDE_FORMATTER
            gl.yformatter = LATITUDE_FORMATTER
            gl.xlabel_style = {'color': 'black', 'weight': 'normal'}
            ax.set_extent([-180, 180, -60, 80], crs = ccrs.PlateCarree())
            ax.outline_patch.set_visible(False)
            ax.spines['top'].set_visible(True)
            ax.spines['bottom'].set_visible(True)
            ax.spines['left'].set_visible(True)
            ax.spines['right'].set_visible(True)
            ax.spines['top'].set_lw(0.2)
            ax.spines['bottom'].set_lw(0.2)
            ax.spines['left'].set_lw(0.2)
            ax.spines['right'].set_lw(0.2)
            ax.add_feature(LAND, facecolor = [0.9, 0.9, 0.9])
            h = ax.contourf(data.lon, data.lat, val_mean, cmap = cmap,
                            levels = levels, extend = 'both',
                            transform = ccrs.PlateCarree())
            pct_pos = np.sum((val > 0.).astype(float) * \
                             np.broadcast_to(vote_model.reshape(-1,1,1),
                                             val.shape), axis = 0)
            pct_neg = np.sum((val < 0.).astype(float) * \
                             np.broadcast_to(vote_model.reshape(-1,1,1),
                                             val.shape), axis = 0)
            hatch1 = ((val_mean > 0) & (pct_pos > pct)) | \
                ((val_mean < 0) & (pct_neg > pct))
            stipple(ax, data.lat, data.lon, mask = hatch1,
                    transform = ccrs.PlateCarree(), hatch = '||||||')
            hatch2 = (~hatch1) & \
                (((val_mean > 0) & (pct_pos > (pct - 0.1))) | \
                 ((val_mean < 0) & (pct_neg > (pct - 0.1))))
            stipple(ax, data.lat, data.lon, mask = hatch2,
                    transform = ccrs.PlateCarree(), hatch = '-----')
            #ax.text(0.5, 1.05, ss, fontsize = 5,
            #        horizontalalignment = 'center',
            #        transform = ax.transAxes)
            ax.text(0.01, 0.05, lab[i*8 + 4 + sind],
                    transform = ax.transAxes, weight = 'bold')
            if (i==0) & (sind == 0):
                ax.text(.5, 1.17, 'ALL', fontsize = 5,
                        horizontalalignment = 'center',
                        transform = ax.transAxes)

    cax = fig.add_axes([0.1, 0.08, 0.8, 0.01])
    cbar = plt.colorbar(h, cax = cax, orientation = 'horizontal', ticks = levels, 
                        label = 'Trend of ' + str(lag) + '-month SSI (year$^{-1}$)')
    cax.text(-0.05, -1., 'Dry', fontsize = 5, transform = cax.transAxes,
             horizontalalignment = 'center') # , color = '#762a83')
    cax.text(1.05,  -1., 'Wet', fontsize = 5, transform = cax.transAxes,
             horizontalalignment = 'center') # , color = '#3288bd')

    if expr == 'historical':
        fig.savefig(os.path.join(mg.path_out(), 'figures', prefix + \
                                 'fig1S-2_' + dist + '_trend_' + str(lag) + \
                                 '_' + res + suffix + '.png'),
                    dpi = 600., bbox_inches = 'tight')
    else:
        fig.savefig(os.path.join(mg.path_out(), 'figures', 'fig1S-2_trend_' + str(lag) + \
                                 '_' + res + suffix + '_bymodel', dist + \
                                 '_' + expr + '.png'),
                    dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
