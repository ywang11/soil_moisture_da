"""
2021/06/28
ywang254@utk.edu

Regression coefficients between pr, evspsbl, mrro, and $\delta$snw_{t-(t-1)}.
"""
import os
import numpy as np
import xarray as xr
from utils_management.constants import *
import utils_management as mg
from sklearn.model_selection import cross_val_predict
from sklearn.linear_model._base import LinearModel
from sklearn.base import RegressorMixin
from sklearn.utils import check_X_y
from sklearn.cross_decomposition import PLSRegression
from sklearn.linear_model import Ridge
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from scipy.optimize import minimize, shgo


#
def get_model_list(expr, drivers, dcm, land_mask, res):
    if expr == 'historical':
        a, _ = mrsol_availability(dcm, land_mask, 'historical', res)
        b, _ = mrsol_availability(dcm, land_mask, 'ssp585', res)
        cmip6_list = sorted(list(set(a) & set(b)))
    else:
        cmip6_list, _ = mrsol_availability(dcm, land_mask, expr, res)
    for var in drivers:
        if expr == 'historical':
            a, _ = one_layer_availability(var, land_mask, 'historical', res)
            b, _ = one_layer_availability(var, land_mask, 'ssp585', res)
            cmip6_list = sorted(set(cmip6_list) & set(a) & set(b))
        else:
            a, _ = one_layer_availability(var, land_mask, expr, res)
            cmip6_list = sorted(set(cmip6_list) & set(a))
    return cmip6_list


def standard_read(expr, vv, model, yrng):
    if expr == 'historical':
        flist = [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', 'historical', model,
                              vv + '_' + res + '_' + str(yy) + '.nc') \
                 for yy in range(max(1950, yrng[0]-1), min(2015, yrng[-1]+1))] + \
                [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', 'ssp585', model,
                              vv + '_' + res + '_' + str(yy) + '.nc') \
                 for yy in range(max(2015, yrng[0]-1), max(2015, yrng[-1]+1))]
    else:
        flist = [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', expr, model,
                              vv + '_' + res + '_' + str(yy) + '.nc') \
                 for yy in range(max(2015, yrng[0]-1), max(2015, yrng[-1]+1))]
    ylist = [int(f.split('_')[-1].split('.')[0]) for f in flist]

    hr = xr.open_mfdataset(flist, decode_times = False)
    hr['time'] = pd.date_range(str(ylist[0]) + '-01-01', str(ylist[-1]) + '-12-31', freq = 'MS')
    var = hr[vv].copy(deep = True)
    hr.close()
    return var


def standard_read2(expr, dcm, model, yrng):
    if expr == 'historical':
        flist = [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', 'historical', model,
                              vv + '_' + res + '_' + str(yy) + '.nc') \
                 for yy in range(max(1950, yrng[0]-1), min(2015, yrng[-1]+1))] + \
                [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', 'ssp585', model,
                              vv + '_' + res + '_' + str(yy) + '.nc') \
                 for yy in range(max(2015, yrng[0]-1), max(2015, yrng[-1]+1))]
    else:
        flist = [os.path.join(mg.path_intrim_out(), 'Interp_DA', land_mask,
                              'CMIP6', expr, model,
                              vv + '_' + res + '_' + str(yy) + '.nc') \
                 for yy in range(max(2015, yrng[0]-1), max(2015, yrng[-1]+1))]
    ylist = [int(f.split('_')[-2]) for f in flist]

    hr = xr.open_mfdataset(flist, decode_times = False)
    hr['time'] = pd.date_range(str(ylist[0]) + '-01-01',
                               str(ylist[-1]) + '-12-31', freq = 'MS')
    var = hr['sm'].copy(deep = True)
    hr.close()
    return var


def to_seasonal(var):
    period = var['time'].to_index().to_period(freq = 'Q-NOV')
    var_seasonal = {}
    for qtr, season in enumerate(['DJF', 'MAM', 'JJA', 'SON'], 1):
        var2 = var[period.quarter == qtr, :, :]
        var2['time'] = period[period.quarter == 1].year
        var2 = var2.groupby('time').mean(dim = 'time', skipna = False)
        if season == 'DJF':
            var2 = var2[1:-1, :, :]
        else:
            var2 = var2[1:, :, :]
        var_seasonal[season] = var2.rename({'time': 'year'})
    return var_seasonal


class ConstrainedLinearRegression(LinearModel, RegressorMixin):
     """ https://stackoverflow.com/questions/50410037/multiple-linear-regression-with-specific-constraint-on-each-coefficients-on-pyth """   

     def __init__(self, fit_intercept=True, normalize=False, copy_X=True, nonnegative=False,
                  alpha = 1., tol=1e-15, min_coef=None, max_coef=None):
         self.fit_intercept = fit_intercept
         self.normalize = normalize
         self.copy_X = copy_X
         self.nonnegative = nonnegative
         self.alpha = alpha
         self.tol = tol
         self.min_coef = min_coef
         self.max_coef = max_coef

     def fit(self, X, y, min_coef=None, max_coef=None):
         try:
             X, y = check_X_y(X, y, accept_sparse=['csr', 'csc', 'coo'],
                              y_numeric=True, multi_output=False)
         except ValueError as ve:
             print(ve)
             print(X)
             print('----')
             print(y)

         X, y, X_offset, y_offset, X_scale = self._preprocess_data(
             X, y, fit_intercept=self.fit_intercept, normalize=self.normalize, copy=self.copy_X)
         if (not min_coef is None) or (self.max_coef is None):
             self.min_coef = min_coef if min_coef is not None else np.repeat(-np.inf, 
                                                                             X.shape[1])
         if (not max_coef is None) or (self.max_coef is None):
             self.max_coef = max_coef if max_coef is not None else np.repeat(np.inf, X.shape[1])
         if self.nonnegative:
             self.min_coef = np.clip(self.min_coef, 0, None)
      
         ##beta = np.zeros(X.shape[1]).astype(float)
         ##prev_beta = beta + 1
         #### hessian = np.dot(X.transpose(), X)
         ### Ridge Regression Hessian,
         ### https://stats.stackexchange.com/questions/453875/hessian-of-ridge-regression
         ##hessian = np.dot(X.transpose(), X) + 2 * self.alpha * np.identity(X.shape[1])
         ##while not (np.abs(prev_beta - beta)<self.tol).all():
         ##    prev_beta = beta.copy()
         ##    for i in range(len(beta)):
         ##        ##grad = np.dot(np.dot(X,beta) - y, X)
         ##        # Ridge Regression gradient
         ##        grad = np.dot(X.transpose(), np.dot(X,beta) - y) + 2 * self.alpha * beta
         ##        beta[i] = np.minimum(self.max_coef[i],
         ##                             np.maximum(self.min_coef[i],
         ##                                        beta[i]-grad[i] / hessian[i,i]))

         self.X = X
         self.y = y
         self.X_scale = X_scale
         self.X_offset = X_offset
         self.y_offset = y_offset

         def __objective(_beta):
             temp = np.sum(np.power(np.dot(self.X,_beta) - self.y, 2)) + \
                    self.alpha * np.sum(np.power(_beta,2))
             ##print(temp)
             return temp
         # bounds = ((-10, 10),) * X.shape[1],
         res = shgo(__objective, 
                    bounds = tuple(zip(self.min_coef, self.max_coef))) 
         self.coef_ = res.x / self.X_scale
         if self.fit_intercept:
             self.intercept_ = self.y_offset - np.dot(self.X_offset, self.coef_.T)
         else:
             self.intercept_ = 0.
         return self    


def calc_pcorr(sm_vector, *args, alpha, disp = True, **kwargs):
    if len(sm_vector) == 1:
        return np.full(len(args), np.nan), np.nan, np.nan, np.nan

    pred_mat = np.stack(args, axis = 1)

    valid = ~( np.isnan(sm_vector) | np.any(np.isnan(pred_mat), axis = 1) )
    if sum(valid) < (len(args) + 1):
        return np.full(len(args), np.nan), np.nan, np.nan, np.nan
    else:
        sm_vector = sm_vector[valid]
        pred_mat = pred_mat[valid, :]

    has_var = np.where(pred_mat.std(axis = 0) > 1e-16)[0]
    ##pred_mat = pred_mat[:, has_var]
    ##coefs = np.full(len(args), 0.)

    ##print(pred_mat)

    ##if method == 'pls':
    ##    pls = PLSRegression(n_components = n_comp)
    ##elif method == 'ridge':
    ##    pls = Ridge(alpha = 0.1)
    ##else:
    ##    pls = LinearRegression()

    ##pls = Ridge(alpha = alpha, normalize = True)
    pls = ConstrainedLinearRegression(normalize = True, alpha = alpha, **kwargs)
    ##pls = LinearRegression()

    y_cv = cross_val_predict(pls, pred_mat, sm_vector, cv = 5)
    # !!!!!!! Manual fix: skip less than zero values; mysterious large negative values
    #                     sometimes pop up.
    temp = y_cv > -1
    if sum(temp) < (len(y_cv) / 10):
        temp = np.full(len(temp), True)
    r2 = r2_score(sm_vector[temp], y_cv[temp])
    mse = mean_squared_error(sm_vector[temp], y_cv[temp])
    rpd = sm_vector[temp].std() / np.sqrt(mse) # residual prediction deviation

    ##print(r2)
    ##print(mse)
    ##print(rpd)

    pls.fit(pred_mat, sm_vector, **kwargs)
    coefs = pls.coef_.reshape(-1) ## [has_var]

    if (len(has_var) < len(args)) and disp:
        print(has_var)
        print(coefs)

    #print(np.sum(np.power(np.dot(pred_mat,pls.coef_) + pls.intercept_ - sm_vector,
    #                      2) + alpha * np.sum(np.power(pls.coef_,2))))

    return coefs, r2, mse, rpd


if __name__ == '__main__':
    #
    dcm = depth_cm[REPLACE1]
    alpha = REPLACE2 # 0., 0.01, 0.1, 0.5, 0.9
    yrng = range(1971, 2017)
    land_mask = 'vanilla'
    res = '0.5'
    expr = REPLACE3
    cmip6_list = get_model_list(expr, drivers, dcm, land_mask, res)    
    model = cmip6_list[REPLACE4]

    sm = to_seasonal(standard_read2(expr, dcm, model, yrng))
    pr = to_seasonal(standard_read(expr, 'pr', model, yrng))
    tas = to_seasonal(standard_read(expr, 'tas', model, yrng))
    lai = to_seasonal(standard_read(expr, 'lai', model, yrng))
    snw = to_seasonal(standard_read(expr, 'snw', model, yrng))
    
    ds = xr.Dataset()
    for season in ['DJF', 'MAM', 'JJA', 'SON']:

        ###DEBUG
        ##_, lat, lon = np.where(~( np.isnan(sm[season]) | np.isnan(pr[season]) | \
        ##                          np.isnan(lai[season]) | \
        ##                          np.isnan(snw[season]) ))
        ##lat, lon = lat[0], lon[0]
        ##sm_vector = sm[season][:, lat, lon]
        ##pr_vector = pr[season][:, lat, lon]
        ##tas_vector = tas[season][:, lat, lon]
        ##lai_vector = lai[season][:, lat, lon]
        ##snw_vector = snw[season][:, lat, lon]
        ##coef, r2, mse, rpd = calc_pcorr(sm_vector, pr_vector, tas_vector,
        ##                                lai_vector, snw_vector, alpha = alpha,
        ##                                min_coef = np.array([-10, -10, 0., -10]),
        ##                                max_coef = np.array([10, 10, 10, 10]))

        coef, r2, mse, rpd = xr.apply_ufunc(calc_pcorr, 
                                            sm[season].chunk({'year':-1}), 
                                            pr[season].chunk({'year':-1}),
                                            tas[season].chunk({'year':-1}),
                                            lai[season].chunk({'year':-1}),
                                            snw[season].chunk({'year':-1}),
                                            input_core_dims = [['year']]*5,
                                            output_core_dims = [['var'], [], [], []],
                                            vectorize = True,
                                            kwargs = {'alpha': alpha,
                                                      'disp': False,
                                                      'min_coef': np.array([-10, -10, -10,
                                                                            -10]),
                                                      'max_coef': np.array([10,10,0,10])},
                                            dask = 'parallelized',
                                            output_dtypes = (float,) * 4,
                                            dask_gufunc_kwargs = { \
                                            'output_sizes': {'var': 4}})
        coef.compute()
        r2.compute()
        mse.compute()
        rpd.compute()
        ds.update({'coef_' + season: coef.transpose('var', 'lat', 'lon'),
                   'r2_' + season: r2, 
                   'mse_' + season: mse, 
                   'rpd_' + season: rpd})
        for ii in ['coef', 'r2', 'mse', 'rpd']:
            ds[ii + '_' + season].encoding['_FillValue'] = -1e16
    ds['var'].encoding['_FillValue'] = None
    ds['lat'].encoding['_FillValue'] = None
    ds['lon'].encoding['_FillValue'] = None
    ds.to_netcdf(os.path.join(mg.path_out(), 'cmip6_drivers_corr', 'map', expr, 
                              'alpha=%.2f' % alpha,
                              model + '_' + dcm + '.nc'))
