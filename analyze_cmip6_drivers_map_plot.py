import os
import numpy as np
import xarray as xr
from utils_management.constants import *
import utils_management as mg
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from analyze_cmip6_drivers_map import get_model_list
from misc.plot_utils import cmap_div


def get_driver_trend(var, model, season, dcm):
    year_str = '1971-2016'
    hr = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_drivers_summary', var,
                                      model + '_' + res + '_g_map_trend_' + season + \
                                      '_' + year_str + '.nc'))
    trend = hr['g_map_trend'].copy(deep = True)
    hr.close()
    return trend


dcm = '0-10cm'
n_comp = 1
land_mask = 'vanilla'
res = '0.5'
expr = 'historical'
cmip6_list = get_cmip6_list(expr, ['mrsol'], dcm, land_mask, res)
alpha = 0.10


for model in cmip6_list:
    hr = xr.open_dataset(os.path.join(mg.path_out(), 'cmip6_drivers_corr', 'map', expr,
                                      'alpha=%.2f' % alpha,
                                      model + '_' + str(n_comp) + '_' + dcm + '.nc'))

    for season in ['DJF', 'MAM', 'JJA', 'SON']:
        #
        fig, axes = plt.subplots(3, 1, figsize = (6.5, 6.5),
                                 subplot_kw = {'projection': ccrs.PlateCarree()})
        for vind, vv in enumerate(['r2', 'mse', 'rpd']):
            ax = axes.flat[vind]
            ax.coastlines(lw = 0.5)
            cf = ax.contourf(hr['lon'], hr['lat'], hr[vv + '_' + season],
                             cmap = 'Spectral')
            ax.set_title(vv)
            plt.colorbar(cf, ax = ax, shrink = 0.7)
        fig.savefig(os.path.join(mg.path_out(), 'cmip6_drivers_corr',
                                 'map_plot', 'metrics_' + model + '_' + dcm + '.png'),
                    dpi = 600., bbox_inches = 'tight')
        plt.close(fig)


        #
        fig, axes = plt.subplots(2, 2, figsize = (6.5, 6.5),
                                 subplot_kw = {'projection': ccrs.PlateCarree()})
        for vind, vv in enumerate(['pr', 'evspsbl', 'mrro', 'snw']):
            ax = axes.flat[vind]
            ax.coastlines(lw = 0.5)
            cf = ax.contourf(hr['lon'], hr['lat'], hr[vv + '_' + season],
                             cmap = 'Spectral')
            ax.set_title(vv + ' corr')
            plt.colorbar(cf, ax = ax, shrink = 0.7)
        fig.savefig(os.path.join(mg.path_out(), 'cmip6_drivers_corr',
                                 'map_plot', 'corr_' + model + '_' + dcm + '.png'),
                    dpi = 600., bbox_inches = 'tight')
        plt.close(fig)


        # accounted-for trend in soil moisture by trend
        fig, axes = plt.subplots(2, 2, figsize = (6.5, 6.5),
                                 subplot_kw = {'projection': ccrs.PlateCarree()})
        for vind, vv in enumerate(['pr', 'evspsbl', 'mrro', 'snw']):
            ax = axes.flat[vind]
            ax.coastlines(lw = 0.5)

            trend = get_driver_trend(vr, model, season, dcm)
            
            cf = ax.contourf(hr['lon'], hr['lat'],
                             hr[vv + '_' + season] * trend,
                             cmap = cmap_div())
            ax.set_title(vv + '_accounted trend')
            plt.colorbar
    
    
    hr.close()

